package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.ActivitiesAdapter;
import com.roamtech.android.sams.dialog_fragments.HandleActivityDialogFragment;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 * A simple {@link Fragment} subclass.
 */
public class ActivitiesFragment extends Fragment implements  View.OnClickListener,LoaderManager.LoaderCallbacks<Cursor>,LaunchFragment{
    public static final String TAG = "ActivitiesFragment";
    public static final int PAGER_FRAGMENT = 2;
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    public static final int LOADER = 1900;
    ListView lstActivities;
    Cursor c;
    View infoView;
    ProgressBar mProgresBar;
    TextView txtNoContacts,txtAddContacts;
    AddActivitiesFragment addActivitiesFragment;
    ActivitiesAdapter mAdapter;
    int activityDone = 0;
    String title2;
    int position,position2;
   // OnAddSelectedListener mCallBack2;
    ImageButton imgAdd;
    View headerView;
    Toolbar mActionBarToolbar;
    FrameLayout frmLayouts2;
    int mFirstVisibleItem;
    int activityID;
    String activityMsg;
    HandleActivityDialogFragment newFragment9;
    boolean argumentsRead;
    CommentsFragment commentsFragment;


    public ActivitiesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            //mCallBack2 = (OnAddSelectedListener)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mainActivity = new MainActivity();
        if(!argumentsRead) {
            Bundle args = getArguments();
            if (args != null) {
                title2 = args.getString(mActivity.getResources().getString(R.string.bundle_title));
                position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
                activityID = args.getInt(mActivity.getResources().getString(R.string.db_table_activities_id), -1);
                activityMsg = args.getString(mActivity.getResources().getString(R.string.db_table_act_schedule));
                Log.d(TAG,"it gets here "+activityID+" "+activityMsg);
            }
            argumentsRead = true;
        }

        newFragment9 = new HandleActivityDialogFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_activities, container, false);
        mActionBarToolbar = (Toolbar) mActivity.findViewById(R.id.toolbar_actionbar);
        lstActivities = (ListView) rootView.findViewById(R.id.lst_contacts);
        infoView = (View) rootView.findViewById(R.id.error_container);
        mProgresBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        txtNoContacts = (TextView) rootView.findViewById(R.id.txt_no_deals);
        txtAddContacts = (TextView) rootView.findViewById(R.id.txt_adddeals);
        headerView = mActivity.getLayoutInflater().inflate(R.layout.listview_fragment_header, null);
        lstActivities.addHeaderView(headerView, null, false);
        imgAdd = (ImageButton) rootView.findViewById(R.id.fab_button);
        imgAdd.setOnClickListener(this);
        frmLayouts2 = (FrameLayout) rootView.findViewById(R.id.frm_layouts);
        frmLayouts2.setBackgroundColor(mActivity.getResources().getColor(R.color.activities_green));
        mAdapter = new ActivitiesAdapter(mActivity, null, 0,this);
        infoView.setVisibility(View.GONE);
        lstActivities.setVisibility(View.VISIBLE);
        lstActivities.setAdapter(mAdapter);
        lstActivities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        lstActivities.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_TOUCH_SCROLL || scrollState == SCROLL_STATE_FLING) {
                    Log.d(TAG, "Scrolling");
                    if (mFirstVisibleItem != 0) {
                        mActionBarToolbar.setBackgroundResource(R.color.toolbar_activities_green);
                    }/*else {
                        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                    }*/
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d(TAG, "First Visible Item " + String.valueOf(firstVisibleItem));
                mFirstVisibleItem = firstVisibleItem;
                if (firstVisibleItem == 0 && MainModel.listIsAtTop(lstActivities)) {
                    mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                } else {
                    mActionBarToolbar.setBackgroundResource(R.color.toolbar_activities_green);
                }
            }
        });

        if(savedInstanceState != null){
            Log.d(TAG,"stalemate 1");

        }else{
            Log.d(TAG,"stalemate 2");
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }
        if(activityMsg != null & activityID != -1){
            Bundle args2 = new Bundle();
            args2.putInt(mActivity.getResources().getString(R.string.bundle_sale_flag),1);
            args2.putInt(mActivity.getResources().getString(R.string.db_table_activities_id), activityID);
            args2.putString(mActivity.getResources().getString(R.string.db_table_act_schedule),activityMsg);
            newFragment9.setArguments(args2);
            newFragment9.setTargetFragment(ActivitiesFragment.this, MainModel.DIALOG_FRAGMENT_ADD_QUOTATION);
            newFragment9.show( mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), "dialog");
        }

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Bundle args = getArguments();
        if(args != null){
            mActivity.getSupportActionBar().setTitle(args.getString(mActivity.getResources().getString(R.string.bundle_title)));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
    }


    public void runStringLoader(){
        getLoaderManager().restartLoader(LOADER, null, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, menu);
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_add).setVisible(false);
        menu.findItem(R.id.action_delete).setVisible(false);
        menu.findItem(R.id.action_edit).setVisible(false);
        menu.findItem(R.id.action_comments).setVisible(true);

        //        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                commentsFragment.setTargetFragment(ActivitiesFragment.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.PAGER_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
                //mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
            }
        }
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"16");
        return new CursorLoader(getActivity(),data, null, null,
                new String[] {}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mProgresBar.setVisibility(View.GONE);
        c = data;
        Log.d(TAG, "--------" + String.valueOf(c.getCount()));
        if(c == null || c.getCount() == 0 ){
           /* infoView.setVisibility(View.VISIBLE);
            txtNoContacts.setText(mActivity.getResources().getString(R.string.txt_noactivities));
            txtAddContacts.setText(mActivity.getResources().getString(R.string.txt_addactivities));
            lstActivities.setVisibility(View.GONE);*/
        }else{

        }
        mAdapter.swapCursor(c);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.fab_button:
                addActivitiesFragment = new AddActivitiesFragment();
                addActivitiesFragment.setTargetFragment(this, MainModel.PAGER_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            default:

                break;
        }

    }

    @Override
    public void launchFragment(int position,Bundle args,int view) {
        if (view == 0) {
            Log.d(TAG, "launchFragment view == 0" + position);
                args.putInt(mActivity.getResources().getString(R.string.bundle_position),position2);
                addActivitiesFragment = new AddActivitiesFragment();
                addActivitiesFragment.setArguments(args);
                addActivitiesFragment.setTargetFragment(ActivitiesFragment.this, MainModel.PAGER_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
            //Log.d(TAG,"tragedy "+ position);
        }else if(view == 1){
            if(c.moveToPosition(position)){
               int id = c.getInt(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_id)));
                activityDone = 1;
                updateActivity(id);
            }
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }
    }

    public int updateActivity(int position){
        Log.d(TAG, "launchFragment view == 1  " + String.valueOf(position));
        int activityID;
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_activity_done), activityDone);
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "13");
        activityID = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(position)});
        Log.d(TAG,"---------------------- launchFragment view ACTIVITY_ID " +String.valueOf(activityID));
        return activityID;
    }
}
