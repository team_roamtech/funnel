package com.roamtech.android.sams.async_tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.roamtech.android.sams.interfaces.ReturnAuthenticationResult;
import com.roamtech.android.sams.interfaces.ReturnRegistrationResult;
import com.roamtech.android.sams.parsers.ResponseParser;
import com.unboundid.ldap.sdk.LDAPException;

import org.apache.http.NameValuePair;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dennis on 4/24/15.
 */
public class CreateUserTask extends AsyncTask<String, Void, Boolean> {
    public static final String TAG = "CreateUserTask";
    Context ctx;
    Fragment fragment;
    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
    ResponseParser responseParser;
    boolean isUserCreated;

    public CreateUserTask(Context ctx,Fragment fragment){
        this.ctx = ctx;
        this.fragment = fragment;
        responseParser = new ResponseParser(ctx);
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        ReturnRegistrationResult ac = (ReturnRegistrationResult) fragment;
        ac.onStartTask();
    }
    @Override
    protected Boolean doInBackground(String... params) {
        /*String username = params[0];
        String password = params[1];
        nameValuePairs =  new ArrayList<NameValuePair>(1);
        nameValuePairs.add(new BasicNameValuePair(ctx.getResources().getString(R.string.bundle_username),params[0]));
        nameValuePairs.add(new BasicNameValuePair(ctx.getResources().getString(R.string.bundle_password), params[1]));
        String json = "{}";
        try {
            json = RestClient.makeRestRequest(RestClient.POST, MainModel.SYNC_URL, nameValuePairs);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }*/
        try {
            isUserCreated = responseParser.parse(loadJSONFromAsset());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isUserCreated;
    }



    @Override
    protected void onPostExecute(Boolean result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        Log.d(TAG, String.valueOf(result));
        ReturnRegistrationResult ac = (ReturnRegistrationResult) fragment;
        try {
            ac.onReturnResult(result);
        } catch (LDAPException e) {
            e.printStackTrace();
        }
    }

    private String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is;

            is = ctx.getAssets().open("error_creation.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }


}
