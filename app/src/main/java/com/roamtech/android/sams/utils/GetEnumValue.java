package com.roamtech.android.sams.utils;

import android.content.Context;


import com.roamtech.android.sams.R;

/**
 *
 * It gets the String value based on value passed
 *
 * Created by dennis on 5/8/15.
 * @author Dennis Mwangi Karuri
 */
public class GetEnumValue {
    public  Context ctx;

    /**
     * Class constructor initializes the context
     * @param ctx The active context that requires the String value
     */
    public GetEnumValue(Context ctx){
        this.ctx = ctx;
    }

    /**
     * Gets the string value based on the integer value passed
     * @param value The value passed to check on the string to return
     * @return The String value gotten based on the value passed
     */
    public String getString(int value){
        String enumValue = null;
        switch(value){
            case 0:
                enumValue = ctx.getResources().getString(R.string.name_value_pending);
                break;
            case 1:
                enumValue = ctx.getResources().getString(R.string.name_value_won);
                break;
            case 2:
                enumValue = ctx.getResources().getString(R.string.name_value_lost);
                break;
        }

        return enumValue;
    }
}
