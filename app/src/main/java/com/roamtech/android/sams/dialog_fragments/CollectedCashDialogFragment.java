package com.roamtech.android.sams.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import com.roamtech.android.sams.R;

import java.text.SimpleDateFormat;

/**
 * Created by dennis on 9/26/15.
 */
public class CollectedCashDialogFragment extends DialogFragment implements View.OnClickListener  {
    AppCompatActivity mActivity;
    EditText editCashCollected;
    Button btnEnter;
    String strCashCollected;

    public CollectedCashDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_collected_cash);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.txt_cash_collected_title));
        dialog.setCanceledOnTouchOutside(false);
        editCashCollected = (EditText) dialog.findViewById(R.id.txt_cash_collected);
        btnEnter = (Button) dialog.findViewById(R.id.btn_enter);
        btnEnter.setOnClickListener(this);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_enter:
                if(editCashCollected.getText().length() < 1){

                }else{
                    strCashCollected = editCashCollected.getText().toString();
                    Intent intent = new Intent();
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_qu_amount_collected),strCashCollected);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    dismiss();
                }

                break;
            default:

                break;
        }
    }
}
