package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 * Created by dennis on 2/20/15.
 */
public class ActivitiesAdapter extends CursorAdapter {
    public static final String TAG = "ActivitiesAdapter";
    private LayoutInflater vi;
    Cursor c;
    Context ctx;
    ViewHolder holder;
    LaunchFragment mCallBack;
    Fragment fragment;
    Cursor cursor2;

    public ActivitiesAdapter(Context context, Cursor c, int flags, Fragment fragment) {
        super(context, c, flags);
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.ctx = context;
        this.c = c;
        this.fragment = fragment;
        Log.d(TAG, "ActivitiesAdapter constructor");
        try {
            mCallBack = (LaunchFragment) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement LaunchFragment");
        }

    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View rootView = vi.inflate(R.layout.list_activity_item, parent, false);
        ViewHolder holder  =   new ViewHolder();
        holder.lytActivityItem = (LinearLayout) rootView.findViewById(R.id.lyt_activity_item);
        holder.btnTarget = (Button) rootView.findViewById(R.id.btn_target);
        holder.txtActivityName = (TextView) rootView.findViewById(R.id.txt_activity_name);
        holder.txtActivityTime = (TextView) rootView.findViewById(R.id.txt_activity_time);
        holder.imgActivity = (ImageView) rootView.findViewById(R.id.img_activity);
        rootView.setTag(holder);
        return rootView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final Cursor c = cursor;
        final Bundle bundle = new Bundle();

        if(view != null) {
            final ViewHolder holder = (ViewHolder) view.getTag();
            holder.cursorPosition = c.getPosition();
            Log.d(TAG, "ActivitiesAdapter constructor " + c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_model))));
            if(c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_model))).equals(ctx.getResources().getString(R.string.db_table_model_2))) {
                Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "45");
                cursor2 = ctx.getContentResolver().query(data_id, null, null, new String[]{c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_foreign_key)))}, null);
                if(cursor2 != null & cursor2.getCount() > 0) {
                    cursor2.moveToFirst();
                    holder.txtActivityName.setText(c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_type))).toUpperCase() + " " + cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_name))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_org_name), cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_name))));
                }
            }else if(c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_model))).equals(ctx.getResources().getString(R.string.db_table_model_1))){
                Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "46");
                cursor2 = ctx.getContentResolver().query(data_id, null, null,new String[] {c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_foreign_key)))}, null);
                if(cursor2 != null & cursor2.getCount() > 0) {
                    cursor2.moveToFirst();
                    holder.txtActivityName.setText(c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_type))).toUpperCase() + " " + cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_con_name))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_org_name), cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_name))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_con_name), cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_con_name))));
                }

            }else if(c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_model))).equals(ctx.getResources().getString(R.string.db_table_model_3))){
                Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "48");
                cursor2 = ctx.getContentResolver().query(data_id, null, null, new String[]{c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_foreign_key)))}, null);
                if(cursor2 != null & cursor2.getCount() > 0) {
                    cursor2.moveToFirst();
                    holder.txtActivityName.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_act_type))).toUpperCase() + " " + cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_name))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_org_name), cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_name))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_con_name), cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_con_name))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_sale_name), cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_name))));
                }

            }else if(c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_model))).equals(ctx.getResources().getString(R.string.db_table_model_4))){
                Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "47");
                cursor2 = ctx.getContentResolver().query(data_id, null, null, new String[]{c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_foreign_key)))}, null);
                if(cursor2 != null & cursor2.getCount() > 0) {
                    cursor2.moveToFirst();
                    holder.txtActivityName.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_act_type))).toUpperCase() + " " + cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_name))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_org_name), cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_name))));
                    Log.d(TAG, cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_name))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_con_name), cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_con_name))));
                    Log.d(TAG, cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_con_name))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_sale_name), cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_name))));
                    Log.d(TAG, cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_name))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_qu_name), cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_name))));
                    Log.d(TAG, cursor2.getString(cursor2.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_name))));
                }
            }


            holder.txtActivityTime.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_act_schedule))));
            holder.lytActivityItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cursor2.moveToFirst();
                    c.moveToPosition(holder.cursorPosition);

                    bundle.putInt(ctx.getResources().getString(R.string.db_table_id), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_id))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_act_slug_id), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_slug_id))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_act_type), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_type))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_act_schedule), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_schedule))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_act_description), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_description))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_act_model), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_model))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_act_foreign_key), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_foreign_key))));
                    bundle.putInt(ctx.getResources().getString(R.string.db_table_act_status), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_status))));
                    bundle.putInt(ctx.getResources().getString(R.string.db_table_sync_done), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sync_done))));
                    bundle.putString(ctx.getResources().getString(R.string.db_table_act_feedback), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_feedback))));

                    mCallBack.launchFragment((holder.cursorPosition), bundle, 0);
                }
            });
            switch(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_act_type)))){
                case "lunch":
                    holder.imgActivity.setImageResource(R.drawable.ic_action_lunch);
                    break;
                case "email":
                    holder.imgActivity.setImageResource(R.drawable.ic_action_email);
                    break;
                case "deadline":
                    holder.imgActivity.setImageResource(R.drawable.ic_action_deadline);
                    break;
                case "task":
                    holder.imgActivity.setImageResource(R.drawable.ic_action_task);
                    break;
                case "meeting":
                    holder.imgActivity.setImageResource(R.drawable.ic_action_meeting);
                    break;
                case "call":
                    holder.imgActivity.setImageResource(R.drawable.ic_action_call);
                    break;
            }
        }

    }

    static class ViewHolder {
        public int cursorPosition;
        LinearLayout lytActivityItem;
        Button btnTarget;
        TextView txtActivityName;
        TextView txtActivityTime;
        ImageView imgActivity;
    }
}
