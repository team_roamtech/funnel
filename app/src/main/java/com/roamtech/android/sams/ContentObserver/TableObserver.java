package com.roamtech.android.sams.ContentObserver;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;

/**
 * Created by dennis on 3/11/15.
 */
public class TableObserver extends ContentObserver {

    /**
     * Creates a content observer.
     *
     * @param handler The handler to run {@link #onChange} on, or null if none.
     */
    public TableObserver(Handler handler) {
        super(handler);
    }

    @Override
    public void onChange(boolean selfChange) {
            /*
             * Invoke the method signature available as of
             * Android platform version 4.1, with a null URI.
             */
        onChange(selfChange, null);
    }
    /*
     * Define a method that's called when data in the
     * observed content provider changes.
     */
    @Override
    public void onChange(boolean selfChange, Uri changeUri) {
            /*
             * Ask the framework to run your sync adapter.
             * To maintain backward compatibility, assume that
             * changeUri is null.*/


         }
}
