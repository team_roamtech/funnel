package com.roamtech.android.sams.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.roamtech.android.sams.R;

/**
 *
 * Gets the username of the already logged in user
 *
 * Created by dennis on 5/27/15.
 * @author Dennis Mwangi Karuri
 */
public class GetUsername {
    String username;
    String email;
    String dnUser;
    String password;
    SharedPreferences prefs;
    Context ctx;

    /**
     * Class constructor initializes the context
     * @param ctx The active context that requires the username
     */
    public GetUsername(Context ctx){
        this.ctx = ctx;
    }

    /**
     * Gets the username of the logged in user
     * @return Username of the logged in user
     */
    public String getUserName(){
        prefs = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        username = prefs.getString(ctx.getResources().getString(R.string.bundle_username),null);

        return username;
    }

    /**
     * Gets the email of the logged in user
     * @return Email of the logged in user
     */
    public String getEmail(){
        prefs = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        email = prefs.getString(ctx.getResources().getString(R.string.bundle_email_id),null);

        return email;
    }

    /**
     * Gets the LDAP dnuser used to authenticate the user in the LDAP server
     * @return LDAP dnUser
     */
    public String getDnUser(){
        prefs = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        dnUser = prefs.getString(ctx.getResources().getString(R.string.bundle_dnuser),null);

        return dnUser;
    }

    /**
     * Gets the Password of the logged in user
     * @return Password of the user logged in
     */

    public String getPassword(){
        prefs = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        password = prefs.getString(ctx.getResources().getString(R.string.bundle_password),null);

        return password;
    }
}
