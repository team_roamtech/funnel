package com.roamtech.android.sams.interfaces;

/**
 *
 * Gets positions in Listview Adapters so that the data in that position can either be displayed or deleted
 *
 * Created by dennis on 2/25/15.
 * @author Dennis Mwangi Karuri
 */
public interface ItemActions {

    /**
     *
     * Gets the adapter position to display items in this position in the Main Thread
     *
     * @param position
     */
    public void displayItems(int position);

    /**
     *
     * Gets the adapter position to delete items in this position in the Main Thread
     *
     * @param position
     */
    public void deleteItems(int position);
}
