package com.roamtech.android.sams;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.roamtech.android.sams.adapters.DrawerAdapter;
import com.roamtech.android.sams.dialog_fragments.SignOutDialog;
import com.roamtech.android.sams.fragments.CreateUsersFragment;
import com.roamtech.android.sams.fragments.PersonalStatisticsFragment;
import com.roamtech.android.sams.fragments.TeamStatisticsFragment;
import com.roamtech.android.sams.interfaces.CheckSignOut;


public class AdminActivity extends AppCompatActivity implements CheckSignOut {
    public static final String TAG = "AdminActivity";
    String [] drawerListArray;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    Toolbar toolBar;
    DrawerAdapter drawerAdapter;
    TypedArray listItemsImgArray;
    private boolean isDrawerLocked = false;
    FrameLayout frameLayout;
    MainActivity mainActivity;
    CreateUsersFragment createUsersFragment;
    TeamStatisticsFragment teamStatisticsFragment;
    PersonalStatisticsFragment personalStatisticsFragment;
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    SignOutDialog newFragment7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);
        mDrawerList = (ListView) findViewById(R.id.listview_drawer);
        drawerListArray = getResources().getStringArray(R.array.main_activity_items);
        drawerListArray = getResources().getStringArray(R.array.admin_items);
        listItemsImgArray = getResources().obtainTypedArray(R.array.admin_imgs);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.listview_drawer);
        drawerAdapter = new DrawerAdapter(this,0,drawerListArray,listItemsImgArray);
        mDrawerList.setAdapter(drawerAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        frameLayout = (FrameLayout)findViewById(R.id.content_frame);
        mainActivity = new MainActivity();
        createUsersFragment = new CreateUsersFragment();
        teamStatisticsFragment = new TeamStatisticsFragment();
        personalStatisticsFragment = new PersonalStatisticsFragment();
        newFragment7 = new SignOutDialog();

        toolBar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolBar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(true);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                toolBar,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                toolBar.setNavigationIcon(R.drawable.ic_action_home2);
            }
            public void onDrawerOpened(View drawerView) {
                toolBar.setNavigationIcon(R.drawable.ic_action_arrow);
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG,"mdrawer Toggle touched");

                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }

            }
        });
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        toolBar.setNavigationIcon(R.drawable.ic_action_home2);

        if(savedInstanceState == null) {
            mainActivity.addFragment(createUsersFragment, false, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) createUsersFragment).getClass().getName());
        }
        prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        editor = prefs.edit();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public void onResume() {
        super.onResume();
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600 && getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            //if(((ViewGroup.MarginLayoutParams)frameLayout.getLayoutParams()).leftMargin == (int)getResources().getDimension(R.dimen.drawer_size)) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mDrawerList);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            //mDrawerLayout.setScrimColor(Color.TRANSPARENT);
            isDrawerLocked = true;
        }else{
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, mDrawerList);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            //if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
            //mDrawerLayout.closeDrawer(mDrawerList);
            mDrawerLayout.closeDrawer(mDrawerList);
        }

    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onDestroy(){
        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_admin, menu);
        menu.findItem(R.id.action_send).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id == android.R.id.home) {
            if(((ViewGroup.MarginLayoutParams)frameLayout.getLayoutParams()).leftMargin == (int)getResources().getDimension(R.dimen.drawer_size)) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN, mDrawerList);
                mDrawerLayout.setScrimColor(Color.TRANSPARENT);
                isDrawerLocked = true;
            }else {
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    mDrawerLayout.openDrawer(mDrawerList);
                }
            }
        }else if(id == R.id.action_logout){
            Bundle args = new Bundle();
            args.putString(getString(R.string.txt_advisory),getString(R.string.txt_advisory_exit));
            newFragment7.setArguments(args);
            newFragment7.show(mainActivity.addDialogFragment(getSupportFragmentManager()), "dialog");
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void confirmSignOut() {
        logout();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG, "position headerflag selected");
            selectItem(position);
        }
    }

    public void selectItem(int position){
        switch(position){
            case 0:
                mainActivity.addFragment(createUsersFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) createUsersFragment).getClass().getName());
                break;
            case 1:
                mainActivity.addFragment(teamStatisticsFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), teamStatisticsFragment.getClass().getName());
                break;
            case 2:
                mainActivity.addFragment(personalStatisticsFragment,true,FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(), personalStatisticsFragment.getClass().getName());
                break;
        }
        mDrawerLayout.closeDrawer(mDrawerList);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(isDrawerLocked == false){
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    Log.d(TAG,"--------||||||--------- RETURN 11");
                    return super.onKeyUp(keyCode, event);
                }
            }else{
                Log.d(TAG,"--------||||||--------- RETURN 12");
                return super.onKeyUp(keyCode, event);
            }
        }
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(!isDrawerLocked){
                if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
                    mDrawerLayout.closeDrawer(mDrawerList);
                } else {
                    Log.d(TAG,"--------||||||--------- RETURN 1");
                    return super.onKeyDown(keyCode, event);
                }
            }else{
                Log.d(TAG,"--------||||||--------- RETURN 2");
                return super.onKeyDown(keyCode, event);
            }
        }
        return true;
    }

    private void logout() {
        editor.clear().commit();
        //Exit the application
        android.os.Process.killProcess(android.os.Process.myPid());
        AdminActivity.super.onDestroy();
        finish();
    }


}
