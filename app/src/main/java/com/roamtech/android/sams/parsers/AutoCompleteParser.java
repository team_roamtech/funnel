package com.roamtech.android.sams.parsers;

import android.content.ContentProvider;
import android.content.Context;
import android.util.Log;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.views.ClearableAutoTextView;
import com.roamtech.android.sams.views.ClearableAutoTextView.AutoCompleteResponseParser;
import com.roamtech.android.sams.views.ClearableAutoTextView.DisplayStringInterface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 *
 * Parses Json data from the server to strings that are added to the AutocompleteTextView
 *
 * Created by dennis on 4/24/15.
 * @author Dennis Mwangi Karuri
 * @see AutoCompleteResponseParser
 */
public class AutoCompleteParser implements AutoCompleteResponseParser {
    Context ctx;

    /**
     * Constructor Initializes the Context
     * @param ctx
     */
    public AutoCompleteParser(Context ctx){
        this.ctx = ctx;
    }

    /**
     *
     * @param jsonString Response from server to be parsed
     * @return String of the data parsed and gotten from the Json data
     * @throws Exception
     */
    public String parse(String jsonString) throws Exception {
        JSONObject jObject = null;
        JSONArray jArray = null;
        try {
            if (jsonString != null) {
                jObject = new JSONObject(jsonString);
                if(jObject.has(ctx.getResources().getString(R.string.db_table_organisation_name)) && !jObject.isNull(ctx.getResources().getString(R.string.db_table_organisation_name))){
                    jArray = jObject.getJSONArray(ctx.getResources().getString(R.string.db_table_organisation_name));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return loadJSONFromAsset();
    }


    /**
     *
     * Gets a sample json string from assests
     *
     * @return sample Json String from assets
     */
    private String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is;

            is = ctx.getAssets().open("arrivals.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    /* Gets the server json response,parses the data and gets the organizations in the Json String
     *  it alsoadds them to an arraylist whic is then added to an AutocompleteTextview.
     */
    @Override
    public ArrayList<ClearableAutoTextView.DisplayStringInterface> parseAutoCompleteResponse(String response) {
        ArrayList<DisplayStringInterface> dsi=null;
        try {
            JSONObject jsonObj = new JSONObject(response.toString());
            final JSONArray predsJsonArray = jsonObj.getJSONArray(ctx.getResources().getString(R.string.json_rows));

            dsi=new ArrayList<DisplayStringInterface>();
            for(int i=0;i<predsJsonArray.length();i++){
                final int j=i;
                DisplayStringInterface ds=new DisplayStringInterface() {

                    @Override
                    public String[] getDisplayString() {

                        try {
                            String result1 = predsJsonArray.getJSONObject(j).getString(ctx.getResources().getString(R.string.name_value_clt_company)).toString();
                            String result2 = predsJsonArray.getJSONObject(j).getString(ctx.getResources().getString(R.string.name_value_clt_id)).toString();
                            String [] result = {result1,result2};
                            return result;
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return null;
                    }
                };
                dsi.add(ds);
            }
        } catch (JSONException e) {
            Log.e("AppUtil", "Cannot process JSON results", e);
        }

        return dsi;
    }
}
