package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.async_tasks.AuthenticateTask;
import com.roamtech.android.sams.client.LDAPServerInstance;
import com.roamtech.android.sams.dialog_fragments.ProgressDialogFragment;
import com.roamtech.android.sams.interfaces.ReturnAuthenticationResult;
import com.roamtech.android.sams.models.LoginItems;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.utils.GetUsername;
import com.unboundid.ldap.sdk.LDAPException;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 *
 * Fragment where settings for the app are set
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment implements View.OnClickListener,ReturnAuthenticationResult {
    public static final String TAG = "SettingsFragment";
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    EditText editUsername,editPassword,editConfirmPassword, editEmail;
    Button btnSave;
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    ProgressDialogFragment progressDialogFragment;
    GetUsername getUsername;
    String mHost;
    private int mPort;
    private int mEncryption = 0;
    LDAPServerInstance ldapServer;
    String strPassword,password;
    String [] drawerListArray;
    ImageButton imgClose;


    /**
     * Constructor
     */
    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mainActivity = new MainActivity();
        prefs = PreferenceManager
                .getDefaultSharedPreferences(mActivity);
        progressDialogFragment = new ProgressDialogFragment();
        editor = prefs.edit();
        getUsername = new GetUsername(mActivity);
        drawerListArray = getResources().getStringArray(R.array.main_activity_items);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        editUsername = (EditText) rootView.findViewById(R.id.txt_username);
        editUsername.setText(getUsername.getUserName());
        editEmail = (EditText) rootView.findViewById(R.id.txt_email);
        editEmail.setText(getUsername.getEmail());
        editPassword = (EditText) rootView.findViewById(R.id.edit_password);
        editConfirmPassword = (EditText) rootView.findViewById(R.id.edit_con_password);
        btnSave = (Button) rootView.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);
        imgClose = (ImageButton) rootView.findViewById(R.id.fab_button);
        imgClose.setOnClickListener(this);

        return rootView;
    }
    @Override
    public void onResume(){
        super.onResume();
        mActivity.getSupportActionBar().setTitle(drawerListArray[8]);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_save:
                // Change password for your account
                mHost = mActivity.getResources().getString(R.string.host_name);
                try {
                    mPort = 9830;
                } catch (NumberFormatException nfe) {
                    Log.i(TAG, "No port given. Set port to 389");
                    mPort = 9830;
                }//http://roamtech.com/
                strPassword = editPassword.getText().toString();
                String salt = strPassword + "{" + getUsername.getUserName() + "}";
                try {
                    password = MainModel.getHash(salt);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                ldapServer = new LDAPServerInstance(mHost, mPort, mEncryption, MainModel.ADMIN_SERVER_DETAILS, MainModel.ADMIN_PASSWORD);
                Log.d(TAG," dnUser------"+getUsername.getDnUser()+" Password------"+getUsername.getPassword()+" mHost--------------"+mHost+" mPort-------------"+mPort);
                new AuthenticateTask(mActivity,this,getUsername.getUserName(), MainModel.SETTINGSFRAGMENTS).execute(ldapServer,getUsername.getPassword(),password);
                break;
            case R.id.fab_button:
                mActivity.getSupportFragmentManager().popBackStackImmediate();
                break;
            default:
                break;
        }
    }

    @Override
    public void onStartTask() {
        progressDialogFragment = new ProgressDialogFragment.Builder()
                .setMessage(getString(R.string.loading)).setCancelableOnTouchOutside(false)
                .build();
        progressDialogFragment.show(mActivity.getSupportFragmentManager(),getString(R.string.dialog_tag));
    }

    @Override
    public void onReturnResult(LoginItems loginItems) throws LDAPException {
        if(progressDialogFragment != null) {
            progressDialogFragment.dismiss(mActivity.getSupportFragmentManager());
        }
    }
}
