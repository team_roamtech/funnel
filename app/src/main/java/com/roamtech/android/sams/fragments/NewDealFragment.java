package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.ItemPagerAdapter;
import com.roamtech.android.sams.dialog_fragments.AddNewContactDialogFragment;
import com.roamtech.android.sams.dialog_fragments.AddNewOrganisationDialogFragment;
import com.roamtech.android.sams.dialog_fragments.DeleteDialog;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.interfaces.CheckProvider;
import com.roamtech.android.sams.interfaces.GoToPager;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.interfaces.SendJsonData;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;

/**
 *
 * Fragment that allows you to create a new sale and save it
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class NewDealFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = "NewDealFragment";
    ImageView ivAdd;
    LinearLayout lytChoosePerson,lytViewpager;
    MainActivity mainActivity;
    AppCompatActivity mActivity;
    AddNewContactDialogFragment newFragment;
    AddNewOrganisationDialogFragment newFragment2;
    public static final int DIALOG_FRAGMENT_ADD_CONTACT = 3;
    public static final int DIALOG_FRAGMENT_ADD_ORGANISATION = 4;
    public static final int PAGER_FRAGMENT = 2;
    TextView txtPerson,txtOrganisation;
    EditText editDealName,editValue,editDescription;
    String contact_name = " ";
    String organisation_name = " ";
    Button btnAdd,btnEdit;
    String organisationID,contactID,organisationContactID;
    String foreignKey;
    String saleName,strDescription;
    String stageID;
    private ViewPager mViewPager;
    int fragmentPosition;
    int position;
    String date,paymentType;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    String [] pagerItemsArray2,pagerItemsArray;
    String stageName;
    Cursor cursor;
    Double cost;
    int salesID;
    long sales_id;
    int status;
    Double value;
    String model;
    GoToPager mCallBack;
    int syncDone;
    String slugID;
    SendJsonData mCallBack3;
    CheckProvider mCallBack4;
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    boolean detailsEntered;
    GetUsername getUsername;
    ImageButton imgFab;
    ImageView ivNext,ivPrevious;
    int currentPosition;
    DeleteDialog newFragment6;
    String [] arrayMainItems;
    AddActivitiesFragment addActivitiesFragment;

    /**
     * Constructor
     */
    public NewDealFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack = (GoToPager) activity;
            mCallBack3 = (SendJsonData) activity;
            mCallBack4 = (CheckProvider) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        arrayMainItems = getResources().getStringArray(R.array.main_activity_items);
        commonHash = new CommonHash(mActivity);
        mainActivity = new MainActivity();
        newFragment = new AddNewContactDialogFragment();
        newFragment2 = new AddNewOrganisationDialogFragment();
        Bundle args = getArguments();
        if(args != null) {
            salesID = args.getInt(mActivity.getResources().getString(R.string.db_table_id));
            saleName = args.getString(mActivity.getResources().getString(R.string.db_table_sale_name));
            stageID = args.getString(mActivity.getResources().getString(R.string.db_table_sale_stage));
            Log.d(TAG,"===============stageID " + String.valueOf(stageID));
            fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
            Log.d(TAG,"===============fragment position " + String.valueOf(fragmentPosition));
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            contact_name = args.getString(mActivity.getResources().getString(R.string.db_table_con_name));
            organisation_name = args.getString(mActivity.getResources().getString(R.string.db_table_org_name));
            organisationID = args.getString(mActivity.getResources().getString(R.string.db_table_org_id));
            contactID = args.getString(mActivity.getResources().getString(R.string.db_table_contact_id));
            cost = args.getDouble(mActivity.getResources().getString(R.string.db_table_sale_cost));
            paymentType = args.getString((mActivity.getResources().getString(R.string.db_table_sale_payment_type)));
            status = args.getInt(mActivity.getResources().getString(R.string.db_table_sale_status));
            Log.d(TAG,"Status is equals to "+String.valueOf(status));
            strDescription = args.getString(mActivity.getResources().getString(R.string.db_table_sale_description));
            slugID = args.getString(mActivity.getResources().getString(R.string.db_table_sale_slug_id));
            foreignKey = args.getString(mActivity.getResources().getString(R.string.db_table_sale_foreign_key));
            model = args.getString(mActivity.getResources().getString(R.string.db_table_sale_model));
            value = cost;
           // salesID = (int) sales_id;
            Log.d(TAG, " Sales ID == "+String.valueOf(salesID));
        }
        Log.d(TAG,"rrrrrrrrrrreeeeeeeeeeeeeeeedddddddddddddddddd "+String.valueOf(fragmentPosition));
        newFragment6 = new DeleteDialog();
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        getUsername = new GetUsername(mActivity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_new_deal, container, false);
        pagerItemsArray2 = getResources().getStringArray(R.array.pager_title_items2);
        txtPerson = (TextView)rootView.findViewById(R.id.txt_person);
        if(contact_name != null && !contact_name.equals(" ") && !contact_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
            txtPerson.setText(contact_name);
        }
        txtOrganisation = (TextView)rootView.findViewById(R.id.txt_organisation);
        if(organisation_name != null && !organisation_name.equals(" ") && !organisation_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
            txtOrganisation.setText(organisation_name);
        }
        editDealName = (EditText)rootView.findViewById(R.id.edit_deal_name);
        if(saleName != null){
            editDealName.setText(saleName);
        }
        editValue = (EditText)rootView.findViewById(R.id.edit_value);
        editValue.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null) {
                    try {
                        value = Double.parseDouble(s.toString().replace(",",""));
                    } catch (NumberFormatException e) {
                        //Error
                    }
                }
            }
        });
        if(cost != 0){
            editValue.setText(MainModel.doubleToStringNoDecimal(cost));
        }
        editDescription = (EditText)rootView.findViewById(R.id.edit_notes);
        if(strDescription != null){
            editDescription.setText(strDescription);
        }
        ivAdd = (ImageView)rootView.findViewById(R.id.img_add);
        ivAdd.setOnClickListener(this);
        btnAdd = (Button)rootView.findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
        btnEdit = (Button)rootView.findViewById(R.id.btn_edit);
        btnEdit.setOnClickListener(this);
        if(salesID != 0){
            btnAdd.setVisibility(View.GONE);
            btnEdit.setVisibility(View.VISIBLE);
        }
        lytViewpager = (LinearLayout)rootView.findViewById(R.id.lyt_viewpager);
//        stageName = pagerItemsArray2[fragmentPosition -1];
        lytChoosePerson = (LinearLayout)rootView.findViewById(R.id.lyt_choose_person);
        lytChoosePerson.setOnClickListener(this);
        imgFab = (ImageButton) rootView.findViewById(R.id.fab_button);
        imgFab.setOnClickListener(this);
        ivNext = (ImageView) rootView.findViewById(R.id.img_next);
        ivNext.setOnClickListener(this);
        ivPrevious = (ImageView) rootView.findViewById(R.id.img_previous);
        ivPrevious.setOnClickListener(this);

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // BEGIN_INCLUDE (setup_viewpager)
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
            mViewPager.setAdapter(new ItemPagerAdapter(mActivity, pagerItemsArray2));
            mViewPager.setCurrentItem(fragmentPosition - 1);
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onResume(){
        super.onResume();
        if(strDescription == null){
            editDescription.setText(null);
        }
        if(saleName == null){
            editDealName.setText(null);
        }
        if(cost == 0){
            editValue.setText(null);
        }
        mActivity.getSupportActionBar().setTitle(arrayMainItems[3]);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_flag).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                addActivitiesFragment.setTargetFragment(NewDealFragment.this, MainModel.ACTIVITY_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            default:

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        setTargetFragment(null, -1);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.img_add:
                newFragment.setTargetFragment(NewDealFragment.this, DIALOG_FRAGMENT_ADD_CONTACT);
                newFragment.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.lyt_choose_person:
                newFragment.setTargetFragment(NewDealFragment.this, DIALOG_FRAGMENT_ADD_CONTACT);
                newFragment.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.img_add2:
                newFragment2.setTargetFragment(NewDealFragment.this, DIALOG_FRAGMENT_ADD_ORGANISATION);
                newFragment2.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.lyt_choose_organisation:
                newFragment2.setTargetFragment(NewDealFragment.this, DIALOG_FRAGMENT_ADD_ORGANISATION);
                newFragment2.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.img_next:
                mViewPager.setCurrentItem(currentPosition + 1);
                break;
            case R.id.img_previous:
                mViewPager.setCurrentItem(currentPosition - 1);
                break;
            case R.id.btn_add:
                Log.d(TAG, "adding Fragment " + fragmentPosition);
                if(fillData()) {
                    updateContacts();
                    sendData();
                }
                break;
            case R.id.btn_edit:
                if(fillData()) {
                    sendData();
                }
                break;
            case R.id.fab_button:
                if(salesID == 0){
                    getFragmentManager().popBackStackImmediate();
                }else{
                    Bundle args = new Bundle();
                    args.putString(mActivity.getResources().getString(R.string.txt_advisory),mActivity.getResources().getString(R.string.txt_advisory_delete) + " " +arrayMainItems[2]);
                    newFragment6.setArguments(args);
                    newFragment6.setTargetFragment(NewDealFragment.this, MainModel.DIALOG_FRAGMENT_DELETE_DIALOG);
                    newFragment6.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                }

                break;
        }
    }

    /**
     * Validates data entered by the user
     *
     * @return boolean true if the data is okay
     */
    public boolean fillData(){
        if(editDealName.length() == 0){
            Toast.makeText(
                    mActivity,
                    mActivity.getResources().getString(R.string.edit_deal_name), Toast.LENGTH_LONG)
                    .show();
            detailsEntered = false;

        }else if(editValue.length() == 0){
            Toast.makeText(
                    mActivity,
                    mActivity.getResources().getString(R.string.advisory_deal_value), Toast.LENGTH_LONG)
                    .show();
            detailsEntered = false;
        }else {
            detailsEntered = true;
        }
        return detailsEntered;
    }

    /**
     * Saves data of sale to DB
     */
    public void sendData(){
            strDescription = editDescription.getText().toString();
            saleName = editDealName.getText().toString();

            if(slugID == null) {
                salesID = (int) insertDeal();
                try {
                    slugID = commonHash.getHash(String.valueOf(salesID));
                    Log.d(TAG, "Common Hash is  " + slugID);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                insertDeal();
            }else{
                insertDeal();
            }
        mActivity.getSupportFragmentManager().popBackStackImmediate();
            Intent intent = new Intent();
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_name),saleName);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_foreign_key),foreignKey);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_model),model);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sync_done),syncDone);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_cost),value);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_stage),stageID);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_status),status);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_id),salesID);
            intent.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position),fragmentPosition);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_description),strDescription);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_slug_id),slugID);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_con_name),contact_name);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_name),organisation_name);
            intent.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), 1);

            mActivity.setResult(Activity.RESULT_OK, intent);
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == DIALOG_FRAGMENT_ADD_CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    contact_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_name));
                    foreignKey = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_slug_id));
                    contactID = String.valueOf(data.getLongExtra(mActivity.getResources().getString(R.string.db_table_id),0));
                    Log.d(TAG," foreign key = "+foreignKey);
                    model = "contacts";
                    Log.d(TAG,"......................." + contactID);
                        Log.d(TAG, TAG + String.valueOf(contactID));
                    if(data.getExtras().containsKey(mActivity.getResources().getString(R.string.db_table_org_name))){
                        organisation_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_name));
                        if(!organisation_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
                            txtOrganisation.setText(organisation_name);
                            editDealName.setText(organisation_name+"_"+"Sale");
                        }else{
                            txtOrganisation.setText(null);
                            organisation_name = null;
                            editDealName.setText(contact_name+"_"+"Sale");
                        }
                       // txtOrganisation.setText(organisation_name);
                        Log.d(TAG, TAG + String.valueOf(organisationID));
                    }else{
                        organisation_name = null;
                       // organisationID = null;
                        txtOrganisation.setText(null);
                        editDealName.setText(contact_name+"_"+"Sale");
                    }
                 if(!contact_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
                        txtPerson.setText(contact_name);
                    }
                }
            }
        }else if(requestCode == DIALOG_FRAGMENT_ADD_ORGANISATION){
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                            foreignKey = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id));
                    Log.d(TAG,"foreign key = "+foreignKey);
                    model = "organisations";
                            Log.d(TAG, TAG + String.valueOf(organisationID));
                            organisation_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_name));
                            txtOrganisation.setText(organisation_name);
                            editDealName.setText(organisation_name + "_" + "Sale");
                    contact_name = null;
                    contactID = null;
                    txtPerson.setText(null);
                }
            }
        }else if(requestCode == MainModel.DIALOG_FRAGMENT_DELETE_DIALOG){
            if (resultCode == Activity.RESULT_OK) {
                status = 2;
                insertDeal();
                Intent intent = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                mActivity.getSupportFragmentManager().popBackStackImmediate();
            } else {

            }
        }
    }

    /**
     * Updates contacts info
     */
    public void updateContacts() {
        coordinates = getCoordinates.getCoordinates();
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_status),1 );
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_longitude), 0.0);
        }
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "11");
        mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(contactID),mActivity.getResources().getString(R.string.flag_sync)});
    }

    /**
     * Inserts new sale into DB
     *
     * @return Id of the new sale entered
     */
    public long insertDeal() {
        stageID = "PROSPECTS";

        syncDone = 1;
        coordinates = getCoordinates.getCoordinates();
        int playListId = 0;
        View view2 = mViewPager.findViewWithTag(mActivity.getResources().getString(R.string.view_tag) + mViewPager.getCurrentItem());
        Log.d(TAG,"++++++++++++++++++===============stageID " + String.valueOf(stageID));
        Log.d(TAG,TAG+" "+ String.valueOf(stageID));
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_stage), stageID);
        Log.d(TAG,"===============stageID " + String.valueOf(stageID));
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_name),saleName);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_status),status);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_cost),value);

        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_foreign_key),foreignKey);
        Log.d(TAG,"foreign key = "+foreignKey);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_model),model);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_description),strDescription);
       // Calendar c = Calendar.getInstance();
        //date = dateFormat.format(c.getTime());
        //initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_update),date);
        //initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_parent_id),salesID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_payment_type),"mpesa");
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_slug_id), slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_longitude), 0.0);
        }
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());
        if(salesID == 0) {
            Log.d(TAG,"===============it gets here");
            initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "2");
            Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
            playListId = (int)ContentUris.parseId(insertUri);
        }else{
            Log.d(TAG,"===============it gets here");
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "9");
            playListId  = mActivity.getContentResolver().update(data, initialValues,null,new String[]{String.valueOf(salesID),mActivity.getResources().getString(R.string.flag_sync)});
        }

        Log.d(TAG, "-------" + String.valueOf(playListId));
        return playListId;
    }
}
