package com.roamtech.android.sams.models;

/**
 *
 * Class that stores Quotation items together
 *
 * Created by dennis on 8/3/15.
 * @author Dennis Mwangi Karuri
 */
public class QuotationsItems {
    public String itemName;
    public double quantity;
    public double price;
    public String description;

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
