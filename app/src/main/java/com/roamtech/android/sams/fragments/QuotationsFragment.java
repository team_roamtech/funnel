package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.QuotationsAdapter;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 *
 * Fragment that lists all quotations
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class QuotationsFragment extends Fragment implements View.OnClickListener,LoaderManager.LoaderCallbacks<Cursor>,LaunchFragment {
    public static final String TAG = "QuotationsFragment";
    public static final int PAGER_FRAGMENT = 2;
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    public static final int LOADER = 1700;
    ListView lstQuotations;
    FrameLayout frmLayouts2;
    Cursor c;
    View infoView;
    ProgressBar mProgresBar;
    TextView txtNoContacts,txtAddContacts,txtTitle;
    AddQuotationFragment addQuotationFragment;
    QuotationsAdapter mAdapter;
    int quotationDone = 0;
//    OnAddSelectedListener mCallBack2;
    int position,position2;
    int fragmentPosition;
    String title2;
    ImageButton ivAdd;
    View headerView;
    Toolbar mActionBarToolbar;
    ViewQuotationFragment viewQuotationFragment;
    int mFirstVisibleItem;
    StagesStateFragment stagesStateFragment;
    AddActivitiesFragment addActivitiesFragment;
    CommentsFragment commentsFragment;

    /**
     * Constructor
     */
    public QuotationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            //mCallBack2 = (OnAddSelectedListener)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        stagesStateFragment = new StagesStateFragment();
        Bundle args = getArguments();
        if(args != null){
            position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position),-1);
            title2 = args.getString(mActivity.getResources().getString(R.string.bundle_title));
        }
        mainActivity = new MainActivity();
    }

    /**
     * Initializes AddQuotationFragment
     */
    public void init(){
        addQuotationFragment = new AddQuotationFragment();
        Bundle args = getArguments();
        if(args != null) {
            mActivity.getSupportActionBar().setTitle(args.getString(mActivity.getResources().getString(R.string.bundle_title)));
            position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            //position2 = position;
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_quotations, container, false);
        mActionBarToolbar = (Toolbar) mActivity.findViewById(R.id.toolbar_actionbar);
        headerView = mActivity.getLayoutInflater().inflate(R.layout.listview_fragment_header, null);
        lstQuotations = (ListView) rootView.findViewById(R.id.lst_contacts);
        infoView = (View) rootView.findViewById(R.id.error_container);
        mProgresBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        txtNoContacts = (TextView) rootView.findViewById(R.id.txt_no_deals);
        txtAddContacts = (TextView) rootView.findViewById(R.id.txt_adddeals);
      //  txtTitle = (TextView) rootView.findViewById(R.id.txt_stage_title);

        Log.d(TAG,"fragmentPosition == " + fragmentPosition);
        mAdapter = new QuotationsAdapter(mActivity, null, 0,this);
        lstQuotations.addHeaderView(headerView, null, false);
        infoView.setVisibility(View.GONE);
        lstQuotations.setVisibility(View.VISIBLE);
        lstQuotations.setAdapter(mAdapter);
        frmLayouts2 = (FrameLayout) rootView.findViewById(R.id.frm_layouts);
        frmLayouts2.setBackgroundColor(mActivity.getResources().getColor(R.color.quotations_green));
        ivAdd = (ImageButton) rootView.findViewById(R.id.fab_button);
        ivAdd.setOnClickListener(this);

        lstQuotations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        lstQuotations.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_TOUCH_SCROLL || scrollState == SCROLL_STATE_FLING) {
                    Log.d(TAG, "Scrolling");
                    if (mFirstVisibleItem != 0) {
                        mActionBarToolbar.setBackgroundResource(R.color.toolbar_quotations_green);
                    }/*else {
                        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                    }*/

                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d(TAG, "First Visible Item " + String.valueOf(firstVisibleItem));
                mFirstVisibleItem = firstVisibleItem;
                if (firstVisibleItem == 0 && MainModel.listIsAtTop(lstQuotations)) {
                    mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                } else {
                    mActionBarToolbar.setBackgroundResource(R.color.toolbar_quotations_green);
                }

            }
        });

        if(savedInstanceState != null){
            Log.d(TAG,"stalemate 1");

        }else{
            Log.d(TAG,"stalemate 2");
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Bundle args = getArguments();
        if(args != null){
            mActivity.getSupportActionBar().setTitle(args.getString(mActivity.getResources().getString(R.string.bundle_title)));
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            position2 = position;
           // txtTitle.setText(title2);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
    }


    /**
     * Starts Loader
     */
    public void runStringLoader(){
        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
           //outState.
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, menu);
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_flag).setVisible(true);
        menu.findItem(R.id.action_comments).setVisible(true);

        //        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                addActivitiesFragment.setTargetFragment(QuotationsFragment.this, MainModel.ACTIVITY_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                commentsFragment.setTargetFragment(QuotationsFragment.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
            default:

                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.PAGER_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
                mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
                Bundle args = new Bundle();
                int fragmentPos = data.getIntExtra(mActivity.getResources().getString(R.string.bundle_fragment_position),2);
                Log.d(TAG,"Fragment pos " + fragmentPos);
                args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPos);
                stagesStateFragment.setArguments(args);
                mainActivity.addFragment(stagesStateFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) stagesStateFragment).getClass().getName());
               // mActivity.getSupportFragmentManager().popBackStackImmediate();
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        init();
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"20");
        return new CursorLoader(getActivity(),data, null, null,
                new String[] {}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mProgresBar.setVisibility(View.GONE);
        c = data;
//        Log.d(TAG, "--------" + String.valueOf(c.getCount()));
        if(c == null || c.getCount() == 0 ){
          /*  infoView.setVisibility(View.VISIBLE);
            txtNoContacts.setText(mActivity.getResources().getString(R.string.txt_noquotations));
            txtAddContacts.setText(mActivity.getResources().getString(R.string.txt_addquotations));
            lstQuotations.setVisibility(View.GONE);*/
        }else{

        }
        mAdapter.swapCursor(c);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.img_add:
                //Intent intent = new Intent();
                // mActivity.setResult(Activity.RESULT_OK, intent);
                // Log.d(TAG,String.valueOf(getTargetRequestCode()));
                //getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                // getFragmentManager().popBackStackImmediate();
               /* Bundle args = new Bundle();
                args.putInt(mActivity.getResources().getString(R.string.bundle_position), position2);
                args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
                mCallBack2.addSelectedItem(args,5,0);*/
                break;
            case R.id.fab_button:
                addQuotationFragment.setTargetFragment(QuotationsFragment.this, MainModel.PAGER_FRAGMENT);
                mainActivity.addFragment(addQuotationFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addQuotationFragment).getClass().getName());
                break;
            default:

                break;
        }
    }

    @Override
    public void launchFragment(int position,Bundle bundles, int view) {
            Log.d(TAG, "launchFragment view == "+ position2);
                bundles.putInt(mActivity.getResources().getString(R.string.bundle_position), position2);
                Log.d(TAG, bundles.getString(mActivity.getResources().getString(R.string.db_table_qu_name)));
                viewQuotationFragment = new ViewQuotationFragment();
                viewQuotationFragment.setArguments(bundles);
                viewQuotationFragment.setTargetFragment(QuotationsFragment.this, PAGER_FRAGMENT);
                mainActivity.addFragment(viewQuotationFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) viewQuotationFragment).getClass().getName());
    }

    /**
     *
     * @param position position of quotation
     * @return int of record updated in activity table
     */
    public int updateActivity(int position){
        Log.d(TAG, "launchFragment view == 1  " + String.valueOf(position));
        int activityID;
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_status), quotationDone);
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "30");
        activityID = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(position)});
        Log.d(TAG,"---------------------- launchFragment view ACTIVITY_ID " +String.valueOf(activityID));
        return activityID;
    }
}
