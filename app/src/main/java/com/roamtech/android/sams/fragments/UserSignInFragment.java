package com.roamtech.android.sams.fragments;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.AdminActivity;
import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.async_tasks.AuthenticateTask;
import com.roamtech.android.sams.async_tasks.CheckStatusTask;
import com.roamtech.android.sams.async_tasks.GetTargetsTasks;
import com.roamtech.android.sams.client.LDAPServerInstance;
import com.roamtech.android.sams.client.LDAPUtilities;
import com.roamtech.android.sams.dialog_fragments.AdvisoryDialogFragment;
import com.roamtech.android.sams.dialog_fragments.ProgressDialogFragment;
import com.roamtech.android.sams.http.NetworkConnectionStatus;
import com.roamtech.android.sams.interfaces.GoToPager;
import com.roamtech.android.sams.interfaces.ReturnAuthenticationResult;
import com.roamtech.android.sams.interfaces.SendJsonData;
import com.roamtech.android.sams.models.LoginItems;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.unboundid.ldap.sdk.LDAPException;
import com.roamtech.android.sams.services.RegistrationIntentService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Fragment that allows user to Signin in to the app. it Captures the user details Username and Password Authenticates against
 * details saved in a LDAP server.
 *
 * Created by Karuri on 2/23/2015.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class UserSignInFragment extends Fragment implements View.OnClickListener,ReturnAuthenticationResult,SendJsonData {
    public static final String TAG = "UserSignInFragment";
    Button btnSignIn;
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    EditText editEmail,editPassword;
    String email,strPassword,password;
    private String mHost;
    private int mPort;
    private int mEncryption = 0;
    protected boolean mRequestNewAccount = true;
    AdvisoryDialogFragment dialogAdvisory = new AdvisoryDialogFragment();
    MainActivity mainActivity;
    ProgressDialogFragment progressDialogFragment;
    AppCompatActivity mActivity;
    int position,position2;
    TextView txtTitle,txtNoAccount;
    String dnUser= null;
    LDAPServerInstance ldapServer;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    GoToPager mCallBack2;
    boolean saveState = true;
    LoginItems loginItems;


    public UserSignInFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack2 = (GoToPager)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Bundle args = getArguments();
            if(args != null) {
                position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager
                .getDefaultSharedPreferences(mActivity);
        progressDialogFragment = new ProgressDialogFragment();
        editor = prefs.edit();
        mainActivity = new MainActivity();
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, TAG + " " + "setUserVisibleHint oncreate view " + position);
        View rootView = inflater.inflate(R.layout.fragment_user_sign_in2, container, false);
        // Inflate the layout for this fragment
        editEmail = (EditText) rootView.findViewById(R.id.edit_email);
        editPassword = (EditText) rootView.findViewById(R.id.edit_password);
        btnSignIn = (Button) rootView.findViewById(R.id.btn_sign_in);
        btnSignIn.setOnClickListener(this);
        txtTitle = (TextView) rootView.findViewById(R.id.txt_title);
        txtNoAccount = (TextView) rootView.findViewById(R.id.txt_no_account);
        txtNoAccount.setOnClickListener(this);

        Bundle args = getArguments();
        if(args != null) {
            Log.d(TAG, TAG + " " + "setUserVisibleHint " +position);
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));

        }
        if(position == 1){
           // txtTitle.setText(mActivity.getResources().getString(R.string.txt_user_title));
        }else if(position == 2){
            //txtTitle.setText(mActivity.getResources().getString(R.string.txt_admin_title));
        }

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        //coordinates = getCoordinates.getCoordinates();
        //Log.d(TAG,"Latitude = "+coordinates[0] + " "+"Longitude = "+coordinates[1]);

    }

    public void onPause(){
        super.onPause();
        saveState = false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    /**
     *
     * Adds an account if the the user is successfully authenticated
     *
     */
    public void addAccount(){
        AccountManager accountManager = AccountManager.get(mActivity); //this is Activity
        Account account = new Account(email,MainModel.PACKAGE_DETAILS);
        boolean success = accountManager.addAccountExplicitly(account,password,null);
        if(success){
            Log.d(TAG, "Account created");
        }else{
            Log.d(TAG,"Account creation failed. Look at previous logs to investigate");
        }
    }

    /**
     *
     * Gets LDAPserver details that will be used to authenticate the user details
     *
     */
    public void getLDAPServerDetails() {
        Log.i(TAG, "handleLogin");
        if (mRequestNewAccount) {
            email = editEmail.getText().toString();
            email = email.replace(" ", "");
            String uid="uid="+email;
            String dn = MainModel.SERVER_DETAILS;
            dnUser= uid+","+dn;
            Log.d(TAG,dnUser);
            if (prefs.contains(getString(R.string.bundle_dnuser))) {
                Log.d(TAG, "Remove prefs");
                prefs.edit().remove(getString(R.string.bundle_dnuser)).commit();
            }
            editor.putString(getString(R.string.bundle_dnuser),dnUser);
            editor.commit();
        }
        try {
            strPassword = editPassword.getText().toString();
            strPassword = strPassword.replace(" ", "");
            String salt = strPassword + "{" + email + "}";
            // String salt = mPasswordEdit.getText().toString()+"{" + mUsername + "}";
             password = MainModel.getHash(salt);
            Log.d(TAG,password);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        //mPassword = mPasswordEdit.getText().toString();
        mHost = mActivity.getResources().getString(R.string.host_name);
        try {
            mPort = 389;
        } catch (NumberFormatException nfe) {
            Log.i(TAG, "No port given. Set port to 389");
            mPort = 389;
        }//http://roamtech.com/


        ldapServer = new LDAPServerInstance(mHost, mPort, mEncryption, dnUser, password);
        LDAPUtilities ldapUtilities = new LDAPUtilities();

        new AuthenticateTask(mActivity,this,email,MainModel.USERSIGINFRAGMENT).execute(ldapServer);
        // Start authenticating...
        //ldapUtilities.attemptAuth(ldapServer, LoginActivity.this);
    }



    @Override
    public void onStartTask() {
        progressDialogFragment = new ProgressDialogFragment.Builder()
                .setMessage(getString(R.string.loading)).setCancelableOnTouchOutside(false)
                .build();
        progressDialogFragment.show(mActivity.getSupportFragmentManager(),getString(R.string.dialog_tag));
    }

    @Override
    public void onReturnResult(LoginItems loginItems) throws LDAPException {
        if(saveState == true) {
            this.loginItems = loginItems;
            if (loginItems != null) {
                if (loginItems.isUser() == true) {
                   // if (mActivity.getSupportFragmentManager() != null) {
                       // progressDialogFragment = (ProgressDialogFragment) mActivity.getSupportFragmentManager().findFragmentByTag(getString(R.string.dialog_tag));
                    new CheckStatusTask(mActivity,this).execute(email);
                  //  }
                } else if (loginItems.isUser() == false & loginItems.getResultCode() == -1) {
                    dismissDialog();
                    MainModel.activateAccount(prefs, false);
                    Bundle args = new Bundle();
                    dialogAdvisory = new AdvisoryDialogFragment();
                    args.putString(mActivity.getResources().getString(R.string.txt_advisory), mActivity.getResources().getString(R.string.toast_advisory_invalid_details));
                    dialogAdvisory.setArguments(args);
                    dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), mActivity.getResources().getString(R.string.dialog_tag));
                } else if (loginItems.isUser() == false & loginItems.getResultCode() == 49) {
                    dismissDialog();
                    MainModel.activateAccount(prefs, false);
                    Bundle args = new Bundle();
                    dialogAdvisory = new AdvisoryDialogFragment();
                    args.putString(mActivity.getResources().getString(R.string.txt_advisory), mActivity.getResources().getString(R.string.txt_advisory_invalid_details));
                    dialogAdvisory.setArguments(args);
                    dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), mActivity.getResources().getString(R.string.dialog_tag));
                } else if (loginItems.isUser() == false & loginItems.getResultCode() == 32) {
                    dismissDialog();
                    MainModel.activateAccount(prefs, false);
                    Bundle args = new Bundle();
                    args.putString(mActivity.getResources().getString(R.string.txt_advisory), mActivity.getResources().getString(R.string.toast_no_such_user));
                    dialogAdvisory.setArguments(args);
                    dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), mActivity.getResources().getString(R.string.dialog_tag));
                } else if (loginItems.isUser() == false & loginItems.getResultCode() == 91) {
                    dismissDialog();
                    MainModel.activateAccount(prefs, false);
                    Bundle args = new Bundle();
                    dialogAdvisory = new AdvisoryDialogFragment();
                    args.putString(mActivity.getResources().getString(R.string.txt_advisory), mActivity.getResources().getString(R.string.toast_connection_error));
                    dialogAdvisory.setArguments(args);
                    dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), mActivity.getResources().getString(R.string.dialog_tag));
                } else {
                    dismissDialog();
                    MainModel.activateAccount(prefs, false);
                    Bundle args = new Bundle();
                    dialogAdvisory = new AdvisoryDialogFragment();
                    args.putString(mActivity.getResources().getString(R.string.txt_advisory), mActivity.getResources().getString(R.string.toast_advisory_invalid_details));
                    dialogAdvisory.setArguments(args);
                    dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), mActivity.getResources().getString(R.string.dialog_tag));
                }
            } else {
                dismissDialog();
                MainModel.activateAccount(prefs, false);
                Bundle args = new Bundle();
                dialogAdvisory = new AdvisoryDialogFragment();
                args.putString(mActivity.getResources().getString(R.string.txt_advisory), mActivity.getResources().getString(R.string.toast_advisory_invalid_details));
                dialogAdvisory.setArguments(args);
                dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), mActivity.getResources().getString(R.string.dialog_tag));
            }
        }
    }

    /**
     *
     * Dismiss the Progress Dialog meant to show activity is taking place
     *
     */
    public void dismissDialog(){
        if(progressDialogFragment != null) {
            progressDialogFragment.dismiss(mActivity.getSupportFragmentManager());
        }
    }

    /**
     *
     * Validates the data to ensure only valid data has been entered in the EditTexts
     *
     */
    private void fillData(){
        Bundle args = new Bundle();
        if(editEmail.getText().length() == 0) {
            args.putString(mActivity.getResources().getString(R.string.txt_advisory),mActivity.getResources().getString(R.string.txt_advisory_enter_email));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), mActivity.getResources().getString(R.string.dialog_tag));
        }else if(editPassword.getText().length() == 0){
            args.putString(mActivity.getResources().getString(R.string.txt_advisory),mActivity.getResources().getString(R.string.txt_advisory_enter_password));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), mActivity.getResources().getString(R.string.dialog_tag));
        }else{
            getLDAPServerDetails();
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_sign_in:
                if(NetworkConnectionStatus.isOnline(mActivity)) {
                    fillData();
                }else{
                    Bundle args = new Bundle();
                    args.putString(mActivity.getResources().getString(R.string.txt_advisory),mActivity.getResources().getString(R.string.txt_advisory_no_connection));
                    dialogAdvisory.setArguments(args);
                    dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), mActivity.getResources().getString(R.string.dialog_tag));
                }
                break;
            case R.id.txt_no_account:
                mCallBack2.moveToPager(0,2);
                break;
            default:

                break;
        }
    }

    /**
     *
     * Checks System roles of the user authenticated
     *
     * @param roles System roles of the Authenticated user
     * @return String of the role that has been found
     */
    public String checkRoles(String [] roles){
        for(int i =0; i < roles.length; i++){
            if(roles[i].equals(MainModel.ROLE_SALES_EXECUTIVE)){
                return roles[i];
            }else if(roles[i].equals(MainModel.ROLE_USER)){
                return roles[i];
            }
        }
        return null;
    }


    @Override
    public void jsonData(String jsonData) {
        if(jsonData.equals("0")){
            String userRoles = checkRoles(loginItems.getRoles());
            if (userRoles != null) {
                dismissDialog();
                addAccount();
                new GetTargetsTasks(mActivity, email).execute();
                if (prefs.contains(mActivity.getResources().getString(R.string.bundle_username))) {
                    Log.d(TAG, "Remove prefs");
                    prefs.edit().remove(mActivity.getResources().getString(R.string.bundle_username)).commit();
                }
                editor.putString(mActivity.getResources().getString(R.string.bundle_username), email);
                editor.commit();
                if (prefs.contains(mActivity.getResources().getString(R.string.bundle_password))) {
                    Log.d(TAG, "Remove prefs");
                    prefs.edit().remove(mActivity.getResources().getString(R.string.bundle_password)).commit();
                }
                editor.putString(mActivity.getResources().getString(R.string.bundle_password), password);
                editor.commit();

                if (prefs.contains(mActivity.getResources().getString(R.string.bundle_email_id))) {
                    Log.d(TAG, "Remove prefs");
                    prefs.edit().remove(mActivity.getResources().getString(R.string.bundle_email_id)).commit();
                }
                editor.putString(mActivity.getResources().getString(R.string.bundle_email_id), loginItems.getMail());
                editor.commit();

                if (prefs.contains(mActivity.getResources().getString(R.string.bundle_user_roles))) {
                    Log.d(TAG, "Remove prefs");
                    prefs.edit().remove(mActivity.getResources().getString(R.string.bundle_user_roles)).commit();
                }
                Log.d(TAG, "Added prefs");
                editor.putString(mActivity.getResources().getString(R.string.bundle_user_roles), userRoles);
                editor.commit();

                MainModel.activateAccount(prefs,true);


                // Start IntentService to register this application with GCM.
                Intent intent = new Intent(mActivity, RegistrationIntentService.class);
                mActivity.startService(intent);
                //}

                // if (userRoles.equals(MainModel.ROLE_USER)) {
                Intent i = new Intent(mActivity, MainActivity.class);
                startActivity(i);
                          /*  } else if (userRoles.equals(MainModel.ROLE_SUPER_ADMIN) || userRoles.equals(MainModel.ROLE_ADMIN)) {
                                Intent i = new Intent(mActivity, AdminActivity.class);
                                startActivity(i);
                            }*/
            } else {
                dismissDialog();
                MainModel.activateAccount(prefs, false);
                Bundle args = new Bundle();
                args.putString(mActivity.getResources().getString(R.string.txt_advisory), mActivity.getResources().getString(R.string.txt_advisory_invalid_details));
                dialogAdvisory.setArguments(args);
                dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
            }
            mActivity.finish();
        }else{
            dismissDialog();
            MainModel.activateAccount(prefs,false);
            Bundle args = new Bundle();
            args.putString(mActivity.getResources().getString(R.string.txt_advisory), mActivity.getResources().getString(R.string.txt_advisory_account_deactivated));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }
    }
}
