package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.roamtech.android.sams.R;

/**
 * Created by dena on 1/29/15.
 */
public class OrganisationsAdapter extends CursorAdapter {

    public static final String TAG = "ContactsAdapter";
    private LayoutInflater vi;
    Cursor c;
    Context context;

    public OrganisationsAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.c = c;
        Log.d(TAG, "OrganisationsAdapter constructor");
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup viewGroup) {
        Log.d(TAG,"OrganisationsAdapter new view");
        return vi.inflate(R.layout.auto_textview_item2, viewGroup, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView txtOrganisation = (TextView) view.findViewById(R.id.txt_organisation);
        txtOrganisation.setText(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_organisation_name))));

    }
}
