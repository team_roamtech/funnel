package com.roamtech.android.sams.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.interfaces.CheckSignOut;

/**
 * Created by dennis on 5/25/15.
 */
public class SignOutDialog extends DialogFragment implements View.OnClickListener {
    public static final String TAG = "SignOutDialog";
    AppCompatActivity mActivity;
    Button btnSignout,btnCancel;
    EditText txtWarning;
    String warning;
    CheckSignOut mCallBack;

    public SignOutDialog() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack = (CheckSignOut) activity;
        }catch (ClassCastException e) {
                throw new ClassCastException(activity.toString()
                        + " must implement OnHeadlineSelectedListener");
            }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args != null){
            warning = args.getString(mActivity.getResources().getString(R.string.txt_advisory));
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_logout_warning);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.action_sign_out));
        dialog.setCanceledOnTouchOutside(false);
        txtWarning = (EditText) dialog.findViewById(R.id.txt_logout_warning);
        txtWarning.setText(warning);
        btnSignout = (Button) dialog.findViewById(R.id.btn_sign_out);
        btnSignout.setOnClickListener(this);
        btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        return dialog;
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_sign_out:
                mCallBack.confirmSignOut();
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
        }

    }
}
