package com.roamtech.android.sams.generators;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.roamtech.android.sams.R;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * Helper Class used to hash String such as passwords
 *
 * Created by dennis on 3/27/15.
 * * @author Dennis Mwangi Karuri
 */
public class CommonHash {
    public static  final String TAG = "CommonHash";
    public SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    public String date;
    public SharedPreferences prefs;
    public SharedPreferences.Editor editor;
    public Context ctx;
    public String username,password;

    /**
     * Constructor
     * Intializes the Context and The Shared Preferences
     * @param ctx
     */
    public CommonHash(Context ctx){
        this.ctx = ctx;
        prefs = PreferenceManager
                .getDefaultSharedPreferences(this.ctx);
    }


    /**
     *
     * Hashes the String value passed to it Using MD5 algorithm
     *
     * @param inputValue String Value to be hashed
     * @return String value already Hashed
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public String getHash(String inputValue) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        Calendar ct = Calendar.getInstance();
        date = dateFormat.format(ct.getTime());
        if(prefs.contains(ctx.getResources().getString(R.string.bundle_username))){
            username =   prefs.getString(ctx.getResources().getString(R.string.bundle_username), ctx.getResources().getString(R.string.app_name));
            Log.d(TAG, username);

        }
        if(prefs.contains(ctx.getResources().getString(R.string.bundle_password))){
            password =   prefs.getString(ctx.getResources().getString(R.string.bundle_password), ctx.getResources().getString(R.string.app_name));
            Log.d(TAG,password);

        }

        inputValue = "["+inputValue + date +username + password+"]" ;
        String salt = inputValue;
        if (inputValue != null) {
            MessageDigest digester = MessageDigest.getInstance("SHA-256");

            digester.update(inputValue.getBytes());

            BigInteger hash = new BigInteger(1, digester.digest());
            salt = hash.toString(16);
            while (salt.length() < 32) { // 40 for SHA-1
                salt = "0" + salt;
            }

        }
        return salt;
    }
}
