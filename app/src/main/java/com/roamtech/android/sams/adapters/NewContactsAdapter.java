package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.interfaces.LaunchFragment;

/**
 * Created by dennis on 2/18/15.
 */
public class NewContactsAdapter extends CursorAdapter {
    public static final String TAG = "NewContactsAdapter";
    private LayoutInflater vi;
    Cursor c;
    Context context;
    LaunchFragment mCallBack;
    Fragment fragment;

    public NewContactsAdapter(Context context, Cursor c, int flags,Fragment fragment) {
        super(context, c, flags);
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.c = c;
        this.fragment = fragment;
        Log.d(TAG, "ContactsAdapter constructor");
        try {
            mCallBack = (LaunchFragment) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        Log.d(TAG, "PlaylistAdapter new view");
        View rootView = vi.inflate(R.layout.list_card_items, parent, false);
        ViewHolder holder  =   new ViewHolder();
        holder.mTitle = (TextView) rootView.findViewById(R.id.txt_title);
        holder.mOrganisation = (TextView) rootView.findViewById(R.id.txt_organisation);
        holder.mAddress = (TextView) rootView.findViewById(R.id.txt_address);
        holder.mCard = (LinearLayout) rootView.findViewById(R.id.lyt_card);
        holder.mInnerElements = (LinearLayout) rootView.findViewById(R.id.lyt_inner_elements);
        holder.imgDown = (ImageView) rootView.findViewById(R.id.img_down);
        holder.mNumber = (TextView) rootView.findViewById(R.id.txt_number);
        holder.mEmail = (TextView) rootView.findViewById(R.id.txt_email);
        holder.mWebsite = (TextView) rootView.findViewById(R.id.txt_website);
        holder.mSource = (TextView) rootView.findViewById(R.id.txt_source);
        holder.animSideDown = AnimationUtils.loadAnimation(context,
                R.anim.slide_down);
        // set animation listener
        holder.animSlideUp = AnimationUtils.loadAnimation(context,
                R.anim.slide_up);
        Log.d(TAG," cursor position " + holder.cursorPosition);
        rootView.setTag(holder);
        return rootView;
    }

    @Override
    public void bindView(View view, final Context context, Cursor cursor) {
        final Cursor c = cursor;
        if(view != null) {
            final ViewHolder holder = (ViewHolder) view.getTag();
            holder.mTitle.setText(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_name))));
            if(!cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_organisation_name))).equals(context.getResources().getString(R.string.db_table_org_entry))) {
                holder.mOrganisation.setText(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_organisation_name))));
            }
            holder.mAddress.setText(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_address))));
            holder.mNumber.setText(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_phone))));
            holder.mEmail.setText(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_email))));
            holder.mWebsite.setText(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_website))));
            holder.mSource.setText(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_source))));
            holder.cursorPosition = c.getPosition();
            holder.animSideDown.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            holder.animSlideUp.setAnimationListener(new Animation.AnimationListener() {
                                                       @Override
                                                       public void onAnimationStart(Animation animation) {

                                                       }

                                                       @Override
                                                       public void onAnimationEnd(Animation animation) {
                                                           Log.d(TAG,"animation end");
                                                           if (animation == holder.animSlideUp) {
                                                               holder.mInnerElements.setVisibility(View.GONE);
                                                           }
                                                       }

                                                       @Override
                                                       public void onAnimationRepeat(Animation animation) {

                                                       }
                                                   });
            holder.imgDown.setBackgroundResource(R.drawable.ic_action_down2);
            holder.imgDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.mInnerElements.getVisibility() == View.GONE){
                        holder.mInnerElements.setVisibility(View.VISIBLE);
                        holder.mInnerElements.startAnimation(holder.animSideDown);
                        holder.imgDown.setBackgroundResource(R.drawable.ic_action_up2);
                    }else{
                        holder.mInnerElements.setVisibility(View.GONE);
                        holder.mInnerElements.startAnimation(holder.animSlideUp);
                        holder.imgDown.setBackgroundResource(R.drawable.ic_action_down2);

                    }



                }
            });


            holder.mCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    c.moveToPosition(holder.cursorPosition);
                    Bundle args = new Bundle();
                    args.putInt(context.getResources().getString(R.string.db_table_id), c.getInt(c.getColumnIndex(context.getResources().getString(R.string.db_table_id))));
                    args.putString(context.getResources().getString(R.string.db_table_con_name), c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_name))));
                    args.putString(context.getResources().getString(R.string.db_table_org_name),c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_org_name))));
                    args.putString(context.getResources().getString(R.string.db_table_con_email),c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_email))));
                    args.putString(context.getResources().getString(R.string.db_table_con_phone),c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_phone))));
                    args.putString(context.getResources().getString(R.string.db_table_con_website),c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_website))));
                    args.putString(context.getResources().getString(R.string.db_table_con_source),c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_source))));
                    args.putString(context.getResources().getString(R.string.db_table_con_address),c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_address))));
                    args.putString(context.getResources().getString(R.string.db_table_con_foreign_key),c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_foreign_key))));
                    args.putString(context.getResources().getString(R.string.db_table_con_model),c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_model))));
                    args.putString(context.getResources().getString(R.string.db_table_con_slug_id), c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_slug_id))));
                    args.putInt(context.getResources().getString(R.string.db_table_sync_done), c.getInt(c.getColumnIndex(context.getResources().getString(R.string.db_table_sync_done))));
                    args.putInt(context.getResources().getString(R.string.db_table_con_status), c.getInt(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_status))));
                    mCallBack.launchFragment(holder.cursorPosition,args,1);
                }
            });
        }
    }

    /**
     * This method builds a card with a collpse/expand section inside
     */


    private class ViewHolder{
        public LinearLayout mCard;
        public LinearLayout mInnerElements;
        public ImageView imgDown;
        public TextView mTitle;
        public TextView mOrganisation;
        public TextView mAddress;
        public TextView mNumber;
        public TextView mEmail;
        public TextView mWebsite;
        public TextView mSource;
        public Animation animSideDown,animSlideUp;
        public int cursorPosition;
    }
}
