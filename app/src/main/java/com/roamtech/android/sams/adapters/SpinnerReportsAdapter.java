package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.roamtech.android.sams.R;

/**
 * Created by dennis on 5/12/15.
 */
public class SpinnerReportsAdapter extends ArrayAdapter<String> {
        public static final String TAG = "SpinnerActivitiesAdapter";
        private LayoutInflater vi;
        Context context;
        String [] spinnerItems;


        public SpinnerReportsAdapter(Context context, int resource, String[] spinnerItems) {
            super(context, resource, spinnerItems);
            this.context = context;
            this.spinnerItems = spinnerItems;
            vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return spinnerItems.length;
        }




        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View v = vi.inflate(R.layout.spinner_report_items, null);
            TextView txtTitle = (TextView)v.findViewById(R.id.txt_activity);
            txtTitle.setText(spinnerItems[position]);
            return v;
        }
        public View getDropDownView(int position, View convertView, ViewGroup parent) {

            View v = vi.inflate(R.layout.spinner_report_items, null);
            TextView txtTitle = (TextView)v.findViewById(R.id.txt_activity);
            txtTitle.setText(spinnerItems[position]);
            return v;
        }
    }

