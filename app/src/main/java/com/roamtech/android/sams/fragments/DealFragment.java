package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.ActivitiesAdapter;
import com.roamtech.android.sams.adapters.ItemPagerAdapter;
import com.roamtech.android.sams.dialog_fragments.AddQuotationDialog;
import com.roamtech.android.sams.dialog_fragments.DeleteDialog;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.interfaces.GoToPager;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * Shows Details of a sale and allows the user to edit the sale
 *
 * Created by Karuri on 2/22/2015.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class DealFragment extends Fragment implements View.OnClickListener,LoaderManager.LoaderCallbacks<Cursor>,LaunchFragment,SwipeRefreshLayout.OnRefreshListener {
    public static final String TAG = "DealFragment";
    public static final int LOADER = 1300;
    public static final int PAGER_FRAGMENT = 2;
    MainActivity mainActivity;
    AppCompatActivity mActivity;
    TextView txtSaleName,txtContact,txtOrganisation;
    EditText editNotes,txtCost;
    String saleName = "";
    String contact = "";
    String organisation = "";
    String stageName = "";
    Double cost;
    int saleID;
    String stageID;
    int orgID,contactID;
    //private ViewPager mViewPager;
    int fragmentPosition;
    String [] pagerItemsArray,pagerItemsArray2;;
    Button btnSave,btnReorder,btnCreateQuote;
    //btnWon,btnLost,
    Cursor cursor,c;
    int saleStatus = 0;
    String strDescription;
    int position;
    //NewDealFragment newDealFragment;
    AddQuotationFragment addQuotationFragment;
    AddActivitiesFragment addActivitiesFragment;
    CommentsFragment commentsFragment;
    DeleteDialog newFragment6;
    Spinner spnPayments;
    String payment;
    int insertID;
    int saleWon,createNewSale;
    int quotationID;
    String quotationName,organisationName,contactName;
    int reorder,syncDone,saleParentID;
    GoToPager mCallBack;
    String date,foreignKey,model,slugID;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    Cursor cu;
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    GetUsername getUsername;
    AddQuotationDialog newFragment8;
    int flagDialogLaunched;
    ListView lstActivities;
    ActivitiesAdapter mAdapter;
    View header;
    ImageButton ivAdd;
    //ImageView ivNext,ivPrevious;
    NewDealFragment newDealFragment;
    int currentPosition;
    String [] paymentOptions;
    SwipeRefreshLayout mSwipeRefreshLayout;
    Toolbar mActionBarToolbar;
    int mFirstVisibleItem;
    String [] arrayMainItems;
    LinearLayout lytButtons;

    /**
     * Constructor
     */
    public DealFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack = (GoToPager) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        arrayMainItems = getResources().getStringArray(R.array.main_activity_items);
        commonHash = new CommonHash(mActivity);
        mainActivity = new MainActivity();
        newFragment6 = new DeleteDialog();
        newFragment8 = new AddQuotationDialog();
        paymentOptions = mActivity.getResources().getStringArray(R.array.payment_options_items);
        Bundle args = getArguments();
        if(args != null){
            saleName = args.getString(mActivity.getResources().getString(R.string.db_table_sale_name),"");
//            Log.d(TAG,saleName);
            foreignKey = args.getString(mActivity.getResources().getString(R.string.db_table_sale_foreign_key),"");
            model = args.getString(mActivity.getResources().getString(R.string.db_table_sale_model), "");
            syncDone = args.getInt(mActivity.getResources().getString(R.string.db_table_sync_done),0);
            cost = args.getDouble(mActivity.getResources().getString(R.string.db_table_sale_cost), 0);
            stageID = args.getString(mActivity.getResources().getString(R.string.db_table_sale_stage));
            date = args.getString(mActivity.getResources().getString(R.string.db_table_sale_update));
            saleStatus = args.getInt(mActivity.getResources().getString(R.string.db_table_sale_status),0);
            Log.d(TAG,"Sale Parent ID is equals to "+String.valueOf(saleStatus) +"  "+ stageID);
            saleID = args.getInt(mActivity.getResources().getString(R.string.db_table_id),0);
          //  Log.d(TAG, "SAlES ID == " + String.valueOf(saleID));
            fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
            strDescription = args.getString(mActivity.getResources().getString(R.string.db_table_sale_description));
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            payment = args.getString(mActivity.getResources().getString(R.string.db_table_sale_payment_type));
            slugID = args.getString(mActivity.getResources().getString(R.string.db_table_sale_slug_id));
            contact = args.getString(mActivity.getResources().getString(R.string.db_table_con_name));
            organisation = args.getString(mActivity.getResources().getString(R.string.db_table_org_name));
            Log.d(TAG, "Payments type == " + slugID);
        }
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        getUsername = new GetUsername(mActivity);
        if(fragmentPosition > 1 ){
            flagDialogLaunched = 1;
        }
     }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_deal, container, false);
        mActionBarToolbar = (Toolbar) mActivity.findViewById(R.id.toolbar_actionbar);
        lstActivities = (ListView) rootView.findViewById(R.id.lst_contacts);
        header = mActivity.getLayoutInflater().inflate(R.layout.listview_deal_header, null);
        lstActivities.addHeaderView(header, null, false);
        pagerItemsArray = getResources().getStringArray(R.array.pager_title_items);
        pagerItemsArray2 = getResources().getStringArray(R.array.pager_title_items2);
        txtSaleName = (TextView) rootView.findViewById(R.id.txt_deal_name);
        txtContact = (TextView) rootView.findViewById(R.id.txt_contact);
        txtOrganisation = (TextView) rootView.findViewById(R.id.txt_organisation);
        txtCost = (EditText) rootView.findViewById(R.id.txt_total_cost);
        lytButtons = (LinearLayout) rootView.findViewById(R.id.lyt_buttons);
        mAdapter = new ActivitiesAdapter(mActivity, null, 0,this);
        spnPayments = (Spinner) rootView.findViewById(R.id.spn_payments);
        spnPayments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //Get the selected value
                payment = spnPayments.getSelectedItem().toString();
                Log.d(TAG, "payments choice is " + String.valueOf(payment));
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });
        btnSave = (Button) rootView.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(this);
        btnCreateQuote = (Button) rootView.findViewById(R.id.btn_create_quote);
        btnCreateQuote.setOnClickListener(this);
       // btnWon = (Button) rootView.findViewById(R.id.btn_won);
       // btnWon.setOnClickListener(this);
        if(fragmentPosition >= 2){
            spnPayments.setVisibility(View.VISIBLE);
            for (int k = 0; k < paymentOptions.length; k++) {
                if (paymentOptions[k].equals(payment)) {
                    spnPayments.setSelection(k);
                    break;
                }
            }
         //   btnWon.setClickable(false);
         //   btnWon.setVisibility(View.INVISIBLE);
        }
        //btnLost = (Button) rootView.findViewById(R.id.btn_lost);
        //btnLost.setOnClickListener(this);
        /*if(fragmentPosition >= 2){
            btnLost.setClickable(false);
            btnLost.setVisibility(View.GONE);
        }*/
        btnReorder = (Button) rootView.findViewById(R.id.btn_reorder);
        btnReorder.setOnClickListener(this);
        if(fragmentPosition >= 2){
            btnReorder.setClickable(true);
            btnReorder.setVisibility(View.VISIBLE);
        }
        txtSaleName.setText(saleName);
        txtCost.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null) {
                    try {
                        cost = Double.parseDouble(s.toString().replace(",", ""));
                    } catch (NumberFormatException e) {
                        //Error
                    }
                }
            }
        });
        if(cost != 0) {
            txtCost.setText(MainModel.doubleToStringNoDecimal(cost));
        }
        txtContact.setText(contact);
        if(organisation != null && !organisation.equals(mActivity.getResources().getString(R.string.db_table_org_entry))) {
            txtOrganisation.setText(organisation);
        }
        editNotes = (EditText) rootView.findViewById(R.id.edit_notes);
        editNotes.setText(strDescription);
        ivAdd = (ImageButton) rootView.findViewById(R.id.fab_button);
        ivAdd.setOnClickListener(this);
        /*ivNext = (ImageView) rootView.findViewById(R.id.img_next);
        ivNext.setOnClickListener(this);
        ivPrevious = (ImageView) rootView.findViewById(R.id.img_previous);
        ivPrevious.setOnClickListener(this);*/

        lstActivities.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_TOUCH_SCROLL || scrollState == SCROLL_STATE_FLING) {
                    Log.d(TAG, "Scrolling");
                    if (mFirstVisibleItem != 0) {
                        mActionBarToolbar.setBackgroundResource(R.color.toolbar_sales_blue);
                    }/*else {
                        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                    }*/
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d(TAG, "First Visible Item " + String.valueOf(firstVisibleItem));
                mFirstVisibleItem = firstVisibleItem;
                if (firstVisibleItem == 0 && MainModel.listIsAtTop(lstActivities)) {
                    mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                } else {
                    mActionBarToolbar.setBackgroundResource(R.color.toolbar_sales_blue);
                }

            }
        });

        if(saleStatus == 1){
            lytButtons.setVisibility(View.GONE);
        }else if(!stageID.equals(pagerItemsArray[1])){
            lytButtons.setVisibility(View.GONE);
        }else{
            lytButtons.setVisibility(View.VISIBLE);
        }

        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_container);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.login_blue, R.color.register_green, R.color.purple_theme);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

   /* @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // BEGIN_INCLUDE (setup_viewpager)
        // Get the ViewPager and set it's PagerAdapter so that it can display items
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
          if(fragmentPosition == 4){
               spnPayments.setVisibility(View.VISIBLE);
           }

        mViewPager.setAdapter(new ItemPagerAdapter(mActivity, pagerItemsArray2));

        mViewPager.setCurrentItem(fragmentPosition - 1);
        Log.d(TAG, "-----------------77777777777777777777 " + String.valueOf(fragmentPosition));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                currentPosition = position;
                if (checkQuotationExists() == false) {
                    newFragment8.setTargetFragment(DealFragment.this, MainModel.DIALOG_FRAGMENT_ADD_QUOTATION);
                    newFragment8.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }*/

    @Override
    public void onResume(){
        super.onResume();
        mActivity.getSupportActionBar().setTitle(arrayMainItems[3]);
        //saleStatus = 0;
    }

    @Override
    public void onPause() {
        super.onPause();
        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, menu);
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_flag).setVisible(true);
        menu.findItem(R.id.action_comments).setVisible(true);

        //        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                Bundle args2 = new Bundle();
                args2.putString(mActivity.getResources().getString(R.string.db_table_sale_name), saleName);
                args2.putString(mActivity.getResources().getString(R.string.db_table_act_foreign_key),slugID);
                args2.putString(mActivity.getResources().getString(R.string.db_table_act_model), mActivity.getResources().getString(R.string.db_table_model_3));
                args2.putString(mActivity.getResources().getString(R.string.db_table_con_name), contact);
                args2.putString(mActivity.getResources().getString(R.string.db_table_org_name), organisation);
                args2.putInt(mActivity.getResources().getString(R.string.db_table_sale_id), saleID);
                args2.putString(mActivity.getResources().getString(R.string.db_table_sale_foreign_key), foreignKey);
                args2.putString(mActivity.getResources().getString(R.string.db_table_sale_model), model);
                args2.putString(mActivity.getResources().getString(R.string.db_table_sale_stage), stageID);
                args2.putString(mActivity.getResources().getString(R.string.db_table_sale_name), saleName);
                args2.putInt(mActivity.getResources().getString(R.string.db_table_sale_status), saleStatus);
                args2.putDouble(mActivity.getResources().getString(R.string.db_table_sale_cost), cost);
                args2.putString(mActivity.getResources().getString(R.string.db_table_sale_description), strDescription);
                args2.putString(mActivity.getResources().getString(R.string.db_table_sale_slug_id), slugID);
                args2.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
                args2.putInt(mActivity.getResources().getString(R.string.bundle_position), position);
                args2.putInt(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
                addActivitiesFragment.setArguments(args2);
                addActivitiesFragment.setTargetFragment(DealFragment.this, MainModel.PAGER_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                Bundle args = new Bundle();
                args.putString(mActivity.getResources().getString(R.string.db_table_sale_name), saleName);
                args.putString(mActivity.getResources().getString(R.string.db_table_act_foreign_key),slugID);
                args.putString(mActivity.getResources().getString(R.string.db_table_act_model), mActivity.getResources().getString(R.string.db_table_model_3));
                commentsFragment.setArguments(args);
                commentsFragment.setTargetFragment(DealFragment.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.PAGER_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG,TAG +"------------");
                if(data.getExtras() != null) {
                    saleName = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_name));
                    foreignKey = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_foreign_key));
                    model = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_model));
                    syncDone = data.getIntExtra(mActivity.getResources().getString(R.string.db_table_sync_done), 0);
                    cost = data.getDoubleExtra(mActivity.getResources().getString(R.string.db_table_sale_cost), 0);
                    stageID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_stage));
                    saleStatus = data.getIntExtra(mActivity.getResources().getString(R.string.db_table_sale_status), 0);
                    saleID = data.getIntExtra(mActivity.getResources().getString(R.string.db_table_sale_id), 0);
                    strDescription = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_description));
                    payment = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_payment_type));
                    slugID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_slug_id));
                    contact = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_name));
                    organisation = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_name));
                    fragmentPosition = data.getIntExtra(mActivity.getResources().getString(R.string.bundle_fragment_position),0);
                    txtSaleName.setText(saleName);
                    txtCost.setText(String.valueOf(cost));
                    txtContact.setText(contact);
                    if(organisation != null && !organisation.equals(mActivity.getResources().getString(R.string.db_table_org_entry))) {
                        txtOrganisation.setText(organisation);
                    }
                    editNotes.setText(strDescription);
                  //  mViewPager.setCurrentItem(fragmentPosition - 1);
                }else{
                    //mActivity.getSupportFragmentManager().popBackStackImmediate();
                }
                mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
            }
        }else if(requestCode == MainModel.DIALOG_FRAGMENT_ADD_QUOTATION) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG,TAG +"------------");
                if(data.getExtras() != null) {
                    flagDialogLaunched = data.getIntExtra(mActivity.getResources().getString(R.string.bundle_sale_flag), -1);
                    if (flagDialogLaunched == 1) {
                        launchFragment();
                    } else {

                    }
                }
            }
        }else if(requestCode == MainModel.QUOTATION_FRAGMENT){
            if(resultCode == Activity.RESULT_OK){
                if(data.getExtras() != null) {
                    int fragmentPos = data.getIntExtra(mActivity.getResources().getString(R.string.bundle_fragment_position),2);
                    mActivity.getSupportFragmentManager().popBackStackImmediate();
                    Intent intent = new Intent();
                    intent.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPos);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                }
            }
        }else if(requestCode == MainModel.DIALOG_FRAGMENT_DELETE_DIALOG ){
            if (resultCode == Activity.RESULT_OK) {
                saleStatus = 2;
                updateStage();

            }else{

            }
        }
    }

    /**
     *
     * Sets views with data to display
     *
     * @param args Bundle with data to be set on the views
     */
    public void setViews(Bundle args){
        saleName = args.getString(mActivity.getResources().getString(R.string.db_table_sale_name),"");
//           Log.d(TAG,saleName);
        contact = args.getString(mActivity.getResources().getString(R.string.db_table_contact_name),"");
        organisation = args.getString(mActivity.getResources().getString(R.string.db_table_organisation_name),"");
        orgID = args.getInt(mActivity.getResources().getString(R.string.db_table_org_id),0);
        //   Log.d(TAG, "ORG ID == " + String.valueOf(orgID));
        contactID = args.getInt(mActivity.getResources().getString(R.string.db_table_contact_id),0);
        //   Log.d(TAG, "CONTACT ID == " + String.valueOf(contactID));
        cost = args.getDouble(mActivity.getResources().getString(R.string.db_table_sale_cost), 0);
        stageID = args.getString(mActivity.getResources().getString(R.string.db_table_sale_stage));
        saleStatus = args.getInt(mActivity.getResources().getString(R.string.db_table_status),0);
        //  Log.d(TAG,"Status is equals to "+String.valueOf(saleStatus));
        saleID = args.getInt(mActivity.getResources().getString(R.string.db_table_id),0);
        //  Log.d(TAG, "SAlES ID == " + String.valueOf(saleID));
        fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
        strDescription = args.getString(mActivity.getResources().getString(R.string.db_table_description));
        position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
        payment = args.getString(mActivity.getResources().getString(R.string.db_table_payment_type));
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_save:
             /*   View view2 = mViewPager.findViewWithTag(mActivity.getResources().getString(R.string.view_tag) + mViewPager.getCurrentItem());
                TextView txtStage = (TextView) view2.findViewById(R.id.txt_stage);
                stageID = txtStage.getText().toString();
                Log.d(TAG, txtStage.getText().toString());
                fragmentPosition = mViewPager.getCurrentItem();
                if(stageID != pagerItemsArray2[0]) {
                    if (checkQuotationExists()) {
                        saleStatus = 1;
                        updateStage();
                }else{

                    }
                }else{
                    updateStage();
                }*/
                if(txtCost.length() > 0) {
                    updateStage();
                }else{
                    Toast.makeText(mActivity,mActivity.getResources().getString(R.string.edit_value),Toast.LENGTH_LONG).show();
                }
                break;
            /*case R.id.btn_won:
                saleWon = 1;
                saleStatus = 1;
                updateStage();
                break;
            case R.id.btn_lost:
                saleStatus = 2;
                updateStage();
                break;*/
            case R.id.btn_reorder:
               // createQuotation(saleID);
                break;
            case R.id.btn_create_quote:
                launchFragment();
                break;
            /*case R.id.img_next:
                mViewPager.setCurrentItem(currentPosition + 1);
                break;
            case R.id.img_previous:
                mViewPager.setCurrentItem(currentPosition -1);
                break;*/
            case R.id.fab_button:
                Bundle args3 = new Bundle();
                args3.putString(mActivity.getResources().getString(R.string.txt_advisory),mActivity.getResources().getString(R.string.txt_advisory_delete) + " " +arrayMainItems[3]);
                newFragment6.setArguments(args3);
                newFragment6.setTargetFragment(DealFragment.this, MainModel.DIALOG_FRAGMENT_DELETE_DIALOG);
                newFragment6.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
               /* Bundle args = new Bundle();
                args.putInt(mActivity.getResources().getString(R.string.db_table_id), saleID);
                args.putString(mActivity.getResources().getString(R.string.db_table_sale_foreign_key), foreignKey);
                args.putString(mActivity.getResources().getString(R.string.db_table_sale_model), model);
                args.putString(mActivity.getResources().getString(R.string.db_table_sale_stage),stageID);
                args.putString(mActivity.getResources().getString(R.string.db_table_sale_name), saleName);
                args.putInt(mActivity.getResources().getString(R.string.db_table_sale_status), saleStatus);
                args.putString(mActivity.getResources().getString(R.string.db_table_con_name), contact);
                args.putString(mActivity.getResources().getString(R.string.db_table_org_name), organisation);
                args.putDouble(mActivity.getResources().getString(R.string.db_table_sale_cost), cost);
                args.putString(mActivity.getResources().getString(R.string.db_table_sale_description), strDescription);
                args.putString(mActivity.getResources().getString(R.string.db_table_sale_slug_id), slugID);
                Log.d(TAG,"FragmentPosition = "+ fragmentPosition);
                args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
                args.putInt(mActivity.getResources().getString(R.string.bundle_position),position);
                args.putString(mActivity.getResources().getString(R.string.db_table_sale_update), date);
                args.putInt(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
                newDealFragment = new NewDealFragment();
                newDealFragment.setArguments(args);
                newDealFragment.setTargetFragment(this, MainModel.PAGER_FRAGMENT);
                mainActivity.addFragment(newDealFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) newDealFragment).getClass().getName());*/
                //mActivity.getSupportFragmentManager().popBackStackImmediate();
                break;
            default:

                break;
        }
    }

    //

    /**
     * Launches new StagesStateFragment and StagesStateFragment2
     */
    public void returnToFragment(){
        Log.d(TAG,"Fragment position ===#####====" +position+"........."+fragmentPosition);
        Bundle args = new Bundle();
        args.putInt(mActivity.getResources().getString(R.string.bundle_position), position);
      /*  if(position == 2) {
            args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
        }else{*/
            args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
     //   }
        Log.d(TAG, " Stage ID " + String.valueOf(stageID));
        mActivity.getSupportFragmentManager().popBackStackImmediate();

       // mCallBack2.addSelectedItem(args,position,2);
        Intent intent = new Intent();
        intent.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
        mActivity.setResult(Activity.RESULT_OK, intent);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);


    }


    /**
     * Launch addQuotationsFragment
     */
     public void launchFragment(){
         //mActivity.getSupportFragmentManager().popBackStackImmediate();
         addQuotationFragment = new AddQuotationFragment();
         Log.d(TAG, "................................" + String.valueOf(slugID));
         Bundle args = new Bundle();
         args.putString(mActivity.getResources().getString(R.string.db_table_qu_sale_id), slugID);
         args.putInt(mActivity.getResources().getString(R.string.db_table_id), saleID);
         args.putString(mActivity.getResources().getString(R.string.db_table_qu_name), saleName + " Quotation");
         args.putString(mActivity.getResources().getString(R.string.db_table_con_name), contact);
         args.putString(mActivity.getResources().getString(R.string.db_table_org_name), organisation);
         args.putString(mActivity.getResources().getString(R.string.db_table_sale_description),strDescription);
         args.putDouble(mActivity.getResources().getString(R.string.db_table_sale_cost), cost);

         addQuotationFragment.setArguments(args);
         addQuotationFragment.setTargetFragment(this, MainModel.QUOTATION_FRAGMENT);
         mainActivity.addFragment(addQuotationFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(),((Object) addQuotationFragment).getClass().getName());
         saleWon = 0;
     }


    /**
     * update current sale being viewed
     */
    public void updateStage(){
        strDescription = editNotes.getText().toString();
       // cost = Double.parseDouble(txtCost.getText().toString());
        int updateUri;
        syncDone = 1;
        coordinates = getCoordinates.getCoordinates();
        ContentValues initialValues = new ContentValues();
        Log.d(TAG," Stage ID won " +String.valueOf(stageID));
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_stage), stageID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_status),saleStatus );
        Log.d(TAG,"Sale status = "+ saleStatus);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_cost),cost);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_payment_type),payment);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_description),strDescription);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_foreign_key),foreignKey);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_model),model);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done),syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_longitude), 0.0);
        }
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_slug_id), slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_name),saleName );
        Uri data_id2 = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "9");
        mActivity.getContentResolver().update(data_id2, initialValues, null, new String[]{String.valueOf(saleID),mActivity.getResources().getString(R.string.flag_sync)});
        if(saleWon == 0) {
            returnToFragment();
        }else if(saleWon == 1) {
            launchFragment();
        }
    }

    /**
     *
     * Checks if quotation exists in the DB
     *
     * @return boolean true if Quotation exists
     */
    public boolean checkQuotationExists(){
        Log.d(TAG, "check quotation exists "+slugID);
            Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"32");
            Cursor c = mActivity.getContentResolver().query(data_id, null, null,new String[] {String.valueOf(slugID)}, null);
            if(c.getCount() != 0 && c != null) {
                saleStatus = 0;
               // mCallBack.moveToPager(fragmentPosition,position);
                //updateStage();
                return true;
            }else{
                Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_quotation),Toast.LENGTH_LONG).show();
                return false;
            }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"43");
        return new CursorLoader(getActivity(),data, null, null,
                new String[] {slugID}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        c = data;
        Log.d(TAG, "--------" + String.valueOf(c.getCount()));
        if(c != null || c.getCount() > 0 ){
            c = data;
            mAdapter.swapCursor(c);
            lstActivities.setVisibility(View.VISIBLE);
            lstActivities.setAdapter(mAdapter);
        }
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void launchFragment(int position, Bundle args, int view) {
        if (view == 0) {
            args.putString(mActivity.getResources().getString(R.string.db_table_sale_name), saleName);
           // args.putString(mActivity.getResources().getString(R.string.db_table_act_foreign_key),slugID);
            //args.putString(mActivity.getResources().getString(R.string.db_table_act_model),mActivity.getResources().getString(R.string.db_table_model_3));
            //args.putString(mActivity.getResources().getString(R.string.db_table_con_name),contact);
            //args.putString(mActivity.getResources().getString(R.string.db_table_org_name), organisation);
            args.putInt(mActivity.getResources().getString(R.string.db_table_sales_id), saleID);
            args.putString(mActivity.getResources().getString(R.string.db_table_sale_foreign_key), foreignKey);
            args.putString(mActivity.getResources().getString(R.string.db_table_sale_model), model);
            args.putString(mActivity.getResources().getString(R.string.db_table_sale_stage),stageID);
            args.putString(mActivity.getResources().getString(R.string.db_table_sale_name), saleName);
            args.putInt(mActivity.getResources().getString(R.string.db_table_sale_status), saleStatus);
            args.putDouble(mActivity.getResources().getString(R.string.db_table_sale_cost), cost);
            args.putString(mActivity.getResources().getString(R.string.db_table_sale_description), strDescription);
            args.putString(mActivity.getResources().getString(R.string.db_table_sale_slug_id), slugID);
            args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
            args.putInt(mActivity.getResources().getString(R.string.bundle_position),position);
            args.putInt(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
            addActivitiesFragment = new AddActivitiesFragment();
            addActivitiesFragment.setArguments(args);
            addActivitiesFragment.setTargetFragment(DealFragment.this, MainModel.PAGER_FRAGMENT);
            mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
        }else if(view == 1){
            if(c.moveToPosition(position)){
                int id = c.getInt(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_id)));
                //activityDone = 1;
                //updateActivity(id);
            }
            getLoaderManager().restartLoader(LOADER, null, this);
        }

    }

    @Override
    public void onRefresh() {
        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
    }
}
