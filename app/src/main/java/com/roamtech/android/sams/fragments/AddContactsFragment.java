package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.dialog_fragments.AddNewOrganisationDialogFragment;
import com.roamtech.android.sams.dialog_fragments.DeleteDialog;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.interfaces.CheckProvider;
import com.roamtech.android.sams.interfaces.GoToPager;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.interfaces.SendJsonData;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddContactsFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = "AddContactsFragment";
    EditText editName,editEmail,editPhoneNum,editAddress,editWebsite,editSource;
    public static final int DIALOG_FRAGMENT_DELETE_DIALOG = 6;
    int contactID;
    String name,email,phoneNum,address,website,source;
    TextView txtOrganisation;
    Button btnAddContact,btnEditContact,btnProspects;
    Spinner spnSources;
    MainActivity mainActivity;
    ImageView ivAdd;
    AddNewOrganisationDialogFragment newFragment2;
    public static final int DIALOG_FRAGMENT_ADD_ORGANISATION = 4;
    String organisationID;
    String organisation_name;
    int contactsDone = 1;
    LinearLayout lytChooseOrganisation;
    DeleteDialog newFragment6;
    String [] arrayMainItems;
    AppCompatActivity mActivity;
    int position;
    int syncDone = 1;
    int status = 0;
    String slugID;
    String model = "organisations";
    SendJsonData mCallBack3;
    CheckProvider mCallBack4;
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    GetUsername getUsername;
    GoToPager mCallBack;
    boolean detailsEntered;
    int fragmentPosition;
    boolean othersSelected = false;
    String [] spnSourcesArray;
    ImageButton imgClose;
    NewDealFragment newDealFragment;
    AddActivitiesFragment addActivitiesFragment;
    CommentsFragment commentsFragment;

    public AddContactsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack = (GoToPager) activity;
            mCallBack3 = (SendJsonData) activity;
            mCallBack4 = (CheckProvider) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        commonHash = new CommonHash(mActivity);
        mainActivity = new MainActivity();
        newFragment2 = new AddNewOrganisationDialogFragment();
        newFragment6 = new DeleteDialog();
        newDealFragment = new NewDealFragment();
        arrayMainItems = mActivity.getResources().getStringArray(R.array.main_activity_items);
        spnSourcesArray = mActivity.getResources().getStringArray(R.array.spn_sources);
        Bundle args = getArguments();
        if(args != null){
            contactID = args.getInt(mActivity.getResources().getString(R.string.db_table_id));
            name = args.getString(mActivity.getResources().getString(R.string.db_table_con_name));
            email = args.getString(mActivity.getResources().getString(R.string.db_table_con_email));
            phoneNum = args.getString(mActivity.getResources().getString(R.string.db_table_con_phone));
            address = args.getString(mActivity.getResources().getString(R.string.db_table_con_address));
            website = args.getString(mActivity.getResources().getString(R.string.db_table_con_website));
            source = args.getString(mActivity.getResources().getString(R.string.db_table_con_source));
            organisation_name = args.getString((mActivity.getResources().getString(R.string.db_table_org_name)));
            organisationID = args.getString(mActivity.getResources().getString(R.string.db_table_con_foreign_key));
            model = args.getString(mActivity.getResources().getString(R.string.db_table_con_model));
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            status = args.getInt(mActivity.getResources().getString(R.string.db_table_con_status));
            syncDone = args.getInt(mActivity.getResources().getString(R.string.db_table_sync_done));
            slugID = args.getString(mActivity.getResources().getString(R.string.db_table_con_slug_id));
            fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
            Log.d(TAG,"fragment position "+String.valueOf(fragmentPosition) +" position "+String.valueOf(position));
        }
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        getUsername = new GetUsername(mActivity);
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_contacts, container, false);
        editName = (EditText) rootView.findViewById(R.id.edit_contact_name);
        if(name != null){
            editName.setText(name);
        }
        editEmail = (EditText) rootView.findViewById(R.id.edit_email);
        if(email != null){
            editEmail.setText(email);
        }
        editPhoneNum = (EditText) rootView.findViewById(R.id.edit_phone_num);
        if(phoneNum != null){
            editPhoneNum.setText(phoneNum);
        }
        editAddress = (EditText) rootView.findViewById(R.id.edit_address);
        if(address != null){
            editAddress.setText(address);
        }
        editWebsite = (EditText) rootView.findViewById(R.id.edit_website);
        if(website != null){
            editWebsite.setText(website);
        }
        editSource = (EditText) rootView.findViewById(R.id.edit_source);
        /*if(source != null){
            for(int i = 0; i < spnSourcesArray.length;i++){
                if(spnSourcesArray[i] == source){

                }else{
                    editSource.setText(source);

                }
            }
        }*/
        txtOrganisation = (TextView) rootView.findViewById(R.id.txt_organisation);
        if(organisation_name != null && !organisation_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
            txtOrganisation.setText(organisation_name);
        }
        spnSources = (Spinner) rootView.findViewById(R.id.spn_sources);
        spnSources.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                source = spnSourcesArray[position];
                if (position == 5) {
                    editSource.setEnabled(true);
                    othersSelected = true;
                } else {
                    editSource.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        if(source != null) {
            Boolean sourceSet = false;
            for (int k = 0; k < spnSourcesArray.length; k++) {
                if (spnSourcesArray[k].equals(source) & !source.equals(spnSourcesArray[5])) {
                    spnSources.setSelection(k);
                    sourceSet = true;
                }
            }
            if(sourceSet == false) {
                editSource.setEnabled(true);
                editSource.setText(source);
                spnSources.setSelection(5);
            }

        }

        btnAddContact = (Button) rootView.findViewById(R.id.btn_add_contact);
        btnAddContact.setOnClickListener(this);
        btnEditContact = (Button) rootView.findViewById(R.id.btn_edit_contact);
        btnEditContact.setOnClickListener(this);
        btnProspects = (Button) rootView.findViewById(R.id.btn_prospects);
        btnProspects.setOnClickListener(this);
        if(contactID != 0){
            btnAddContact.setVisibility(View.GONE);
            btnEditContact.setVisibility(View.VISIBLE);
            btnProspects.setVisibility(View.VISIBLE);
        }
        ivAdd = (ImageView)rootView.findViewById(R.id.img_add);
        ivAdd.setOnClickListener(this);
        imgClose = (ImageButton) rootView.findViewById(R.id.fab_button);
        imgClose.setOnClickListener(this);
        lytChooseOrganisation = (LinearLayout)rootView.findViewById(R.id.lyt_choose_organisation);
        lytChooseOrganisation.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        if(contactID != 0) {
            menu.findItem(R.id.action_flag).setVisible(true);
            menu.findItem(R.id.action_comments).setVisible(true);
        }
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                Bundle args2 = new Bundle();
                args2.putString(mActivity.getResources().getString(R.string.db_table_org_name),organisation_name);
                args2.putString(mActivity.getResources().getString(R.string.db_table_con_name),name);
                args2.putString(mActivity.getResources().getString(R.string.db_table_act_model),mActivity.getResources().getString(R.string.db_table_model_1));
                args2.putString(mActivity.getResources().getString(R.string.db_table_act_foreign_key),slugID);
                addActivitiesFragment.setArguments(args2);
                addActivitiesFragment.setTargetFragment(AddContactsFragment.this, MainModel.ADD_ACTIVITIES);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                Bundle args = new Bundle();
                args.putString(mActivity.getResources().getString(R.string.db_table_con_name),name);
                args.putString(mActivity.getResources().getString(R.string.db_table_act_model),mActivity.getResources().getString(R.string.db_table_model_1));
                args.putString(mActivity.getResources().getString(R.string.db_table_act_foreign_key),slugID);
                commentsFragment.setArguments(args);
                commentsFragment.setTargetFragment(AddContactsFragment.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean fillData(){
        if(editName.length() < 1){
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_name),Toast.LENGTH_LONG).show();
            detailsEntered = false;
        }else if(editPhoneNum.length() < 1){
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_no_contact),Toast.LENGTH_LONG).show();
            detailsEntered = false;
        }else if(editPhoneNum.getText().length() > 0 && isValidPhoneNum(editPhoneNum.getText()) == false){
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_valid_phone), Toast.LENGTH_LONG).show();
            detailsEntered = false;
        }else if(editEmail.getText().length() > 0 && isValidEmail(editEmail.getText()) == false) {
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_valid_email), Toast.LENGTH_LONG).show();
            detailsEntered = false;
        }else{
            detailsEntered = true;
        }
        return detailsEntered;
    }

    public void sendData(){
        Bundle args = new Bundle();
        if(slugID == null) {
            contactID = (int) insertContacts();
            try {
                slugID = commonHash.getHash(String.valueOf(contactID));
                Log.d(TAG, "Common Hash is  " + slugID);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            insertContacts();
        }else{
            insertContacts();
        }
        Intent intent = new Intent();
        mActivity.setResult(Activity.RESULT_OK, intent);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        mActivity.getSupportFragmentManager().popBackStackImmediate();
       /* args.putInt(mActivity.getResources().getString(R.string.bundle_position),position);
        args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position),fragmentPosition);
        mCallBack2.addSelectedItem(args,position,2);*/
    }

    public void clearData(){

    }

    public long insertContacts(){
        int playListId = 0;
        if(othersSelected){
            source = editSource.getText().toString();
        }
        coordinates = getCoordinates.getCoordinates();
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_name), editName.getText().toString());
        Log.d(TAG," ''''----------......... "+editName.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_phone), editPhoneNum.getText().toString());
        Log.d(TAG," ''''----------......... "+editPhoneNum.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_email), editEmail.getText().toString());
        Log.d(TAG," ''''----------......... "+editEmail.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_website), editWebsite.getText().toString());
        Log.d(TAG," ''''----------......... "+editWebsite.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_source), source);
        Log.d(TAG," ''''----------......... "+source);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_address), editAddress.getText().toString());
        Log.d(TAG," ''''----------......... "+editAddress.getText().toString());
        if(organisationID == null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_foreign_key), String.valueOf(0));
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_foreign_key), organisationID);
        }
        Log.d(TAG," ''''----------......... "+organisationID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_model), model);
        Log.d(TAG," ''''----------......... "+model);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_status), status);
        Log.d(TAG," ''''----------......... "+contactsDone);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_slug_id), slugID);
        Log.d(TAG," ''''----------......... "+slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), 1);
        Log.d(TAG," ''''----------......... "+syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_latitude), 0.0);
        }
        Log.d(TAG," ''''----------......... "+coordinates[0]);
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_longitude), 0.0);
        }
        Log.d(TAG," ''''----------......... "+coordinates[1]);
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());
        if(contactID == 0) {
            initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "0");
            Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
            playListId = (int)ContentUris.parseId(insertUri);
        }else{
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "11");
            playListId = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(contactID),mActivity.getResources().getString(R.string.flag_sync)});
        }
        Log.d(TAG,"-------"+ String.valueOf(playListId));
        return playListId;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == DIALOG_FRAGMENT_ADD_ORGANISATION){
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    String orgID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id));
                    organisationID = orgID;
                    Log.d(TAG," 88888888888888888m "+String.valueOf(orgID));
                    Log.d(TAG,TAG + String.valueOf(organisationID));
                    organisation_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_name));
                    txtOrganisation.setText(organisation_name);
                }
            }
        }else if(requestCode == DIALOG_FRAGMENT_DELETE_DIALOG ) {
            if (resultCode == Activity.RESULT_OK) {
                    status = 2;
                    insertContacts();
                    Intent intent = new Intent();
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    getFragmentManager().popBackStackImmediate();
                } else {

                }
        }else if(requestCode == MainModel.PAGER_FRAGMENT){
            if(resultCode == Activity.RESULT_OK){
                if(data.getExtras() != null){
                    Log.d(TAG, "AddContactsFragment 1");
                    Intent intent = new Intent();
                    intent.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), 1);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_add_contact:
                if(fillData()) {
                    sendData();
                }
                break;
            case R.id.btn_edit_contact:
                if(fillData()) {
                    sendData();
                }
                break;
            case R.id.btn_prospects:
                if(fillData()) {
                    sendData();
                    Bundle args2 = new Bundle();
                    args2.putString(mActivity.getResources().getString(R.string.db_table_con_name),editName.getText().toString());
                    args2.putString(mActivity.getResources().getString(R.string.db_table_org_name),organisation_name);
                    args2.putString(mActivity.getResources().getString(R.string.db_table_sale_model),mActivity.getResources().getString(R.string.db_table_model_1) );
                    args2.putString(mActivity.getResources().getString(R.string.db_table_sale_foreign_key),slugID);
                    if(organisation_name != null & !organisation_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))) {
                        args2.putString(mActivity.getResources().getString(R.string.db_table_sale_name), organisation_name + "_" + "Sale");
                    }else{
                        args2.putString(mActivity.getResources().getString(R.string.db_table_sale_name), editName.getText().toString() + "_" + "Sale");
                    }
                    args2.putString(mActivity.getResources().getString(R.string.db_table_sale_stage),mActivity.getResources().getString(R.string.prospects));
                    args2.putString(mActivity.getResources().getString(R.string.db_table_contact_id),String.valueOf(contactID));
                    newDealFragment.setArguments(args2);
                    newDealFragment.setTargetFragment(this,MainModel.PAGER_FRAGMENT);
                    mainActivity.addFragment(newDealFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) newDealFragment).getClass().getName());
                }
                break;
            case R.id.img_add:
                newFragment2.setTargetFragment(AddContactsFragment.this, DIALOG_FRAGMENT_ADD_ORGANISATION);
                newFragment2.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.lyt_choose_organisation:
                newFragment2.setTargetFragment(AddContactsFragment.this, DIALOG_FRAGMENT_ADD_ORGANISATION);
                newFragment2.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.fab_button:
                if(contactID == 0){
                    getFragmentManager().popBackStackImmediate();
                }else{
                    Bundle args = new Bundle();
                    args.putString(mActivity.getResources().getString(R.string.txt_advisory),mActivity.getResources().getString(R.string.txt_advisory_delete) + " " +arrayMainItems[6]);
                    newFragment6.setArguments(args);
                    newFragment6.setTargetFragment(AddContactsFragment.this, DIALOG_FRAGMENT_DELETE_DIALOG);
                    newFragment6.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                }
                break;
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    //Check validity of the phone number
    public final static boolean isValidPhoneNum(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.PHONE.matcher(target).matches();
    }

    public void clearAll(){
        editName.setText(null);
        editEmail.setText(null);
        editPhoneNum.setText(null);
        editAddress.setText(null);
        editWebsite.setText(null);
        editSource.setText(null);
        txtOrganisation.setText(null);
    }

}
