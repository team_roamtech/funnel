package com.roamtech.android.sams.interfaces;

import com.roamtech.android.sams.models.LoginItems;
import com.unboundid.ldap.sdk.LDAPException;

/**
 *
 * Monitors and returns result of an asyntask thread running in the background authenticating whether a user exists in the server
 *
 * Created by dennis on 4/24/15.
 * @author Dennis Mwangi Karuri
 *
 */
public interface ReturnAuthenticationResult {

    /**
     * Defines what should happen before the background task is started
     */
    void onStartTask();

    /**
     *
     * Defines what should happen once the background task is completed and also returns to the foreground thread the result
     * of the Background thread running
     *
     * @param loginItems returns LoginItems containing details of the Authenitcated User
     * @throws LDAPException
     */
    void onReturnResult(LoginItems loginItems) throws LDAPException;
}
