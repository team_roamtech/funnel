package com.roamtech.android.sams.receivers;

import android.accounts.Account;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.services.AlarmService;
import com.roamtech.android.sams.services.FunnelAccountService;
import com.roamtech.android.sams.utils.SyncUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * An alarm to do an activity when the set time is reached
 * Cancel an alarm and also postpones it
 *
 * Created by dennis on 5/11/15.
 * @author Dennis Mwangi Karuri
 * @see WakefulBroadcastReceiver
 */
public class AlarmReceiver extends WakefulBroadcastReceiver {
    public static final String TAG = "AlarmReceiver";
    // The app's AlarmManager, which provides access to the system alarm services.
    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent,syncIntent;
    ArrayList<Integer> dateID = new ArrayList<Integer>();
    int sdk = android.os.Build.VERSION.SDK_INT;
    Account account;
    @Override
    public void onReceive(Context context, Intent intent) {
            String activityScheduled = intent.getStringExtra(context.getResources().getString(R.string.db_table_act_schedule));
        if(activityScheduled != null) {
            int notificationID = intent.getIntExtra(context.getResources().getString(R.string.extra_activity_id), 1);
            Intent service = new Intent(context, AlarmService.class);
            service.putExtra(context.getResources().getString(R.string.db_table_act_schedule), activityScheduled);
            service.putExtra(context.getResources().getString(R.string.extra_activity_id), notificationID);
            // Start the service, keeping the device awake while it is launching.
            startWakefulService(context, service);
        }else{
            account = FunnelAccountService.GetAccount();
            Bundle bundle = new Bundle();
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true); // Performing a sync no matter if it's off
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true); // Performing a sync no matter if it's off
            context.getContentResolver().requestSync(account, FunnelProvider.AUTHORITY, bundle);
            SyncUtils.TriggerRefresh();
        }
    }

    /**
     *
     * Sets a repeating alarm that runs once a day at approximately 8:30 a.m. When the
     * alarm fires, the app broadcasts an Intent to this WakefulBroadcastReceiver.
     *
     * @param context
     * @param date Arraylist with Integers of the date and time for the alarm to be set
     * @param dateID Unique ID for each alarm
     * @param activityType type of activity to be carried out
     */
    public void setAlarm(Context context,ArrayList<Integer> date,int dateID,String activityType) {
        this.dateID = date;
        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(context.getResources().getString(R.string.extra_activity_id), dateID);
        intent.putExtra(context.getResources().getString(R.string.db_table_act_schedule),activityType);
        alarmIntent = PendingIntent.getBroadcast(context, dateID, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        // Set the alarm's trigger time
        calendar.set(Calendar.YEAR,date.get(0));
        Log.d(TAG, String.valueOf(date.get(0)));
        calendar.set(Calendar.MONTH,date.get(1));
        Log.d(TAG, String.valueOf(date.get(1)));
        calendar.set(Calendar.DAY_OF_MONTH,date.get(2));
        Log.d(TAG, String.valueOf(date.get(2)));
        calendar.set(Calendar.HOUR_OF_DAY, date.get(3));
        Log.d(TAG, String.valueOf(date.get(3)));
        calendar.set(Calendar.MINUTE, date.get(4));
        Log.d(TAG, String.valueOf(date.get(4)));

        /*
         * If you don't have precise time requirements, use an inexact repeating alarm
         * the minimize the drain on the device battery.
         *
         * The call below specifies the alarm type, the trigger time, the interval at
         * which the alarm is fired, and the alarm's associated PendingIntent.
         * It uses the alarm type RTC_WAKEUP ("Real Time Clock" wake up), which wakes up
         * the device and triggers the alarm according to the time of the device's clock.
         *
         * Alternatively, you can use the alarm type ELAPSED_REALTIME_WAKEUP to trigger
         * an alarm based on how much time has elapsed since the device was booted. This
         * is the preferred choice if your alarm is based on elapsed time--for example, if
         * you simply want your alarm to fire every 60 minutes. You only need to use
         * RTC_WAKEUP if you want your alarm to fire at a particular date/time. Remember
         * that clock-based time may not translate well to other locales, and that your
         * app's behavior could be affected by the user changing the device's time setting.
         *
         * Here are some examples of ELAPSED_REALTIME_WAKEUP:
         *
         * // Wake up the device to fire a one-time alarm in one minute.
         * alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
         *         SystemClock.elapsedRealtime() +
         *         60*1000, alarmIntent);
         *
         * // Wake up the device to fire the alarm in 30 minutes, and every 30 minutes
         * // after that.
         * alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
         *         AlarmManager.INTERVAL_HALF_HOUR,
         *         AlarmManager.INTERVAL_HALF_HOUR, alarmIntent);
         */

        // Set the alarm to fire according to the device's clock

        if(sdk < Build.VERSION_CODES.KITKAT){
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(), alarmIntent);
        }else{
            alarmMgr.set(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(), alarmIntent);
        }



        // Enable {@code SampleBootReceiver} to automatically restart the alarm when the
        // device is rebooted.
        ComponentName receiver = new ComponentName(context, SampleBootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    /**
     *
     * Postpones the alarm to an hour later
     *
     * @param context
     * @param calendar The date and time for the alarm to be reset
     * @param dateID Unique ID for each alarm
     * @param activityType Type of activity to be carried out
     */
    public void setPostponeAlarm(Context context,Calendar calendar,int dateID,String activityType) {
        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(context.getResources().getString(R.string.extra_activity_id), dateID);
        intent.putExtra(context.getResources().getString(R.string.db_table_act_schedule),activityType);
        alarmIntent = PendingIntent.getBroadcast(context, dateID, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        /*
         * If you don't have precise time requirements, use an inexact repeating alarm
         * the minimize the drain on the device battery.
         *
         * The call below specifies the alarm type, the trigger time, the interval at
         * which the alarm is fired, and the alarm's associated PendingIntent.
         * It uses the alarm type RTC_WAKEUP ("Real Time Clock" wake up), which wakes up
         * the device and triggers the alarm according to the time of the device's clock.
         *
         * Alternatively, you can use the alarm type ELAPSED_REALTIME_WAKEUP to trigger
         * an alarm based on how much time has elapsed since the device was booted. This
         * is the preferred choice if your alarm is based on elapsed time--for example, if
         * you simply want your alarm to fire every 60 minutes. You only need to use
         * RTC_WAKEUP if you want your alarm to fire at a particular date/time. Remember
         * that clock-based time may not translate well to other locales, and that your
         * app's behavior could be affected by the user changing the device's time setting.
         *
         * Here are some examples of ELAPSED_REALTIME_WAKEUP:
         *
         * // Wake up the device to fire a one-time alarm in one minute.
         * alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
         *         SystemClock.elapsedRealtime() +
         *         60*1000, alarmIntent);
         *
         * // Wake up the device to fire the alarm in 30 minutes, and every 30 minutes
         * // after that.
         * alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
         *         AlarmManager.INTERVAL_HALF_HOUR,
         *         AlarmManager.INTERVAL_HALF_HOUR, alarmIntent);
         */

        // Set the alarm to fire at approximately 8:30 a.m., according to the device's
        // clock, and to repeat once a day.

        if(sdk < Build.VERSION_CODES.KITKAT){
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(), alarmIntent);
        }else{
            alarmMgr.set(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(), alarmIntent);
        }



        // Enable {@code SampleBootReceiver} to automatically restart the alarm when the
        // device is rebooted.
        ComponentName receiver = new ComponentName(context, SampleBootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
    }

    /**
     *
     * Sets a repeating alarm that runs once every hour When the
     * alarm fires, the app broadcasts an Intent to this WakefulBroadcastReceiver.
     *
     * @param context
     */
    public void setSyncAlarm(Context context) {
        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        Log.d(TAG,"TArget time 4.00 pm");

        syncIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        //calendar.set(Calendar.HOUR_OF_DAY, 9);
        //calendar.set(Calendar.MINUTE, 00);

        /*
         * If you don't have precise time requirements, use an inexact repeating alarm
         * the minimize the drain on the device battery.
         *
         * The call below specifies the alarm type, the trigger time, the interval at
         * which the alarm is fired, and the alarm's associated PendingIntent.
         * It uses the alarm type RTC_WAKEUP ("Real Time Clock" wake up), which wakes up
         * the device and triggers the alarm according to the time of the device's clock.
         *
         * Alternatively, you can use the alarm type ELAPSED_REALTIME_WAKEUP to trigger
         * an alarm based on how much time has elapsed since the device was booted. This
         * is the preferred choice if your alarm is based on elapsed time--for example, if
         * you simply want your alarm to fire every 60 minutes. You only need to use
         * RTC_WAKEUP if you want your alarm to fire at a particular date/time. Remember
         * that clock-based time may not translate well to other locales, and that your
         * app's behavior could be affected by the user changing the device's time setting.
         *
         * Here are some examples of ELAPSED_REALTIME_WAKEUP:
         *
         * // Wake up the device to fire a one-time alarm in one minute.
         * alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
         *         SystemClock.elapsedRealtime() +
         *         60*1000, alarmIntent);
         *
         * // Wake up the device to fire the alarm in 30 minutes, and every 30 minutes
         * // after that.
         * alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
         *         AlarmManager.INTERVAL_HALF_HOUR,
         *         AlarmManager.INTERVAL_HALF_HOUR, alarmIntent);
         */

        // Set the alarm to fire , according to the device's
        // clock, and to repeat once every hour

        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_HOUR, syncIntent);

        ComponentName receiver = new ComponentName(context, BootSyncReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);


        // Enable {@code SampleBootReceiver} to automatically restart the alarm when the
        // device is rebooted.

    }

    /**
     * Cancels the alarm.
     * @param context
     * @param date
     * @param dateID Unique ID for each alarm to be cancelled
     * @param activityType Type of activity to be carried out
     */

    public void cancelAlarm(Context context,ArrayList<Integer> date,int dateID,String activityType) {
        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        intent.putExtra(context.getResources().getString(R.string.extra_activity_id), dateID);
        intent.putExtra(context.getResources().getString(R.string.db_table_act_schedule),activityType);
        alarmIntent = PendingIntent.getBroadcast(context, dateID, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // If the alarm has been set, cancel it.
        if (alarmMgr!= null) {
            alarmMgr.cancel(alarmIntent);
        }

        // Disable {@code SampleBootReceiver} so that it doesn't automatically restart the
        // alarm when the device is rebooted.
        ComponentName receiver = new ComponentName(context, SampleBootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }

    /**
     *
     * Converts String to date
     *
     * @param dateString String to be converted to date
     * @return Converted date
     */
    private Date ConvertToDate(String dateString) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return convertedDate;
    }
}
