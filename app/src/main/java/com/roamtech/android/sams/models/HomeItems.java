package com.roamtech.android.sams.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 *
 * Class that stores HomeItems
 *
 * Created by Karuri on 2/22/2015.
 * @author Dennis Mwangi Karuri
 * @see Parcelable
 */
public class HomeItems implements Parcelable{
    public String number;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String title;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
