package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.DealAdapter;
import com.roamtech.android.sams.interfaces.GoToPager;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 *
 * Lists all the sales in the prospects Stage
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class StagesFragment extends Fragment implements View.OnClickListener,LoaderManager.LoaderCallbacks<Cursor>,LaunchFragment {
    public static final String TAG = "StagesFragment";
    AppCompatActivity mActivity;
    String title,title2;
    TextView txtTitle;
    MainActivity mainActivity;
    NewDealFragment newDealFragment;
    DealFragment dealFragment;
    ListView lstDeals;
    Cursor c;
    View infoView;
    String stageName;
    int saleID;
    int position2;
    public static final int LOADER = 2010;
    DealAdapter mAdapter;
    int vTag = 0;
    public static final int PAGER_FRAGMENT = 2;
    ProgressBar mProgresBar;
    String saleName,contactName,organisation;
    String strDescription;
    int cost,organisationContact;
    int fragmentPosition;
    int status,syncDone;
    int position,parentID;
    int orgID,contactID;
    GoToPager mCallBack;
    String strPayment;
    String foreignKey,slugID,model;
    String date;
    boolean isVisible = false;
    String [] pagerItemsArray;

    /**
     *
     * Constructor
     *
     */
    public StagesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack = (GoToPager) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mainActivity = new MainActivity();
        Log.d(TAG,"on create");
        pagerItemsArray = mActivity.getResources().getStringArray(R.array.pager_title_items);
        newDealFragment = new NewDealFragment();
        dealFragment = new DealFragment();
        if(savedInstanceState != null){
            fragmentPosition = savedInstanceState.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
            position = savedInstanceState.getInt(mActivity.getResources().getString(R.string.bundle_position));
            Log.d(TAG,"Fragment position on create " + fragmentPosition);
        }
        Bundle args = getArguments();
        if(args != null){
            position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
            title2 = args.getString(mActivity.getResources().getString(R.string.bundle_title));
        }
        Log.d(TAG,TAG+" "+ " onCreate " + title2);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_stages, container, false);
        lstDeals = (ListView) rootView.findViewById(R.id.lst_deals);
        infoView = (View) rootView.findViewById(R.id.error_container);
        //txtTitle = (TextView) rootView.findViewById(R.id.txt_stage_title);
        mProgresBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        lstDeals.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle args = new Bundle();
                if(c.moveToPosition(position)) {
                    saleID = c.getInt(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_id)));
                    foreignKey = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_foreign_key)));
                    //contactID = c.getInt(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_contact_id)));
                    saleName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_name)));
                    status = c.getInt(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_status)));
                    //contactName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_contact_name)));
                    //organisation = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_organisation_name)));
                    //Log.d(TAG,"9999999999999 "+String.valueOf(c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.query_name_oid)))));
                    cost = c.getInt(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_cost)));

                    Log.d(TAG,"Status is equals to "+String.valueOf(status));
                    stageName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_stage)));
                    strDescription = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_description)));
                    strPayment = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_payment_type)));
                    //date = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_update)));
                    //parentID = c.getInt(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_parent_id)));
                    slugID = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_slug_id)));
                    Log.d(TAG,"Status is equals to "+slugID);
                    model = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_model)));
                    syncDone = c.getInt(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sync_done)));
                    args.putInt(mActivity.getResources().getString(R.string.db_table_id), saleID);
                    args.putString(mActivity.getResources().getString(R.string.db_table_sale_foreign_key), foreignKey);
                    args.putString(mActivity.getResources().getString(R.string.db_table_sale_model),model);
                    args.putString(mActivity.getResources().getString(R.string.db_table_sale_stage),stageName );
                    args.putString(mActivity.getResources().getString(R.string.db_table_sale_name), saleName);
                    args.putInt(mActivity.getResources().getString(R.string.db_table_sale_status), status);
                   // args.putString(mActivity.getResources().getString(R.string.db_table_sale_update),date);
                    args.putString(mActivity.getResources().getString(R.string.db_table_con_name), contactName);
                    //args.putInt(mActivity.getResources().getString(R.string.db_table_sale_parent_id),parentID);
                    args.putString(mActivity.getResources().getString(R.string.db_table_sale_payment_type), strPayment);
                    Log.d(TAG,"******** "+ slugID);
                    args.putString(mActivity.getResources().getString(R.string.db_table_sale_slug_id), slugID);
                    args.putInt(mActivity.getResources().getString(R.string.db_table_sale_cost), cost);
                    args.putString(mActivity.getResources().getString(R.string.db_table_sale_description), strDescription);
                    args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position),fragmentPosition);
                    args.putInt(mActivity.getResources().getString(R.string.bundle_position),position2);
                    Log.d(TAG,"position changed "+ position2);

                    //mCallBack.moveToPager(fragmentPosition);
                    //mCallBack2.addSelectedItem(args,position2,1);
                    dealFragment.setArguments(args);
                    dealFragment.setTargetFragment(StagesFragment.this, MainModel.DEAL_FRAGMENT);
                    mainActivity.addFragment(dealFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) dealFragment).getClass().getName());
                }
            }
        });

        mAdapter = new DealAdapter(getActivity(), null, 0,fragmentPosition,this);
        Log.d(TAG,TAG+" "+ " onCreateView " + title2);
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Log.d(TAG, TAG + " " + " onResume stageName " + title2);
        //txtTitle.setText(title2);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position),fragmentPosition);
        savedInstanceState.putInt(mActivity.getResources().getString(R.string.bundle_position), position);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.d(TAG, TAG + " " + "setUserVisibleHint");
        if (isVisibleToUser) {
            isVisible = isVisibleToUser;
            Bundle args = getArguments();
            if(args != null) {
                fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
                title2 = args.getString(mActivity.getResources().getString(R.string.bundle_title));
                position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
                Log.d(TAG," position setUserVisibleHint " + position2);

              //  mActivity.getSupportActionBar().setTitle(title2);
            }
            mActivity.getSupportActionBar().setTitle(pagerItemsArray[1]);
          //  Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"8");
           // cursor = mActivity.getContentResolver().query(data_id, null, null,new String[] {title.toLowerCase()}, null);
            //cursor.moveToFirst();
            //if(cursor.getCount() != 0 && cursor != null) {fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
         //   if(position == 2){
           //     stageID = fragmentPosition;
            //}else {
                //stageID = fragmentPosition + 1;
            //}
              //  stageID = cursor.getLong(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_id)));
               // Log.d(TAG,TAG+" "+ String.valueOf(stageID));
           // }
            Log.d(TAG, TAG + " " + " setUserVisibleHint title2 "+title2);
            stageName = title2;
            switch(fragmentPosition){
                case 1:
                    mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
                    break;
            }


        }else{

        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, menu);

        //        getActivity().invalidateOptionsMenu();
    }




    @Override
    public void onClick(View v) {
        switch(v.getId()){
            default:

                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.PAGER_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG,TAG +"------------");
                if(data.getExtras() != null) {
                    Log.d(TAG,"MAIN MODEL PAGER FRAGMENT WORKS");

                   // mActivity.getSupportFragmentManager().popBackStackImmediate();
                }
               mCallBack.moveToPager(1,2);
            }
        }else if(requestCode == MainModel.DEAL_FRAGMENT){
            if (resultCode == Activity.RESULT_OK) {
                Log.d(TAG, TAG + "DEAL_FRAGMENT ------------");
                if(data.getExtras() != null) {
                    int fragmentPos = data.getIntExtra(mActivity.getResources().getString(R.string.bundle_fragment_position),2);
                    Intent intent = new Intent();
                    intent.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position),fragmentPos);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    Log.d(TAG," get Target request code "+ getTargetFragment());
                    getParentFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                }
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
    Log.d(TAG,"Stage id is equals to "+String.valueOf(stageName));
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "5");
            return new CursorLoader(getActivity(), data, null, null,
                    new String[]{stageName,String.valueOf(2)}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mProgresBar.setVisibility(View.GONE);
        c = data;
        Log.d(TAG, "On Load Finished --------" + String.valueOf(c.getCount()));
        if(c == null || c.getCount() == 0 ){
            infoView.setVisibility(View.VISIBLE);
            lstDeals.setVisibility(View.GONE);
        }else{
            c = data;
            mAdapter.swapCursor(c);
            infoView.setVisibility(View.GONE);
            lstDeals.setVisibility(View.VISIBLE);
            lstDeals.setAdapter(mAdapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void launchFragment(int position, Bundle bundle, int view) {
        bundle.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
        bundle.putInt(mActivity.getResources().getString(R.string.bundle_position), position2);
        dealFragment.setArguments(bundle);
        dealFragment.setTargetFragment(StagesFragment.this, MainModel.DEAL_FRAGMENT);
        mainActivity.addFragment(dealFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) dealFragment).getClass().getName());
    }
}
