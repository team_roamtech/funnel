package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.roamtech.android.sams.R;

/**
 * Created by Karuri on 2/20/2015.
 */

    public class SpinnerActivitiesAdapter extends ArrayAdapter<String> {
        public static final String TAG = "SpinnerActivitiesAdapter";
        private LayoutInflater vi;
        TypedArray typedArray;
        Context context;
        String [] spinnerItems;


        public SpinnerActivitiesAdapter(Context context, int resource, String[] spinnerItems,TypedArray typedArray) {
            super(context, resource, spinnerItems);
            this.context = context;
            this.spinnerItems = spinnerItems;
            this.typedArray = typedArray;
            vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            // TODO Auto-generated method stub
            return spinnerItems.length;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            // TODO Auto-generated method stub
            View v = vi.inflate(R.layout.spinner_activity_items, null);
            ImageView ivtitle = (ImageView)v.findViewById(R.id.img_activity);
            TextView txtTitle = (TextView)v.findViewById(R.id.txt_activity);
            txtTitle.setText(spinnerItems[position]);
            ImageView imgTitle = (ImageView)v.findViewById(R.id.img_menu_item);
            ivtitle.setImageResource(typedArray.getResourceId(position,-1));
            return v;
        }
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        View v = vi.inflate(R.layout.spinner_activity_items, null);
        ImageView ivtitle = (ImageView)v.findViewById(R.id.img_activity);
        TextView txtTitle = (TextView)v.findViewById(R.id.txt_activity);
        txtTitle.setText(spinnerItems[position]);
        ImageView imgTitle = (ImageView)v.findViewById(R.id.img_menu_item);
        ivtitle.setImageResource(typedArray.getResourceId(position,-1));
        return v;
    }
    }