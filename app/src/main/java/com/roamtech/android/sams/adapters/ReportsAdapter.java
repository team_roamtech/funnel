package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.content.res.Configuration;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.models.ReportItems;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dennis on 5/14/15.
 */
public class ReportsAdapter extends ArrayAdapter<ArrayList<ReportItems>> {
    public static final String TAG = "ReportsAdapter";
    private LayoutInflater vi;
    Context context;
    ArrayList<ReportItems> reportItems = new ArrayList<ReportItems>();
    Boolean isTablet = false;


    public ReportsAdapter(Context context, int resource, List<ArrayList<ReportItems>> objects,ArrayList<ReportItems> reportItems) {
        super(context, resource, objects);
        this.context = context;
        this.reportItems = reportItems;
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Configuration config = context.getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600 && context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isTablet = true;
        }else{
            isTablet = false;
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return reportItems.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        ReportItems reportItem = reportItems.get(position);
        View v = vi.inflate(R.layout.report_item_layout, null);
        TextView txtQuName = (TextView)v.findViewById(R.id.txt_qu_name);
        TextView txtConName = (TextView)v.findViewById(R.id.txt_con_name);
        TextView txtItems = (TextView)v.findViewById(R.id.txt_item);
        if(reportItem.getQuName() != null) {
            txtQuName.setText(reportItem.getQuName());
        }else{
            txtQuName.setVisibility(View.GONE);
        }
        if(reportItem.getConName() != null) {
            txtConName.setText(reportItem.getConName());
        }else{
            txtConName.setVisibility(View.GONE);
        }
        if(reportItem.getItems() != null) {
            txtItems.setText(reportItem.getItems());
        }else{
            txtItems.setVisibility(View.GONE);
        }
        return v;
    }

}
