package com.roamtech.android.sams.interfaces;

/**
 *
 * Passes to the MainActivity the command to Sign out the user from the App
 *
 * Created by dennis on 5/25/15.
 * @author Dennis Mwangi Karuri
 */
public interface CheckSignOut {

    /**
     *
     * Used to pass a command to the MainActivity to Sign out the user
     *
     */
    public void confirmSignOut();
}
