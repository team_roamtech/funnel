/**
 *
 */
package com.roamtech.android.sams;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.roamtech.android.sams.activities.SignInActivity;
import com.roamtech.android.sams.async_tasks.GetTargetsTasks;
import com.roamtech.android.sams.databases.FunnelDB;
import com.roamtech.android.sams.models.MainModel;

/**
 * The Activity displays a Splash screen with the app logo for a set period
 * It checks whether the device has Google Play services before proceeding and stops when Google play services are not found.
 * The Activity also retrieves a user's set targets to be used later in the app
 *
 * @author  @author Dennis Mwangi Karuri
 * @version 1.0
 * @since   2015-01-01
 */

public class SplashActivity extends AppCompatActivity {
    public static final String TAG = "SplashActivity";
    private FunnelDB mDbController;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static int SPLASH_TIME_OUT = 3000;
    private SharedPreferences prefs;
    String username;
    boolean boolUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        prefs = PreferenceManager
                .getDefaultSharedPreferences(this);

            if (checkPlayServices()) {
                Log.d(TAG," Google play services present");
                //check if username has already been saved
                boolUsername = prefs.contains(getString(R.string.bundle_username));
                if(boolUsername){
                    username = prefs.getString(getString(R.string.bundle_username),null);
                    //use username to get the user's set targets
                    if(username != null){
                        new GetTargetsTasks(this,username).execute();
                    }
                }

                if (savedInstanceState != null) {
                    return;
                }else {
                    //Create the database if it doesn't exist and initialize it if it does
                    if (getApplicationContext().getDatabasePath("funnel_db").exists() != true) {
                        mDbController = new FunnelDB(getApplicationContext());
                    }
                    new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer show case app logo
			 */

                        @Override
                        public void run() {
                            // This method will be executed once the timer is over
                            // Start MainActivity if username is present since Signin has already happened once else start SigninActivity
                            if(boolUsername) {
                                if (prefs.getBoolean(MainModel.ACCOUNT_ACTIVATED, false) == false) {
                                    Intent i = new Intent(SplashActivity.this, SignInActivity.class);
                                    startActivity(i);
                                }else{
                                Intent i = new Intent(SplashActivity.this, MainActivity.class);
                                startActivity(i);
                            }
                            }else {
                                Intent i = new Intent(SplashActivity.this, SignInActivity.class);
                                startActivity(i);
                            }
                            // close this activity
                            finish();
                        }
                    }, SPLASH_TIME_OUT);
                }

            }else{

            }
    }

    /**
     *Checks if google play services are available returns true if present false otherwise
     * @return boolean true or false depending on whether google play services are available
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {

                finish();
            }
            return false;
        }
        return true;
    }
}
