package com.roamtech.android.sams.models;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.widget.ListView;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 *
 * Contains Common Tasks Methods and variables used throughout the App
 *
 * Created by dennis on 3/13/15.
 * @author Dennis Mwangi Karuri
 */
public class MainModel {
    public static final int PAGER_FRAGMENT = 2;
    public static final int ADD_ACTIVITIES = 21;
    public static final int QUOTATION_FRAGMENT = 11;
    public static final int ACTIVITY_FRAGMENT = 8;
    public static final int COMMENTS_FRAGMENT = 1604;
    //public static final String SYNC_URL = "http://teamroot.mobi/fapi/";
    //public static final String SYNC_URL = "http://192.168.1.172/fapi/";
    public static final String SYNC_URL = "http://197.248.6.28/fapi/";

    //public static final String SITE_URL2="http://192.168.1.194/leoapi/";
    public static final int DIALOG_FRAGMENT_ADD_CONTACT = 3;
    public static final int DEAL_FRAGMENT = 3;
    public static final int DIALOG_FRAGMENT_ADD_ORGANISATION = 4;
    public static final int DIALOG_FRAGMENT_ADD_SALE = 5;
    public static final int DIALOG_FRAGMENT_DATE = 6;
    public static final int DIALOG_FRAGMENT_DELETE_DIALOG = 7;
    public static final int DIALOG_FRAGMENT_ADD_QUOTATION = 10;
    public static final int DIALOG_COLLECT_CASH = 11;
    public static final int USERSIGINFRAGMENT = 1455;
    public static final int SETTINGSFRAGMENTS = 1456;
    public static final String PROPERTY_APP_VERSION = "appVersion";
    public static final String SERVER_DETAILS = "ou=sams,dc=roamtech,dc=com";
    public static final String ADMIN_SERVER_DETAILS = "cn=Directory Manager";
    public static final String ADMIN_PASSWORD = "d1rectoryMan";
    public static final String PACKAGE_DETAILS = "com.roamtech.android.sams";
    public static final String BASEDNS = "dc=roamtech,dc=com";
    public static final String ATTR_CN_NAME = "cn";
    public static final String ATTR_SN_NAME = "sn";
    public static final String ATTR_CLIENT_ID = "clientid";
    public static final String ATTR_EMALI_ROLES = "emaliroles";
    public static final String ATTR_GIVEN_NAME = "givenName";
    public static final String ATTR_PRIMARY_MAIL= "mail";
    public static final String ATTR_UID= "uid";
    public static final String ROLE_SUPER_ADMIN= "SUPERADMIN";
    public static final String ROLE_SALES_EXECUTIVE= "SalesExecutive";
    public static final String ROLE_USER= "USER";
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String TOKEN_FROM_SERVER = "tokenFromServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PROJECT_ID = "831870867531";
    public static final String RESPONSE = "response";
    public static final String RESULT = "result";
    public static final String ACCOUNT_ACTIVATED = "account_activated";
    public static final String ACTIVATE = "activate";
    public static final String DEACTIVATE = "deactivate";
    public static final String MESSAGE = "message";
    public static final String ACTIVE = "0";
    public static final String INACTIVE = "1";


    /**
     *
     * Converts a double number with scientific notations to a user readable string to be displayed
     *
     * @param number nmber to be converted to string with Scientific Notation
     * @return String converted from Scientific Notation
     */
    public static String convertFromScientificNotation(double number) {
        // Check if in scientific notation
        if (String.valueOf(number).toLowerCase().contains("e")) {
            System.out.println("The scientific notation number'"
                    + number
                    + "' detected, it will be converted to normal representation with 25 maximum fraction digits.");
            NumberFormat formatter = new DecimalFormat();
            formatter.setMaximumFractionDigits(25);
            return formatter.format(number);
        } else
            return String.valueOf(number);
    }

    /**
     *
     * Check whether the ListView is at the top
     *
     * @param view view to be analysed
     * @return boolean true if view passed is at position 0 of listview
     */
    public static boolean listIsAtTop(ListView view)   {
        if(view.getChildCount() == 0) return true;
        return view.getChildAt(0).getTop() == 0;
    }

    /**
     *
     * Saves the new status of the account once its changed
     *
     * @param prefs prefs where the status of the account activation is saved
     * @param activation the status of the account if true means its activated if false means its been deactivated
     */
    public static void activateAccount(SharedPreferences prefs,boolean activation){
        if (prefs.contains(MainModel.ACCOUNT_ACTIVATED)) {
            prefs.edit().remove(MainModel.ACCOUNT_ACTIVATED).commit();
        }
        prefs.edit().putBoolean(MainModel.ACCOUNT_ACTIVATED, activation).commit();
    }

    /**
     *
     * Gets the app version
     *
     * @param context
     * @return integer showing the apps version
     */
    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     *
     * Hashes the password entered by a user
     *
     * @param inputValue The password string to be hashed
     * @return The new hashed string
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     */
    public static String getHash(String inputValue) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String salt = inputValue;
        if (inputValue != null) {
            MessageDigest digester = MessageDigest.getInstance("SHA-256");

            digester.update(inputValue.getBytes());

            BigInteger hash = new BigInteger(1, digester.digest());
            salt = hash.toString(16);
            while (salt.length() < 32) { // 40 for SHA-1
                salt = "0" + salt;
            }
        }
        return salt;
    }

    /**
     *
     * Converts a number double to a decimal value then to a string
     *
     * @param d Number to be converted to decimal figure then converted to a string
     * @return String converted to a decimal value
     */
    public static String doubleToStringNoDecimal(double d) {
        DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance();
        formatter .applyPattern("#,###.00");
        return formatter.format(d);
    }

}
