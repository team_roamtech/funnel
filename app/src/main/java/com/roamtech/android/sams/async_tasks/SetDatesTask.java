package com.roamtech.android.sams.async_tasks;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.roamtech.android.sams.interfaces.ReturnCursor;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 * Created by dennis on 3/6/15.
 */
public class SetDatesTask extends AsyncTask<Void, Void, Cursor> {
    public static final String TAG = "SetDatesTask";
    Context ctx;
    Fragment fragment2;
    int activityDone;
    String activityDate;
    ReturnCursor frg;

    public SetDatesTask(Context ctx, Fragment fragment){
        this.ctx = ctx;
        this.fragment2 = fragment;
        frg = (ReturnCursor) fragment2;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        frg.onStartTaskCursor();
    }

    @Override
    protected Cursor doInBackground(Void... params) {
        Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"33");
        Cursor c1 = ctx.getContentResolver().query(data_id, null, null,new String[] {}, null);
        if(c1 != null || c1.getCount() != 0 ) {
            Log.d(TAG, "--------" + String.valueOf(c1.getCount()));
         /*   c1.moveToFirst();
            while (c1.isAfterLast() == false) {
                activityDone = c1.getInt(c1.getColumnIndex(ctx.getResources().getString(R.string.db_table_activity_done)));
                activityDate = c1.getString(c1.getColumnIndex(ctx.getResources().getString(R.string.db_table_time)));
                setCustomResourceForDates(activityDone,activityDate);
                c1.moveToNext();
            }*/
            return c1;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Cursor cursor){
        if(cursor != null || cursor.getCount() != 0){
            frg.onReturnCursor(cursor);
        }
    }
}
