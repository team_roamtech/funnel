package com.roamtech.android.sams.interfaces;

import com.roamtech.android.sams.models.ReportItems;

import java.util.ArrayList;

/**
 *
 * Monitors and returns result of an asyntask thread running in the background getting reports from the server
 *
 * Created by dennis on 5/14/15.
 * @author Dennis Mwangi Karuri
 */
public interface ReturnReports {

    /**
     * Defines what should happen before the background task is started
     */
    void onStartTask();

    /**
     *
     * Defines what should happen once the background task is completed and also returns to the foreground thread the result
     * of the Background thread running
     *
     * @param result Arraylist of ReportsItems gotten from the server
     */
    void onReturnResult(ArrayList<ReportItems> result);
}
