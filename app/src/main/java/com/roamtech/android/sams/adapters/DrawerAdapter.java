package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.roamtech.android.sams.R;

/**
 * Created by dennis on 2/17/15.
 */
public class DrawerAdapter extends ArrayAdapter<String> {
    public static final String TAG = "DrawerAdapter";
    String [] drawerItems;
    private LayoutInflater vi;
    TypedArray typedArray;
    Context context;


    public DrawerAdapter(Context context, int resource, String[] drawerItems,TypedArray typedArray) {
        super(context, resource, drawerItems);
        this.context = context;
        this.drawerItems = drawerItems;
        this.typedArray = typedArray;
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return drawerItems.length;
    }




    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View v = vi.inflate(R.layout.list_drawer_items, null);
        ImageView ivtitle = (ImageView)v.findViewById(R.id.img_menu_item);
        //Log.d(TAG, "It get here " + items.getImageUrl());
        TextView txtTitle = (TextView)v.findViewById(R.id.txt_menu_item);
        txtTitle.setText(drawerItems[position]);
        ImageView imgTitle = (ImageView)v.findViewById(R.id.img_menu_item);
        imgTitle.setImageResource(typedArray.getResourceId(position,-1));
        return v;
    }

}
