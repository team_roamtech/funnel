package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.fragments.AnalyticsFragment;

/**
 * Created by dennis on 7/10/15.
 */
public class HomePagerAdapter extends FragmentStatePagerAdapter {
    private String titles[];
    AnalyticsFragment analyticsFragment;
    Context ctx;
    int fragmentPosition;

    public HomePagerAdapter(FragmentManager fm,Context ctx) {
        super(fm);
        this.ctx = ctx;
        titles = ctx.getResources().getStringArray(R.array.home_items);
    }

    @Override
    public Fragment getItem(int position) {
        analyticsFragment = new AnalyticsFragment();
        Bundle args = new Bundle();
        args.putInt(ctx.getResources().getString(R.string.bundle_fragment_position), position);
        analyticsFragment.setArguments(args);
        return analyticsFragment;
    }

    @Override
    public int getCount() {
        return 5;
    }

    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public Parcelable saveState(){
        return null;
    }

}
