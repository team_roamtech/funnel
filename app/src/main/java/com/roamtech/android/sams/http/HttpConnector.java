package com.roamtech.android.sams.http;

import android.util.Log;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * Helper Class that Checks whether a network connection is available
 *
 * Created by Karuri on 2/23/2015.
 * @author Dennis Mwangi Karuri
 */


public class HttpConnector {

	/**
	 *
	 * Makes Http requests to the Url Provided
	 *
	 * @param urlString String of the Url being requested
	 * @return String response from the server
	 */

	public static String getResponse(String urlString){
	    HttpURLConnection conn = null;
	    StringBuilder jsonResults = new StringBuilder();
	    try {

	        URL url = new URL(urlString);
	        conn = (HttpURLConnection) url.openConnection();
	        InputStreamReader in = new InputStreamReader(conn.getInputStream());

	        // Load the results into a StringBuilder
	        int read;
	        char[] buff = new char[1024];
	        while ((read = in.read(buff)) != -1) {
	            jsonResults.append(buff, 0, read);
	        }
	        
	        
	    } catch (MalformedURLException e) {
	        Log.e("AppUtil", "Error processing Autocomplete API URL", e);
	    } catch (IOException e) {
	        Log.e("AppUtil", "Error connecting to Autocomplete API", e);
	    } finally {
	        if (conn != null) {
	            conn.disconnect();
	        }
	    }
	    Log.d("Result", jsonResults.toString());
	    return jsonResults.toString();
	}
}
