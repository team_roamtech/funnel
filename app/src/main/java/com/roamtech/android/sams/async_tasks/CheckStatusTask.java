package com.roamtech.android.sams.async_tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.http.RestClient;
import com.roamtech.android.sams.interfaces.SendJsonData;
import com.roamtech.android.sams.models.MainModel;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dennis on 12/2/15.
 */
public class CheckStatusTask extends AsyncTask<String, Void, String> {
    public static final String TAG = "CheckStatusTask";
    Context context;
    Fragment fragment;

    public CheckStatusTask(Context context, Fragment fragment){
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... params) {
        String response = statusResponse(params[0],context);
        return response;
    }

    @Override
    protected void onPostExecute(String result){
        Log.d(TAG, "Result is " + result);
        SendJsonData ac = (SendJsonData) fragment;
        ac.jsonData(parseJson(result,context,MainModel.INACTIVE));
    }

    public static String statusResponse(String params,Context context){
        String response = null;
        String email = params;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
        nameValuePairs.add(new BasicNameValuePair(context.getResources().getString(R.string.name_value_request), context.getResources().getString(R.string.name_value_accesscontrol)));
        nameValuePairs.add(new BasicNameValuePair(context.getResources().getString(R.string.name_value_data), CheckStatusTask.createUserDetailsJson(email, context)));
        try {
            response = RestClient.makeRestRequest(RestClient.POST, MainModel.SYNC_URL, nameValuePairs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    public static String parseJson(String response,Context context,String currentStatus){
        //String status =  MainModel.INACTIVE;
        String status =  currentStatus;
        if(response != null) {
            Log.d(TAG, response);
                try {
                    JSONObject jObject = new JSONObject(response);
                    if (jObject.has(MainModel.RESPONSE) & !jObject.isNull(MainModel.RESPONSE)) {
                        if (jObject.get(MainModel.RESPONSE) instanceof JSONObject) {
                            JSONObject json = jObject.getJSONObject(MainModel.RESPONSE);
                            if (json.has(MainModel.RESULT) & !json.isNull(MainModel.RESULT)) {
                                JSONArray jsonArray = json.getJSONArray(MainModel.RESULT);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    if (jsonObject.has(context.getResources().getString(R.string.db_table_status)) & !jsonObject.isNull(context.getResources().getString(R.string.db_table_status))) {
                                        status = jsonObject.getString(context.getResources().getString(R.string.db_table_status));
                                        Log.d(TAG," Account Status active or inactive "+status);
                                    }
                                }
                            }
                        }else{
                            try {
                                JSONArray Array = jObject.getJSONArray(MainModel.RESPONSE);
                                status = MainModel.ACTIVE;
                                Log.d(TAG," Account Status active "+status);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
        }
        return status;
    }

    public static String createUserDetailsJson(String email,Context context){
        String json = "";
        JSONObject user = new JSONObject();
        try {
            user.put(context.getResources().getString(R.string.user_id), email);
            user.put(context.getResources().getString(R.string.transaction_action), context.getResources().getString(R.string.transaction_fetch));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return user.toString();
    }
}
