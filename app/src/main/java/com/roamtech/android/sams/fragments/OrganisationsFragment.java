package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.NewOrganisationsAdapter;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 *
 * Fragment that lists all Organisations in the DB
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class OrganisationsFragment extends Fragment implements View.OnClickListener,LoaderManager.LoaderCallbacks<Cursor>,LaunchFragment {
    public static final String TAG = "OrganisationsFragment";
    public static final int PAGER_FRAGMENT = 2;
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    public static final int LOADER = 1670;
    ListView lstOrganisations;
    Cursor c;
    View infoView;
    ProgressBar mProgresBar;
    TextView txtNoContacts,txtAddContacts;
    AddOrganisationFragment addOrganisationFragment;
    NewOrganisationsAdapter mAdapter;
    int position,position2;
    int mFirstVisibleItem;
    //OnAddSelectedListener mCallBack2;
    ImageButton ivAdd;
    View headerView;
    Toolbar mActionBarToolbar;
    Bitmap bitmap;
    AddActivitiesFragment addActivitiesFragment;
    CommentsFragment commentsFragment;

    public OrganisationsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mainActivity = new MainActivity();
        Bundle args = getArguments();
        if(args != null){
            position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_organisations, container, false);
        // Inflate the layout for this fragment
        lstOrganisations = (ListView) rootView.findViewById(R.id.lst_contacts);
        infoView = (View) rootView.findViewById(R.id.error_container);
        mProgresBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mActionBarToolbar = (Toolbar) mActivity.findViewById(R.id.toolbar_actionbar);

        txtNoContacts = (TextView) rootView.findViewById(R.id.txt_no_deals);
        txtAddContacts = (TextView) rootView.findViewById(R.id.txt_adddeals);
        headerView = mActivity.getLayoutInflater().inflate(R.layout.listview_fragment_header, null);
        mAdapter = new NewOrganisationsAdapter(mActivity, null, 0, this);
        lstOrganisations.addHeaderView(headerView, null, false);
        infoView.setVisibility(View.GONE);
        lstOrganisations.setVisibility(View.VISIBLE);
        lstOrganisations.setAdapter(mAdapter);
        lstOrganisations.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
        lstOrganisations.setOnScrollListener(new AbsListView.OnScrollListener() {
            private int mLastFirstVisibleItem;
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_TOUCH_SCROLL || scrollState == SCROLL_STATE_FLING) {
                    Log.d(TAG, "Scrolling");
                    if (mFirstVisibleItem != 0) {
                        mActionBarToolbar.setBackgroundResource(R.color.toolbar_organisations_purple);
                    }/*else {
                        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                    }*/
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d(TAG,"First Visible Item "+String.valueOf(firstVisibleItem));
                mFirstVisibleItem = firstVisibleItem;
                if (firstVisibleItem == 0 && MainModel.listIsAtTop(lstOrganisations)) {
                    mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                } else {
                    mActionBarToolbar.setBackgroundResource(R.color.toolbar_organisations_purple);
                }
            }
        });

        ivAdd = (ImageButton) rootView.findViewById(R.id.fab_button);
        ivAdd.setOnClickListener(this);
        if(savedInstanceState != null){
            Log.d(TAG,"stalemate 1");

        }else{
            Log.d(TAG,"stalemate 2");
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Bundle args = getArguments();
        if(args != null) {
            mActivity.getSupportActionBar().setTitle(args.getString(mActivity.getResources().getString(R.string.bundle_title)));
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            position2 = position;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);

    }

    public void runStringLoader(){
        getLoaderManager().restartLoader(LOADER, null, this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, menu);
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_flag).setVisible(true);
        menu.findItem(R.id.action_comments).setVisible(true);

        //        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                addActivitiesFragment.setTargetFragment(OrganisationsFragment.this, MainModel.ACTIVITY_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                commentsFragment.setTargetFragment(OrganisationsFragment.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
            default:

                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /*if(items!=null){
            outState.putParcelableArrayList("items",items);
        }*/
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.PAGER_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
               // mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "-------- it gets here orgs");
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"15");
        return new CursorLoader(getActivity(),data, null, null,
                new String[] {}, null);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "-------- it gets here on load finished");
        mProgresBar.setVisibility(View.GONE);
        c = data;
        Log.d(TAG, "--------" + String.valueOf(c.getCount()));
        if(c == null || c.getCount() == 0){
          /*  infoView.setVisibility(View.VISIBLE);
            txtNoContacts.setText(mActivity.getResources().getString(R.string.txt_noorganisation));
            txtAddContacts.setText(mActivity.getResources().getString(R.string.txt_addorganisation));*/
        }else{


        }
        mAdapter.swapCursor(c);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.fab_button:
                addOrganisationFragment = new AddOrganisationFragment();
                addOrganisationFragment.setTargetFragment(OrganisationsFragment.this, MainModel.PAGER_FRAGMENT);
                mainActivity.addFragment(addOrganisationFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addOrganisationFragment).getClass().getName());
                break;
            default:

                break;
        }

    }

    @Override
    public void launchFragment(int position, Bundle bundle, int view) {
        bundle.putInt(mActivity.getResources().getString(R.string.bundle_position),position2);
        addOrganisationFragment = new AddOrganisationFragment();
        addOrganisationFragment.setArguments(bundle);
        addOrganisationFragment.setTargetFragment(OrganisationsFragment.this, MainModel.PAGER_FRAGMENT);
        mainActivity.addFragment(addOrganisationFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addOrganisationFragment).getClass().getName());
    }
}
