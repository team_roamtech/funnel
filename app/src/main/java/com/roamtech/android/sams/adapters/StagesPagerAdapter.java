package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.fragments.ContactsFragment;
import com.roamtech.android.sams.fragments.LeadsFragment;
import com.roamtech.android.sams.fragments.QuotationStageFragment;
import com.roamtech.android.sams.fragments.QuotationsFragment;
import com.roamtech.android.sams.fragments.StagesFragment;

/**
 * Created by dena on 1/28/15.
 */
public class StagesPagerAdapter extends FragmentStatePagerAdapter {
    public static final String TAG = "StagesPagerAdapter";
    StagesFragment stagesFragment;
    LeadsFragment leadsFragment;
    QuotationStageFragment quotationStageFragment;
    String [] title;
    Context ctx;
    Fragment fragment;
    int setTargetFragment;
    int position;
    private String titles[];

    public StagesPagerAdapter(Context ctx,FragmentManager fm,String [] title,Fragment fragment,int setTargetFragment,int position) {
        super(fm);
        this.title = title;
        this.ctx = ctx;
        this.fragment = fragment;
        this.setTargetFragment = setTargetFragment;
        this.position = position;
        titles = ctx.getResources().getStringArray(R.array.pager_title_items);
    }

    @Override
    public Fragment getItem(int i) {
        Log.d(TAG,"This is the position sent from mainactivity "+ String.valueOf(position));
        if(i == 0){
            leadsFragment = new LeadsFragment();
            Bundle args = new Bundle();
            args.putString(ctx.getResources().getString(R.string.bundle_title), title[i]);
            args.putInt(ctx.getResources().getString(R.string.bundle_fragment_position), i);
            args.putInt(ctx.getResources().getString(R.string.bundle_position), position);
            leadsFragment.setTargetFragment(fragment, setTargetFragment);
            leadsFragment.setArguments(args);
            return leadsFragment;
        }/*else if(i == 2){
            quotationsFragment = new QuotationsFragment();
            Bundle args = new Bundle();
            args.putString(ctx.getResources().getString(R.string.bundle_title), title[i]);
            args.putInt(ctx.getResources().getString(R.string.bundle_fragment_position), i);
            args.putInt(ctx.getResources().getString(R.string.bundle_position), position);
            quotationsFragment.setTargetFragment(fragment, setTargetFragment);
            quotationsFragment.setArguments(args);
            return quotationsFragment;
        }*/else if(i == 1){
            Log.d(TAG,"----------- position is --------- "+position);
            stagesFragment = new StagesFragment();
            Bundle args = new Bundle();
            args.putString(ctx.getResources().getString(R.string.bundle_title), title[i]);
            args.putInt(ctx.getResources().getString(R.string.bundle_fragment_position), i);
            args.putInt(ctx.getResources().getString(R.string.bundle_position), position);
            stagesFragment.setTargetFragment(fragment, setTargetFragment);
            stagesFragment.setArguments(args);
            return stagesFragment;
        }else{
            Log.d(TAG,"----------- position is --------- "+position);
            quotationStageFragment = new QuotationStageFragment();
            Bundle args = new Bundle();
            args.putString(ctx.getResources().getString(R.string.bundle_title), title[i]);
            args.putInt(ctx.getResources().getString(R.string.bundle_fragment_position), i);
            args.putInt(ctx.getResources().getString(R.string.bundle_position), position);
            quotationStageFragment.setTargetFragment(fragment, setTargetFragment);
            quotationStageFragment.setArguments(args);
            return quotationStageFragment;
        }
    }

    @Override
    public int getCount() {
        return 5;
    }

    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public Parcelable saveState(){
        return null;
    }

}
