package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.models.HomeItems;

import java.util.ArrayList;

/**
 * Created by Karuri on 2/22/2015.
 */
public class HomeAdapter extends ArrayAdapter<HomeItems> {
    Context context;
    ArrayList<HomeItems> homeItems = new ArrayList<HomeItems>();
    private LayoutInflater vi;
    HomeHolder holder;
    Fragment fragment;
    LaunchFragment mCallBack;

    public HomeAdapter(Context context, ArrayList<HomeItems> homeItems,Fragment fragment) {
        super(context, 0, homeItems);
        this.context = context;
        this.homeItems = homeItems;
        this.fragment = fragment;
        try {
            mCallBack = (LaunchFragment) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return homeItems.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        HomeHolder holder;
        HomeItems items = homeItems.get(position);
        if(convertView==null){
            vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.grid_analytics_item, null);
            holder = new HomeHolder();
            holder.lytGrdAnalytics = (LinearLayout) convertView.findViewById(R.id.lyt_grd_analytics);
            holder.lytGrdAnalytics.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallBack.launchFragment(position,null,1);
                }
            });
            holder.txtTitle = (TextView) convertView.findViewById(R.id.txt_sale_target);
            holder.txtNumber = (TextView) convertView.findViewById(R.id.txt_sale_target_num);
       ;
            convertView.setTag(holder);
        }else{
            holder = (HomeHolder) convertView.getTag();
        }
        holder.txtTitle.setText(items.getTitle());
        holder.txtNumber.setText(items.getNumber());
        return convertView;
    }

    static class HomeHolder {
        LinearLayout lytGrdAnalytics;
        TextView txtTitle;
        TextView txtNumber;

    }
}