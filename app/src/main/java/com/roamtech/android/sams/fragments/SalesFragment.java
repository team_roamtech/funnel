package com.roamtech.android.sams.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.DealAdapter;
import com.roamtech.android.sams.interfaces.GoToPager;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 *
 * Fragment where sales are listed from the DB
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class SalesFragment extends Fragment implements View.OnClickListener,LoaderManager.LoaderCallbacks<Cursor>,LaunchFragment {
    public static final String TAG = "SalesFragment";
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    NewDealFragment newDealFragment;
    DealFragment dealFragment;
    ListView lstDeals;
    View infoView;
    int fragmentPosition,position2;
    String title2;
    View headerView;
    ProgressBar mProgresBar;
    Cursor c;
    DealAdapter mAdapter;
    Toolbar mActionBarToolbar;
    TextView txtNoContacts,txtAddContacts,txtTitle;
    FrameLayout frmLayouts,frmLayouts2;
    ImageButton ivAdd;
    int mFirstVisibleItem;
    StagesStateFragment stagesStateFragment;
    String[] pagerItemsArray;
    public static final int LOADER = 1550;
    public static final int LOADER1 = 1551;
    int convertedSale = -1;
    AddActivitiesFragment addActivitiesFragment;
    CommentsFragment commentsFragment;

    /**
     * Constructor
     */
    public SalesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mainActivity = new MainActivity();
        Log.d(TAG, "on create");
        newDealFragment = new NewDealFragment();
        dealFragment = new DealFragment();
        Bundle args = getArguments();
        if(args != null){
            position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            title2 = args.getString(mActivity.getResources().getString(R.string.bundle_title));
            convertedSale = args.getInt(mActivity.getResources().getString(R.string.bundle_converted_sale));
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_stages, container, false);
        pagerItemsArray = mActivity.getResources().getStringArray(R.array.pager_title_items);
        mActionBarToolbar = (Toolbar) mActivity.findViewById(R.id.toolbar_actionbar);
        headerView = mActivity.getLayoutInflater().inflate(R.layout.listview_fragment_header, null);
        lstDeals = (ListView) rootView.findViewById(R.id.lst_deals);
        infoView = (View) rootView.findViewById(R.id.error_container);
        mProgresBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        txtNoContacts = (TextView) rootView.findViewById(R.id.txt_no_deals);
        txtAddContacts = (TextView) rootView.findViewById(R.id.txt_adddeals);


        // txtTitle = (TextView) rootView.findViewById(R.id.txt_stage_title);
        //ivAdd = (ImageButton) rootView.findViewById(R.id.fab_button);
        //ivAdd.setOnClickListener(this);
        lstDeals.addHeaderView(headerView, null, false);
        frmLayouts2 = (FrameLayout) rootView.findViewById(R.id.frm_layouts);
        frmLayouts2.setBackgroundColor(mActivity.getResources().getColor(R.color.login_blue));
        ivAdd = (ImageButton) rootView.findViewById(R.id.fab_button);
        ivAdd.setOnClickListener(this);

        lstDeals.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_TOUCH_SCROLL || scrollState == SCROLL_STATE_FLING) {
                    Log.d(TAG, "Scrolling");
                    if (mFirstVisibleItem != 0) {
                        mActionBarToolbar.setBackgroundResource(R.color.toolbar_sales_blue);
                    }/*else {
                            mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                        }*/
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d(TAG, "First Visible Item " + String.valueOf(firstVisibleItem));
                mFirstVisibleItem = firstVisibleItem;
                if (firstVisibleItem == 0 && MainModel.listIsAtTop(lstDeals)) {
                    mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                } else {
                    mActionBarToolbar.setBackgroundResource(R.color.toolbar_sales_blue);
                }

            }
        });

        if(convertedSale != -1){
            mActivity.getSupportLoaderManager().restartLoader(LOADER1, null, this);
        }else{
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }

        mAdapter = new DealAdapter(getActivity(), null, 0,fragmentPosition,this);
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Bundle args = getArguments();
        if(args != null){
            mActivity.getSupportActionBar().setTitle(args.getString(mActivity.getResources().getString(R.string.bundle_title)));
            // txtTitle.setText(title2);
        }
        //  mActivity.getSupportActionBar().setTitle(title2);
        if(convertedSale != 0){
            mActivity.getSupportLoaderManager().restartLoader(LOADER1, null, this);
        }else{
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_flag).setVisible(true);
        menu.findItem(R.id.action_comments).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                addActivitiesFragment.setTargetFragment(SalesFragment.this, MainModel.ACTIVITY_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                commentsFragment.setTargetFragment(SalesFragment.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
            default:

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.DEAL_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    int fragmentPos = data.getIntExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), 0);
                    stagesStateFragment = new StagesStateFragment();
                    Bundle args = new Bundle();
                    args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPos);
                    stagesStateFragment.setArguments(args);
                    mainActivity.addFragment(stagesStateFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(),((Object) stagesStateFragment).getClass().getName());
                    // mActivity.getSupportFragmentManager().popBackStackImmediate();
                }
            }
        }else if (requestCode == MainModel.PAGER_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
                mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
            }
        }
    }


    @Override
    public void launchFragment(int position, Bundle bundle, int view) {
        bundle.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
        bundle.putInt(mActivity.getResources().getString(R.string.bundle_position), position2);
        dealFragment.setArguments(bundle);
        dealFragment.setTargetFragment(SalesFragment.this, MainModel.DEAL_FRAGMENT);
        mainActivity.addFragment(dealFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) dealFragment).getClass().getName());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        if(id == LOADER) {
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "51");
            return new CursorLoader(getActivity(), data, null, null,
                    new String[]{String.valueOf(2)}, null);
        }else if(id == LOADER1){
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "53");
            return new CursorLoader(getActivity(), data, null, null,
                    new String[]{String.valueOf(convertedSale)}, null);
        }else{
            return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mProgresBar.setVisibility(View.GONE);
        c = data;
//        Log.d(TAG, "On Load Finished --------" + String.valueOf(c.getCount()));
        if(c == null || c.getCount() == 0 ){
           /* infoView.setVisibility(View.VISIBLE);
            lstDeals.setVisibility(View.GONE);*/
        }else{

        }
        mAdapter.swapCursor(c);
        infoView.setVisibility(View.GONE);
        lstDeals.setVisibility(View.VISIBLE);
        lstDeals.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onClick(View v) {
        Bundle args = new Bundle();
        switch (v.getId()) {
            case R.id.fab_button:
                args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
                args.putString(mActivity.getResources().getString(R.string.db_table_sale_stage), pagerItemsArray[fragmentPosition]);
                newDealFragment.setArguments(args);
                newDealFragment.setTargetFragment(SalesFragment.this, MainModel.DEAL_FRAGMENT);
                mainActivity.addFragment(newDealFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) newDealFragment).getClass().getName());
                break;
            default:

                break;
        }

    }
}
