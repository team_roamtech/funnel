package com.roamtech.android.sams.interfaces;

import android.database.Cursor;

/**
 *
 * Monitors and returns result of an asyntask thread running in the background to get a cursor with data gotten from the DB
 *
 * Created by dennis on 3/6/15.
 * @author Dennis Mwangi Karuri
 */
public interface ReturnCursor {

    /**
     * Defines what should happen before the background task is started
     */
    void onStartTaskCursor();

    /**
     *
     * Defines what should happen once the background task is completed and also returns to the foreground thread the cursor with
     * the data from the DB
     *
     * @param cursor Cursor containing the DB results
     */
    void onReturnCursor(Cursor cursor);
}
