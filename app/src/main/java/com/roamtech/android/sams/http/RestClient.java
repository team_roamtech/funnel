package com.roamtech.android.sams.http;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;


/**
 *
 * Helper Class that Handles Http requests from the server
 *
 * Created by Karuri on 2/23/2015.
 * @author Dennis Mwangi Karuri*
 */
public class RestClient {
    public static final String TAG = "RestClient";

    private static final String ENCODING = "UTF-8";

    public static final int GET = 0;
    public static final int POST = 1;

    static HttpResponse response;
    static String data;

    /**
     *
     * Makes Http requests to the Url Provided and also adds parameters provided to the request
     *
     * @param method Integer representing Http method to be requested
     * @param url String of the URL being requested
     * @param params URL parameters to be added to the URL
     * @return String response from the server
     * @throws Exception
     */
    public static String makeRestRequest(int method, String url, List<NameValuePair> params) throws Exception{

        HttpClient client=new DefaultHttpClient();
        if (method==POST){
            try {
                UrlEncodedFormEntity entity=new UrlEncodedFormEntity(params,ENCODING);
                HttpPost httpPost=new HttpPost(url);
                httpPost.setEntity(entity);
                response=client.execute(httpPost);
                Log.d(TAG,String.valueOf(response));

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else if(method==GET){
            if(params!=null){
                String paramString = URLEncodedUtils.format(params, ENCODING);
                url += "?" + paramString;
            }
            
            Log.d("URL", url);
            HttpGet httpGet=new HttpGet(url);
            try {
                response=client.execute(httpGet);
            } catch (IOException e) {
                throw e;
            }
        }

        if(response!=null){
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                try {
                data= EntityUtils.toString(response.getEntity(), ENCODING);;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("JSON", "Failed to download file");
            }
        }else{
            throw new Exception("No connectivity.");
        }

        return data;

    }
}
