package com.roamtech.android.sams.http;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 *
 * Helper Class that Checks whether a network connection is available
 *
 * Created by Karuri on 2/23/2015.
 * @author Dennis Mwangi Karuri
 */

public class NetworkConnectionStatus {

	/**
	 *
	 * Checks if network connection is available
	 *
	 * @param context
	 * @return boolean true if network connection is available and false if not available
	 */
	public static boolean isOnline(Context context) {
		ConnectivityManager conMgr = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = conMgr.getActiveNetworkInfo();

		if (netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable()) {
			//Toast.makeText(context, "No Internet connection. Try again later!",
				//	Toast.LENGTH_LONG).show();
			return false;
		}
		return true;
	}

}
