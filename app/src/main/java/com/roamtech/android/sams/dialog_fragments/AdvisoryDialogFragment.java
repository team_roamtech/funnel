package com.roamtech.android.sams.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.roamtech.android.sams.R;

/**
 * Created by dennis on 4/14/15.
 */
public class AdvisoryDialogFragment extends DialogFragment {
    AppCompatActivity mActivity;
    Button btnOk;
    EditText txtMessage;
    String message;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            message = args.getString(mActivity.getResources().getString(R.string.txt_advisory));
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_advisory);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.txt_advisory));
        dialog.setCanceledOnTouchOutside(false);
        txtMessage = (EditText) dialog.findViewById(R.id.txt_advisory);
        txtMessage.setText(message);
        btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        return dialog;
    }
}
