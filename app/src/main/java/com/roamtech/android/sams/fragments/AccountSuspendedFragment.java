package com.roamtech.android.sams.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.roamtech.android.sams.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountSuspendedFragment extends Fragment implements View.OnClickListener{
    Button btnRetry;

    public AccountSuspendedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_account_suspended, container, false);
        btnRetry = (Button) rootView.findViewById(R.id.btn_retry);
        btnRetry.setOnClickListener(this);

        return rootView;
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_retry:

                break;
            default:

                break;
        }
    }
}
