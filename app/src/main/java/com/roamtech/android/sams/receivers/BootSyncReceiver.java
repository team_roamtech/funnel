package com.roamtech.android.sams.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.roamtech.android.sams.fragments.AddActivitiesFragment;

/**
 *
 * This BroadcastReceiver automatically (re)starts the alarm when the device is
 * rebooted. This receiver is set to be disabled (android:enabled="false") in the
 * application's manifest file. When the user sets the alarm, the receiver is enabled.
 * When the user cancels the alarm, the receiver is disabled, so that rebooting the
 * device will not trigger this receiver.
 *
 * Created by dennis on 5/11/15.
 * @author Dennis Mwangi Karuri
 * @see BroadcastReceiver
 */
public class BootSyncReceiver extends BroadcastReceiver {
    public static final String TAG = "BootSyncReceiver";
    AlarmReceiver alarm = new AlarmReceiver();

    /**
     *
     * Constructor
     */
    public BootSyncReceiver(){

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "Calls boot Receiver");
        alarm.setSyncAlarm(context);
    }
}
