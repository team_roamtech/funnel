package com.roamtech.android.sams.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.OrganisationsAdapter;
import com.roamtech.android.sams.fragments.AddOrganisationFragment;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.interfaces.CheckProvider;
import com.roamtech.android.sams.interfaces.SendJsonData;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by dena on 1/28/15.
 */
public class AddNewOrganisationDialogFragment extends DialogFragment implements View.OnClickListener,LoaderManager.LoaderCallbacks<Cursor> {
    AppCompatActivity mActivity;
    AutoCompleteTextView actName;
    EditText editAddress;
    LinearLayout lytAddNewContact;
    OrganisationsAdapter mAdapter;
    Cursor c;
    String strSequence = null;
    String organisationName,organisationAddress;
    TextView txtAddNewContact;
    long organisation_id;
    String slugID;
    int syncDone;
    public static final int LOADER = 1000;
    public static final String TAG = "AddNewOrganisationDialogFragment";
    AddOrganisationFragment addOrganisationFragment;
    SendJsonData mCallBack3;
    CheckProvider mCallBack4;
    int status;
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    GetUsername getUsername;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack3 = (SendJsonData) activity;
            mCallBack4 = (CheckProvider) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        commonHash = new CommonHash(mActivity);
        addOrganisationFragment = new AddOrganisationFragment();
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        getUsername = new GetUsername(mActivity);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog =  new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_link_to);
        dialog.show();
        Log.d(TAG, TAG);
        dialog.setTitle(mActivity.getResources().getString(R.string.dialog_title));
        dialog.setCanceledOnTouchOutside(false);
        actName = (AutoCompleteTextView) dialog.findViewById(R.id.edit_contact_name);
        actName.setThreshold(3);
        editAddress = (EditText) dialog.findViewById(R.id.edit_address);
        txtAddNewContact = (TextView) dialog.findViewById(R.id.txt_add_new_contact);
        txtAddNewContact.setText(mActivity.getResources().getString(R.string.txt_add_new_organisation));
        lytAddNewContact = (LinearLayout) dialog.findViewById(R.id.lyt_add_contact);
        lytAddNewContact.setOnClickListener(this);
        mAdapter = new OrganisationsAdapter(getActivity(), null, 0);
        actName.setAdapter(mAdapter);
        mAdapter.setFilterQueryProvider(filter);

        actName.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                //if(cameFromUser == true){
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(actName.getWindowToken(), 0);
                if(c.moveToPosition(arg2)){
                    organisationName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_org_name)));
                    organisation_id = c.getLong(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_id)));
                    slugID = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_org_slug_id)));
                    Intent intent = new Intent();
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_id),organisation_id);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_name),organisationName);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id),slugID);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    Log.d(TAG,String.valueOf(getTargetRequestCode()));
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    dismiss();
                }
            }

        });
        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    FilterQueryProvider filter = new FilterQueryProvider() {
        public Cursor runQuery(CharSequence str) {
            if(str != null && str.length() > 2){
                strSequence = str.toString();
                mActivity.getSupportLoaderManager().restartLoader(LOADER,null,AddNewOrganisationDialogFragment.this);
                lytAddNewContact.setVisibility(View.VISIBLE);
            }
            return c;
        }
    };

    /*public void runStringLoader3(){
        getLoaderManager().restartLoader(LOADER, null, this);
    }*/



    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.lyt_add_contact:
                if(actName.length() < 1) {
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_no_name), Toast.LENGTH_LONG).show();
                }else{
                    organisationName = actName.getText().toString();
                    organisationAddress = editAddress.getText().toString();
                    organisation_id = insertOrganisation();
                    try {
                        slugID = commonHash.getHash(String.valueOf(organisation_id));
                        Log.d(TAG, "Common Hash is  " + slugID);
                    } catch (NoSuchAlgorithmException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    String jsonData = jsonData("update");
                    Log.d(TAG,jsonData);
                    mCallBack3.jsonData(jsonData);
                    mCallBack4.transactionID(FunnelProvider.UPDATE_ORGANISATION);
                    updateOrganisation((int)organisation_id);
                    Log.d(TAG, "-------" + String.valueOf(organisation_id));
                    Intent intent = new Intent();

                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id),slugID);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_name), organisationName);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    Log.d(TAG, String.valueOf(getTargetRequestCode()));
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    dismiss();
                }
                break;
        }
    }

    public int updateOrganisation(int orgID){
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_slug_id), slugID);
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "12");
        orgID = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(orgID),mActivity.getResources().getString(R.string.flag_sync)});
        Log.d(TAG,"updated orgID == "+String.valueOf(orgID));

    return orgID;
    }

    public long insertOrganisation(){
        status = 1;
        syncDone = 1;
        coordinates = getCoordinates.getCoordinates();
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"1");
        ContentValues initialValues = new ContentValues();
        initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_name), organisationName);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_address), organisationAddress);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_status), status);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_slug_id), slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_org_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_org_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_org_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_org_longitude), 0.0);
        }
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());
        Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
        Long playListId = ContentUris.parseId(insertUri);
        return playListId;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"7");
        return new CursorLoader(getActivity(),data, null, null,
                new String[] {strSequence}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        c = data;
        if(c.getCount() != 0 && c != null) {
            Log.d(TAG,String.valueOf(c.getCount()));
            mAdapter.swapCursor(c);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    public String jsonData(String transaction){
        JSONObject org = new JSONObject();
        Log.d(TAG,transaction);
        try {
            org.put(mActivity.getResources().getString(R.string.user_id), "dennis.panda");
            org.put(mActivity.getResources().getString(R.string.db_table_org_name), organisationName);
            org.put(mActivity.getResources().getString(R.string.db_table_org_address), organisationAddress);
            org.put(mActivity.getResources().getString(R.string.db_table_org_status), status);
            org.put(mActivity.getResources().getString(R.string.db_table_org_slug_id), slugID);
            org.put(mActivity.getResources().getString(R.string.transaction_org),transaction);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(org);
        return jsonArray.toString();
    }
}
