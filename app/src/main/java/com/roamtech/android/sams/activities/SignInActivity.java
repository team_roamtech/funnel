package com.roamtech.android.sams.activities;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.fragments.SignInStateFragment;
import com.roamtech.android.sams.interfaces.GoToPager;

public class SignInActivity extends AppCompatActivity implements GoToPager {
    public static final String TAG = "SignInActivity";
    MainActivity mainActivity;
    SignInStateFragment signInStateFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600 && getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        signInStateFragment = new SignInStateFragment();
        mainActivity = new MainActivity();
        mainActivity.addFragment(signInStateFragment,false, FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(), ((Object) signInStateFragment).getClass().getName());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_sign_in, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void moveToPager(int fragmentPosition, int position) {
        signInStateFragment.addScrollToFragment(fragmentPosition);
    }
}
