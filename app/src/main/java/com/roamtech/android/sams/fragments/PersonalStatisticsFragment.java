package com.roamtech.android.sams.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.AdminUsersAdapter;
import com.roamtech.android.sams.adapters.ReportsAdapter;
import com.roamtech.android.sams.adapters.SpinnerReportsAdapter;
import com.roamtech.android.sams.dialog_fragments.AddDateDialogFragment;
import com.roamtech.android.sams.dialog_fragments.AdvisoryDialogFragment;
import com.roamtech.android.sams.http.RestClient;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.models.ReportItems;
import com.roamtech.android.sams.parsers.ReportsParser;
import com.roamtech.android.sams.utils.AsyncLoader;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * List the statistics of a particular user to their Admin
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class PersonalStatisticsFragment extends Fragment implements View.OnClickListener,LoaderManager.LoaderCallbacks<ArrayList<ReportItems>> {
    public static final String TAG = "PersonalStatisticsFragment";
    Spinner spnPeople,spnReports;
    GridView grdReports;
    String report,person,reportName;
    LinearLayout lytChooseFromDate,lytChooseToDate,lytTitles;
    AppCompatActivity mActivity;
    SpinnerReportsAdapter spinnerReportsAdapter;
    AdminUsersAdapter adminUsersAdapter;
    String [] spinnerListArray;
    String [] spinnerListPeopleArray;
    public static final int DIALOG_FRAGMENT_TO_DATE = 6;
    public static final int DIALOG_FRAGMENT_FROM_DATE = 7;
    String dateTo,dateFrom;
    TextView txtToTime,txtFromTime,txtTitleQuName,txtTitleConName,txtTitleItem;
    AddDateDialogFragment newFragment4;
    AdvisoryDialogFragment dialogAdvisory;
    MainActivity mainActivity;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String username;
    ProgressBar mProgressBar;
    public static final int LOADER = 1003;
    public static final int LOADER2 = 1002;
    ReportsAdapter reportsAdapter;
    int dateMissing;
    String jsonPeople;
    private boolean isTablet = false;
    ReportsParser reportsParser;
    ArrayList<ReportItems> people = new ArrayList<ReportItems>();

    /**
     * Constructor
     */
    public PersonalStatisticsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        spinnerListArray = getResources().getStringArray(R.array.spn_personal_reports);
        spinnerListPeopleArray = getResources().getStringArray(R.array.spn_users);
        dialogAdvisory = new AdvisoryDialogFragment();
        mainActivity = new MainActivity();
        newFragment4 = new AddDateDialogFragment();
        prefs = PreferenceManager
                .getDefaultSharedPreferences(mActivity);
        editor = prefs.edit();
        username = prefs.getString(mActivity.getResources().getString(R.string.bundle_username),null);
        reportsParser = new ReportsParser(this.mActivity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_team_statistics, container, false);
        spnPeople = (Spinner) rootView.findViewById(R.id.spn_people);
        spnReports = (Spinner) rootView.findViewById(R.id.spn_reports);
        lytChooseFromDate = (LinearLayout) rootView.findViewById(R.id.lyt_choose_from_date);
        lytChooseFromDate.setOnClickListener(this);
        lytChooseToDate = (LinearLayout) rootView.findViewById(R.id.lyt_choose_to_date);
        lytChooseToDate.setOnClickListener(this);
        lytTitles = (LinearLayout) rootView.findViewById(R.id.lyt_titles);
        txtToTime = (TextView) rootView.findViewById(R.id.txt_to_date);
        txtFromTime = (TextView) rootView.findViewById(R.id.txt_from_date);
        txtTitleQuName = (TextView) rootView.findViewById(R.id.txt_title_qu_name);
        txtTitleConName = (TextView) rootView.findViewById(R.id.txt_title_con_name);
        txtTitleItem = (TextView) rootView.findViewById(R.id.txt_title_item);
        grdReports = (GridView) rootView.findViewById(R.id.grd_reports);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);

        spinnerReportsAdapter = new SpinnerReportsAdapter(mActivity,0,spinnerListArray);

        spnReports.setAdapter(spinnerReportsAdapter);
        spnReports.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View view,
                                       int position, long arg3) {
                // TODO Auto-generated method stub
                report = spinnerListArray[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        if (!prefs.contains(mActivity.getResources().getString(R.string.name_value_admin_users))) {
            mActivity.getSupportLoaderManager().initLoader(LOADER2, null, this);
        }else{
            jsonPeople = prefs.getString(mActivity.getResources().getString(R.string.name_value_admin_users),null);
            try {
                Log.d(TAG,jsonPeople);
                people = reportsParser.parse(jsonPeople);
            } catch (Exception e) {
                e.printStackTrace();
            }
            adminUsersAdapter = new AdminUsersAdapter(mActivity,0,null,people);
            spnPeople.setAdapter(adminUsersAdapter);
            mActivity.getSupportLoaderManager().restartLoader(LOADER2, null, this);
        }

        spnPeople.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View view,
                                       int position, long arg3) {
                // TODO Auto-generated method stub
                person = people.get(position).getItems();
                Log.d(TAG,person);
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600 && getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isTablet = true;
        }else{
           isTablet = false;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_send).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_send:
                mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == DIALOG_FRAGMENT_TO_DATE){
            if (resultCode == Activity.RESULT_OK){
                if(data.getExtras() != null) {
                    dateTo = data.getStringExtra(mActivity.getResources().getString(R.string.bundle_set_date));
                    txtToTime.setText(dateTo);
                }
            }
        }else if(requestCode == DIALOG_FRAGMENT_FROM_DATE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getExtras() != null) {
                    dateFrom = data.getStringExtra(mActivity.getResources().getString(R.string.bundle_set_date));
                    txtFromTime.setText(dateFrom);
                }
            }
        }
    }

    /**
     *
     * Creates JSON data of the reports to be retrieved from the server within a certain period
     *
     * @return String of the JSON Data to be sent
     */
    public String reportsJsonData(){
        JSONArray jsonArray = new JSONArray();
        JSONObject reports = new JSONObject();
        Calendar c = Calendar.getInstance();
        try {
            reports.put(mActivity.getResources().getString(R.string.name_value_username), person);
            reports.put(mActivity.getResources().getString(R.string.name_value_querykey), selectReport(report));
            reports.put(mActivity.getResources().getString(R.string.name_value_from_date), dateFrom);
            reports.put(mActivity.getResources().getString(R.string.name_value_to_date), dateTo);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        jsonArray.put(reports);
        return jsonArray.toString();
    }

    /**
     *
     * Creates JSON data of the reports to be retrieved from the server
     *
     * @return String of the JSON Data to be sent
     */
    public String usersJsonData(){
        JSONArray jsonArray = new JSONArray();
        JSONObject reports = new JSONObject();
        Calendar c = Calendar.getInstance();
        try {
            reports.put(mActivity.getResources().getString(R.string.name_value_querykey), mActivity.getResources().getString(R.string.name_value_admin_users));
            reports.put(mActivity.getResources().getString(R.string.name_value_admin_id), username);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        jsonArray.put(reports);
        return jsonArray.toString();
    }

    /**
     *
     * @return String JSON of data from assets
     */
    private String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is;

            is = mActivity.getAssets().open("videos.json");

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");

        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lyt_choose_from_date:
                newFragment4.setTargetFragment(PersonalStatisticsFragment.this, DIALOG_FRAGMENT_FROM_DATE);
                newFragment4.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.lyt_choose_to_date:
                newFragment4.setTargetFragment(PersonalStatisticsFragment.this, DIALOG_FRAGMENT_TO_DATE);
                newFragment4.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
        }
    }

    /**
     *
     * Select report to be retrieved from server
     *
     * @param report String passed from the
     * @return Report of to be retrieved from the server
     */
    public String selectReport(String report){
        if(report.equals(mActivity.getResources().getStringArray(R.array.spn_personal_reports)[0])){
            reportName = mActivity.getResources().getString(R.string.name_value_individualsales);
        }else if(report.equals(mActivity.getResources().getStringArray(R.array.spn_personal_reports)[1])){
            reportName = mActivity.getResources().getString(R.string.name_value_accountmgr);
        }
        return reportName;
    }

    /**
     * Loads reports from the server in the background
     */
    public static class GetReports extends AsyncLoader<ArrayList<ReportItems>> {
        Context ctx;
        ArrayList<ReportItems> report = new ArrayList<ReportItems>();
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
        String jsonData;
        ReportsParser reportsParser;
        int flag;
        private SharedPreferences prefs;
        SharedPreferences.Editor editor;

        /**
         *
         * @param context Context that expects results from the Background Thread
         * @param jsonData Json Data containing parameters for the API to be Requested
         * @param flag Flag of wich particular API to Request
         */
        public GetReports(Context context,String jsonData,int flag){
            super(context);
            this.ctx = context;
            this.jsonData = jsonData;
            this.flag = flag;
            prefs = PreferenceManager
                    .getDefaultSharedPreferences(ctx);
            editor = prefs.edit();
            reportsParser = new ReportsParser(this.ctx);
        }

        @Override
        public ArrayList<ReportItems> loadInBackground() {
            if(jsonData != null) {
                nameValuePairs.add(new BasicNameValuePair(ctx.getResources().getString(R.string.name_value_data), jsonData));
                nameValuePairs.add(new BasicNameValuePair(ctx.getResources().getString(R.string.name_value_request), ctx.getResources().getString(R.string.name_value_report)));
                String json = "{}";
                try {
                    json = RestClient.makeRestRequest(RestClient.POST, MainModel.SYNC_URL, nameValuePairs);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Log.d(TAG, json);
                if(flag == 0) {
                    try {
                        report = reportsParser.parse(json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }else if(flag == 1){
                    if (prefs.contains(ctx.getResources().getString(R.string.name_value_admin_users))) {
                        Log.d(TAG, "Remove prefs");
                        prefs.edit().remove(ctx.getResources().getString(R.string.name_value_admin_users)).commit();
                    }
                    editor.putString(ctx.getResources().getString(R.string.name_value_admin_users),json);
                    editor.commit();
                    try {
                        report = reportsParser.parse(json);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return report;
            }else{
                return null;
            }
        }
    }

    @Override
    public Loader<ArrayList<ReportItems>> onCreateLoader(int id, Bundle args) {
        switch(id){
            case LOADER:
            if (dateFrom == null) {
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.txt_advisory_dates_not_entered), Toast.LENGTH_LONG).show();
                dateMissing = 1;
                return new GetReports(mActivity, null, 0);
            } else if (dateTo == null) {
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.txt_advisory_dates_not_entered), Toast.LENGTH_LONG).show();
                dateMissing = 1;
                return new GetReports(mActivity, null, 0);
            } else {
                mProgressBar.setVisibility(View.VISIBLE);
                dateMissing = 0;
                return new GetReports(mActivity, reportsJsonData(), 0);
            }
            case LOADER2:
                return new GetReports(mActivity, usersJsonData(), 1);
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<ReportItems>> loader, ArrayList<ReportItems> data) {
        if (data != null) {
            if(loader.getId() == LOADER) {
                grdReports.setVisibility(View.VISIBLE);
                lytTitles.setVisibility(View.VISIBLE);
                if (reportName.equals(mActivity.getResources().getString(R.string.name_value_individualsales))) {
                    txtTitleQuName.setText(mActivity.getResources().getStringArray(R.array.main_activity_items)[5]);
                    txtTitleConName.setText(mActivity.getResources().getStringArray(R.array.main_activity_items)[6]);
                    txtTitleItem.setText(mActivity.getResources().getStringArray(R.array.database_tables)[5]);
                } else if (reportName.equals(mActivity.getResources().getString(R.string.name_value_accountmgr))) {
                    txtTitleQuName.setText(mActivity.getResources().getString(R.string.txt_title_distinct_sales));
                    txtTitleConName.setText(mActivity.getResources().getString(R.string.txt_contacts));
                    txtTitleItem.setText(mActivity.getResources().getStringArray(R.array.spn_report_items)[1]);
                }

                reportsAdapter = new ReportsAdapter(mActivity, 0, null, data);
                grdReports.setAdapter(reportsAdapter);
            }else if(loader.getId() == LOADER2){
                people = data;
                adminUsersAdapter = new AdminUsersAdapter(mActivity,0,null,people);
                spnPeople.setAdapter(adminUsersAdapter);
            }
        }else{
            if(dateMissing == 0){
                Toast.makeText(mActivity,"Data not Found",Toast.LENGTH_LONG).show();
            }
            grdReports.setVisibility(View.GONE);
            lytTitles.setVisibility(View.GONE);
        }
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<ReportItems>> loader) {
        if(loader.getId() == LOADER) {
            grdReports.setAdapter(null);
        }else if(loader.getId() == LOADER2){
            spnPeople.setAdapter(null);
        }
    }
}

