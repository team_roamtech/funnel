package com.roamtech.android.sams.async_tasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.http.RestClient;
import com.roamtech.android.sams.interfaces.ReturnRegistrationResult;
import com.roamtech.android.sams.models.MainModel;
import com.unboundid.ldap.sdk.LDAPException;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dennis on 8/19/15.
 */
public class GetTargetsTasks extends AsyncTask<String, Void, String> {
    public static final String TAG = "GetTargetsTasks";
    Context ctx;
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    String username;

    public GetTargetsTasks(Context ctx,String username){
        this.ctx = ctx;
        prefs = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        editor = prefs.edit();
        this.username = username;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();

    }

    @Override
    protected String doInBackground(String... params) {
        String response = null;
        Log.d(TAG,username);
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
        nameValuePairs.add(new BasicNameValuePair(ctx.getResources().getString(R.string.name_value_request), ctx.getResources().getString(R.string.name_value_report)));
        nameValuePairs.add(new BasicNameValuePair(ctx.getResources().getString(R.string.name_value_data), prepareTargetsData()));
        try {
            response = RestClient.makeRestRequest(RestClient.POST, MainModel.SYNC_URL, nameValuePairs);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        Log.d(TAG,response);
        return parseJson(response);
    }

    public String prepareTargetsData(){
        String json = null;
        JSONArray jsonArray = new JSONArray();
        JSONObject targets = new JSONObject();
        try {
            targets.put(ctx.getResources().getString(R.string.name_value_username), username);
            targets.put(ctx.getResources().getString(R.string.name_value_querykey), ctx.getResources().getString(R.string.name_value_user_target));
            jsonArray.put(targets);
        }catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return jsonArray.toString();
    }

    public String parseJson(String response){
        Log.d(TAG," response "+ response);
        String targets = null;
        String activeTargets = null;
        String status = null;
        JSONObject json = null;
        JSONObject jsonObject = null;
        JSONArray jsonArray = null;

        try{
            if(response != null){
                json = new JSONObject(response);
                if (json.has(ctx.getResources().getString(R.string.json_rows)) & !json.isNull(ctx.getResources().getString(R.string.json_rows))) {
                    jsonArray = json.getJSONArray(ctx.getResources().getString(R.string.json_rows));
                    for(int i = 0;i < jsonArray.length(); i++){
                        jsonObject = jsonArray.getJSONObject(i);
                        status = jsonObject.getString(ctx.getResources().getString(R.string.db_table_status));
                        targets = jsonObject.getString(ctx.getResources().getString(R.string.json_targets));
                        if(status.equals("1")){
                            activeTargets = targets;
                            if (prefs.contains(ctx.getResources().getString(R.string.bundle_targets))) {
                                Log.d(TAG, "Remove prefs");
                                prefs.edit().remove(ctx.getResources().getString(R.string.bundle_targets)).commit();
                            }
                            Log.d(TAG, "Added prefs");
                            editor.putFloat(ctx.getResources().getString(R.string.bundle_targets), Float.parseFloat(activeTargets));
                            editor.commit();
                        }else{
                            activeTargets = null;
                        }
                    }
                }else{
                    activeTargets = null;
                }
            }else{
                activeTargets = null;
            }
        }catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return activeTargets;
    }

    @Override
    protected void onPostExecute(String result){

    }
}
