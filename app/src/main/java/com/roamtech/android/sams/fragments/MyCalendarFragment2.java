package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.interfaces.ReturnCursor;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * Fragment that Displays Calendar
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 * {@see ReturnCursor } interface.
 */
 public class MyCalendarFragment2 extends CaldroidFragment implements ReturnCursor {

     int activityDone;
     AppCompatActivity mActivity;
     String activityDate;
     Date actDate;
     Cursor cursor;
     ArrayList<Date> arrayListDates = new ArrayList<Date>();
     ArrayList<Integer> arrayListInts = new ArrayList<Integer>();
     Date dateItem;
     AddActivitiesFragment addActivitiesFragment;
     CommentsFragment commentsFragment;
     MainActivity mainActivity;

     @Override
     public void onAttach(Activity activity){
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mainActivity = new MainActivity();
    }


    /**
     * Constructor
     */
    public MyCalendarFragment2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View rootView = super.onCreateView(inflater,container,savedInstanceState);
       if(savedInstanceState == null) {

       }
       return  rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Bundle args = getArguments();
        if(args != null) {
            mActivity.getSupportActionBar().setTitle(args.getString(mActivity.getResources().getString(R.string.bundle_title)));
        }
        Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"33");
        cursor = mActivity.getContentResolver().query(data_id, null, null,new String[] {}, null);
        if(cursor != null || cursor.getCount() != 0 ) {
            setCustomResourceForDates(cursor);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_flag).setVisible(true);
        menu.findItem(R.id.action_comments).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                addActivitiesFragment.setTargetFragment(MyCalendarFragment2.this, MainModel.ACTIVITY_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                commentsFragment.setTargetFragment(MyCalendarFragment2.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     *
     * Sets the dates on the Calendar
     *
     * @param cursor Cursor that contains dates to be set on the calendar
     */
    private void setCustomResourceForDates(Cursor cursor) {
        setCaldroidListener(listener);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if(cursor != null || cursor.getCount() != 0 ) {
            while (cursor.moveToNext()) {
                activityDone = cursor.getInt(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_status)));
                activityDate = cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_schedule)));
                actDate = new Date();
                try {
                    actDate = dateFormat.parse(activityDate);
                    arrayListDates.add(actDate);
                    arrayListInts.add(cursor.getPosition());
                    Log.d(TAG, String.valueOf(activityDate));
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (activityDone == 1) {
                        setBackgroundResourceForDate(R.color.purple_theme,
                                actDate);
                        setTextColorForDate(R.color.white_text, actDate);

                    } else if (activityDone == 0) {
                        setBackgroundResourceForDate(R.color.gray,
                                actDate);
                        setTextColorForDate(R.color.white_text, actDate);
                    } else {
                          //setBackgroundResourceForDate(R.color.lost_red,
                        //        activityDate);
                      //  setTextColorForDate(R.color.white_text, activityDate);
                    }

                }
            }
        }


    final CaldroidListener listener = new CaldroidListener() {

        @Override
        public void onSelectDate(Date date, View view) {
            if(arrayListDates.contains(date)) {
                int index = arrayListDates.indexOf(date);
               // Toast.makeText(mActivity, "works", Toast.LENGTH_LONG).show();
                if (cursor.moveToPosition(arrayListInts.get(index))) {
                    Bundle args = new Bundle();
                    args.putInt(mActivity.getResources().getString(R.string.bundle_position),4);
                    args.putInt(mActivity.getResources().getString(R.string.db_table_id), cursor.getInt(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_id))));
//                    args.putInt(mActivity.getResources().getString(R.string.db_table_act_links), cursor.getInt(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_links))));
                    args.putString(mActivity.getResources().getString(R.string.db_table_act_schedule), cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_schedule))));
                    args.putString(mActivity.getResources().getString(R.string.db_table_act_description), cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_description))));
                    args.putString(mActivity.getResources().getString(R.string.db_table_act_model), cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_model))));
                    args.putString(mActivity.getResources().getString(R.string.db_table_act_foreign_key), cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_foreign_key))));
                    args.putInt(mActivity.getResources().getString(R.string.db_table_act_status), cursor.getInt(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_status))));
                    args.putInt(mActivity.getResources().getString(R.string.db_table_sync_done), cursor.getInt(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sync_done))));

                    args.putString(mActivity.getResources().getString(R.string.db_table_act_feedback), cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_feedback))));
                    //mCallBack2.addSelectedItem(args, 4, 0);
                }
            }
        }

        @Override
        public void onChangeMonth(int month, int year) {


        }

        @Override
        public void onLongClickDate(Date date, View view) {

        }

        @Override
        public void onCaldroidViewCreated() {
            refreshView();
            if (getLeftArrowButton() != null) {

            }
        }

        @Override
        public void onCaldroidButtonClicked() {
            addActivitiesFragment = new AddActivitiesFragment();
            addActivitiesFragment.setTargetFragment(MyCalendarFragment2.this, MainModel.PAGER_FRAGMENT);
            mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
        }

    };


    @Override
    public void onStartTaskCursor() {

    }

    @Override
    public void onReturnCursor(Cursor cursor) {
        setCustomResourceForDates(cursor);
    }
}



