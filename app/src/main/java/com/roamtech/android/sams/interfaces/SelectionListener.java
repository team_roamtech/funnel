package com.roamtech.android.sams.interfaces;

import com.roamtech.android.sams.views.ClearableAutoTextView.DisplayStringInterface;


/**
 *
 * Acts a listener for the Autocomplete textview adapter
 *
 * Created by dennis on 4/9/15.
 * @author Dennis Mwangi Karuri
 */
public interface SelectionListener {
	/*
	 * Called when user selects an item from autocomplete view
	 */
	public void onItemSelection(DisplayStringInterface selectedItem);
	
	/*
	 * Called only in case of Google Places API (autocomplete_url = null)
	 */
	public void onReceiveLocationInformation(double lat, double lng);
}
