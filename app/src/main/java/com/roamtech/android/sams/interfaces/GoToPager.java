package com.roamtech.android.sams.interfaces;

/**
 *
 * Gets the position of Fragment String reference in the NavigationDrawer ListView Adapter with a ViewPager(There are two of them)
 * Then Scrolls to position passed in the ViewPager
 *
 * Created by dennis on 2/26/15.
 * @author Dennis Mwangi Karuri
 */
public interface GoToPager {

    /**
     *
     * Passes the Position of the Fragment String reference with the viewpager in the Adapter and Position of the ViewPager
     * to scroll to
     *
     * @param fragmentPosition Position of Fragment String reference with ViewPager in the NavigationDrawer ListView Adapter
     * @param position position to Scroll to in ViewPager
     */
    public void moveToPager(int fragmentPosition,int position);
}
