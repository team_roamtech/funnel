package com.roamtech.android.sams.interfaces;

/**
 *
 * Gets JsonData gotten from the server and returns the result to the foreground thread
 *
 * Created by dennis on 4/9/15.
 * @author Dennis Mwangi Karuri
 */
public interface SendJsonData {
    /**
     * Called when data has already been received from the server
     * @param jsonData The Json Data gotten from the server
     */
   public void jsonData(String jsonData);
}
