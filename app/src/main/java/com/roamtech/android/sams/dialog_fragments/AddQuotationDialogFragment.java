package com.roamtech.android.sams.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FilterQueryProvider;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.QuotationsLikeAdapter;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 * Created by dennis on 6/8/15.
 */
public class AddQuotationDialogFragment extends DialogFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    public static final String TAG = "AddQuotationDialogFragment";
    AppCompatActivity mActivity;
    AutoCompleteTextView actName;
    String strSequence = null;
    Cursor c;
    QuotationsLikeAdapter mAdapter;
    public static final int LOADER = 1000;
    String quotationID,contactID,organisationID,salesID;
    String quotationName,organisationName,contactName,saleName;


    public AddQuotationDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.fragment_add_sale_dialog);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.dialog_link_title));
        dialog.setCanceledOnTouchOutside(false);
        actName = (AutoCompleteTextView) dialog.findViewById(R.id.edit_contact_name);
        actName.setThreshold(3);
        mAdapter = new QuotationsLikeAdapter(mActivity, null, 0);
        actName.setAdapter(mAdapter);
        mAdapter.setFilterQueryProvider(filter);
        actName.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(actName.getWindowToken(), 0);
                if(c.moveToPosition(arg2)){
                    quotationID = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_qu_slug_id)));
                    quotationName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_qu_name)));
                   // Log.d(TAG,quotationName + " " +quotationID);
                    salesID = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_slug_id)));
                    saleName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_name)));
                    organisationName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_org_name)));
                    contactName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_con_name)));
                    contactID = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_con_slug_id)));
                    organisationID = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_org_slug_id)));
                    Intent intent = new Intent();
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_qu_slug_id),quotationID);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_qu_name),quotationName);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_con_slug_id),contactID);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_con_name),contactName);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id),organisationID);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_name),organisationName);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_slug_id),salesID);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_name),saleName);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                  //  Log.d(TAG,String.valueOf(getTargetRequestCode()));
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    dismiss();
                }

            }
        });
        return dialog;
    }

    FilterQueryProvider filter = new FilterQueryProvider() {
        public Cursor runQuery(CharSequence str) {
            if(str != null && str.length() > 2){
                strSequence = str.toString();
                runStringLoader3();
            }
            return c;
        }
    };

    public void runStringLoader3(){
        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri data = null;
        data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"41");
        return new CursorLoader(getActivity(),data, null, null,
                new String[] {strSequence}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        c = data;
      //  Log.d(TAG, String.valueOf(c.getCount()));
        mAdapter.swapCursor(c);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }
}
