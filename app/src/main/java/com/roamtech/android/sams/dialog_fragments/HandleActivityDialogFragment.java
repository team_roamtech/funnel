package com.roamtech.android.sams.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.receivers.AlarmReceiver;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Karuri on 9/28/2015.
 */
public class HandleActivityDialogFragment extends DialogFragment implements View.OnClickListener  {
    AppCompatActivity mActivity;
    Button btnPostpone,btnDone;
    TextView txtActivity;
    String strActivity,strActivityTitle;
    int activityID;
    AlarmReceiver alarm = new AlarmReceiver();

    public HandleActivityDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args =  getArguments();
        strActivity = args.getString(mActivity.getResources().getString(R.string.db_table_act_schedule));
        activityID = args.getInt(mActivity.getResources().getString(R.string.db_table_activities_id));
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_handle_activity);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.notification_title));
        dialog.setCanceledOnTouchOutside(false);
        txtActivity = (TextView) dialog.findViewById(R.id.txt_activity);
        txtActivity.setText(strActivity);
        btnPostpone = (Button) dialog.findViewById(R.id.btn_postpone);
        btnPostpone.setOnClickListener(this);
        btnDone = (Button) dialog.findViewById(R.id.btn_done);
        btnDone.setOnClickListener(this);
        return dialog;
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_postpone:
                Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.HOUR, +1);
                alarm.setPostponeAlarm(mActivity, calendar, activityID, strActivity);
                dismiss();
                break;
            case R.id.btn_done:
                insertActivity(activityID);
                dismiss();
                break;
            default:

                break;
        }
    }



    public long insertActivity(int notification){
        int syncDone = 1;
        int playListId = 0;
        ContentValues initialValues = new ContentValues();
        initialValues.put(getResources().getString(R.string.db_table_act_status), 0);
        initialValues.put(getResources().getString(R.string.db_table_sync_done), syncDone);
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "13");
        playListId = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(notification),getResources().getString(R.string.flag_sync)});
        return playListId;
    }
}
