package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.models.ReportItems;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dennis on 5/21/15.
 */
public class AdminUsersAdapter extends ArrayAdapter<ArrayList<ReportItems>> {
    public static final String TAG = "AdminUsersAdapter";
    private LayoutInflater vi;
    Context context;
    ArrayList<ReportItems> reportItems = new ArrayList<ReportItems>();

    public AdminUsersAdapter(Context context, int resource, List<ArrayList<ReportItems>> objects,ArrayList<ReportItems> reportItems) {
        super(context, resource, objects);
        this.context = context;
        this.reportItems = reportItems;
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return reportItems.size();
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ReportItems reportItem = reportItems.get(position);
        View v = vi.inflate(R.layout.spinner_report_items, null);
        TextView txtTitle = (TextView)v.findViewById(R.id.txt_activity);
        txtTitle.setText(reportItem.getItems());
        return v;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ReportItems reportItem = reportItems.get(position);
        View v = vi.inflate(R.layout.spinner_report_items, null);
        TextView txtTitle = (TextView)v.findViewById(R.id.txt_activity);
        txtTitle.setText(reportItem.getItems());
        return v;
    }

}
