package com.roamtech.android.sams.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.StagesPagerAdapter;
import com.roamtech.android.sams.utils.AsyncLoader;

/**
 *
 * Fragment that contains a ViewPager that has other Fragments that have the different sale stages
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class StagesStateFragment2 extends Fragment implements LoaderManager.LoaderCallbacks<String[]> {
    public static final String TAG = "StagesStateFragment";
    AppCompatActivity mActivity;
    ViewPager mPager;
    StagesPagerAdapter mAdapter;
    String [] pagerItemsArray2;
    NewDealFragment newDealFragment;
    MainActivity mainActivity;
    public static final int LOADER = 1005;
    public static final int PAGER_FRAGMENT = 2;
    int position;
    int fragmentPosition;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (savedInstanceState != null) {
            Log.d(TAG,"---------||||||||||||||||||----------"+ mActivity.toString());
            pagerItemsArray2 = savedInstanceState.getStringArray(mActivity.getResources().getString(R.string.bundle_pager_items));
            position = savedInstanceState.getInt(mActivity.getResources().getString(R.string.bundle_position));
            fragmentPosition = savedInstanceState.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
        }else {
            if (args != null) {
                pagerItemsArray2 = args.getStringArray(mActivity.getResources().getString(R.string.bundle_pager_items));
                position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
                fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_stages_state, container, false);
        pagerItemsArray2 = getResources().getStringArray(R.array.pager_title_items2);
        mPager = (ViewPager) rootView.findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(0);
        mainActivity = new MainActivity();
        newDealFragment = new NewDealFragment();
        Log.d(TAG," OnCreateView ");
        if(pagerItemsArray2 != null && pagerItemsArray2.length > 1){
            mAdapter = new StagesPagerAdapter(mActivity,getChildFragmentManager(),pagerItemsArray2,this,PAGER_FRAGMENT,position);
            if(mAdapter != null) mPager.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            mPager.setCurrentItem(fragmentPosition, true);
        }else {
            getLoaderManager().restartLoader(LOADER, null, this);
        }
        return rootView;
    }

    public void onResume(){
        super.onResume();
        Bundle args = getArguments();
        if(args != null) {
            mActivity.getSupportActionBar().setTitle(args.getString(mActivity.getResources().getString(R.string.bundle_title)));
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(pagerItemsArray2 != null){
            outState.putStringArray(mActivity.getResources().getString(R.string.bundle_pager_items),pagerItemsArray2);
            outState.putInt(mActivity.getResources().getString(R.string.bundle_position),position);
            outState.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position),fragmentPosition);
        }
    }

    /**
     *
     * Sets the ViewPager to Scroll to a position
     *
     * @param args contains data showing where the ViewPager should scroll to
     */
    public void addScrollToFragment(Bundle args){
        if(args != null) {
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
            mAdapter.notifyDataSetChanged();
            mPager.setCurrentItem(fragmentPosition, true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PAGER_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    mainActivity.addFragment(newDealFragment,true, FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getFragmentManager(),((Object) newDealFragment).getClass().getName());
                }
                break;
        }
    }

    /**
     *
     * Closes the Fragment
     *
     * @param position Position of fragment to be closed in the Navigation Drawer ListView Adapter
     * @param atv Activity to close the fragment
     */
    public void closeStateFragment(int position,ActionBarActivity atv) {
        atv.getSupportFragmentManager().popBackStackImmediate();
    }

    /**
     *
     * Class that runs in the Background Loading String references to Fragments to be added to the ViewPager
     * Main thread as an ArrayList<ReportItems>
     *
     */

    public static class GetFragments extends AsyncLoader<String[]> {
        String [] arrayItems;
        Context ctx;

        public GetFragments(Context context,String [] arrayItems) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.arrayItems = arrayItems;

        }

        @Override
        public String[] loadInBackground() {
            return arrayItems;
        }
        //Process the JSON result to Data we can use
    }



    @Override
    public Loader<String[]> onCreateLoader(int id, Bundle args) {
        return new GetFragments(getActivity(),pagerItemsArray2);
    }

    @Override
    public void onLoadFinished(Loader<String[]> loader, String[] data) {
        mAdapter = new StagesPagerAdapter(mActivity,getChildFragmentManager(),pagerItemsArray2,this,PAGER_FRAGMENT,position);
        if(mAdapter != null) mPager.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        mPager.setCurrentItem(fragmentPosition, true);

    }

    @Override
    public void onLoaderReset(Loader<String[]> loader) {

    }
}