package com.roamtech.android.sams.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.roamtech.android.sams.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by dennis on 2/19/15.
 */
public class AddDateDialogFragment extends DialogFragment implements View.OnClickListener {
    public static final String TAG = "AddDateDialogFragment";
    AutoCompleteTextView actName;
    AppCompatActivity mActivity;
    DatePicker dtpDate;
    TimePicker tpTime;
    SimpleDateFormat dateFormat;
    Button btnSet,btnCancel;
    String setDate;
    ArrayList<Integer> dates = new ArrayList<Integer>();

    public AddDateDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_date);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.dialog_title_date));
        dialog.setCanceledOnTouchOutside(false);
        dtpDate = (DatePicker) dialog.findViewById(R.id.dtp_date);
        tpTime = (TimePicker) dialog.findViewById(R.id.ttp_time);
        btnSet = (Button) dialog.findViewById(R.id.btn_set);
        btnSet.setOnClickListener(this);
        btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_set:
                setDate = dateFormat.format(getDateFromDatePicker(dtpDate,tpTime));
                Intent intent = new Intent();
                intent.putExtra(mActivity.getResources().getString(R.string.bundle_set_date), setDate);
                intent.putExtra(mActivity.getResources().getString(R.string.bundle_array_dates),dates);
                mActivity.setResult(Activity.RESULT_OK, intent);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                dismiss();
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
        }
    }


    public Date getDateFromDatePicker(DatePicker datePicker,TimePicker timePicker){
        int year =  datePicker.getYear();
        Log.d(TAG, " value of year " + year);
        dates.add(year);
        int month = datePicker.getMonth();
        Log.d(TAG," value of month "+month);
        dates.add(month);
        int day = datePicker.getDayOfMonth();
        Log.d(TAG, " value of day " + day);
        dates.add(day);
        int hours = timePicker.getCurrentHour();
        Log.d(TAG, " value of hours " + hours);
        dates.add(hours);
        int minutes = timePicker.getCurrentMinute();
        Log.d(TAG, " value of minutes " + minutes);
        dates.add(minutes);
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, day,hours,minutes,00);
        return calendar.getTime();
    }

    public static Date getTimeFromTimePicker(TimePicker timePicker){
        int hours = timePicker.getCurrentHour();
        int minutes = timePicker.getCurrentMinute();
        Calendar calendar = Calendar.getInstance();
        calendar.set(hours,minutes,00);
        return calendar.getTime();
    }
}
