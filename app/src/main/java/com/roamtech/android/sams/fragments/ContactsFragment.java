package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.NewContactsAdapter;
import com.roamtech.android.sams.interfaces.GoToPager;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 *
 * Fragment that lists all your contacts in the DB
 *
 * Created by Karuri on 2/22/2015.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends Fragment implements View.OnClickListener,LoaderManager.LoaderCallbacks<Cursor>,LaunchFragment{
    public static final String TAG = "ContactsFragment";
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    public static final int LOADER = 1450;
    ListView lstContacts;
    Cursor c;
    FrameLayout frmLayouts,frmLayouts2;
    View infoView;
    ProgressBar mProgresBar;
    TextView txtNoContacts,txtAddContacts,txtTitle;
    NewContactsAdapter mAdapter;
    int savedState;
    int position,position2;
    int fragmentPosition;
    String title2;
    ImageButton ivAdd;
    View headerView;
    Toolbar mActionBarToolbar;
    AddContactsFragment addContactsFragment;
    int mFirstVisibleItem;
    StagesStateFragment stagesStateFragment;
    AddActivitiesFragment addActivitiesFragment;
    CommentsFragment commentsFragment;

    /**
     * Constructor
     */
    public ContactsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mainActivity = new MainActivity();

        Bundle args = getArguments();
        if(args != null){
            position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position),-1);
            title2 = args.getString(mActivity.getResources().getString(R.string.bundle_title));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contacts, container, false);
        //frmLayouts = (FrameLayout) rootView.findViewById(R.id.frm_layouts);
        mActionBarToolbar = (Toolbar) mActivity.findViewById(R.id.toolbar_actionbar);
        headerView = mActivity.getLayoutInflater().inflate(R.layout.listview_fragment_header, null);
        lstContacts = (ListView) rootView.findViewById(R.id.lst_contacts);
        infoView = (View) rootView.findViewById(R.id.error_container);
        mProgresBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        txtNoContacts = (TextView) rootView.findViewById(R.id.txt_no_deals);
        txtAddContacts = (TextView) rootView.findViewById(R.id.txt_adddeals);


       // txtTitle = (TextView) rootView.findViewById(R.id.txt_stage_title);
        //ivAdd = (ImageButton) rootView.findViewById(R.id.fab_button);
        //ivAdd.setOnClickListener(this);
            lstContacts.addHeaderView(headerView, null, false);
            frmLayouts2 = (FrameLayout) rootView.findViewById(R.id.frm_layouts);
            frmLayouts2.setBackgroundColor(mActivity.getResources().getColor(R.color.contacts_green));
            ivAdd = (ImageButton) rootView.findViewById(R.id.fab_button);
            ivAdd.setOnClickListener(this);

            lstContacts.setOnScrollListener(new AbsListView.OnScrollListener() {

                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    if (scrollState == SCROLL_STATE_TOUCH_SCROLL || scrollState == SCROLL_STATE_FLING) {
                        Log.d(TAG, "Scrolling");
                        if (mFirstVisibleItem != 0) {
                            mActionBarToolbar.setBackgroundResource(R.color.toolbar_contacts_green);
                        }/*else {
                            mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                        }*/
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                    Log.d(TAG, "First Visible Item " + String.valueOf(firstVisibleItem));
                    mFirstVisibleItem = firstVisibleItem;
                    if (firstVisibleItem == 0 && MainModel.listIsAtTop(lstContacts)) {
                        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                    } else {
                        mActionBarToolbar.setBackgroundResource(R.color.toolbar_contacts_green);
                    }

                }
            });


        mAdapter = new NewContactsAdapter(mActivity, null, 0,this);
        lstContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(c.moveToPosition(position)) {
                    Bundle args = new Bundle();
                }
            }
        });

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Bundle args = getArguments();
        if(args != null){
            mActivity.getSupportActionBar().setTitle(args.getString(mActivity.getResources().getString(R.string.bundle_title)));
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            position2 = position;
            // txtTitle.setText(title2);
        }
      //  mActivity.getSupportActionBar().setTitle(title2);
        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
    }

    @Override
    public void onPause() {
        super.onPause();
        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.PAGER_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    int fragmentPos = data.getIntExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), 0);
                    stagesStateFragment = new StagesStateFragment();
                        Bundle args = new Bundle();
                        args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPos);
                        stagesStateFragment.setArguments(args);
                        mainActivity.addFragment(stagesStateFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(),((Object) stagesStateFragment).getClass().getName());
                       // mActivity.getSupportFragmentManager().popBackStackImmediate();
                }
            }
        }
    }

    /**
     * Runs the Loader
     */
    public void runStringLoader(){
        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);

    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_flag).setVisible(true);
        menu.findItem(R.id.action_comments).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       switch(item.getItemId()){
          case R.id.action_add:
               break;
           case R.id.action_flag:
               addActivitiesFragment = new AddActivitiesFragment();
               addActivitiesFragment.setTargetFragment(ContactsFragment.this, MainModel.ACTIVITY_FRAGMENT);
               mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
               break;
           case R.id.action_comments:
               commentsFragment = new CommentsFragment();
               commentsFragment.setTargetFragment(ContactsFragment.this, MainModel.COMMENTS_FRAGMENT);
               mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
               break;
           default:

               break;

       }
        return super.onOptionsItemSelected(item);
    }



    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "-------- on create loader -----------------");
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"14");
        return new CursorLoader(getActivity(),data, null, null,
                new String[] {}, null);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mProgresBar.setVisibility(View.GONE);
        c = data;
        if(c == null || c.getCount() == 0 ){
            /*Log.d(TAG, "-------- contacts count is " + String.valueOf(c.getCount()));
            infoView.setVisibility(View.VISIBLE);
            txtNoContacts.setText(mActivity.getResources().getString(R.string.txt_nocontacts));
            txtAddContacts.setText(mActivity.getResources().getString(R.string.txt_addcontacts));
            lstContacts.setVisibility(View.GONE);*/
        }else{

        }
        mAdapter.swapCursor(c);
        Log.d(TAG, "-------- contacts count is greater than 0 is  " + String.valueOf(c.getCount()));
        infoView.setVisibility(View.GONE);
        lstContacts.setVisibility(View.VISIBLE);
        lstContacts.setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.img_add:

                break;
            case R.id.fab_button:
                addContactsFragment = new AddContactsFragment();
                addContactsFragment.setTargetFragment(this, MainModel.PAGER_FRAGMENT);
                mainActivity.addFragment(addContactsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addContactsFragment).getClass().getName());
                break;
            default:

                break;
        }
    }

    @Override
    public void launchFragment(int position, Bundle bundle, int view) {
        bundle.putInt(mActivity.getResources().getString(R.string.bundle_position),position2);
        addContactsFragment = new AddContactsFragment();
        addContactsFragment.setArguments(bundle);
        addContactsFragment.setTargetFragment(ContactsFragment.this, MainModel.PAGER_FRAGMENT);
        mainActivity.addFragment(addContactsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addContactsFragment).getClass().getName());
    }
}
