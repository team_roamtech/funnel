package com.roamtech.android.sams.async_tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.http.RestClient;
import com.roamtech.android.sams.interfaces.ReturnAuthenticationResult;
import com.roamtech.android.sams.interfaces.ReturnRegistrationResult;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.parsers.ResponseParser;
import com.roamtech.android.sams.sync_adapters.SyncAdapter;
import com.unboundid.ldap.sdk.LDAPException;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dennis on 4/7/15.
 */
public class SendData extends AsyncTask<String, Void, Boolean> {
    public static final String TAG = "SendData";
    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
    List<NameValuePair> nameValuePairs2 = new ArrayList<NameValuePair>(1);
    Context ctx;
    static HttpResponse response;
    Fragment fragment;
    SyncAdapter syncAdapter;
    ResponseParser responseParser;
    String flag;

    public SendData(Context ctx,Fragment fragment){
        this.ctx = ctx;
        this.fragment = fragment;
        syncAdapter = new SyncAdapter(this.ctx,true);
        responseParser = new ResponseParser(ctx);
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        ReturnRegistrationResult ac = (ReturnRegistrationResult) fragment;
        ac.onStartTask();
    }


    @Override
    protected Boolean doInBackground(String... params) {
        Boolean response = false;
        Boolean response2 = false;
        nameValuePairs =  new ArrayList<NameValuePair>(1);
        nameValuePairs.add(new BasicNameValuePair(ctx.getResources().getString(R.string.name_value_request),params[1]));
        nameValuePairs.add(new BasicNameValuePair(ctx.getResources().getString(R.string.name_value_data), params[0]));
        nameValuePairs2.add(new BasicNameValuePair(ctx.getResources().getString(R.string.name_value_data), params[2]));
        nameValuePairs2.add(new BasicNameValuePair(ctx.getResources().getString(R.string.name_value_request), params[3]));
        Log.d(TAG,params[0]);
        Log.d(TAG,params[2]);

        String json = "{}";
        try {
            json = RestClient.makeRestRequest(RestClient.POST, MainModel.SYNC_URL, nameValuePairs);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        try {
            Log.d(TAG,json);
            response = responseParser.parse(json);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(response == true){
            Log.d(TAG,"it gets here 1 "+response);
            String json2 = "{}";
            try {
                json2 = RestClient.makeRestRequest(RestClient.POST, MainModel.SYNC_URL, nameValuePairs2);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                Log.d(TAG,json2);
                response2 = responseParser.parse(json2);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response2;
        }else{
            return response;
        }
    }

    @Override
    protected void onPostExecute(Boolean result){
        boolean registered = false;
        if(result != null){
           registered = result;
        }
        ReturnRegistrationResult ac = (ReturnRegistrationResult) fragment;
        try {
            ac.onReturnResult(registered);
        } catch (LDAPException e) {
            e.printStackTrace();
        }
    }
}