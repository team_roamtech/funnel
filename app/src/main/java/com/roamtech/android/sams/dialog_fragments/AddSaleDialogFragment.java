package com.roamtech.android.sams.dialog_fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FilterQueryProvider;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.DealAdapter;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddSaleDialogFragment extends DialogFragment implements LoaderManager.LoaderCallbacks<Cursor>,LaunchFragment {
    public static final String TAG = "AddSaleDialogFragment";
    AppCompatActivity mActivity;
    AutoCompleteTextView actName;
    String strSequence = null;
    Cursor c;
    DealAdapter mAdapter;
    String saleName,contactName,organisationName;
    int contact_id;
    int organisation_id;
    int saleID;
    String saleSlugID,contactID,organisationID;
    public static final int LOADER = 1000;


    public AddSaleDialogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.fragment_add_sale_dialog);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.dialog_title));
        dialog.setCanceledOnTouchOutside(false);
        actName = (AutoCompleteTextView) dialog.findViewById(R.id.edit_contact_name);
        actName.setThreshold(3);
        mAdapter = new DealAdapter(mActivity, null, 0,0,this);
        actName.setAdapter(mAdapter);
        mAdapter.setFilterQueryProvider(filter);
        actName.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                //if(cameFromUser == true){
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(actName.getWindowToken(), 0);
               /* if(c.moveToPosition(arg2)){
                    saleID = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_slug_id)));
                    saleName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sale_name)));
                    Log.d(TAG,saleName + " " +saleID);
                    Intent intent = new Intent();
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_slug_id),saleID);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_name),saleName);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    Log.d(TAG,String.valueOf(getTargetRequestCode()));
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    dismiss();
                }*/
            }

        });

        return dialog;
    }

    FilterQueryProvider filter = new FilterQueryProvider() {
        public Cursor runQuery(CharSequence str) {
            if(str != null && str.length() > 2){
                strSequence = str.toString();
                runStringLoader3();
            }
            return c;
        }
    };

    public void runStringLoader3(){
        getLoaderManager().restartLoader(LOADER, null, this);
    }


    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri data = null;
        data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"18");
        return new CursorLoader(getActivity(),data, null, null,
                new String[] {strSequence}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        c = data;
        Log.d(TAG, String.valueOf(c.getCount()));
        mAdapter.swapCursor(c);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    @Override
    public void launchFragment(int position, Bundle bundle, int view) {
        saleID = bundle.getInt(mActivity.getResources().getString(R.string.db_table_id));
        saleSlugID = bundle.getString(mActivity.getResources().getString(R.string.db_table_sale_slug_id));
        saleName = bundle.getString(mActivity.getResources().getString(R.string.db_table_sale_name));
        contactID = bundle.getString(mActivity.getResources().getString(R.string.db_table_con_slug_id));
        contactName = bundle.getString(mActivity.getResources().getString(R.string.db_table_con_name));
        organisationID = bundle.getString(mActivity.getResources().getString(R.string.db_table_org_slug_id));
        organisationName = bundle.getString(mActivity.getResources().getString(R.string.db_table_org_name));
        //organisationName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_organisation_name)));
        //contactName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_contact_name)));
        //contact_id = c.getInt(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_contact_id)));
        //organisation_id = c.getInt(c.getColumnIndex(mActivity.getResources().getString(R.string.query_name_oid)));
        Intent intent = new Intent();
        intent.putExtra(mActivity.getResources().getString(R.string.db_table_id),saleID);
        intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_slug_id),saleSlugID);
        intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_name),saleName);
        intent.putExtra(mActivity.getResources().getString(R.string.db_table_con_slug_id),contactID);
        intent.putExtra(mActivity.getResources().getString(R.string.db_table_con_name),contactName);
        intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id),organisationID);
        intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_name),organisationName);
        mActivity.setResult(Activity.RESULT_OK, intent);
        Log.d(TAG,String.valueOf(getTargetRequestCode()));
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
        dismiss();
    }
}
