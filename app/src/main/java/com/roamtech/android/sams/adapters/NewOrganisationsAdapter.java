package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.interfaces.LaunchFragment;

/**
 * Created by dennis on 2/18/15.
 */
public class NewOrganisationsAdapter extends CursorAdapter {
    public static final String TAG = "NewOrganisationsAdapter";
    private LayoutInflater vi;
    Cursor c;
    Context ctx;
    LaunchFragment mCallBack;
    Fragment fragment;

    public NewOrganisationsAdapter(Context context, Cursor c, int flags,Fragment fragment) {
        super(context, c, flags);
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.ctx = context;
        this.c = c;
        this.fragment = fragment;
        Log.d(TAG, "ContactsAdapter constructor");
        try {
            mCallBack = (LaunchFragment) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        Log.d(TAG,"PlaylistAdapter new view");
        View rootView = vi.inflate(R.layout.list_organisation_item, parent, false);
        ViewHolder holder  =   new ViewHolder();
        holder.mOrganisation = (TextView) rootView.findViewById(R.id.txt_org_name);
        holder.mAddress = (TextView) rootView.findViewById(R.id.txt_address);
        holder.mCard = (LinearLayout) rootView.findViewById(R.id.lyt_card);
        holder.mInnerElements = (LinearLayout) rootView.findViewById(R.id.lyt_inner_elements);
        holder.imgDown = (ImageView) rootView.findViewById(R.id.img_down);
        holder.mNumber = (TextView) rootView.findViewById(R.id.txt_number);
        holder.mEmail = (TextView) rootView.findViewById(R.id.txt_email);
        holder.mWebsite = (TextView) rootView.findViewById(R.id.txt_website);
        holder.animSideDown = AnimationUtils.loadAnimation(context,
                R.anim.slide_down);
        // set animation listener
        holder.animSlideUp = AnimationUtils.loadAnimation(context,
                R.anim.slide_up);
        Log.d(TAG," cursor position " + holder.cursorPosition);
        rootView.setTag(holder);
        return rootView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final Cursor c = cursor;
        if(view != null) {
            final ViewHolder holder = (ViewHolder) view.getTag();
            holder.mOrganisation.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_org_name))));
            holder.mAddress.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_org_address))));
            holder.mNumber.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_org_phone))));
            holder.mEmail.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_org_email))));
            holder.mWebsite.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_org_website))));
            holder.cursorPosition = c.getPosition();
            holder.animSideDown.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            holder.animSlideUp.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    Log.d(TAG,"animation end");
                    if (animation == holder.animSlideUp) {
                        holder.mInnerElements.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            holder.imgDown.setBackgroundResource(R.drawable.ic_action_down2);
            holder.imgDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(holder.mInnerElements.getVisibility() == View.GONE){
                        holder.mInnerElements.setVisibility(View.VISIBLE);
                        holder.mInnerElements.startAnimation(holder.animSideDown);
                        holder.imgDown.setBackgroundResource(R.drawable.ic_action_up2);
                    }else{
                        holder.mInnerElements.setVisibility(View.GONE);
                        holder.mInnerElements.startAnimation(holder.animSlideUp);
                        holder.imgDown.setBackgroundResource(R.drawable.ic_action_down2);
                    }
                }
            });

            holder.mCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    c.moveToPosition(holder.cursorPosition);
                    Bundle args = new Bundle();
                    args.putInt(ctx.getResources().getString(R.string.db_table_id), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_id))));
                    args.putString(ctx.getResources().getString(R.string.db_table_org_name), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_name))));
                    args.putString(ctx.getResources().getString(R.string.db_table_org_address), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_address))));
                    args.putString(ctx.getResources().getString(R.string.db_table_org_slug_id), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_slug_id))));
                    args.putString(ctx.getResources().getString(R.string.db_table_org_phone), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_phone))));
                    args.putString(ctx.getResources().getString(R.string.db_table_org_email), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_email))));
                    args.putString(ctx.getResources().getString(R.string.db_table_org_website), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_website))));
                    args.putInt(ctx.getResources().getString(R.string.db_table_sync_done), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sync_done))));
                    args.putInt(ctx.getResources().getString(R.string.db_table_org_status), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_status))));
                    mCallBack.launchFragment(holder.cursorPosition,args,1);
                }
            });
        }
    }

    private class ViewHolder{
        public LinearLayout mCard;
        public LinearLayout mInnerElements;
        public ImageView imgDown;
        public TextView mOrganisation;
        public TextView mAddress;
        public TextView mNumber;
        public TextView mEmail;
        public TextView mWebsite;
        public Animation animSideDown,animSlideUp;
        public int cursorPosition;
    }
}
