package com.roamtech.android.sams.services;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.providers.FunnelProvider;

import java.util.Calendar;

/**
 *
 * This class creates alarms to go of at set times and carry out certain actions
 *
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 *
 * @author Dennis Mwangi Karuri
 */
public class AlarmService extends IntentService {

    private static final String TAG = "AlarmService";
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;
    public static final int NOTIFICATION_ID = 1;
    int notification;
    public AlarmService(){
        super("AlarmService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String activityScheduled = intent.getStringExtra(this.getString(R.string.db_table_act_schedule));
        notification = intent.getIntExtra(this.getString(R.string.extra_activity_id),1);
//        Log.d(TAG, activityScheduled);
       // insertActivity(notification);
        sendNotification(activityScheduled, notification);
    }

    private void execute() {
        Intent i;
        PendingIntent pi;
        AlarmManager am = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        Cursor c;
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "16");
        c = getContentResolver().query(data, null, null,
                new String[]{}, null);
    }


    /**
     *
     *  Post a notification indicating whether an activity should be done.
     *
     * @param msg Message which will be displayed on the notification
     * @param notification notification Id which uniquely identifies each notification
     */

    private void sendNotification(String msg,int notification) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);
        //Notification.DEFAULT_LIGHTS | Notification.FLAG_AUTO_CANCEL
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(getString(R.string.db_table_activities_id),notification);
        Log.d(TAG,"notification id is" +notification);
        intent.putExtra(getString(R.string.db_table_act_schedule),msg);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.appicon)
                        .setContentTitle(this.getString(R.string.notification_title))
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setDefaults(Notification.DEFAULT_VIBRATE)
                        .setDefaults(Notification.DEFAULT_SOUND)
                        .setAutoCancel(true)
                        .setOngoing(true)
                        .setContentText(msg);

        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(notification, mBuilder.build());
    }
    }

