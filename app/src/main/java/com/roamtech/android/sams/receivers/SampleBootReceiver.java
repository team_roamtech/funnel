package com.roamtech.android.sams.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.fragments.AddActivitiesFragment;
import com.roamtech.android.sams.providers.FunnelProvider;

import java.util.ArrayList;

/**
 * This BroadcastReceiver automatically (re)starts the alarm when the device is
 * rebooted. This receiver is set to be disabled (android:enabled="false") in the
 * application's manifest file. When the user sets the alarm, the receiver is enabled.
 * When the user cancels the alarm, the receiver is disabled, so that rebooting the
 * device will not trigger this receiver.
 *
 * Created by dennis on 5/11/15.
 * @author Dennis Mwangi Karuri
 * @see BroadcastReceiver
 */

public class SampleBootReceiver extends BroadcastReceiver {
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    ArrayList<Integer> dates = new ArrayList<Integer>();
    AlarmReceiver alarm = new AlarmReceiver();
    AddActivitiesFragment addActivitiesFragment;
    Cursor cursor;

    /**
     *
     * Constructor
     * Initializes the AddActivitiesFragment
     */
    public SampleBootReceiver(){
        addActivitiesFragment = new AddActivitiesFragment();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED"))        {
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"16");
            cursor = context.getContentResolver().query(data, null, null,
                new String[] {}, null);

            //Reset all the alarms on Device restart
            while (cursor.moveToNext()) {
                alarm.setAlarm(context,addActivitiesFragment.getDateFromString(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_schedule)))),cursor.getInt(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_id))),cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_type))));
            }
        }
    }
}

