package com.roamtech.android.sams.sync_adapters;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.preference.PreferenceManager;
import android.util.Log;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.async_tasks.CheckStatusTask;
import com.roamtech.android.sams.databases.FunnelDB;
import com.roamtech.android.sams.http.RestClient;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.services.RegistrationIntentService;
import com.roamtech.android.sams.utils.GetEnumValue;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dennis on 3/10/15.
 * An adapter which handles the uploading of the data from the DB to the server and handles the response
 * @author Dennis Mwangi Karuri
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    public static final String TAG = "SyncAdapter";

    private final ContentResolver mContentResolver;
    Context context;
    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
    Cursor cursor;
    String[] databaseTablesArray;
    String username;
    private SharedPreferences prefs;
    GetEnumValue getEnumValue;

    /**
     * Constructor. Obtains handle to content resolver for later use.
     * @param context The context that requires the sync to be done
     * @param autoInitialize boolean that auto initializes the adapter in the super constructor
     */
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        Log.d(TAG, "On Perform Sync 1");
        mContentResolver = context.getContentResolver();
        this.context = context;
        databaseTablesArray = context.getResources().getStringArray(R.array.database_tables);
        getEnumValue = new GetEnumValue(this.context);
        prefs=PreferenceManager
                .getDefaultSharedPreferences(context);
    }

    /**
     * Constructor. Obtains handle to content resolver for later use.
     * @param context The context that requires the sync to be done
     * @param autoInitialize boolean that auto initializes the adapter in the super constructor
     * @param allowParallelSyncs boolean that allows parallel sync in the super constructor
     */
    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        Log.d(TAG, "On Perform Sync 2");
        this.context = context;
        mContentResolver = context.getContentResolver();
        prefs=PreferenceManager
                .getDefaultSharedPreferences(context);
    }

    /**
     * Initializes the Shared preferences and gets the username to be used to sync data
     * @param prefs The apps SharedPreferences to get the username
     * @param context The app context that requires the username
     * @return String of the username
     */
    public static String initPrefs(SharedPreferences prefs,Context context) {
        String username;
        prefs=PreferenceManager
                .getDefaultSharedPreferences(context);
        username=prefs.getString(context.getResources().getString(R.string.bundle_username), null);
        return username;
    }



    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
       //Checks whether the username is saved and the account is activated
        if(prefs.contains(context.getResources().getString(R.string.bundle_username)) & prefs.getBoolean(MainModel.ACCOUNT_ACTIVATED,false) == true) {
            //Checks whether the account is activated in the server and whether the phone has been registered
            if(checkStatus().equals(MainModel.ACTIVE)) {
                        boolean sentToken = prefs
                                .getBoolean(MainModel.SENT_TOKEN_TO_SERVER, false);
                        if (sentToken) {
                            int registeredVersion = prefs.getInt(MainModel.PROPERTY_APP_VERSION, 1240);
                            int currentVersion = MainModel.getAppVersion(context);
                            if (registeredVersion != currentVersion) {
                                Intent intent = new Intent(context, RegistrationIntentService.class);
                                context.startService(intent);
                            } else {
                                doSync(provider);
                                Log.d(TAG, " On Perform Sync ");
                            }
                        } else {
                            Log.d(TAG, "No token present");
                            // Start IntentService to register this application with GCM.
                            Intent intent = new Intent(context, RegistrationIntentService.class);
                            context.startService(intent);
                        }
                    } else if (checkStatus().equals(MainModel.INACTIVE)) {
                        MainModel.activateAccount(prefs, false);
                    }
                }
    }

    /**
     *
     * Checks whether this account is activated in the server
     * @return String with the status of the account
     */
    public String checkStatus(){
        String status =  MainModel.ACTIVE;
        String response = CheckStatusTask.statusResponse(prefs.getString(context.getResources().getString(R.string.bundle_username),null),context);
        status = CheckStatusTask.parseJson(response,context,MainModel.ACTIVE);
        Log.d(TAG, " Status is "+status);
        return status;
    }

    /**
     * Send data by looping through an array of all tables in the DB and picking the data that has been edited and sending
     * the data to the server
     * @param provider Content provider to get the data from the tables in the DB to be passed to sendData method
     */
    public void doSync(ContentProviderClient provider){
        int counter = 0;
        for(int i = 0;i < databaseTablesArray.length; i++){
            Log.d(TAG,databaseTablesArray[i]);
            if(sendData(provider,databaseTablesArray[i])){
                counter++;
            }
            Log.d(TAG,"counter tally = "+String.valueOf(counter));
        }
    }

    /**
     * Gets from the tables in the DB
     * @param provider Content provider to get the data from the tables in the DB to be passed to sendData method
     * @param table The string of the table which data will be gotten from
     * @return
     */
    public boolean sendData(ContentProviderClient provider,String table){
        String json = "{}";
        String requestEndPoint = table;
        String jsonData = null;
        boolean result = false;
        Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"36");

        if(requestEndPoint.equals(databaseTablesArray[0])) {
            //Gets data from organisations table and creates a json string to be sent to the Server
        try {
            cursor = provider.query(data_id, null, null, new String[]{"organisations"}, null);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        Log.d(TAG,"..............."+ cursor.getCount());
        if(cursor != null && cursor.getCount() > 0) {
            jsonData = orgJsonData(cursor);
            Log.d(TAG,"----------"+jsonData);
            Log.d(TAG,requestEndPoint);
            json = syncData(context.getResources().getString(R.string.name_value_org),jsonData);
            result = parseJson(json);
            if(result){
                Log.d(TAG,"updated row "+updateTable("organisations",provider));
            }
        }
            //Gets data from contacts table and creates a json string to be sent to the Server
        }else if(requestEndPoint.equals(databaseTablesArray[1])){
            try {
                cursor = provider.query(data_id, null, null, new String[]{"contacts"}, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            Log.d(TAG,"............... "+ cursor.getCount());
            if(cursor != null && cursor.getCount() > 0) {
                jsonData = conJsonData(cursor);
                Log.d(TAG,"---------- con = "+jsonData);
                Log.d(TAG,requestEndPoint);
                json = syncData(context.getResources().getString(R.string.name_value_con),jsonData);
                result = parseJson(json);
                if(result){
                    Log.d(TAG,"updated row "+updateTable("contacts",provider));
                }
            }
            //Gets data from activities table and creates a json string to be sent to the Server
        }else if(requestEndPoint.equals(databaseTablesArray[2])){
            try {
                cursor = provider.query(data_id, null, null, new String[]{"activities"}, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            Log.d(TAG,"............... "+ cursor.getCount());
            if(cursor != null && cursor.getCount() > 0) {
                jsonData = actJsonData(cursor);
                Log.d(TAG,"---------- act = "+jsonData);
                Log.d(TAG,requestEndPoint);
                json = syncData(context.getResources().getString(R.string.name_value_act),jsonData);
                result = parseJson(json);
                if(result){
                    Log.d(TAG,"updated row "+updateTable("activities",provider));
                }
            }
            //Gets data from sales table and creates a json string to be sent to the Server
        }else if(requestEndPoint.equals(databaseTablesArray[3])){
            try {
                cursor = provider.query(data_id, null, null, new String[]{"sales"}, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            Log.d(TAG,"............... "+ cursor.getCount());
            if(cursor != null && cursor.getCount() > 0) {
                jsonData = saleJsonData(cursor);
                Log.d(TAG,"---------- sale = "+jsonData);
                Log.d(TAG,requestEndPoint);
                json = syncData(context.getResources().getString(R.string.name_value_sale),jsonData);
                result = parseJson(json);
                if(result){
                    Log.d(TAG,"updated row "+updateTable("sales",provider));
                }
            }
            //Gets data from quotations table and creates a json string to be sent to the Server
        }else if(requestEndPoint.equals(databaseTablesArray[4])){
            try {
                cursor = provider.query(data_id, null, null, new String[]{"quotations"}, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            Log.d(TAG,"............... "+ cursor.getCount());
            if(cursor != null && cursor.getCount() > 0) {
                jsonData = quotationsJsonData(cursor);
                Log.d(TAG,"---------- quotations = "+jsonData);
                Log.d(TAG,requestEndPoint);
                json = syncData(context.getResources().getString(R.string.name_value_quo),jsonData);
                result = parseJson(json);
                if(result){
                    Log.d(TAG,"updated row "+updateTable("quotations",provider));
                }
            }
            //Gets data from items table and creates a json string to be sent to the Server
        }else if(requestEndPoint.equals(databaseTablesArray[5])){
            try {
                cursor = provider.query(data_id, null, null, new String[]{"items"}, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            Log.d(TAG,"............... "+ cursor.getCount());
            if(cursor != null && cursor.getCount() > 0) {
                jsonData = itemsJsonData(cursor);
                Log.d(TAG,"---------- items = "+jsonData);
                Log.d(TAG,requestEndPoint);
                json = syncData(context.getResources().getString(R.string.name_value_item),jsonData);
                result = parseJson(json);
                if(result){
                    Log.d(TAG,"updated row "+updateTable("items",provider));
                }
            }
            //Gets data from comments table and creates a json string to be sent to the Server
        }/*else if(requestEndPoint.equals(databaseTablesArray[6])){
            try {
                cursor = provider.query(data_id, null, null, new String[]{"comments"}, null);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
            Log.d(TAG,"............... "+ cursor.getCount());
            if(cursor != null && cursor.getCount() > 0) {
                jsonData = commentsJsonData(cursor);
                Log.d(TAG,"---------- items = "+jsonData);
                Log.d(TAG,requestEndPoint);
                json = syncData(context.getResources().getString(R.string.name_value_com),jsonData);
                result = parseJson(json);
                if(result){
                    Log.d(TAG,"updated row "+updateTable("comments",provider));
                }
            }
        }*/
        //Close cursor once its done
            try {
                if( cursor != null && !cursor.isClosed() )
                    cursor.close();
            } catch(Exception ex) {}
        return result;
    }


    /**
     *
     * Check whether the database exists and is open
     *
     * @return boolean true if it exists and is open else return boolean false
     */
    private boolean checkDataBase(){

        SQLiteDatabase checkDB = null;

        try{
            checkDB = SQLiteDatabase.openDatabase(FunnelDB.DB_PATH, null, SQLiteDatabase.OPEN_READONLY);
        } catch(SQLiteException e){
            //database doesn't exist yet
        }

        if(checkDB != null){
            checkDB.close();
        }

        return checkDB != null ? true : false;
    }

    /**
     *
     * Used to extract the result from the Json response sent from the data
     * @param jsonData Json String the response gotten from server once data is sent
     * @return boolean true if the Json Objects has a success response
     */
    public boolean parseJson(String jsonData) {
        boolean success = false;
        if (jsonData != null){
            try {
                Log.d(TAG,jsonData.toString());
                JSONObject jObject = new JSONObject(jsonData);
                if (jObject.has(context.getResources().getString(R.string.status_number_success)) && !jObject.isNull(context.getResources().getString(R.string.status_number_success))) {
                    success = true;
                } else {
                    success = false;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }
        return success;
    }

    /**
     * Updates table once the data has been sent to the server
     *
     * @param table String of the table which is updated once the data has been uploaded
     * @param provider The content provider to update tables
     * @return Integer to indicate that the table has been updated
     */
    public int updateTable(String table,ContentProviderClient provider){
        ContentValues initialValues = new ContentValues();
        initialValues.put(context.getResources().getString(R.string.db_table_sync_done), 0);
        Uri data_id2 = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "37");
        try {
            return provider.update(data_id2, initialValues, null, new String[]{table});
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     *
     * Sends data to the server from the DB
     *
     * @param requestEndPoint End point for the url to send the table data
     * @param jsonData The data to be sent from the table
     * @return The server response
     */
    public String syncData(String requestEndPoint,String jsonData){
        nameValuePairs.add(new BasicNameValuePair(context.getResources().getString(R.string.name_value_request),requestEndPoint));
        nameValuePairs.add(new BasicNameValuePair(context.getResources().getString(R.string.name_value_data), jsonData));

        String json = "{}";
        try {
            json = RestClient.makeRestRequest(RestClient.POST, MainModel.SYNC_URL, nameValuePairs);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }

        return json;
    }

    /**
     *
     * Creates a Json String of the organisations data from the Organisations table
     *
     * @param cursor Contains the data gotten from the Organisations table
     * @return Json String of the organisations data
     */
    public String orgJsonData(Cursor cursor){
        JSONArray jsonArray = new JSONArray();
        while(cursor.moveToNext()) {
            JSONObject org = new JSONObject();
            try {
                org.put(context.getResources().getString(R.string.user_id), initPrefs(prefs,this.context));
                org.put(context.getResources().getString(R.string.db_table_org_name), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_org_name))));
                org.put(context.getResources().getString(R.string.db_table_org_address), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_org_address))));
                org.put(context.getResources().getString(R.string.db_table_org_status), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_org_status))));
                org.put(context.getResources().getString(R.string.db_table_org_slug_id), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_org_slug_id))));
                org.put(context.getResources().getString(R.string.name_value_latitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_org_latitude))));
                org.put(context.getResources().getString(R.string.name_value_longitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_org_longitude))));
                org.put(context.getResources().getString(R.string.transaction_org), "update");

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(org);
        }
        return jsonArray.toString();
    }

    /**
     *
     * Creates a Json String of the Contacts data from the Contacts table
     *
     * @param cursor Contains the data gotten from the Contacts table
     * @return Json String of the Contacts data
     */
    public String conJsonData(Cursor cursor) {
        JSONArray jsonArray = new JSONArray();
        while (cursor.moveToNext()) {
            JSONObject con = new JSONObject();
            try {
                con.put(context.getResources().getString(R.string.user_id), initPrefs(prefs,this.context));
                con.put(context.getResources().getString(R.string.db_table_con_name), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_name))));
                con.put(context.getResources().getString(R.string.db_table_con_phone), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_phone))));
                con.put(context.getResources().getString(R.string.db_table_con_email), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_email))));
                con.put(context.getResources().getString(R.string.db_table_con_website), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_website))));
                con.put(context.getResources().getString(R.string.db_table_con_source), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_source))));
                con.put(context.getResources().getString(R.string.db_table_con_address), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_address))));
                con.put(context.getResources().getString(R.string.db_table_con_foreign_key), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_foreign_key))));
                con.put(context.getResources().getString(R.string.db_table_con_status), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_status))));
                con.put(context.getResources().getString(R.string.db_table_con_model), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_model))));
                con.put(context.getResources().getString(R.string.db_table_con_slug_id), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_slug_id))));
                con.put(context.getResources().getString(R.string.name_value_latitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_latitude))));
                con.put(context.getResources().getString(R.string.name_value_longitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_longitude))));
                con.put(context.getResources().getString(R.string.transaction_con), "update");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(con);
        }
        return jsonArray.toString();
    }

    /**
     *
     * Creates a Json String of the activities data from the activities table
     *
     * @param cursor Contains the data gotten from the activities table
     * @return Json String of the activities data
     */
    public String actJsonData(Cursor cursor){
        JSONArray jsonArray = new JSONArray();
        while (cursor.moveToNext()) {
            JSONObject act = new JSONObject();
            try {
                act.put(context.getResources().getString(R.string.user_id), initPrefs(prefs,this.context));
                act.put(context.getResources().getString(R.string.db_table_act_slug_id), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_slug_id))));
                act.put(context.getResources().getString(R.string.db_table_act_type), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_type))));
                act.put(context.getResources().getString(R.string.db_table_act_description), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_description))));
                act.put(context.getResources().getString(R.string.db_table_act_schedule), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_schedule))));
                act.put(context.getResources().getString(R.string.db_table_act_feedback), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_feedback))));
                act.put(context.getResources().getString(R.string.db_table_act_status), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_status))));
                act.put(context.getResources().getString(R.string.db_table_act_model), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_model))));
                act.put(context.getResources().getString(R.string.db_table_act_foreign_key), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_foreign_key))));
                act.put(context.getResources().getString(R.string.name_value_latitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_latitude))));
                act.put(context.getResources().getString(R.string.name_value_longitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_act_longitude))));
                act.put(context.getResources().getString(R.string.transaction_act), "update");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(act);
        }
        return jsonArray.toString();
    }

    /**
     *
     * Creates a Json String of the sales data from the sales table
     *
     * @param cursor Contains the data gotten from the sales table
     * @return Json String of the sales data
     */
    public String saleJsonData(Cursor cursor){
        JSONArray jsonArray = new JSONArray();
        while (cursor.moveToNext()) {
            JSONObject sale = new JSONObject();
            try {
                sale.put(context.getResources().getString(R.string.user_id), initPrefs(prefs,this.context));
                sale.put(context.getResources().getString(R.string.db_table_sale_stage), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_stage))));
                sale.put(context.getResources().getString(R.string.db_table_sale_name), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_name))));
                sale.put(context.getResources().getString(R.string.db_table_sale_status), getEnumValue.getString(cursor.getInt(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_status)))));
                Log.d(TAG, " sale status  = " + cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_status))));
                sale.put(context.getResources().getString(R.string.db_table_sale_cost), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_cost))));
                sale.put(context.getResources().getString(R.string.db_table_sale_description), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_description))));
                sale.put(context.getResources().getString(R.string.db_table_sale_payment_type), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_payment_type))));
                sale.put(context.getResources().getString(R.string.db_table_sale_slug_id), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_slug_id))));
                sale.put(context.getResources().getString(R.string.db_table_sale_foreign_key), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_foreign_key))));
                sale.put(context.getResources().getString(R.string.db_table_sale_model), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_model))));
                sale.put(context.getResources().getString(R.string.name_value_latitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_latitude))));
                sale.put(context.getResources().getString(R.string.name_value_longitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_sale_longitude))));
                sale.put(context.getResources().getString(R.string.transaction_sale), "update");

            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(sale);
        }
        return jsonArray.toString();
    }

    /**
     *
     * Creates a Json String of the quotations data from the quotations table
     *
     * @param cursor Contains the data gotten from the quotations table
     * @return Json String of the quotations data
     */
    public String quotationsJsonData(Cursor cursor){
        JSONArray jsonArray = new JSONArray();
        while (cursor.moveToNext()) {
            JSONObject quo = new JSONObject();
            try {
                quo.put(context.getResources().getString(R.string.user_id), initPrefs(prefs,this.context));
                quo.put(context.getResources().getString(R.string.db_table_qu_name), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_name))));
                quo.put(context.getResources().getString(R.string.db_table_qu_sale_id), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_sale_id))));
                quo.put(context.getResources().getString(R.string.db_table_qu_status), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_status))));
                quo.put(context.getResources().getString(R.string.db_table_qu_slug_id), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_slug_id))));
                quo.put(context.getResources().getString(R.string.name_value_latitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_latitude))));
                quo.put(context.getResources().getString(R.string.name_value_longitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_longitude))));
                quo.put(context.getResources().getString(R.string.db_table_qu_stage), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_stage))));
                quo.put(context.getResources().getString(R.string.db_table_qu_payment_type), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_payment_type))));
                quo.put(context.getResources().getString(R.string.db_table_qu_delivered), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_delivered))));
                quo.put(context.getResources().getString(R.string.db_table_qu_lpo), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_lpo))));
                quo.put(context.getResources().getString(R.string.db_table_qu_amount_collected), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_amount_collected))));
                quo.put(context.getResources().getString(R.string.transaction_qu), "update");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(quo);
        }
        return jsonArray.toString();
    }

    /**
     *
     * Creates a Json String of the items data from the items table
     *
     * @param cursor Contains the data gotten from the items table
     * @return Json String of the items data
     */
    public String itemsJsonData(Cursor cursor){
        JSONArray jsonArray = new JSONArray();
        while (cursor.moveToNext()) {
            JSONObject items = new JSONObject();
            try {
                items.put(context.getResources().getString(R.string.user_id), initPrefs(prefs,this.context));
                items.put(context.getResources().getString(R.string.db_table_item_qu_slug_id), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_item_qu_slug_id))));
                items.put(context.getResources().getString(R.string.db_table_item_name), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_item_name))));
                items.put(context.getResources().getString(R.string.db_table_item_quantity),cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_item_quantity))));
                items.put(context.getResources().getString(R.string.db_table_item_description), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_item_description))));
                items.put(context.getResources().getString(R.string.db_table_item_unit_price), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_item_unit_price))));
                items.put(context.getResources().getString(R.string.db_table_item_status), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_item_status))));
                items.put(context.getResources().getString(R.string.db_table_item_slug_id), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_item_slug_id))));
                items.put(context.getResources().getString(R.string.name_value_latitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_item_latitude))));
                items.put(context.getResources().getString(R.string.name_value_longitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_item_longitude))));
                items.put(context.getResources().getString(R.string.transaction_items), "update");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(items);
        }
        return jsonArray.toString();
    }

    /**
     *
     * Creates a Json String of the comments data from the comments table
     *
     * @param cursor Contains the data gotten from the comments table
     * @return Json String of the comments data
     */
    public String commentsJsonData(Cursor cursor){
        JSONArray jsonArray = new JSONArray();
        while (cursor.moveToNext()) {
            JSONObject comments = new JSONObject();
            try {
                comments.put(context.getResources().getString(R.string.user_id), initPrefs(prefs,this.context));
                comments.put(context.getResources().getString(R.string.db_table_cm_slug_id), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_cm_slug_id))));
                comments.put(context.getResources().getString(R.string.db_table_cm_title), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_cm_title))));
                comments.put(context.getResources().getString(R.string.db_table_cm_issue),cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_cm_issue))));
                comments.put(context.getResources().getString(R.string.db_table_cm_description), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_cm_description))));
                comments.put(context.getResources().getString(R.string.db_table_cm_model), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_cm_model))));
                comments.put(context.getResources().getString(R.string.db_table_cm_foreign_key), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_cm_foreign_key))));
                comments.put(context.getResources().getString(R.string.name_value_latitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_cm_latitude))));
                comments.put(context.getResources().getString(R.string.name_value_longitude), cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_cm_longitude))));
                comments.put(context.getResources().getString(R.string.transaction_items), "update");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(comments);
        }
        return jsonArray.toString();
    }
}
