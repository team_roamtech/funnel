package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.interfaces.ItemActions;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.models.MainModel;

/**
 * Created by Karuri on 2/22/2015.
 */
public class QuotationItemsAdapter extends CursorAdapter {
    public static final String TAG = "QuotationItemsAdapter";
    private LayoutInflater vi;
    Cursor c;
    Context ctx;
    ViewHolder holder;
    LaunchFragment mCallBack;
    Fragment fragment;

    public QuotationItemsAdapter(Context context, Cursor c, int flags, Fragment fragment) {
        super(context, c, flags);
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.ctx = context;
        this.c = c;
        Log.d(TAG, "QuotationItemsAdapter constructor");
        try {
            mCallBack = (LaunchFragment) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View rootView = vi.inflate(R.layout.list_quotation_item, parent, false);
        ViewHolder holder  =   new ViewHolder();
        holder.lytQuotationItems = (LinearLayout) rootView.findViewById(R.id.lyt_quotation);
        holder.txtItemName = (TextView) rootView.findViewById(R.id.txt_item_name);
        holder.txtQuantity = (TextView) rootView.findViewById(R.id.txt_quantity);
        holder.txtUnitPrice = (TextView) rootView.findViewById(R.id.txt_price);
        holder.txtTotal = (TextView) rootView.findViewById(R.id.txt_total);
        holder.imgDelete = (ImageView) rootView.findViewById(R.id.btn_cancel);
        rootView.setTag(holder);
        return rootView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final Cursor c = cursor;
        if(view != null) {
            final ViewHolder holder = (ViewHolder) view.getTag();
            holder.txtItemName.setText(c.getString(cursor.getColumnIndex(ctx.getResources().getString(R.string.db_table_item_name))));
            holder.txtQuantity.setText(MainModel.doubleToStringNoDecimal(c.getDouble(cursor.getColumnIndex(ctx.getResources().getString(R.string.db_table_item_quantity)))));
            holder.txtUnitPrice.setText(MainModel.doubleToStringNoDecimal(c.getDouble(cursor.getColumnIndex(ctx.getResources().getString(R.string.db_table_item_unit_price)))));
            holder.txtTotal.setText(MainModel.doubleToStringNoDecimal(c.getDouble(cursor.getColumnIndex(ctx.getResources().getString(R.string.txt_total)))));
            holder.cursorPosition = c.getPosition();
            holder.lytQuotationItems.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    c.moveToPosition(holder.cursorPosition);
                    Bundle args = new Bundle();
                    args.putInt(ctx.getResources().getString(R.string.db_table_id),c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_id))));
                    args.putString(ctx.getResources().getString(R.string.db_table_item_qu_slug_id),c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_item_qu_slug_id))));
                    args.putString(ctx.getResources().getString(R.string.db_table_item_name),c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_item_name))));
                    args.putDouble(ctx.getResources().getString(R.string.db_table_item_quantity),c.getDouble(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_item_quantity))));
                    args.putDouble(ctx.getResources().getString(R.string.db_table_item_unit_price),c.getDouble(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_item_unit_price))));
                    args.putString(ctx.getResources().getString(R.string.db_table_item_description),c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_item_description))));
                    args.putString(ctx.getResources().getString(R.string.db_table_item_slug_id),c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_item_slug_id))));
                    args.putInt(ctx.getResources().getString(R.string.db_table_item_status), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_item_status))));
                    args.putInt(ctx.getResources().getString(R.string.db_table_sync_done),c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sync_done))));
                    mCallBack.launchFragment(holder.cursorPosition,args,1);
                }
            });
        }
    }

    static class ViewHolder {
        LinearLayout lytQuotationItems;
        TextView txtItemName;
        TextView txtQuantity;
        TextView txtUnitPrice;
        TextView txtTotal;
        ImageView imgDelete;
        public int cursorPosition;
    }
}
