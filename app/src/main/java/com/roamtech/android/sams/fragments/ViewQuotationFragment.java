package com.roamtech.android.sams.fragments;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.content.CursorLoader;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.adapters.QuotationItemsAdapter;
import com.roamtech.android.sams.dialog_fragments.CollectedCashDialogFragment;
import com.roamtech.android.sams.dialog_fragments.DeleteDialog;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.interfaces.LaunchFragment;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import org.w3c.dom.Text;

import java.util.Calendar;

/**
 *
 * Fragments that display Quotation Items allows removing of items from the Quotations
 * It also allows the user to email their quotation to a client
 *
 * Created by Karuri on 2/23/2015.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass
 */
public class ViewQuotationFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,LaunchFragment,View.OnClickListener {
    public static final String TAG = "ViewQuotationFragment";
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    public static final int LOADER = 1234;
    ListView lstItems;
    Cursor c;
    ProgressBar mProgresBar;
    ImageButton ivAdd,ivAdd2;
    View headerView;
    Toolbar mActionBarToolbar;
    String quotationSlug;
    int saleID;
    String saleSlugID;
    int quotationID;
    String dateCreated;
    int status = 1,syncDone;
    String quotationName,contactName,organisationName,saleName,quotationStage,quotationPaymentType;
    TextView txtQuotationName,txtContactName,txtOrganisationName,txtNumberOfItems;
    TextView txtInvoice,txtCollected,txtPending;
    LinearLayout lytLPOReceived,lytDeliveryComplete,lytCloseDetails;
    QuotationItemsAdapter mAdapter;
    AddQuotationsItemsFragment addQuotationsItemsFragment;
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    GetUsername getUsername;
    DeleteDialog newFragment6;
    String [] arrayMainItems;
    int mFirstVisibleItem;
    Button btnCloseQuote,btnCollectQuote,btnAmountCollected;
    String [] arrayStages;
    AddActivitiesFragment addActivitiesFragment;
    CommentsFragment commentsFragment;
    Spinner spnPayments;
    LinearLayout lytAmounts;
    TextView txtLPOHeading,txtDeliveryHeading;
    RadioButton rdLpoYes,rdLpoNo,rdDeliveredYes,rdDeliveredNo;
    CollectedCashDialogFragment newFragment8;
    int deliveryStatus,lpoStatus;
    Double amountCollected = 0.0;
    String totalCost;
    String contactEmail;
    String emailString = " ";



    public ViewQuotationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;

    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mainActivity = new MainActivity();
        commonHash = new CommonHash(mActivity);
        arrayMainItems = getResources().getStringArray(R.array.main_activity_items);
        arrayStages = getResources().getStringArray(R.array.pager_title_items2);
        Bundle args = getArguments();
        if(args != null){
            quotationID = args.getInt(mActivity.getResources().getString(R.string.db_table_id));
            saleID = args.getInt(mActivity.getResources().getString(R.string.db_table_sales_id));
            saleSlugID = args.getString(mActivity.getResources().getString(R.string.db_table_qu_sale_id));
            quotationSlug = args.getString(mActivity.getResources().getString(R.string.db_table_qu_slug_id));
            dateCreated = args.getString(mActivity.getResources().getString(R.string.db_table_qu_created));
            status = args.getInt(mActivity.getResources().getString(R.string.db_table_qu_status));
            syncDone = args.getInt(mActivity.getResources().getString(R.string.db_table_sync_done));
            quotationName = args.getString(mActivity.getResources().getString(R.string.db_table_qu_name));
            quotationStage = args.getString(mActivity.getResources().getString(R.string.db_table_qu_stage));
            contactName = args.getString(mActivity.getResources().getString(R.string.db_table_con_name));
            contactEmail = args.getString(mActivity.getResources().getString(R.string.db_table_con_email));
            organisationName = args.getString(mActivity.getResources().getString(R.string.db_table_org_name));
            saleName = args.getString(mActivity.getResources().getString(R.string.db_table_sale_name));
            deliveryStatus = Integer.parseInt(args.getString(mActivity.getResources().getString(R.string.db_table_qu_delivered)));
            lpoStatus = Integer.parseInt(args.getString(mActivity.getResources().getString(R.string.db_table_qu_lpo)));
            amountCollected = Double.parseDouble(args.getString(mActivity.getResources().getString(R.string.db_table_qu_amount_collected)));
            quotationPaymentType = args.getString(mActivity.getResources().getString(R.string.db_table_qu_payment_type));
            Log.d(TAG,"sale name "+saleName);
        }
        newFragment8 = new CollectedCashDialogFragment();
        newFragment6 = new DeleteDialog();
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        getUsername = new GetUsername(mActivity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add_quotations2, container, false);
        mActionBarToolbar = (Toolbar) mActivity.findViewById(R.id.toolbar_actionbar);
        headerView = mActivity.getLayoutInflater().inflate(R.layout.listview_quotation_header, null);
        lstItems = (ListView) rootView.findViewById(R.id.lst_contacts);
        mProgresBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        mActionBarToolbar = (Toolbar) mActivity.findViewById(R.id.toolbar_actionbar);
        lstItems.addHeaderView(headerView, null, false);
        lytLPOReceived = (LinearLayout) rootView.findViewById(R.id.lyt_lpo_received);
        lytDeliveryComplete = (LinearLayout) rootView.findViewById(R.id.lyt_delivery_complete);
        lytCloseDetails = (LinearLayout) rootView.findViewById(R.id.lyt_close_details);
        txtLPOHeading = (TextView) lytLPOReceived.findViewById(R.id.txt_delivery_complete);
        txtLPOHeading.setText(mActivity.getResources().getString(R.string.txt_lpo_received));
        txtDeliveryHeading = (TextView) lytDeliveryComplete.findViewById(R.id.txt_delivery_complete);
        txtDeliveryHeading.setText(mActivity.getResources().getString(R.string.txt_delivery_complete));
        rdLpoYes = (RadioButton) lytLPOReceived.findViewById(R.id.rdb_delivered_yes);
        rdLpoNo = (RadioButton) lytLPOReceived.findViewById(R.id.rdb_no);
        rdDeliveredYes = (RadioButton) lytDeliveryComplete.findViewById(R.id.rdb_delivered_yes);
        rdDeliveredNo = (RadioButton) lytDeliveryComplete.findViewById(R.id.rdb_no);
        txtQuotationName = (TextView) rootView.findViewById(R.id.txt_quotation_name);
        txtQuotationName.setText(quotationName);
        txtContactName = (TextView) rootView.findViewById(R.id.txt_contact_name);
        txtContactName.setText(contactName);
        txtInvoice = (TextView) rootView.findViewById(R.id.txt_invoiced);
        txtCollected = (TextView) rootView.findViewById(R.id.txt_collected);
        txtPending = (TextView) rootView.findViewById(R.id.txt_pending);
        lytAmounts = (LinearLayout) rootView.findViewById(R.id.lyt_amounts);
        txtOrganisationName = (TextView) rootView.findViewById(R.id.txt_organisation_name);
        if(organisationName != null && !organisationName.equals(mActivity.getResources().getString(R.string.db_table_org_entry))) {
            txtOrganisationName.setText(organisationName);
        }
        txtNumberOfItems = (TextView) rootView.findViewById(R.id.txt_number_of_items);
        mAdapter = new QuotationItemsAdapter(mActivity,null,0,this);
        lstItems.setAdapter(mAdapter);
        lstItems.setOnScrollListener(new AbsListView.OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                if (scrollState == SCROLL_STATE_TOUCH_SCROLL || scrollState == SCROLL_STATE_FLING) {
                    Log.d(TAG, "Scrolling");
                    if (mFirstVisibleItem != 0) {
                        //Change Background colour for toolbar
                        mActionBarToolbar.setBackgroundResource(R.color.toolbar_quotations_green);
                    }
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                Log.d(TAG, "First Visible Item " + String.valueOf(firstVisibleItem));
                mFirstVisibleItem = firstVisibleItem;
                //Change Background colour for toolbar
                if (firstVisibleItem == 0 && MainModel.listIsAtTop(lstItems)) {
                    mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
                } else {
                    mActionBarToolbar.setBackgroundResource(R.color.toolbar_quotations_green);
                }
            }
        });
        ivAdd = (ImageButton) rootView.findViewById(R.id.fab_button);
        ivAdd.setOnClickListener(this);
        ivAdd2 = (ImageButton) rootView.findViewById(R.id.fab_button2);
        ivAdd2.setOnClickListener(this);
        btnCloseQuote = (Button) rootView.findViewById(R.id.btn_close_quote);
        btnCloseQuote.setOnClickListener(this);
        btnCollectQuote = (Button) rootView.findViewById(R.id.btn_collect_quote);
        btnCollectQuote.setOnClickListener(this);
        btnAmountCollected = (Button) rootView.findViewById(R.id.btn_amount_collected);
        btnAmountCollected.setOnClickListener(this);
        spnPayments = (Spinner) rootView.findViewById(R.id.spn_payments);
        spnPayments.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //Get the selected value
                quotationPaymentType = spnPayments.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });

        /**
         * Hide and display views based on the stage which the sale is at
         */
        if(arrayStages[1].equals(quotationStage)){
            btnCloseQuote.setVisibility(View.VISIBLE);
            btnCollectQuote.setVisibility(View.GONE);
        }else if(arrayStages[2].equals(quotationStage)){
            btnCloseQuote.setVisibility(View.GONE);
            lytCloseDetails.setVisibility(View.VISIBLE);
            btnCollectQuote.setVisibility(View.VISIBLE);
        }else{
            lytAmounts.setVisibility(View.VISIBLE);
            ivAdd2.setVisibility(View.VISIBLE);
            ivAdd.setVisibility(View.GONE);
            spnPayments.setVisibility(View.GONE);
            btnCloseQuote.setVisibility(View.GONE);
            btnCollectQuote.setVisibility(View.GONE);
            btnAmountCollected.setVisibility(View.VISIBLE);
        }
        if(savedInstanceState != null){
            Log.d(TAG, "stalemate 1");

        }else{
            Log.d(TAG,"stalemate 2");
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }

        if(lpoStatus == 1){
            rdLpoYes.setChecked(true);
        }
        if(deliveryStatus == 1){
            rdDeliveredYes.setChecked(true);
        }

        rdLpoYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                lpoStatus = 1;
                insertQuotation(quotationStage);
            }
        });

        rdLpoNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                lpoStatus = 0;
                insertQuotation(quotationStage);
            }
        });

        rdDeliveredYes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                deliveryStatus = 1;
                insertQuotation(quotationStage);
            }
        });

        rdDeliveredNo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                deliveryStatus = 0;
                insertQuotation(quotationStage);
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_email).setVisible(true);
        menu.findItem(R.id.action_flag).setVisible(true);
        menu.findItem(R.id.action_delete).setVisible(true);
        menu.findItem(R.id.action_comments).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_delete:
                Bundle args = new Bundle();
                args.putString(mActivity.getResources().getString(R.string.txt_advisory),mActivity.getResources().getString(R.string.txt_advisory_delete) + " " +arrayMainItems[5]);
                newFragment6.setArguments(args);
                newFragment6.setTargetFragment(ViewQuotationFragment.this, MainModel.DIALOG_FRAGMENT_DELETE_DIALOG);
                newFragment6.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                Bundle args2 = new Bundle();
                args2.putString(mActivity.getResources().getString(R.string.db_table_org_name),organisationName);
                args2.putString(mActivity.getResources().getString(R.string.db_table_con_name),contactName);
                args2.putString(mActivity.getResources().getString(R.string.db_table_sale_name),saleName);
                args2.putString(mActivity.getResources().getString(R.string.db_table_qu_name),quotationName);
                args2.putString(mActivity.getResources().getString(R.string.db_table_act_model),mActivity.getResources().getString(R.string.db_table_model_4));
                args2.putString(mActivity.getResources().getString(R.string.db_table_act_foreign_key),quotationSlug);
                addActivitiesFragment.setArguments(args2);
                addActivitiesFragment.setTargetFragment(ViewQuotationFragment.this, MainModel.PAGER_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            case R.id.action_email:
                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto",contactEmail, null));
                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
                emailIntent.putExtra(Intent.EXTRA_TEXT, emailString);
                //emailIntent.putExtra(Intent.EXTRA_EMAIL, addresses);
                startActivity(Intent.createChooser(emailIntent, "Send email..."));
                break;
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                Bundle args3 = new Bundle();
                args3.putString(mActivity.getResources().getString(R.string.db_table_qu_name),quotationName);
                args3.putString(mActivity.getResources().getString(R.string.db_table_act_model),mActivity.getResources().getString(R.string.db_table_model_4));
                args3.putString(mActivity.getResources().getString(R.string.db_table_act_foreign_key),quotationSlug);
                commentsFragment.setArguments(args3);
                commentsFragment.setTargetFragment(ViewQuotationFragment.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        mActivity.getSupportActionBar().setTitle(arrayMainItems[5]);
    }

    @Override
    public void onPause() {
        super.onPause();
        mActionBarToolbar.setBackgroundResource(R.color.android_transparent);
    }


    /**
     *
     * Creates an Email String which will be used as the Email body
     *
     * @param c Cursor of data gotten from the DB to be emailed
     * @return String to be Emailed
     */
    public String emailString(Cursor c){

        c.moveToFirst();
        while(!c.isAfterLast()){
            Log.d(TAG, "++++++++++ " + c.getCount());
            emailString = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_item_name))) + " \n " +
                    mActivity.getResources().getString(R.string.edit_quantity)+" "+c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_item_quantity))) + " \n " +
                    mActivity.getResources().getString(R.string.edit_price)+" "+c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_item_unit_price)))+ " \n " +
                    mActivity.getResources().getString(R.string.txt_title_total)+" "+c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.txt_total)))+ " \n ";
            c.moveToNext();
        }
        c.close();
       // Log.d(TAG,emailString);
        emailString = emailString + mActivity.getResources().getString(R.string.txt_amount_invoiced)+ " "+totalCost;
        emailString = emailString.replace("\\\n", System.getProperty("line.separator"));
        return emailString;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == MainModel.DIALOG_FRAGMENT_DELETE_DIALOG ) {
            if (resultCode == Activity.RESULT_OK) {
                status = 2;
              //  updateStage(arrayStages[2],2);
                insertQuotation(quotationStage);
                mActivity.getSupportFragmentManager().popBackStackImmediate();
                Intent intent = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);

            } else {

            }
        }else if (requestCode == MainModel.PAGER_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
             //   mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
            }
        }
        else if (requestCode == MainModel.DIALOG_COLLECT_CASH) {
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    String strAmountCollected = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_qu_amount_collected));
                    Double amountEntered = Double.parseDouble(strAmountCollected);
                    amountCollected = amountEntered + amountCollected;
                    txtCollected.setText(MainModel.convertFromScientificNotation(amountCollected));
                    txtPending.setText(MainModel.convertFromScientificNotation(Double.parseDouble(totalCost) - amountCollected));
                    insertQuotation(quotationStage);
                    //mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
                }
            }
        }
    }

    @Override
    public void launchFragment(int position, Bundle bundle, int view) {
        addQuotationsItemsFragment = new AddQuotationsItemsFragment();
        addQuotationsItemsFragment.setArguments(bundle);
        addQuotationsItemsFragment.setTargetFragment(ViewQuotationFragment.this, MainModel.PAGER_FRAGMENT);
        mainActivity.addFragment(addQuotationsItemsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addQuotationsItemsFragment).getClass().getName());
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "23");
        return new CursorLoader(getActivity(),data, null, null,
                new String[] {String.valueOf(quotationSlug)}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        c = data;
        Log.d(TAG, "++++++++++ " + c.getCount());
        mAdapter.swapCursor(c);
        Uri data_id2 = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"29");
        Cursor cursor = mActivity.getContentResolver().query(data_id2, null, null, new String[]{String.valueOf(quotationSlug)}, null);
        if(cursor != null & cursor.getCount() > 0) {
            cursor.moveToFirst();
            txtNumberOfItems.setText(cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.query_name_item_count))));
            totalCost = cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.query_name_price_sum)));
            txtInvoice.setText(totalCost);
            txtCollected.setText(MainModel.convertFromScientificNotation(amountCollected));
            txtPending.setText(MainModel.convertFromScientificNotation(Double.parseDouble(totalCost) - amountCollected));
            c.moveToFirst();
            while(!c.isAfterLast()){
                Log.d(TAG, "++++++++++ " + c.getCount());
                emailString = emailString + c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_item_name))) + " \n " +
                        mActivity.getResources().getString(R.string.edit_quantity)+" "+c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_item_quantity))) + " \n " +
                        mActivity.getResources().getString(R.string.edit_price)+" "+c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_item_unit_price)))+ " \n " +
                        mActivity.getResources().getString(R.string.txt_title_total)+" "+c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.txt_total)))+ " \n "+
                " "+ " \n ";
                c.moveToNext();
            }
            emailString = emailString + mActivity.getResources().getString(R.string.txt_amount_invoiced)+ " "+totalCost;
            emailString = emailString.replace("\\\n", System.getProperty("line.separator"));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }

    /**
     *
     * Inserts or updates a record in the database
     *
     * @param stageName name of the stage to be inserted or updated in the DB
     * @return int id of the record inserted or updated by the DB
     */
    public int insertQuotation(String stageName){
        coordinates = getCoordinates.getCoordinates();
        int quotation_ID = 0;
        syncDone = 1;
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_sale_id), saleSlugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_slug_id), quotationSlug);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_created), dateCreated);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_status), status);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_delivered),deliveryStatus);
        Log.d(TAG, " Delivery status " + deliveryStatus);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_lpo), lpoStatus);
        Log.d(TAG, " lpo status " + lpoStatus);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_amount_collected),amountCollected);

        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_longitude), 0.0);
        };
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_stage),stageName);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_payment_type), quotationPaymentType);
        if(quotationID == 0) {
            initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_name), quotationName);
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "21");
            Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
            quotation_ID = (int) ContentUris.parseId(insertUri);
            Log.d(TAG,"Insert data "+String.valueOf(quotation_ID));
        }else{
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "30");
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_name), quotationName);
            quotation_ID = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(quotationID),mActivity.getResources().getString(R.string.flag_sync)});
            Log.d(TAG,"Update data "+String.valueOf(quotation_ID));
        }
        Log.d(TAG, "-------" + String.valueOf(quotation_ID));
        return quotation_ID;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.fab_button:
                Bundle args = new Bundle();
                args.putString(mActivity.getResources().getString(R.string.db_table_item_qu_slug_id),quotationSlug);
                addQuotationsItemsFragment = new AddQuotationsItemsFragment();
                addQuotationsItemsFragment.setArguments(args);
                addQuotationsItemsFragment.setTargetFragment(ViewQuotationFragment.this, MainModel.PAGER_FRAGMENT);
                mainActivity.addFragment(addQuotationsItemsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addQuotationsItemsFragment).getClass().getName());
                break;
            case R.id.fab_button2:
               mActivity.getSupportFragmentManager().popBackStackImmediate();
                break;
            case R.id.btn_close_quote:
                status = 1;
                updateStage(arrayStages[2],1);
                insertQuotation(arrayStages[2]);
                mActivity.getSupportFragmentManager().popBackStackImmediate();
                Intent intent = new Intent();
                intent.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), 3);
                mActivity.setResult(Activity.RESULT_OK, intent);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                break;
            case R.id.btn_collect_quote:
                status = 1;
                updateStage(arrayStages[3],1);
                insertQuotation(arrayStages[3]);
                mActivity.getSupportFragmentManager().popBackStackImmediate();
                Intent intent2 = new Intent();
                intent2.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), 4);
                mActivity.setResult(Activity.RESULT_OK, intent2);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent2);
                break;
            case R.id.btn_amount_collected:
                newFragment8.setTargetFragment(ViewQuotationFragment.this, MainModel.DIALOG_COLLECT_CASH);
                newFragment8.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            default:

                break;
        }
    }

    /**
     *
     * Updates the stage of a sale or the status of a sale in the DB
     *
     * @param stageName Stage of the sale which is to be updated in the DB
     * @param saleStatus int status of the sale
     */
    public void updateStage(String stageName,int saleStatus){
        syncDone = 1;
        Log.d(TAG,stageName);
        coordinates = getCoordinates.getCoordinates();
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_stage), stageName);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_status),saleStatus );
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_payment_type),quotationPaymentType);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_longitude), 0.0);
        }
        Uri data_id2 = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "9");
        mActivity.getContentResolver().update(data_id2, initialValues,null,new String[]{String.valueOf(saleID),mActivity.getResources().getString(R.string.flag_sync)});
    }
}
