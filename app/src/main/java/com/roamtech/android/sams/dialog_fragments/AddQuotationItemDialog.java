package com.roamtech.android.sams.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.interfaces.CheckProvider;
import com.roamtech.android.sams.interfaces.SendJsonData;
import com.roamtech.android.sams.models.QuotationsItems;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 * Created by Karuri on 2/21/2015.
 */
public class AddQuotationItemDialog extends DialogFragment implements View.OnClickListener {
    public static final String TAG = "AddQuotationItemDialog";
    AppCompatActivity mActivity;
    EditText editItemName,editQuantity,editPrice,editDescription;
    Button btnAdd,btnDone;
    String quotationID;
    int quotationItemID;
    int status,syncDone;
    String itemName,quantity,unitPrice,description;
    MainActivity mainActivity;
    String slugID;
    SendJsonData mCallBack3;
    CheckProvider mCallBack4;
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    GetUsername getUsername;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack3 = (SendJsonData) activity;
            mCallBack4 = (CheckProvider) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        commonHash = new CommonHash(mActivity);
        mainActivity = new MainActivity();
        Bundle args = getArguments();
        if(args != null){
            quotationItemID = args.getInt(mActivity.getResources().getString(R.string.db_table_id));
            Log.d(TAG,"-----quotation Item ID---------- "+quotationItemID);
            quotationID = args.getString(mActivity.getResources().getString(R.string.db_table_item_qu_slug_id));
            Log.d(TAG,"-----quotation ID---------- "+quotationID);
            itemName = args.getString(mActivity.getResources().getString(R.string.db_table_item_name));
            quantity = args.getString(mActivity.getResources().getString(R.string.db_table_item_quantity));
            unitPrice = args.getString(mActivity.getResources().getString(R.string.db_table_item_unit_price));
            description = args.getString(mActivity.getResources().getString(R.string.db_table_item_description));
            slugID = args.getString(mActivity.getResources().getString(R.string.db_table_item_slug_id));
            Log.d(TAG,"Slug ID "+ String.valueOf(slugID));
            syncDone = args.getInt(mActivity.getResources().getString(R.string.db_table_sync_done));
        }
        Log.d(TAG,"quotation ID "+ String.valueOf(quotationID));
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        getUsername = new GetUsername(mActivity);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_items);
        dialog.show();
        Log.d(TAG, TAG);
        dialog.setTitle(mActivity.getResources().getString(R.string.btn_add_items));
        dialog.setCanceledOnTouchOutside(false);
        editItemName = (EditText) dialog.findViewById(R.id.edit_item_name);
        if(itemName != null){
            editItemName.setText(itemName);
        }
        editQuantity = (EditText) dialog.findViewById(R.id.edit_quantity);
        if(quantity != null){
            editQuantity.setText(quantity);
        }
        editPrice = (EditText) dialog.findViewById(R.id.edit_price);
        if(unitPrice != null){
            editPrice.setText(unitPrice);
        }
        editDescription = (EditText) dialog.findViewById(R.id.edit_notes);
        if(description != null){
            editDescription.setText(description);
        }
        btnAdd = (Button) dialog.findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
        btnDone = (Button) dialog.findViewById(R.id.btn_done);
        btnDone.setOnClickListener(this);

        return dialog;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void fillData(){
        if(editItemName.getText().length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_item_name), Toast.LENGTH_LONG).show();
        }else if(editQuantity.getText().length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_quantity), Toast.LENGTH_LONG).show();
        }else if(editPrice.getText().length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_price), Toast.LENGTH_LONG).show();
        }else {
            if (slugID == null) {
                quotationItemID = (int) insertItems();
                try {
                    slugID = commonHash.getHash(String.valueOf(quotationItemID));
                    Log.d(TAG, "Common Hash is  " + slugID);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                insertItems();
            } else {
                insertItems();
            }
        }
    }

    public int insertItems(){
        status = 1;
        syncDone = 1;
        coordinates = getCoordinates.getCoordinates();
        int quotationDetailsID;
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_qu_slug_id), quotationID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_name), editItemName.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_quantity), Integer.parseInt(editQuantity.getText().toString()));
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_unit_price), Integer.parseInt(editPrice.getText().toString()));
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_description), editDescription.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_slug_id), slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_status), status);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_longitude), 0.0);
        }
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());

        if(quotationItemID == 0){
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"22");
            Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
            initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
            quotationDetailsID = (int) ContentUris.parseId(insertUri);
        }else{
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"31");
            quotationDetailsID = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(quotationItemID),mActivity.getResources().getString(R.string.flag_sync)});
        }
        Log.d(TAG, "-------" + String.valueOf(quotationID));
        return quotationDetailsID;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_add:
                fillData();
                clearAll();
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_add_new_item), Toast.LENGTH_LONG).show();
                break;
            case R.id.btn_done:
                Intent intent = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent);
                Log.d(TAG,String.valueOf(getTargetRequestCode()));
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                dismiss();
                break;

        }
    }

    public void clearAll(){
        editItemName.setText("");
        editQuantity.setText("");
        editDescription.setText("");
        editPrice.setText("");
        quotationItemID = 0;
        slugID = null;
    }
}
