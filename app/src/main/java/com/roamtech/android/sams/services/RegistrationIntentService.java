/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.roamtech.android.sams.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.http.RestClient;
import com.roamtech.android.sams.models.MainModel;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * This Class registers the GCM id for the phone with the app
 *
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 *
 * @author Dennis Mwangi Karuri
 *
 */

public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegistrationIntentService";
    private static final String[] TOPICS = {"global"};
    SharedPreferences sharedPreferences;


    /**
     * Constructor
     *
     */
    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String token = null;

        try {
            // [START register_for_gcm]
            // Initially this call goes out to the network to retrieve the token, subsequent calls
            // are local.
            // R.string.gcm_defaultSenderId (the Sender ID) is typically derived from google-services.json.
            // See https://developers.google.com/cloud-messaging/android/start for details on this file.
            // [START get_token]
            boolean sentToken = sharedPreferences
                    .getBoolean(MainModel.SENT_TOKEN_TO_SERVER, false);
            if (sentToken) {
                Log.d(TAG,"token present");
                int registeredVersion = sharedPreferences.getInt(MainModel.PROPERTY_APP_VERSION, 1240);
                int currentVersion = MainModel.getAppVersion(this);
                token = sharedPreferences.getString(MainModel.TOKEN_FROM_SERVER,null);
                if (registeredVersion != currentVersion) {
                    sharedPreferences.edit().remove(MainModel.SENT_TOKEN_TO_SERVER).commit();
                    Log.d(TAG, "token present with different version");
                    token = getPhoneID();
                }
            }else {
                Log.d(TAG, "No token present");
                token = getPhoneID();
            }

            sendRegistrationToServer(token);

            // TODO: Implement this method to send any registration to your app's servers.



            // Store a boolean this indicates that the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.

            // [END register_for_gcm]
        } catch (Exception e) {
            Log.d(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on a third-party server, this ensures that we'll attempt the update at a later time.
            sharedPreferences.edit().putBoolean(MainModel.SENT_TOKEN_TO_SERVER, false).apply();
        }
        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent(MainModel.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }


    /**
     *
     *
     * This method registers and gets the phoneID in the Google cloud servers
     * @return a string with the phoneID
     */
    public String getPhoneID(){
        InstanceID instanceID = InstanceID.getInstance(this);
        String token = null;
        try {
            token = instanceID.getToken(MainModel.PROJECT_ID,
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // [END get_token]
        Log.i(TAG, "GCM Registration Token: " + token);
        sharedPreferences.edit().putString(MainModel.TOKEN_FROM_SERVER, token).apply();
        int appVersion = MainModel.getAppVersion(this);
        sharedPreferences.edit().putInt(MainModel.PROPERTY_APP_VERSION, appVersion).apply();

        return token;
    }

    /**
     *
     *
     * This method sends the user's GCM registration token to the server
     * maintained by your application.
     *
     * @param token The new token to be sent to the server
     */
    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
        Log.d(TAG,"GCM token is "+token);
        String result = sendToServer(token);
        if(!result.equals(null)){
            if(parseJson(result).equals(getString(R.string.json_operation_successful))){
                sharedPreferences.edit().putBoolean(MainModel.SENT_TOKEN_TO_SERVER, true).apply();
            }else{
                sharedPreferences.edit().putBoolean(MainModel.SENT_TOKEN_TO_SERVER, false).apply();
            }
        }else{
            sharedPreferences.edit().putBoolean(MainModel.SENT_TOKEN_TO_SERVER, false).apply();
        }
    }

    /**
     *
     * Parse the Json result to see if the phoneID sent was successfully stored in the server
     *
     * @param result Json response from the server once the phoneID is sent
     * @return String result once the Json String is parsed shows whether the data sent was stored successfully or failed
     */
    public String parseJson(String result){
        Log.d(TAG,"response is "+result);
        String response = null;
        if(result != null) {
            try {
                JSONObject jObject = new JSONObject(result);
                if (jObject.has(MainModel.RESPONSE) & !jObject.isNull(MainModel.RESPONSE)) {
                    response = jObject.getString(MainModel.RESPONSE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
         return response;
    }


    /**
     *
     * Sends the PhoneId to the server
     *
     * @param token The phoneID sent to the server
     * @return String response from the server
     */
    public String sendToServer(String token){
        String response = null;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
        nameValuePairs.add(new BasicNameValuePair(getString(R.string.name_value_request), getString(R.string.name_value_phoneuserReg)));
        nameValuePairs.add(new BasicNameValuePair(getString(R.string.name_value_data), createPhoneDetailsJson(token)));
        try {
            response = RestClient.makeRestRequest(RestClient.POST, MainModel.SYNC_URL, nameValuePairs);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    /**
     *
     *  Creates a Json String with the phoneID to be sent to the server
     *
     * @param token PhoneID to be sent to the server
     * @return Json String that was created to be sent to the server with the phoneID
     */

    public String createPhoneDetailsJson(String token){
        JSONObject user = new JSONObject();
        try {
            user.put(getString(R.string.json_phone_id), token);
            user.put(getString(R.string.user_id), sharedPreferences.getString(getString(R.string.bundle_username), null));
            user.put(getString(R.string.transaction_action), getString(R.string.txt_add).toLowerCase());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return user.toString();
    }

    /**
     * Subscribe to any GCM topics of interest, as defined by the TOPICS constant.
     *
     * @param token GCM token
     * @throws IOException if unable to reach the GCM PubSub service
     */

    private void subscribeTopics(String token) throws IOException {
        GcmPubSub pubSub = GcmPubSub.getInstance(this);
        for (String topic : TOPICS) {
            pubSub.subscribe(token, "/topics/" + topic, null);
        }
    }
    // [END subscribe_topics]

}
