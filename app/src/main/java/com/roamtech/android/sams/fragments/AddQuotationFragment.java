package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.QuotationItemsAdapter;
import com.roamtech.android.sams.dialog_fragments.AddQuotationItemDialog;
import com.roamtech.android.sams.dialog_fragments.AddSaleDialogFragment;
import com.roamtech.android.sams.dialog_fragments.DeleteDialog;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.interfaces.CheckProvider;
import com.roamtech.android.sams.interfaces.ItemActions;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.interfaces.SendJsonData;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.models.QuotationsItems;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddQuotationFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = "AddQuotationFragment";
    LinearLayout lytChooseSale,lytAnotherItem;
    TextView txtOrganisation,txtContacts,txtSaleName;
    EditText editItemName,editQuantity,editPrice,editDescription;
    boolean editTextStatus;
    Button btnCreate;
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    AddSaleDialogFragment newFragment3;
    public static final int DIALOG_FRAGMENT_ADD_SALE = 5;
    String organisationID,contactID;
    String saleSlugID;
    int saleID;
    String contact_name,organisation_name,sale_name;
    String dateCreated;
    Calendar c;
    SimpleDateFormat df;
    Cursor cursor;
    int quotationID;
    int quotationDone = 0;
    public static final int LOADER = 1700;
    View header;
    String quotationName;
    String quotationPaymentType = "CASH";
    int position;
    int status,syncDone;
    String slugID;
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    GetUsername getUsername;
    ArrayList<QuotationsItems> quotationItems;
    int quotationItemID;
    String itemSlugID = null;
    ImageButton ivAdd;
    String [] arrayStages;
    int deliveryStatus,lpoStatus;
    Double amountCollected = 0.0;
    Double cost = 0.0;
    String product;
    String [] arrayMainItems;
    AddActivitiesFragment addActivitiesFragment;
    CommentsFragment commentsFragment;


    public AddQuotationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {

        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        quotationItems = new ArrayList<QuotationsItems>();
        commonHash = new CommonHash(mActivity);
        mainActivity = new MainActivity();
        newFragment3 = new AddSaleDialogFragment();
        arrayMainItems = mActivity.getResources().getStringArray(R.array.main_activity_items);
        c = Calendar.getInstance();
        df = new SimpleDateFormat("dd/MM/yyyy");
        arrayMainItems = getResources().getStringArray(R.array.main_activity_items);
        arrayStages = getResources().getStringArray(R.array.pager_title_items2);
        Bundle args = getArguments();
        if(args != null){
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            contact_name = args.getString(mActivity.getResources().getString(R.string.db_table_con_name));
            organisation_name = args.getString(mActivity.getResources().getString(R.string.db_table_org_name));
            quotationName = args.getString(mActivity.getResources().getString(R.string.db_table_qu_name));
            sale_name = args.getString(mActivity.getResources().getString(R.string.db_table_sale_name));
            saleSlugID = args.getString(mActivity.getResources().getString(R.string.db_table_qu_sale_id));
            saleID = args.getInt(mActivity.getResources().getString(R.string.db_table_id));
            cost = args.getDouble(mActivity.getResources().getString(R.string.db_table_sale_cost));
            product = args.getString(mActivity.getResources().getString(R.string.db_table_sale_description));
        }

        Log.d(TAG,"sales id = "+ String.valueOf(saleID));
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        getUsername = new GetUsername(mActivity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_quotation, container, false);
        txtContacts = (TextView) rootView.findViewById(R.id.txt_contact);
        if(contact_name != null && !contact_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
            txtContacts.setText(contact_name);
        }
        txtOrganisation = (TextView) rootView.findViewById(R.id.txt_organisation);
        if(organisation_name != null){
            if(!organisation_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))) {
                txtOrganisation.setText(organisation_name);
            }
        }
        txtSaleName = (TextView) rootView.findViewById(R.id.txt_sale_name);
        if(quotationName != null){
            txtSaleName.setText(quotationName);
        }
        editItemName = (EditText) rootView.findViewById(R.id.edit_item_name);
        editItemName.setText(product);
        editQuantity = (EditText) rootView.findViewById(R.id.edit_quantity);
        editPrice = (EditText) rootView.findViewById(R.id.edit_price);
        //editPrice.setText(MainModel.convertFromScientificNotation(cost));
        editDescription = (EditText) rootView.findViewById(R.id.edit_notes);
        lytChooseSale = (LinearLayout) rootView.findViewById(R.id.lyt_choose_sale);
        lytChooseSale.setOnClickListener(this);
        lytAnotherItem = (LinearLayout) rootView.findViewById(R.id.lyt_another_item);
        lytAnotherItem.setOnClickListener(this);
        btnCreate = (Button) rootView.findViewById(R.id.btn_create);
        btnCreate.setOnClickListener(this);
        ivAdd = (ImageButton) rootView.findViewById(R.id.fab_button);
        ivAdd.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onResume(){
        super.onResume();
        mActivity.getSupportActionBar().setTitle(arrayMainItems[5]);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
        //        getActivity().invalidateOptionsMenu();
        menu.findItem(R.id.action_flag).setVisible(true);
        menu.findItem(R.id.action_comments).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                addActivitiesFragment.setTargetFragment(AddQuotationFragment.this, MainModel.ACTIVITY_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                commentsFragment.setTargetFragment(AddQuotationFragment.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
            default:

                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.lyt_choose_sale:
                newFragment3.setTargetFragment(AddQuotationFragment.this, DIALOG_FRAGMENT_ADD_SALE);
                newFragment3.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.lyt_another_item:
                fillData();
                break;
            case R.id.btn_create:
                Calendar c = Calendar.getInstance();
                dateCreated = dateFormat.format(c.getTime());
                fillQuotation();
                break;
            case R.id.btn_done:

                break;
            case R.id.fab_button:
                /*Intent intent3 = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent3);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent3);*/
                //mActivity.getSupportFragmentManager().popBackStackImmediate();
                if(fillData()){
                    Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_item_added),Toast.LENGTH_LONG).show();
                    clearAll();
                }else{
                    Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_item_not_added),Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void fillQuotation(){
        checkEditText();
        Log.d(TAG," Edit text status "+ editTextStatus);
        if(quotationItems != null & quotationItems.size() < 1) {
            if (fillData()) {
                if (saleSlugID == null && quotationName == null) {
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_select_sale), Toast.LENGTH_LONG).show();
                } else if (quotationItems.size() < 1) {
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_add_quotation_items), Toast.LENGTH_LONG).show();
                } else {
                    createQuotation();
                }
            }else{

            }
        }else{
            if(editTextStatus == true){
                if (saleSlugID == null && quotationName == null) {
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_select_sale), Toast.LENGTH_LONG).show();
                } else if (quotationItems.size() < 1) {
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_add_quotation_items), Toast.LENGTH_LONG).show();
                } else {
                    createQuotation();
                }
            }else{
                if (fillData()) {
                    if (saleSlugID == null && quotationName == null) {
                        Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_select_sale), Toast.LENGTH_LONG).show();
                    } else if (quotationItems.size() < 1) {
                        Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_add_quotation_items), Toast.LENGTH_LONG).show();
                    } else {
                        createQuotation();
                    }
                }
            }
        }
    }

    public void createQuotation(){
        if (slugID == null) {
            Log.d(TAG, "Common Hash is  number 4 " + slugID);
            quotationID = (int) insertQuotation();
            try {
                slugID = commonHash.getHash(String.valueOf(quotationID));
                Log.d(TAG, "Common Hash is  " + slugID);
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            insertQuotation();
        } else {
            insertQuotation();
        }
        for (int i = 0; i < quotationItems.size(); i++) {
            insertItemsSlug(quotationItems.get(i).getItemName(), quotationItems.get(i).getQuantity(), quotationItems.get(i).getPrice(), quotationItems.get(i).getDescription());
            quotationItemID = 0;
            itemSlugID = null;
        }
        updateStage();
        mActivity.getSupportFragmentManager().popBackStackImmediate();
        Intent intent = new Intent();
        intent.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), 2);
        mActivity.setResult(Activity.RESULT_OK, intent);
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
    }

    public boolean fillData(){
        boolean fillData = false;
        if(editItemName.getText().length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_item_name), Toast.LENGTH_LONG).show();
            fillData = false;
        }else if(editQuantity.getText().length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_quantity), Toast.LENGTH_LONG).show();
            fillData = false;
        }else if(editPrice.getText().length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_price), Toast.LENGTH_LONG).show();
            fillData = false;
        }else {
            QuotationsItems quotationsItems = new QuotationsItems();
            quotationsItems.setItemName(editItemName.getText().toString());
            quotationsItems.setDescription(editDescription.getText().toString());
            quotationsItems.setPrice(Double.parseDouble(editPrice.getText().toString()));
            quotationsItems.setQuantity(Double.parseDouble(editQuantity.getText().toString()));
            quotationItems.add(quotationsItems);
            //Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_add_new_item),Toast.LENGTH_LONG).show();
            clearAll();
            fillData = true;
        }
        return fillData;
    }

    public boolean checkEditText() {
        editTextStatus = true;
        if(editItemName.getText().length() > 1 | editQuantity.getText().length() > 1 | editPrice.getText().length() > 1 ){
            editTextStatus = false;
        }
        return editTextStatus;
    }


    public void clearAll(){
        editItemName.setText(null);
        editQuantity.setText(null);
        editDescription.setText(null);
        editPrice.setText(null);
    }



    public void insertItemsSlug(String itemName,double quantity,double price,String description){
        if (itemSlugID == null) {
            quotationItemID = (int) insertItems(itemName,quantity,price,description,itemSlugID);
            try {
                itemSlugID = commonHash.getHash(String.valueOf(quotationItemID));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            insertItems(itemName,quantity,price,description,itemSlugID);
        } else {
            insertItems(itemName,quantity,price,description,itemSlugID);
        }
    }

    public int insertItems(String itemName,double quantity,double price,String description,String itemSlugID){

        status = 1;
        syncDone = 1;
        coordinates = getCoordinates.getCoordinates();
        int quotationDetailsID;
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_qu_slug_id), slugID);
        Log.d(TAG, "slug id is" + slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_name), itemName);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_quantity), quantity);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_unit_price), price);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_description), description);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_slug_id), itemSlugID);
        Log.d(TAG, "Item slug id is" + itemSlugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_status), status);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_longitude), 0.0);
        }
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());
        if(quotationItemID == 0){
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"22");
            Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
            initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
            quotationDetailsID = (int) ContentUris.parseId(insertUri);
        }else{
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"31");
            quotationDetailsID = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(quotationItemID),mActivity.getResources().getString(R.string.flag_sync)});
        }
        Log.d(TAG, "-------" + String.valueOf(quotationID));
        return quotationDetailsID;
    }

    public int insertQuotation(){
        int quotation_ID = 0;
        syncDone = 1;
        coordinates = getCoordinates.getCoordinates();
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_sale_id), saleSlugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_slug_id), slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_created), dateCreated);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_status), quotationDone);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_delivered),deliveryStatus);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_lpo),lpoStatus);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_amount_collected),amountCollected);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_longitude), 0.0);
        }
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_stage), arrayStages[1]);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_payment_type), quotationPaymentType);
        if(quotationID == 0) {
            initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_name), quotationName);
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "21");
            Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
            quotation_ID = (int)ContentUris.parseId(insertUri);
            Log.d(TAG,"Insert data "+String.valueOf(quotation_ID));
        }else{
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "30");
            initialValues.put(mActivity.getResources().getString(R.string.db_table_qu_name), quotationName);
            quotation_ID = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(quotationID),mActivity.getResources().getString(R.string.flag_sync)});
            Log.d(TAG,"Update data "+String.valueOf(quotation_ID));
        }
        Log.d(TAG, "-------" + String.valueOf(quotation_ID));
        return quotation_ID;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG,"00000000000000000 88888888888888 delete position "+String.valueOf(requestCode));
        if (requestCode == DIALOG_FRAGMENT_ADD_SALE) {
            if(resultCode == Activity.RESULT_OK){
                if(data.getExtras() != null) {
                    organisationID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id));
                    organisation_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_name));
                    contactID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_slug_id));
                    contact_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_name));
                    saleSlugID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_slug_id));
                    sale_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_name));
                    saleID = data.getIntExtra(mActivity.getResources().getString(R.string.db_table_id),0);
                    quotationName = sale_name + " Quotation";
                    txtContacts.setText(contact_name);
                    if(!organisation_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))) {
                        txtOrganisation.setText(organisation_name);
                    }
                    txtSaleName.setText(quotationName);
                }
            }
        }
    }

    public void updateStage(){
        syncDone = 1;
        Log.d(TAG,String.valueOf(saleID));
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_stage), arrayStages[1]);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_status), 1);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_sale_longitude), 0.0);
        }
        Uri data_id2 = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "9");
        mActivity.getContentResolver().update(data_id2, initialValues,null,new String[]{String.valueOf(saleID),mActivity.getResources().getString(R.string.flag_sync)});
    }
}
