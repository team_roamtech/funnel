package com.roamtech.android.sams.async_tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.http.RestClient;
import com.roamtech.android.sams.interfaces.ReturnReports;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.models.ReportItems;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by dennis on 5/14/15.
 */
public class GetReportsTask extends AsyncTask<String, Void, ArrayList<ReportItems>> {
    Context context;
    Fragment fragment;
    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);

    public GetReportsTask(Context context, Fragment fragment){
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        ReturnReports rp = (ReturnReports) fragment;
        rp.onStartTask();
    }

    @Override
    protected ArrayList<ReportItems> doInBackground(String... params) {
        nameValuePairs.add(new BasicNameValuePair(context.getResources().getString(R.string.name_value_data),params[0]));
        nameValuePairs.add(new BasicNameValuePair(context.getResources().getString(R.string.name_value_request),context.getResources().getString(R.string.name_value_report)));
        String json = "{}";
        try {
            json = RestClient.makeRestRequest(RestClient.POST, MainModel.SYNC_URL, nameValuePairs);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

        }
        return null;
    }

    @Override
    protected void onPostExecute(ArrayList<ReportItems> result) {
        // TODO Auto-generated method stub
        super.onPostExecute(result);
        if(result != null){
            ReturnReports rp = (ReturnReports) fragment;
            rp.onReturnResult(result);
        }
    }
}
