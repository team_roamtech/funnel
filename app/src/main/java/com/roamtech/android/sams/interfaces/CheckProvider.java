package com.roamtech.android.sams.interfaces;

/**
 *
 * Checks the ContentProvider ID of the Inserted and Updated Record in the DB
 *
 * Created by dennis on 4/9/15.
 * @author Dennis Mwangi Karuri
 */
public interface CheckProvider {

    /**
     *
     * Passes the ID of the affected record In the DB
     *
     * @param code ID of affected Record;
     */
    public void transactionID(int code);
}
