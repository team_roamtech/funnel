package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.models.MainModel;

/**
 * Created by Karuri on 1/28/2015.
 */
public class DealAdapter extends CursorAdapter {
    public static final String TAG = "DealAdapter";
    private LayoutInflater vi;
    Cursor c;
    Context ctx;
    String cost;
    int status;
    ViewHolder holder;
    int fragmentPosition;
    LaunchFragment mCallBack;
    Fragment fragment;
    int sdk = android.os.Build.VERSION.SDK_INT;

    public DealAdapter(Context context, Cursor c, int flags,int fragmentPosition,Fragment fragment) {
        super(context, c, flags);
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.ctx = context;
        this.c = c;
        this.fragmentPosition = fragmentPosition;
        this.fragment = fragment;
        try {
            mCallBack = (LaunchFragment) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
        Log.d(TAG, "DealAdapter constructor");
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View rootView = vi.inflate(R.layout.list_deal_items, parent, false);
        ViewHolder holder  =   new ViewHolder();
        holder.mSaleName = (TextView) rootView.findViewById(R.id.txt_sale_name);
        holder.mSaleCost = (EditText) rootView.findViewById(R.id.txt_sale_cost);
        holder.mOrganisation = (TextView) rootView.findViewById(R.id.txt_organisation);
        holder.mContact = (TextView) rootView.findViewById(R.id.txt_contacts);
        holder.mSales = (LinearLayout) rootView.findViewById(R.id.lyt_sales);
        holder.mInnerElements = (LinearLayout) rootView.findViewById(R.id.lyt_inner_elements);
        holder.imgDown = (ImageView) rootView.findViewById(R.id.img_down);
        holder.animSideDown = AnimationUtils.loadAnimation(context,
                R.anim.slide_down);

        holder.animSlideUp = AnimationUtils.loadAnimation(context,
                R.anim.slide_up);
        Log.d(TAG," cursor position " + holder.cursorPosition);
        rootView.setTag(holder);
        return rootView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final Cursor c = cursor;
        if(view != null) {
            final ViewHolder holder = (ViewHolder) view.getTag();
            holder.mSaleName.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_sale_name))));
            holder.mSaleCost.setText(MainModel.doubleToStringNoDecimal(c.getDouble(c.getColumnIndex(context.getResources().getString(R.string.db_table_sale_cost)))));
            if(!c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_org_name))).equals(ctx.getResources().getString(R.string.db_table_org_entry))) {
                holder.mOrganisation.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_org_name))));
            }
            holder.mContact.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_name))));
            holder.cursorPosition = c.getPosition();
            holder.animSideDown.setAnimationListener(new Animation.AnimationListener() {

                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {

                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            holder.animSlideUp.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    Log.d(TAG, "animation end");
                    if (animation == holder.animSlideUp) {
                        holder.mInnerElements.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });

            /*if(c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_status))) == 1){
                holder.imgDown.setBackgroundResource(R.drawable.ic_action_down);
            }else {*/
                holder.imgDown.setBackgroundResource(R.drawable.ic_action_down2);
            //}
            holder.imgDown.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.mInnerElements.getVisibility() == View.GONE) {
                        holder.mInnerElements.setVisibility(View.VISIBLE);
                        holder.mInnerElements.startAnimation(holder.animSideDown);
                       /* if(c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_status))) == 1){
                            holder.imgDown.setBackgroundResource(R.drawable.ic_action_up);
                        }else {*/
                            holder.imgDown.setBackgroundResource(R.drawable.ic_action_up2);
                        //}
                    } else {
                        holder.mInnerElements.setVisibility(View.GONE);
                        holder.mInnerElements.startAnimation(holder.animSlideUp);
                       // holder.imgDown.setBackgroundResource(R.drawable.ic_action_down2);
                        /*if(c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_status))) == 1){
                            holder.imgDown.setBackgroundResource(R.drawable.ic_action_down);
                        }else {*/
                            holder.imgDown.setBackgroundResource(R.drawable.ic_action_down2);
                       // }
                    }
                }
            });
           /* if(c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_status))) == 1){
                holder.mSaleName.setTextColor(ctx.getResources().getColor(R.color.white_text));
                holder.mSaleCost.setTextColor(ctx.getResources().getColor(R.color.white_text));
                holder.mOrganisation.setTextColor(ctx.getResources().getColor(R.color.white_text));
                holder.mContact.setTextColor(ctx.getResources().getColor(R.color.white_text));
                holder.imgDown.setBackgroundResource(R.drawable.ic_action_down);
                if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    holder.mSales.setBackgroundDrawable(ctx.getResources().getDrawable(R.drawable.layout_border_green));
                }else{
                    holder.mSales.setBackground(ctx.getResources().getDrawable(R.drawable.layout_border_green));
                }
            }*/

            holder.mSales.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    c.moveToPosition(holder.cursorPosition);
                    Bundle args = new Bundle();
                    args.putInt(ctx.getResources().getString(R.string.db_table_id), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_id))));
                    args.putString(ctx.getResources().getString(R.string.db_table_sale_foreign_key), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_foreign_key))));
                    args.putString(ctx.getResources().getString(R.string.db_table_sale_model), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_model))));
                    args.putString(ctx.getResources().getString(R.string.db_table_sale_stage),c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_stage))));
                    args.putString(ctx.getResources().getString(R.string.db_table_sale_name), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_name))));
                    args.putInt(ctx.getResources().getString(R.string.db_table_sale_status), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_status))));
                    args.putString(ctx.getResources().getString(R.string.db_table_con_name), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_con_name))));
                    args.putString(ctx.getResources().getString(R.string.db_table_con_slug_id), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_con_slug_id))));
                    args.putString(ctx.getResources().getString(R.string.db_table_org_name), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_name))));
                    args.putString(ctx.getResources().getString(R.string.db_table_org_slug_id), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_slug_id))));
                    args.putString(ctx.getResources().getString(R.string.db_table_sale_payment_type), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_payment_type))));
                    args.putString(ctx.getResources().getString(R.string.db_table_sale_slug_id), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_slug_id))));
                    args.putDouble(ctx.getResources().getString(R.string.db_table_sale_cost), c.getDouble(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_cost))));
                    args.putString(ctx.getResources().getString(R.string.db_table_sale_description), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_description))));

                    mCallBack.launchFragment(holder.cursorPosition,args,1);
                }
            });
        }
    }


    private class ViewHolder{
        public LinearLayout mSales;
        public LinearLayout mInnerElements;
        public ImageView imgDown;
        public TextView mSaleName;
        public TextView mOrganisation;
        public TextView mContact;
        public EditText mSaleCost;
        public Animation animSideDown,animSlideUp;
        public int cursorPosition;
    }
}
