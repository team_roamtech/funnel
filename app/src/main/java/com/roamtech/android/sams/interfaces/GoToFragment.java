package com.roamtech.android.sams.interfaces;

/**
 *
 * Gets the position of Fragment String reference in the NavigationDrawer ListView Adapter
 *
 * Created by Karuri on 2/23/2015.
 * @author Dennis Mwangi Karuri
 */
public interface GoToFragment {

    /**
     *
     * Position of Fragment String reference in the NavigationDrawer ListView Adapter
     *
     * @param position Position of Fragment String reference in the NavigationDrawer ListView Adapter
     */
    public void moveToFragment(int position);
}
