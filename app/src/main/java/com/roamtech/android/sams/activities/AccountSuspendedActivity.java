package com.roamtech.android.sams.activities;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.fragments.AccountSuspendedFragment;
import com.roamtech.android.sams.fragments.SignInStateFragment;

public class AccountSuspendedActivity extends AppCompatActivity {
    public static final String TAG = "AccountSuspendedActivity";
    MainActivity mainActivity;
    AccountSuspendedFragment accountSuspendedFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account_suspended);
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600 && getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }else{
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        accountSuspendedFragment = new AccountSuspendedFragment();
        mainActivity = new MainActivity();
        mainActivity.addFragment(accountSuspendedFragment,false, FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(), ((Object) accountSuspendedFragment).getClass().getName());
    }
}
