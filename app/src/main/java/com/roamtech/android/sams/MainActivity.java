package com.roamtech.android.sams;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncStatusObserver;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.roamtech.android.sams.ContentObserver.TableObserver;
import com.roamtech.android.sams.activities.SignInActivity;
import com.roamtech.android.sams.adapters.DrawerAdapter;
import com.roamtech.android.sams.dialog_fragments.SignOutDialog;
import com.roamtech.android.sams.fragments.ActivitiesFragment;
import com.roamtech.android.sams.fragments.CommentsFragment;
import com.roamtech.android.sams.fragments.ContactsFragment;
import com.roamtech.android.sams.fragments.HomeFragment;
import com.roamtech.android.sams.fragments.MyCalendarFragment2;
import com.roamtech.android.sams.fragments.NewDealFragment;
import com.roamtech.android.sams.fragments.OrganisationsFragment;
import com.roamtech.android.sams.fragments.QuotationsFragment;
import com.roamtech.android.sams.fragments.SalesFragment;
import com.roamtech.android.sams.fragments.SettingsFragment;
import com.roamtech.android.sams.fragments.StagesStateFragment;
import com.roamtech.android.sams.http.NetworkConnectionStatus;
import com.roamtech.android.sams.interfaces.CheckProvider;
import com.roamtech.android.sams.interfaces.CheckSignOut;
import com.roamtech.android.sams.interfaces.GoToFragment;
import com.roamtech.android.sams.interfaces.GoToPager;
import com.roamtech.android.sams.interfaces.SendJsonData;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.receivers.AlarmReceiver;
import com.roamtech.android.sams.services.FunnelAccountService;
import com.roamtech.android.sams.utils.GetUsername;
import com.roamtech.android.sams.utils.SyncUtils;
import com.roomorama.caldroid.CaldroidFragment;
import java.util.Calendar;

/**
 * The activity is the parent for most of the fragments.
 * It has the container view that most fragments are added and removed.
 * <p>
 * It also has a navigation drawer menu allows the user navigate the app by adding fragments
 * It also allows the user to Sign out
 * It creates a Sync Adapter and alarm to allow the app to Sync data periodically

 * @author  Dennis Mwangi Karuri
 * @version 1.0
 * @since   2015-01-01
 */

public class MainActivity extends AppCompatActivity implements GoToFragment,GoToPager,CheckProvider,SendJsonData,
        CheckSignOut {
    public static final String TAG = "MainActivity";
    public static final int PAGER_FRAGMENT = 2;
    public static final int DIALOG_FRAGMENT_SIGN_OUT_DIALOG = 7;
    public static final long SECONDS_PER_MINUTE = 60L;
    public static final long SYNC_INTERVAL_IN_MINUTES = 60L;
    public static final long SYNC_INTERVAL =
            SYNC_INTERVAL_IN_MINUTES *
                    SECONDS_PER_MINUTE * 60L;
    StagesStateFragment stagesStateFragment;
    ContactsFragment contactsFragment;
    ActivitiesFragment activitiesFragment;
    OrganisationsFragment organisationsFragment;
    QuotationsFragment quotationsFragment;
    CommentsFragment commentsFragment;
    HomeFragment homeFragment;
    MyCalendarFragment2 myCalendarFragment;
    NewDealFragment newDealFragment;
    SettingsFragment settingsFragment;
    SalesFragment salesFragment;
    SignOutDialog newFragment7;
    TypedArray listItemsImgArray;
    String [] drawerListArray;
    private ListView mDrawerList;
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    Toolbar toolBar;
    DrawerAdapter drawerAdapter;
    int selectPosition;
    private Object mSyncObserverHandle;
    TableObserver tableObserver = new TableObserver(null);
    private Menu mOptionsMenu;
    FrameLayout frameLayout,frameLayout2;
    private boolean isDrawerLocked = false;
    int code;
    String jsonData;
    Account account;
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    LinearLayout lytDrawer;
    TextView txtNavUsername,txtNavEmail;
    GetUsername getUsername;
    AlarmReceiver alarm = new AlarmReceiver();
    int activityID;
    String activityMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            prefs = PreferenceManager
                    .getDefaultSharedPreferences(MainActivity.this);
            editor = prefs.edit();
            setContentView(R.layout.activity_main);

            SyncUtils.CreateSyncAccount(this);

            Intent intent = getIntent();
            if (intent.getExtras() != null) {
                activityID = intent.getIntExtra(getString(R.string.db_table_activities_id), -1);
                Log.d(TAG, "Activity ID = " + activityID);
                activityMsg = intent.getStringExtra(getString(R.string.db_table_act_schedule));
                Log.d(TAG, "Activity Message = " + activityMsg);
            }


            getUsername = new GetUsername(this);
            stagesStateFragment = new StagesStateFragment();
            contactsFragment = new ContactsFragment();
            organisationsFragment = new OrganisationsFragment();
            quotationsFragment = new QuotationsFragment();
            activitiesFragment = new ActivitiesFragment();
            newDealFragment = new NewDealFragment();
            settingsFragment = new SettingsFragment();
            newFragment7 = new SignOutDialog();
            drawerListArray = getResources().getStringArray(R.array.main_activity_items);
            listItemsImgArray = getResources().obtainTypedArray(R.array.main_activity_imgs);
            frameLayout = (FrameLayout) findViewById(R.id.content_frame);
            frameLayout2 = (FrameLayout) findViewById(R.id.content_frame2);
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            lytDrawer = (LinearLayout) findViewById(R.id.lyt_drawer);
            mDrawerList = (ListView) findViewById(R.id.listview_drawer);
            txtNavUsername = (TextView) findViewById(R.id.txt_nav_username);
            txtNavUsername.setText(getUsername.getUserName());
            txtNavEmail = (TextView) findViewById(R.id.txt_nav_email);
            txtNavEmail.setText("");
            drawerAdapter = new DrawerAdapter(this, 0, drawerListArray, listItemsImgArray);
            mDrawerList.setAdapter(drawerAdapter);

            // set up the drawer's list view with items and click listener
            mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

            toolBar = (Toolbar) findViewById(R.id.toolbar_actionbar);
            setSupportActionBar(toolBar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(true);

            // set a custom shadow that overlays the main content when the drawer opens
            mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

            mDrawerToggle = new ActionBarDrawerToggle(
                    this,                  /* host Activity */
                    mDrawerLayout,         /* DrawerLayout object */
                    toolBar,  /* nav drawer image to replace 'Up' caret */
                    R.string.drawer_open,  /* "open drawer" description for accessibility */
                    R.string.drawer_close  /* "close drawer" description for accessibility */
            ) {
                public void onDrawerClosed(View view) {
                    super.onDrawerClosed(view);
                    toolBar.setNavigationIcon(R.drawable.ic_action_res);
                }

                public void onDrawerOpened(View drawerView) {
                    toolBar.setNavigationIcon(R.drawable.ic_action_arrow);
                }
            };
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            mDrawerToggle.setToolbarNavigationClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "mdrawer Toggle touched");

                    if (mDrawerLayout.isDrawerOpen(lytDrawer)) {
                        mDrawerLayout.closeDrawer(lytDrawer);
                    } else {
                        mDrawerLayout.openDrawer(lytDrawer);
                    }

                }
            });
            mDrawerLayout.setDrawerListener(mDrawerToggle);
            toolBar.setNavigationIcon(R.drawable.ic_action_res);


            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "0");
            getContentResolver().registerContentObserver(data, true, tableObserver);


            //Add the initial fragment which is the home fragment
            if (savedInstanceState == null) {
                Bundle args = new Bundle();
                args.putString(getString(R.string.bundle_title), drawerListArray[0]);
                homeFragment = new HomeFragment();
                homeFragment.setArguments(args);
                addFragment(homeFragment, false, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) homeFragment).getClass().getName());
            }

            //If the app is being launched from a notification add the activity fragment
            // after adding the home fragment
            if (activityMsg != null & activityID != -1) {
                Log.d(TAG, "Activity ID = " + activityID);
                Log.d(TAG, "Activity Message = " + activityMsg);
                Bundle args = new Bundle();
                args.putInt(getString(R.string.db_table_activities_id), activityID);
                args.putString(getString(R.string.db_table_act_schedule), activityMsg);
                activitiesFragment = new ActivitiesFragment();
                activitiesFragment.setArguments(args);
                addFragment(activitiesFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) activitiesFragment).getClass().getName());
            }
            //Set an alarm for everytime you want it to sync
            alarm.setSyncAlarm(this);
        }catch(Exception e){
            //Incase of any exception close this activity and launch splash activity again.
            e.printStackTrace();
            finish();
            Intent i = new Intent(MainActivity.this, SplashActivity.class);
            startActivity(i);
        }

     }

    @Override
    protected void onStart() {
        super.onStart();
        //Register content observer for any changes to the DB to be noted
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "0");
        getContentResolver().registerContentObserver(data, true, tableObserver);
    }

    @Override
    public void onResume(){
        super.onResume();
        //If account is not activated close this activity and signin again
        if (prefs.getBoolean(MainModel.ACCOUNT_ACTIVATED, false) == false) {
            Intent i = new Intent(MainActivity.this, SignInActivity.class);
            startActivity(i);
            finish();
        }

        //If its a small screen stick to portrait orientation and if its a 7" screen and above stick to landscape orientation
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600 && getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, lytDrawer);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
            isDrawerLocked = true;
        }else{
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED, lytDrawer);
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            mDrawerLayout.closeDrawer(lytDrawer);
        }

        //Check when the status changed
        mSyncStatusObserver.onStatusChanged(1);

        //Add a period sync for the DB by watching the Content Provider
        ContentResolver.addPeriodicSync(
                account,
                FunnelProvider.AUTHORITY,
                Bundle.EMPTY,
                5000L);

        //Watch for sync state changes
        final int mask = ContentResolver.SYNC_OBSERVER_TYPE_PENDING |
                ContentResolver.SYNC_OBSERVER_TYPE_ACTIVE;
        mSyncObserverHandle = ContentResolver.addStatusChangeListener(mask, mSyncStatusObserver);
    }

    @Override
    public void onPause() {
        super.onPause();
        // When this activity pauses Remove the status change listener
        if (mSyncObserverHandle != null) {
            ContentResolver.removeStatusChangeListener(mSyncObserverHandle);
            mSyncObserverHandle = null;
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        //Unregister Content Observer once activity is closed
        getContentResolver().unregisterContentObserver(tableObserver);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(isDrawerLocked == false){
                if (mDrawerLayout.isDrawerOpen(lytDrawer)) {
                    mDrawerLayout.closeDrawer(lytDrawer);
                } else {
                    Log.d(TAG,"--------||||||--------- RETURN 11");
                    return super.onKeyUp(keyCode, event);
                }
            }else{
                Log.d(TAG,"--------||||||--------- RETURN 12");
                return super.onKeyUp(keyCode, event);
            }
        }
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if(!isDrawerLocked){
                if (mDrawerLayout.isDrawerOpen(lytDrawer)) {
                    mDrawerLayout.closeDrawer(lytDrawer);
                } else {
                    Log.d(TAG,"--------||||||--------- RETURN 1");
                    return super.onKeyDown(keyCode, event);
                }
            }else{
                Log.d(TAG,"--------||||||--------- RETURN 2");
                return super.onKeyDown(keyCode, event);
            }
        }
        return true;
    }



    @Override
    public void moveToFragment(int position) {
        selectItem(position, 0);
    }

    //Closes stagesStateFragment
    @Override
    public void moveToPager(int fragmentPosition,int position) {
        Log.d(TAG, "closing Fragment " + fragmentPosition);
        //removeFragment(getSupportFragmentManager(),((Object) stagesStateFragment).getClass().getName());
       // Bundle args = new Bundle();
        //args.putInt(getResources().getString(R.string.bundle_fragment_position),1);
        stagesStateFragment.addScrollToFragment(fragmentPosition);
        //selectItem(2,1);
    }

    @Override
    public void transactionID(int code) {
        this.code = code;
        Log.d(TAG, "...................." + code);
    }

    @Override
    public void jsonData(String jsonData) {
        this.jsonData = jsonData;
    }

    @Override
    public void confirmSignOut() {
        logout();
    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Log.d(TAG,"Position is " + position);
                selectItem(position, 0);
        }
    }

    @Override
    public void setTitle(CharSequence title) {
        String mTitle = title.toString();
        getSupportActionBar().setTitle(mTitle);
    }

    /**
     *This method selects a fragment to be added to the activity based on the position passed
     * @param position The position in the listview passed so that the actual fragment can be added
     * @param fragmentPosition position to scroll to in viewpager
     */

    public void selectItem(int position,int fragmentPosition){
        selectPosition = position;
        Bundle args = new Bundle();
        args.putString(getString(R.string.bundle_title), drawerListArray[position]);
        if(isDrawerLocked == true){
            mDrawerLayout.closeDrawer(lytDrawer);
        }else {
            mDrawerLayout.closeDrawer(lytDrawer);
        }
       switch(position){
           case 0:
               homeFragment = new HomeFragment();
               Log.d(TAG,homeFragment.getClass().getName());
               homeFragment.setArguments(args);
               addFragment(homeFragment,true,FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(), homeFragment.getClass().getName());
               break;
           case 1:
               Calendar cal = Calendar.getInstance();
               args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
               args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
               args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
               args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
               args.putInt(getString(R.string.bundle_position),position);
               myCalendarFragment = new MyCalendarFragment2();
               myCalendarFragment.setArguments(args);
               addFragment(myCalendarFragment,true,FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(),((Object) myCalendarFragment).getClass().getName());
               break;
           case 2:
               args.putInt(getString(R.string.bundle_position), position);
               args.putInt(getString(R.string.bundle_fragment_position),fragmentPosition);
               stagesStateFragment = new StagesStateFragment();
               stagesStateFragment.setArguments(args);
               addFragment(stagesStateFragment,true,FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(),((Object) stagesStateFragment).getClass().getName());
               break;
           case 3:
               args.putInt(getString(R.string.bundle_position),position);
               salesFragment = new SalesFragment();
               salesFragment.setArguments(args);
               addFragment(salesFragment,true,FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(),((Object) salesFragment).getClass().getName());
               break;
           case 4:
               args.putInt(getString(R.string.bundle_position),position);
               activitiesFragment = new ActivitiesFragment();
               activitiesFragment.setArguments(args);
               addFragment(activitiesFragment,true,FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(),((Object) activitiesFragment).getClass().getName());
               break;
           case 5:
               args.putInt(getString(R.string.bundle_position),position);
               quotationsFragment = new QuotationsFragment();
               quotationsFragment.setArguments(args);
               addFragment(quotationsFragment,true,FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(),((Object) quotationsFragment).getClass().getName());
               break;
           case 6:
               args.putInt(getString(R.string.bundle_position),position);
               contactsFragment = new ContactsFragment();
               contactsFragment.setArguments(args);
               addFragment(contactsFragment,true,FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(),((Object) contactsFragment).getClass().getName());
               break;
           case 7:
               args.putInt(getString(R.string.bundle_position),position);
               organisationsFragment = new OrganisationsFragment();
               organisationsFragment.setArguments(args);
               addFragment(organisationsFragment,true,FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(),((Object) organisationsFragment).getClass().getName());
               break;
           case 8:
               args.putInt(getString(R.string.bundle_position),position);
               settingsFragment = new SettingsFragment();
               settingsFragment.setArguments(args);
               addFragment(settingsFragment,true,FragmentTransaction.TRANSIT_NONE,R.id.content_frame,getSupportFragmentManager(),((Object) settingsFragment).getClass().getName());
               break;
           case 9:
               args.putInt(getString(R.string.bundle_position),position);
               commentsFragment = new CommentsFragment();
               commentsFragment.setArguments(args);
               addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
               break;
           case 10:
               args.putString(getString(R.string.txt_advisory),getString(R.string.txt_advisory_exit));
               newFragment7.setArguments(args);
               newFragment7.show(addDialogFragment(getSupportFragmentManager()), "dialog");
               break;
       }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        mOptionsMenu = menu;
        getMenuInflater().inflate(R.menu.menu_main, menu);
        menu.findItem(R.id.action_email).setVisible(false);
        menu.findItem(R.id.action_add).setVisible(false);
        menu.findItem(R.id.action_edit).setVisible(false);
        menu.findItem(R.id.action_delete).setVisible(false);
        menu.findItem(R.id.action_flag).setVisible(false);
        menu.findItem(R.id.action_comments).setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (id == android.R.id.home) {
           if(((ViewGroup.MarginLayoutParams)frameLayout.getLayoutParams()).leftMargin == (int)getResources().getDimension(R.dimen.drawer_size)) {
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_OPEN, lytDrawer);
                mDrawerLayout.setScrimColor(Color.TRANSPARENT);
                isDrawerLocked = true;
            }else {
                if (mDrawerLayout.isDrawerOpen(lytDrawer)) {
                    mDrawerLayout.closeDrawer(lytDrawer);
                } else {
                    mDrawerLayout.openDrawer(lytDrawer);
                }
            }
        }else if(id == R.id.menu_refresh){
            Bundle bundle = new Bundle();
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true); // Performing a sync no matter if it's off
            bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true); // Performing a sync no matter if it's off
            getContentResolver().requestSync(account, FunnelProvider.AUTHORITY, bundle);
            SyncUtils.TriggerRefresh();
        }else if(id == R.id.action_logout){
            Bundle args = new Bundle();
            args.putString(getString(R.string.txt_advisory),getString(R.string.txt_advisory_exit));
            newFragment7.setArguments(args);
            newFragment7.show(addDialogFragment(getSupportFragmentManager()), "dialog");
        }
        return super.onOptionsItemSelected(item);
    }

    private Bundle setCustomResourceForDates() {

        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
        args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
        args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, true);

        // Uncomment this to customize startDayOfWeek
        args.putInt(CaldroidFragment.START_DAY_OF_WEEK,
                CaldroidFragment.MONDAY); // Tuesday

        // Uncomment this line to use Caldroid in compact mode
        args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, false);

        return args;

    }

    /**
     * This method adds a fragment to a layout in the activity
     * @param fragment The fragment to be added
     * @param addToBackStack Boolean to select whether to add the fragment to backstack or not
     * @param transition Which transition to use when adding the fragment
     * @param layoutResourceID The layout where the fragment will be added
     * @param fm The fragment manager which handles the adding and removing of the fragments
     * @param tag Tag which will uniquely identify the fragment added
     */
    public void addFragment(Fragment fragment, boolean addToBackStack, int transition,int layoutResourceID, FragmentManager fm, String tag){
        FragmentTransaction ft = fm.beginTransaction();
       // if(fm.findFragmentByTag(tag) == null) {
            //if (!fragmentPopped) {
            ft.replace(layoutResourceID, fragment, tag);
            Log.d(TAG, " boolean fragment  Added ");
            //}
            ft.setTransition(transition);
            if (addToBackStack)
                ft.addToBackStack(tag);
            ft.commit();
        /*}else{

            boolean fragmentPopped = fm.popBackStackImmediate(tag,FragmentManager.POP_BACK_STACK_INCLUSIVE);
            //ft.show(fragment).commit();


        }*/

            /*else{
                fm.beginTransaction().show(fragment).commit();
                Log.d(TAG," boolean fragment  removed ");

            }*/
    }

    public void removeFragment(FragmentManager fm, String tag){
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fm.findFragmentByTag(tag));
        ft.commit();
    }

    /**
     *
     * @param fm The fragment manager which handles the adding and removing of the fragments
     * @return The FragmentTransaction which add the dialog
     */

    public FragmentTransaction addDialogFragment(FragmentManager fm){

        FragmentTransaction ft = fm.beginTransaction();
        Fragment prev = fm.findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }

        return ft;
    }

    private SyncStatusObserver mSyncStatusObserver = new SyncStatusObserver() {
        /** Callback invoked with the sync adapter status changes. */
        @Override
        public void onStatusChanged(int which) {
            runOnUiThread(new Runnable() {
                /**
                 * The SyncAdapter runs on a background thread. To update the UI, onStatusChanged()
                 * runs on the UI thread.
                 */
                @Override
                public void run() {
                    // Create a handle to the account that was created by
                    // SyncService.CreateSyncAccount(). This will be used to query the system to
                    // see how the sync status has changed.
                    account = FunnelAccountService.GetAccount();
                    if (account == null) {
                        // GetAccount() returned an invalid value. This shouldn't happen, but
                        // we'll set the status to "not refreshing".
                        setRefreshActionButtonState(false);
                        Log.d(TAG, "No account");

                        return;
                    }

                    Log.d(TAG, "Yes account indeterminate progress bar");
                    // sendData(code,jsonData);

                    // Test the ContentResolver to see if the sync adapter is active or pending.
                    // Set the state of the refresh button accordingly.

                    if (NetworkConnectionStatus.isOnline(MainActivity.this)) {
                        boolean syncActive = ContentResolver.isSyncActive(
                                account, FunnelProvider.AUTHORITY);
                        boolean syncPending = ContentResolver.isSyncPending(
                                account, FunnelProvider.AUTHORITY);
                        setRefreshActionButtonState(syncActive || syncPending);
                    } else {
                        Toast.makeText(MainActivity.this, getString(R.string.txt_advisory_no_connection), Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    };

    /**
     * Set the state of the Refresh button. If a sync is active, turn on the ProgressBar widget.
     * Otherwise, turn it off.
     *
     * @param refreshing True if an active sync is occuring, false otherwise
     */
    public void setRefreshActionButtonState(boolean refreshing) {
        if (mOptionsMenu == null) {
            return;
        }
        final MenuItem refreshItem = mOptionsMenu.findItem(R.id.menu_refresh);
        if (refreshItem != null) {
            if (refreshing) {
                refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
            } else {
                refreshItem.setActionView(null);
            }
        }
    }

    /**
     * Removes all the saved items in the shared preferences and exits the application
     */
    private void logout() {
        editor.remove(getString(R.string.bundle_username)).commit();
        editor.remove(getString(R.string.bundle_password)).commit();
        editor.remove(getString(R.string.bundle_email_id)).commit();
        editor.remove(getString(R.string.bundle_user_roles)).commit();

        //Exit the application
        android.os.Process.killProcess(android.os.Process.myPid());
        MainActivity.super.onDestroy();
        finish();
    }
}
