package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.async_tasks.SendData;
import com.roamtech.android.sams.dialog_fragments.AdvisoryDialogFragment;
import com.roamtech.android.sams.dialog_fragments.ProgressDialogFragment;
import com.roamtech.android.sams.http.NetworkConnectionStatus;
import com.roamtech.android.sams.interfaces.ReturnRegistrationResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

/**
 *
 * Fragment that Creates user in the server
 *
 * Created by Karuri on 2/22/2015.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class CreateUsersFragment extends Fragment implements View.OnClickListener,ReturnRegistrationResult {
    public static final String TAG = "CreateUsersFragment";
    EditText editEmail,editUsername,editPassword,editConPassword;
    Button btnCreateUser;
    AdvisoryDialogFragment dialogAdvisory = new AdvisoryDialogFragment();
    MainActivity mainActivity;
    AppCompatActivity mActivity;
    ProgressDialogFragment progressDialogFragment;
    private SharedPreferences prefs;

    /**
     * Constructor
     */
    public CreateUsersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mainActivity = new MainActivity();
        progressDialogFragment = new ProgressDialogFragment();
        prefs = PreferenceManager
                .getDefaultSharedPreferences(mActivity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_create_users, container, false);
        editEmail = (EditText) rootView.findViewById(R.id.edit_email);
        editUsername = (EditText) rootView.findViewById(R.id.edit_username);
        editPassword = (EditText) rootView.findViewById(R.id.edit_password);
        editConPassword = (EditText) rootView.findViewById(R.id.edit_con_password);
        btnCreateUser = (Button) rootView.findViewById(R.id.btn_create_user);
        btnCreateUser.setOnClickListener(this);
        return rootView;
    }

    /**
     * Validates data that the user has entered before sending to the server
     */
    private void fillData(){
        Bundle args = new Bundle();
        if(editEmail.getText().length() == 0){
            args.putString(this.getResources().getString(R.string.txt_advisory), this.getResources().getString(R.string.txt_advisory_enter_email));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }else if(RegistrationFragment.isValidEmail(editEmail.getText()) == false) {
            args.putString(this.getResources().getString(R.string.txt_advisory), this.getResources().getString(R.string.txt_advisory_enter_valid_email));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }else if(editUsername.getText().length() == 0) {
            args.putString(this.getResources().getString(R.string.txt_advisory),this.getResources().getString(R.string.txt_advisory_enter_username));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }else if(editPassword.getText().length() == 0){
            args.putString(this.getResources().getString(R.string.txt_advisory),this.getResources().getString(R.string.txt_advisory_enter_password));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }else if(editConPassword.getText().length() == 0) {
            args.putString(this.getResources().getString(R.string.txt_advisory), this.getResources().getString(R.string.txt_advisory_enter_password));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }else if(!editPassword.getText().toString().equals(editConPassword.getText().toString())){
            args.putString(this.getResources().getString(R.string.txt_advisory), this.getResources().getString(R.string.txt_advisory_confirm_password));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }else{
            if(NetworkConnectionStatus.isOnline(mActivity)){
                new SendData(mActivity,this).execute(regUserJsonData(),mActivity.getResources().getString(R.string.name_value_userMgt),regJsonData(),mActivity.getResources().getString(R.string.name_value_admin_user));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_create_user:
                fillData();
                break;
        }
    }

   /**
    *
    *Creates JSON data with company details entered
    *
    * @return String of JSON data created from company details
    */
    public String regJsonData(){
        JSONArray jsonArray = new JSONArray();
        JSONObject reg = new JSONObject();
        Calendar c = Calendar.getInstance();
        try {
            reg.put(mActivity.getResources().getString(R.string.name_value_username), editUsername.getText().toString());
            Log.d(TAG,prefs.getString(mActivity.getResources().getString(R.string.bundle_username),""));
            reg.put(mActivity.getResources().getString(R.string.name_value_admin), prefs.getString(mActivity.getResources().getString(R.string.bundle_username),""));
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        jsonArray.put(reg);
        return jsonArray.toString();
    }

    /**
     *
     * Creates JSON data with user details entered
     *
     * @return String of JSON data created from user details
     */
    public String regUserJsonData(){
        JSONArray jsonArray = new JSONArray();
        JSONObject reg = new JSONObject();
        Calendar c = Calendar.getInstance();
        try {
            reg.put(mActivity.getResources().getString(R.string.name_value_email), editEmail.getText().toString());
            reg.put(mActivity.getResources().getString(R.string.name_value_username), editUsername.getText().toString());
            reg.put(mActivity.getResources().getString(R.string.name_value_password), editPassword.getText().toString());
            reg.put(mActivity.getResources().getString(R.string.name_value_usseroles),"ADMIN");
            reg.put(mActivity.getResources().getString(R.string.name_value_firstname),editUsername.getText().toString());
            reg.put(mActivity.getResources().getString(R.string.name_value_lastname), editUsername.getText().toString());
            reg.put(mActivity.getResources().getString(R.string.name_value_ou), mActivity.getResources().getString(R.string.name_value_roamtech));
            reg.put(mActivity.getResources().getString(R.string.name_value_action), "add");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        jsonArray.put(reg);
        return jsonArray.toString();
    }

    @Override
    public void onStartTask() {
        progressDialogFragment = new ProgressDialogFragment.Builder()
                .setMessage(getString(R.string.loading)).setCancelableOnTouchOutside(false)
                .build();
        progressDialogFragment.show(mActivity.getSupportFragmentManager(),getString(R.string.dialog_tag));
    }

    @Override
    public void onReturnResult(Boolean authenticate) {
        if(progressDialogFragment != null) {
            progressDialogFragment.dismiss(mActivity.getSupportFragmentManager());
        }
        if(authenticate == true){
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_user_created),Toast.LENGTH_LONG).show();
            clearFields();
        }else{
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_user_created_failed),Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Clear all the TextFields entered
     */
    public void clearFields(){
        editUsername.setText("");
        editPassword.setText("");
        editConPassword.setText("");
    }
}
