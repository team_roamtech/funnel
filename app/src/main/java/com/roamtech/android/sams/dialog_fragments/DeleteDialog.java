package com.roamtech.android.sams.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.roamtech.android.sams.R;

/**
 * Created by dennis on 2/27/15.
 */
public class DeleteDialog extends DialogFragment implements View.OnClickListener {
    public static final String TAG = "DeleteDialog";
    AppCompatActivity mActivity;
    Button btnDelete,btnCancel;
    EditText txtDeleteWarning;
    String warning;

    public DeleteDialog() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if(args != null){
            warning = args.getString(mActivity.getResources().getString(R.string.txt_advisory));
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_delete_warning);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.dialog_title_date));
        dialog.setCanceledOnTouchOutside(false);
        txtDeleteWarning = (EditText) dialog.findViewById(R.id.txt_delete_warning);
        txtDeleteWarning.setText(warning);
        btnDelete = (Button) dialog.findViewById(R.id.btn_delete);
        btnDelete.setOnClickListener(this);
        btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        return dialog;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_delete:
                Intent intent = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent);
                Log.d(TAG, String.valueOf(getTargetRequestCode()));
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                dismiss();
                break;
            case R.id.btn_cancel:
                dismiss();
                break;
        }

    }

}
