package com.roamtech.android.sams.interfaces;

import com.roamtech.android.sams.models.LoginItems;
import com.unboundid.ldap.sdk.LDAPException;

/**
 *
 * Monitors and returns result of an asyntask thread running in the background registering new user to the server
 *
 * Created by dennis on 6/30/15.
 * @author Dennis Mwangi Karuri
 */
public interface ReturnRegistrationResult {

    /**
     * Defines what should happen before the background task is started
     */
    void onStartTask();

    /**
     *
     * Defines what should happen once the background task is completed and also returns to the foreground thread the result
     * of the Background thread running
     *
     * @param authenticate Boolean result true if registration was successful and false if registration failed
     * @throws LDAPException
     */
    void onReturnResult(Boolean authenticate) throws LDAPException;
}
