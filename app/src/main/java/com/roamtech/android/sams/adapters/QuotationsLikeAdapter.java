package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.interfaces.LaunchFragment;

/**
 * Created by dennis on 6/8/15.
 */
public class QuotationsLikeAdapter extends CursorAdapter {
    public static final String TAG = "QuotationsLikeAdapter";
    private LayoutInflater vi;
    Cursor c;
    Context context;
    TextView txtQuotationName;

    public QuotationsLikeAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.c = c;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View rootView = vi.inflate(R.layout.list_like_quotations, parent, false);
        return rootView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView txtQuotationName = (TextView) view.findViewById(R.id.txt_quotation_name);
        txtQuotationName.setText(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_qu_name))));
    }
}
