package com.roamtech.android.sams.models;

/**
 *
 * Class that stores LoginItems together
 *
 * Created by dennis on 6/30/15.
 * @author Dennis Mwangi Karuri
 */
public class LoginItems {
    public boolean isUser = false;
    public String [] roles;
    public int resultCode;
    public String mail;

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public boolean isUser() {
        return isUser;
    }

    public void setUser(boolean isUser) {
        this.isUser = isUser;
    }


}
