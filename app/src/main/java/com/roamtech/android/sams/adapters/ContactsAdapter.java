package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.database.Cursor;
import android.database.StaleDataException;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.roamtech.android.sams.R;

/**
 * Created by Karuri on 1/28/2015.
 */
public class ContactsAdapter extends CursorAdapter {
    public static final String TAG = "ContactsAdapter";
    private LayoutInflater vi;
    Cursor c;
    Context context;

    public ContactsAdapter(Context context, Cursor c, int flags) {
        super(context, c, flags);
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.context = context;
        this.c = c;
        Log.d(TAG, "ContactsAdapter constructor");
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {

        Log.d(TAG,"PlaylistAdapter new view");
        return vi.inflate(R.layout.auto_textview_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        if (cursor != null && !cursor.isClosed()) {
            TextView txtContact = (TextView) view.findViewById(R.id.txt_contact);
            TextView txtOrganisation = (TextView) view.findViewById(R.id.txt_organisation);
            try {
                txtContact.setText(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_con_name))));
                String organisationName = cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_organisation_name)));
                if (!organisationName.equals(context.getResources().getString(R.string.db_table_org_entry))) {
                    txtOrganisation.setText(organisationName);
                }
            } catch (StaleDataException e) {
                e.printStackTrace();
            }
        }
    }
}
