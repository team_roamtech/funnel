package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.QuotationsAdapter;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 *
 * Fragment that lists all quotations at a certain stage
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class QuotationStageFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,LaunchFragment {
    public static final String TAG = "QuotationStageFragment";
    public static final int PAGER_FRAGMENT = 2;
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    StagesStateFragment stagesStateFragment;
    public static final int LOADER = 1800;
    ListView lstQuotations;
    Cursor c;
    View infoView;
    ProgressBar mProgresBar;
    TextView txtNoContacts,txtAddContacts;
    QuotationsAdapter mAdapter;
    ViewQuotationFragment viewQuotationFragment;
    int position,position2;
    int fragmentPosition;
    boolean isVisible = false;
    String title2;
    String stageName;
    public static final int LOADER1 = 2011;
    public static final int LOADER2 = 2012;
    public static final int LOADER3 = 2013;
    String [] pagerItemsArray;

    /**
     * Constructor
     */
    public QuotationStageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            //mCallBack2 = (OnAddSelectedListener)activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        stagesStateFragment = new StagesStateFragment();
        pagerItemsArray = mActivity.getResources().getStringArray(R.array.pager_title_items);
        Bundle args = getArguments();
        if(args != null){
            //position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
           // fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position),-1);
            //title2 = args.getString(mActivity.getResources().getString(R.string.bundle_title));
        }
        mainActivity = new MainActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_quotations, container, false);
        lstQuotations = (ListView) rootView.findViewById(R.id.lst_contacts);
        infoView = (View) rootView.findViewById(R.id.error_container);
        mProgresBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        txtNoContacts = (TextView) rootView.findViewById(R.id.txt_no_deals);
        txtNoContacts.setText(mActivity.getResources().getString(R.string.txt_noquotations));
        txtAddContacts = (TextView) rootView.findViewById(R.id.txt_adddeals);
        txtAddContacts.setText(" ");
        mAdapter = new QuotationsAdapter(mActivity, null, 0,this);
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.d(TAG, TAG + " " + "setUserVisibleHint");
        if (isVisibleToUser) {
            isVisible = isVisibleToUser;
            Bundle args = getArguments();
            if(args != null) {
                fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
                title2 = args.getString(mActivity.getResources().getString(R.string.bundle_title));
                position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
                Log.d(TAG," position setUserVisibleHint " + position2);

                //mActivity.getSupportActionBar().setTitle(title2);
            }
            Log.d(TAG, TAG + " " + " setUserVisibleHint title2 "+title2);
            stageName = title2;
            switch(fragmentPosition){
                case 2:
                    mActivity.getSupportLoaderManager().restartLoader(LOADER1, null, this);
                    mActivity.getSupportActionBar().setTitle(pagerItemsArray[2]);
                    break;
                case 3:
                    mActivity.getSupportLoaderManager().restartLoader(LOADER2, null, this);
                    mActivity.getSupportActionBar().setTitle(pagerItemsArray[3]);
                    break;
                case 4:
                    mActivity.getSupportLoaderManager().restartLoader(LOADER3, null, this);
                    mActivity.getSupportActionBar().setTitle(pagerItemsArray[4]);
                    break;
            }
        }else{

        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.PAGER_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    int fragmentPos = data.getIntExtra(mActivity.getResources().getString(R.string.bundle_fragment_position),2);
                    Intent intent = new Intent();
                    intent.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPos);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    //mActivity.getSupportFragmentManager().findFragmentByTag(((Object) stagesStateFragment).getClass().getName()).onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                }
            }
        }
    }

    @Override
    public void launchFragment(int position, Bundle bundles, int view) {
        Log.d(TAG, "launchFragment view == " + position2);
        bundles.putInt(mActivity.getResources().getString(R.string.bundle_position), position2);
        Log.d(TAG, bundles.getString(mActivity.getResources().getString(R.string.db_table_qu_name)));
        viewQuotationFragment = new ViewQuotationFragment();
        viewQuotationFragment.setArguments(bundles);
        viewQuotationFragment.setTargetFragment(QuotationStageFragment.this, MainModel.PAGER_FRAGMENT);
        mainActivity.addFragment(viewQuotationFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) viewQuotationFragment).getClass().getName());

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"49");
        return new CursorLoader(getActivity(),data, null, null,
                new String[] {stageName}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mProgresBar.setVisibility(View.GONE);
        c = data;
        Log.d(TAG, "On Load Finished --------" + String.valueOf(c.getCount()));
        if(c == null || c.getCount() == 0 ){
            infoView.setVisibility(View.VISIBLE);
            lstQuotations.setVisibility(View.GONE);
        }else{
            c = data;
            mAdapter.swapCursor(c);
            infoView.setVisibility(View.GONE);
            lstQuotations.setVisibility(View.VISIBLE);
            lstQuotations.setAdapter(mAdapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
