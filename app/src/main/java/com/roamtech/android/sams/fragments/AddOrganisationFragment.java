package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.dialog_fragments.DeleteDialog;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.interfaces.CheckProvider;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.interfaces.SendJsonData;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddOrganisationFragment extends Fragment  implements View.OnClickListener{
    public static final String TAG = "AddOrganisationFragment";
    EditText editOrgName,editAddress,editOrgEmail,editOrgPhone,editOrgWebsite;
    int orgID;
    String orgName,orgAddress,orgPhone,orgEmail,orgWebsite;
    Button btnAddOrg,btnEditOrg;
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    int orgsDone = 1;
    DeleteDialog newFragment6;
    String [] arrayMainItems;
    int position;
    SendJsonData mCallBack3;
    CheckProvider mCallBack4;
    int syncDone;
    String slugID;
    int status = 1;
    SharedPreferences.Editor editor;
    SharedPreferences prefs;
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    GetUsername getUsername;
    ImageButton imgClose;
    AddActivitiesFragment addActivitiesFragment;
    CommentsFragment commentsFragment;

    public AddOrganisationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack3 = (SendJsonData) activity;
            mCallBack4 = (CheckProvider) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        commonHash = new CommonHash(mActivity);
        mainActivity = new MainActivity();
        arrayMainItems = getResources().getStringArray(R.array.main_activity_items);
        Bundle args = getArguments();
        if(args != null){
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            orgName = args.getString(mActivity.getResources().getString(R.string.db_table_org_name));
            orgAddress = args.getString(mActivity.getResources().getString(R.string.db_table_org_address));
            orgEmail = args.getString(mActivity.getResources().getString(R.string.db_table_org_email));
            orgPhone = args.getString(mActivity.getResources().getString(R.string.db_table_org_phone));
            orgWebsite = args.getString(mActivity.getResources().getString(R.string.db_table_org_website));
            orgID =args.getInt(mActivity.getResources().getString(R.string.db_table_id));
            syncDone = args.getInt(mActivity.getResources().getString(R.string.db_table_sync_done));
            status = args.getInt(mActivity.getResources().getString(R.string.db_table_org_status));
            slugID = args.getString(mActivity.getResources().getString(R.string.db_table_org_slug_id));
            Log.d(TAG, "Common Hash is  " + slugID);
        }
        newFragment6 = new DeleteDialog();
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        getUsername = new GetUsername(mActivity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_organisation, container, false);
        editOrgName = (EditText) rootView.findViewById(R.id.edit_organisation_name);
        if(orgName != null){
            editOrgName.setText(orgName);
        }
        editAddress = (EditText) rootView.findViewById(R.id.edit_address);
        if(orgAddress != null){
            editAddress.setText(orgAddress);
        }
        editOrgEmail = (EditText) rootView.findViewById(R.id.edit_email);
        if(orgEmail != null){
            editOrgEmail.setText(orgEmail);
        }
        editOrgPhone = (EditText) rootView.findViewById(R.id.edit_phone_num);
        if(orgPhone != null){
            editOrgPhone.setText(orgPhone);
        }
        editOrgWebsite = (EditText) rootView.findViewById(R.id.edit_website);
        if(orgWebsite != null){
            editOrgWebsite.setText(orgWebsite);
        }
        btnAddOrg = (Button) rootView.findViewById(R.id.btn_add_organisation);
        btnAddOrg.setOnClickListener(this);
        btnEditOrg = (Button) rootView.findViewById(R.id.btn_edit_organisation);
        btnEditOrg.setOnClickListener(this);
        imgClose = (ImageButton) rootView.findViewById(R.id.fab_button);
        imgClose.setOnClickListener(this);
        if(orgID != 0){
            btnAddOrg.setVisibility(View.GONE);
            btnEditOrg.setVisibility(View.VISIBLE);
        }
        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
        if(orgID != 0) {
            menu.findItem(R.id.action_flag).setVisible(true);
            menu.findItem(R.id.action_comments).setVisible(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                Bundle args2 = new Bundle();
                args2.putString(mActivity.getResources().getString(R.string.db_table_org_name),orgName);
                args2.putString(mActivity.getResources().getString(R.string.db_table_act_model),mActivity.getResources().getString(R.string.db_table_model_2));
                args2.putString(mActivity.getResources().getString(R.string.db_table_act_foreign_key),slugID);
                addActivitiesFragment.setArguments(args2);
                addActivitiesFragment.setTargetFragment(AddOrganisationFragment.this, MainModel.PAGER_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                Bundle args = new Bundle();
                args.putString(mActivity.getResources().getString(R.string.db_table_org_name),orgName);
                args.putString(mActivity.getResources().getString(R.string.db_table_act_model),mActivity.getResources().getString(R.string.db_table_model_2));
                args.putString(mActivity.getResources().getString(R.string.db_table_act_foreign_key),slugID);
                commentsFragment.setArguments(args);
                commentsFragment.setTargetFragment(AddOrganisationFragment.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
            default:
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == MainModel.DIALOG_FRAGMENT_DELETE_DIALOG ) {
            if (resultCode == Activity.RESULT_OK) {
                status = 0;
                insertOrganisations();
                Intent intent = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                getFragmentManager().popBackStackImmediate();
            } else {

            }
        }
    }

    public void fillData(){
        Bundle args = new Bundle();
        if(editOrgName.length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_no_name), Toast.LENGTH_LONG).show();
        }else{
            if(slugID == null) {
                Log.d(TAG, "Common Hash is  number 2" + slugID);
                orgID = (int)insertOrganisations();
                try {
                    slugID = commonHash.getHash(String.valueOf(orgID));
                    Log.d(TAG, "Common Hash is  " + slugID);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                insertOrganisations();
            }else{
                insertOrganisations();
            }
            Intent intent = new Intent();
            mActivity.setResult(Activity.RESULT_OK, intent);
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
            getFragmentManager().popBackStackImmediate();
        }
    }

    public long insertOrganisations(){
        int playListId = 0;
        syncDone = 1;
        coordinates = getCoordinates.getCoordinates();
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_name), editOrgName.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_address), editAddress.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_phone), editOrgPhone.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_email), editOrgEmail.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_website), editOrgWebsite.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_status), status);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_org_slug_id), slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_org_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_org_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_org_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_org_longitude), 0.0);
        }
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());
        if(orgID == 0) {
            initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "1");
            Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
            playListId = (int)ContentUris.parseId(insertUri);
        }else{
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "12");
            playListId = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(orgID),mActivity.getResources().getString(R.string.flag_sync)});
        }
        Log.d(TAG, "-------" + String.valueOf(playListId));
        return playListId;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_add_organisation:
                fillData();
                break;
            case R.id.btn_edit_organisation:
                fillData();
                break;
            case R.id.fab_button:
                if(orgID == 0){
                    Intent intent = new Intent();
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    getFragmentManager().popBackStackImmediate();
                }else{
                    Bundle args = new Bundle();
                    args.putString(mActivity.getResources().getString(R.string.txt_advisory),mActivity.getResources().getString(R.string.txt_advisory_delete) + " " +arrayMainItems[7]);
                    newFragment6.setArguments(args);
                    newFragment6.setTargetFragment(AddOrganisationFragment.this, MainModel.DIALOG_FRAGMENT_DELETE_DIALOG);
                    newFragment6.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                }
                break;
        }
    }
}
