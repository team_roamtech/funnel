package com.roamtech.android.sams.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.StagesPagerAdapter;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.utils.AsyncLoader;

/**
 *
 * Fragment that contains a ViewPager that has other Fragments that have the different sale stages
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class StagesStateFragment extends Fragment implements LoaderManager.LoaderCallbacks<String[]>, View.OnClickListener {
    public static final String TAG = "StagesStateFragment";
    AppCompatActivity mActivity;
    ViewPager mPager;
    StagesPagerAdapter mAdapter;
    String[] pagerItemsArray;
    NewDealFragment newDealFragment;
    MainActivity mainActivity;
    public static final int LOADER = 1105;
    public static final int PAGER_FRAGMENT = 2;
    int position;
    int fragmentPosition, pagerScrollPosition;
    ImageButton imgFab, imgFabClose;
    AddContactsFragment addContactsFragment;
    OnAddSelectedListener mCallBack2;
    String title;
    AddActivitiesFragment addActivitiesFragment;
    CommentsFragment commentsFragment;

    /**
     * Constructor
     */
    public StagesStateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Bundle args = getArguments();
        if(args != null) {
            pagerScrollPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
            Log.d(TAG, "Pager Scroll position1 = " + pagerScrollPosition);
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
        }
        mainActivity = new MainActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_stages_state, container, false);
        pagerItemsArray = mActivity.getResources().getStringArray(R.array.pager_title_items);
        imgFab = (ImageButton) rootView.findViewById(R.id.fab_button);
        imgFab.setOnClickListener(this);
        imgFabClose = (ImageButton) rootView.findViewById(R.id.fab_button_close);
        imgFabClose.setOnClickListener(this);
        mPager = (ViewPager) rootView.findViewById(R.id.pager);
        mPager.setOffscreenPageLimit(0);
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                fragmentPosition = position;
               // if (position == 0) {
                title = pagerItemsArray[position];
                    //mActivity.getSupportActionBar().setTitle(pagerItemsArray[position]);
                //}
                if (fragmentPosition > 1) {
                    imgFabClose.setVisibility(View.VISIBLE);
                    imgFab.setVisibility(View.GONE);
                } else {
                    imgFabClose.setVisibility(View.GONE);
                    imgFab.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mainActivity = new MainActivity();
        newDealFragment = new NewDealFragment();
        mActivity.getSupportLoaderManager().initLoader(LOADER, null, this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        //mActivity.getSupportActionBar().setTitle(title);
        /*Bundle args = getArguments();
        if(args != null) {
            mActivity.getSupportActionBar().setTitle(args.getString(mActivity.getResources().getString(R.string.bundle_title)));
        }*/
      //  mActivity.getSupportActionBar().setTitle(pagerItemsArray[0]);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_flag).setVisible(true);
        menu.findItem(R.id.action_comments).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                addActivitiesFragment.setTargetFragment(StagesStateFragment.this, MainModel.ACTIVITY_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                commentsFragment.setTargetFragment(StagesStateFragment.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
            default:

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (pagerItemsArray != null) {
            outState.putStringArray(mActivity.getResources().getString(R.string.bundle_pager_items), pagerItemsArray);
            outState.putInt(mActivity.getResources().getString(R.string.bundle_position), position);
            outState.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
        }
    }

    /**
     * Assigns position of the ViewPager to a Variable
     * @param position postion of the ViewPager
     */
    public void addScrollToFragment(int position) {
        Log.d(TAG, String.valueOf(position));

        pagerScrollPosition = position;
//        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
      // mPager.setCurrentItem(position, true);
       // if (args != null) {
            // position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            //fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
            //mAdapter.notifyDataSetChanged();
            //mPager.setCurrentItem(fragmentPosition, true);
            //mActivity.getSupportLoaderManager().initLoader(LOADER, null, this);
        //}
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case MainModel.PAGER_FRAGMENT:
                if (resultCode == Activity.RESULT_OK) {
                    if(data != null) {
                        Log.d(TAG,"StagesStateFragment 1");
                        pagerScrollPosition = data.getIntExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), 0);
                        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
                    }
                    //mainActivity.addFragment(newDealFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, getFragmentManager(), ((Object) newDealFragment).getClass().getName());
                }
                break;
        }
    }

    /**
     *
     * Closes the Fragment
     *
     * @param position Position of fragment to be closed in the Navigation Drawer ListView Adapter
     * @param atv Activity to close the fragment
     */
    public void closeStateFragment(int position, AppCompatActivity atv) {
        atv.getSupportFragmentManager().popBackStackImmediate();
    }

    @Override
    public void onClick(View v) {
        Bundle args = new Bundle();
        Log.d(TAG, " fragment position " + fragmentPosition);
        switch (v.getId()) {
            case R.id.fab_button:
                switch (fragmentPosition) {
                    case 0:
                        addContactsFragment = new AddContactsFragment();
                        addContactsFragment.setTargetFragment(StagesStateFragment.this, MainModel.PAGER_FRAGMENT);
                        mainActivity.addFragment(addContactsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addContactsFragment).getClass().getName());
                        break;
                    case 1:
                        //StagesFragment stagesFragment;
                        //stagesFragment = new StagesFragment();
                        args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
                        args.putString(mActivity.getResources().getString(R.string.db_table_sale_stage), pagerItemsArray[fragmentPosition]);
                        newDealFragment.setArguments(args);
                        newDealFragment.setTargetFragment(StagesStateFragment.this, MainModel.PAGER_FRAGMENT);
                        mainActivity.addFragment(newDealFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) newDealFragment).getClass().getName());
                        break;
                }
                break;
            case R.id.fab_button_close:
                mActivity.getSupportFragmentManager().popBackStackImmediate();
                break;
        }

    }

    /**
     *
     * Class that runs in the Background Loading String references to Fragments to be added to the ViewPager
     * Main thread as an ArrayList<ReportItems>
     *
     */
    public static class GetFragments extends AsyncLoader<String[]> {
        String[] arrayItems;
        Context ctx;

        public GetFragments(Context context, String[] arrayItems) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.arrayItems = arrayItems;
            Log.d(TAG, " loader constructor ");

        }

        @Override
        public String[] loadInBackground() {
            return arrayItems;
        }
        //Process the JSON result to Data we can use
    }

    @Override
    public Loader<String[]> onCreateLoader(int id, Bundle args) {
        return new GetFragments(getActivity(), pagerItemsArray);
    }

    @Override
    public void onLoadFinished(Loader<String[]> loader, String[] data) {
        pagerItemsArray = data;
        mAdapter = new StagesPagerAdapter(mActivity, getChildFragmentManager(), pagerItemsArray, this, MainModel.PAGER_FRAGMENT, position);
        if (mAdapter != null) {
            mPager.setAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
            Log.d(TAG, "Pager Scroll position = " + pagerScrollPosition);
            mPager.setCurrentItem(pagerScrollPosition, true);
        }
    }

    @Override
    public void onLoaderReset(Loader<String[]> loader) {

    }
}
