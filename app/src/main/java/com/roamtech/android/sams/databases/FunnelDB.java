package com.roamtech.android.sams.databases;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.roamtech.android.sams.utils.GetUsername;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by dena on 1/20/15.
 */
public class FunnelDB {
    public static final String TAG = "FunnelDB";
    private final Context mCtx;
    private DbController mDbController;
    SQLiteDatabase db;
    public static String DB_PATH = "/data/data/com.roamtech.android.sams/databases/";
    public static String DB_NAME = "funnel_db";
    GetUsername getUsername;

    public FunnelDB(Context ctx) {
        this.mCtx = ctx;
        db = null;
        mDbController = new DbController(mCtx);
        mDbController.onCreate(db);
        getUsername = new GetUsername(ctx);
    }

    public static class DbController extends SQLiteOpenHelper {
        Context ctx;


        private SQLiteDatabase myDataBase;
        public DbController(Context context) {
            super(context, DB_NAME, null, 2);
            ctx = context;
            Log.d(TAG, "DB started");
        }

        /**
         * Creates a empty database on the system and rewrites it with your own database.
         * */
        public void createDataBase() throws IOException {

            boolean dbExist = checkDataBase();

            if(dbExist){
                //do nothing - database already exist
            }else{

                //By calling this method and empty database will be created into the default system path
                //of your application so we are gonna be able to overwrite that database with our database.
                this.getReadableDatabase();

                try {
                    copyDataBase();
                } catch (IOException e) {
                    e.printStackTrace();
                    throw new Error(" copying database");
                }
            }
        }

        /**
         * Check if the database already exist to avoid re-copying the file each time you open the application.
         * @return true if it exists, false if it doesn't
         */
        private boolean checkDataBase(){

            SQLiteDatabase checkDB = null;
            Boolean itExists = false;

            try{
                itExists = ctx.getDatabasePath(DB_NAME).exists();
                //checkDB = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

            }catch(SQLiteException e){
                e.printStackTrace();
                //database does't exist yet.

            }

	    	/*if(checkDB != null){

	    		checkDB.close();

	    	}*/

            return itExists;
        }

        /**
         * Copies your database from your local assets-folder to the just created empty database in the
         * system folder, from where it can be accessed and handled.
         * This is done by transfering bytestream.
         * */
        private void copyDataBase() throws IOException{

            //Open your local db as the input stream
            InputStream myInput = ctx.getAssets().open(DB_NAME);

            // Path to the just created empty db
            String outFileName = DB_PATH + DB_NAME;

            //Open the empty db as the output stream
            OutputStream myOutput = new FileOutputStream(outFileName);

            //transfer bytes from the inputfile to the outputfile
            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer))>0){
                myOutput.write(buffer, 0, length);
            }

            //Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();

        }

        public void openDataBase() throws SQLException {

            //Open the database
            String myPath = DB_PATH + DB_NAME;
            myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READONLY);

        }

        @Override
        public synchronized void close() {

            if(myDataBase != null)
                myDataBase.close();

            super.close();

        }
        @Override
        public void onCreate(SQLiteDatabase arg0) {
            // TODO Auto-generated method stub
            try {
                createDataBase();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
            // TODO Auto-generated method stub
            arg0.execSQL("CREATE TABLE IF NOT EXISTS 'comments' " +
                    "(_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    "cm_slug_id VARCHAR UNIQUE," +
                    "cm_title VARCHAR," +
                    "cm_issue STRING,"+
                    "cm_description TEXT," +
                    "sync_done INTEGER," +
                    "cm_model VARCHAR(45)," +
                    "cm_foreign_key STRING," +
                    "cm_latitude DOUBLE," +
                    "cm_longitude DOUBLE," +
                    "user_id VARCHAR" +
                    ")");
        }
    }

    public Cursor getSaleActivities(String sale_slug_id){
        return mDbController.getReadableDatabase().rawQuery("SELECT a._id,a.act_slug_id,a.act_type,a.act_schedule,a.act_foreign_key,a.act_feedback,a.act_description," +
                "a.act_status,a.act_model,a.sync_done FROM activities AS a WHERE a.act_foreign_key = '"+sale_slug_id+"' AND a.act_model = 'sales' AND a.act_status <> 0",null);
    }

    public Cursor getSales(String stage,int status){
        return mDbController.getReadableDatabase().rawQuery("SELECT s._id,s.sale_slug_id,s.sale_stage,s.sale_name,s.sale_status,s.sale_cost,s.sale_foreign_key,s.sale_model," +
                "s.sale_description,s.sale_payment_type,s.sync_done,s.sale_foreign_key,s.sale_model,c.con_name,c.con_slug_id,o.org_name,o.org_slug_id FROM sales AS s,contacts AS c, organisations AS o" +
                "  WHERE s.sale_foreign_key = c.con_slug_id AND s.sale_model = 'contacts' AND c.con_foreign_key = o.org_slug_id AND c.con_model = 'organisations' AND " +
                "s.sale_stage = '"+ stage +"' AND s.sale_status <> '"+status+"' AND s.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getSales(int status){
        return mDbController.getReadableDatabase().rawQuery("SELECT s._id,s.sale_slug_id,s.sale_stage,s.sale_name,s.sale_status,s.sale_cost,s.sale_foreign_key,s.sale_model," +
                "s.sale_description,s.sale_payment_type,s.sync_done,s.sale_foreign_key,s.sale_model,c.con_name,c.con_slug_id,o.org_name,o.org_slug_id FROM sales AS s,contacts AS c, organisations AS o" +
                "  WHERE s.sale_foreign_key = c.con_slug_id AND s.sale_model = 'contacts' AND c.con_foreign_key = o.org_slug_id AND c.con_model = 'organisations' AND " +
                " s.sale_status <> '"+status+"' AND s.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getConvertedSales(int status){
        return mDbController.getReadableDatabase().rawQuery("SELECT s._id,s.sale_slug_id,s.sale_stage,s.sale_name,s.sale_status,s.sale_cost,s.sale_foreign_key,s.sale_model," +
                "s.sale_description,s.sale_payment_type,s.sync_done,s.sale_foreign_key,s.sale_model,c.con_name,c.con_slug_id,o.org_name,o.org_slug_id FROM sales AS s,contacts AS c, organisations AS o" +
                "  WHERE s.sale_foreign_key = c.con_slug_id AND s.sale_model = 'contacts' AND c.con_foreign_key = o.org_slug_id AND c.con_model = 'organisations' AND " +
                " s.sale_status = '"+status+"' AND s.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getSale(String slugID){
        return mDbController.getReadableDatabase().rawQuery("SELECT s._id,s.sale_slug_id,s.sale_stage,s.sale_name,s.sale_status,s.sale_cost,s.sale_foreign_key,s.sale_model," +
                "s.sale_description,s.sale_payment_type,s.sync_done,s.sale_foreign_key,s.sale_model,c.con_name,c.con_slug_id,o.org_name,o.org_slug_id FROM sales AS s,contacts AS c, organisations AS o" +
                "  WHERE s.sale_foreign_key = c.con_slug_id AND s.sale_model = 'contacts' AND c.con_foreign_key = o.org_slug_id AND c.con_model = 'organisations' AND " +
                "s.sale_slug_id = '"+ slugID +"' AND s.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getSalesLike(String name){
        return mDbController.getReadableDatabase().rawQuery("SELECT s._id,s.sale_slug_id,s.sale_stage,s.sale_name,s.sale_status,s.sale_cost,s.sale_foreign_key,s.sale_model," +
                "s.sale_description,s.sale_payment_type,s.sync_done,s.sale_foreign_key,s.sale_model,c.con_name,c.con_slug_id,o.org_name,o.org_slug_id FROM sales AS s,contacts AS c, organisations AS o" +
                " WHERE s.sale_foreign_key = c.con_slug_id AND s.sale_model = 'contacts' AND c.con_foreign_key = o.org_slug_id AND c.con_model = 'organisations' AND" +
                " s.sale_status <> 2 AND s.user_id = '"+getUsername.getUserName()+"' " +
                "AND s.sale_name LIKE ?"
                ,new String[]{"%" +name + "%"});
    }

    public Cursor getQuotationsLike(String name){
        return mDbController.getReadableDatabase().rawQuery("SELECT q._id,q.qu_name,q.qu_slug_id,q.qu_created,q.qu_sale_id,c.con_name,c.con_slug_id,o.org_name,o.org_slug_id,s.sale_name,s.sale_slug_id " +
                "FROM quotations AS q, sales AS s, contacts AS c, organisations AS o " +
                "WHERE q.qu_sale_id = s.sale_slug_id " +
                "AND s.sale_foreign_key = c.con_slug_id AND s.sale_model = 'contacts'" +
                "AND  c.con_foreign_key = o.org_slug_id AND c.con_model = 'organisations'" +
                "AND q.qu_status <> 2 "+
                "AND q.qu_name LIKE ?"
                ,new String[]{"%" +name + "%"});
    }

    public Cursor getSalesWonLike(String name){
        return mDbController.getReadableDatabase().rawQuery("SELECT s._id,s.sale_slug_id,s.sale_name,s.sale_cost,s.sale_status " +
                "FROM sales AS s " +
                "WHERE s.sale_status = 1 " +
                "AND s.user_id = '"+getUsername.getUserName()+"' " +
                "AND s.sale_name LIKE ?"
                ,new String[]{"%" +name + "%"});
    }

    public Cursor getSalesCount(int status){
        return mDbController.getReadableDatabase().rawQuery("SELECT COUNT(sale_status) as sale_status FROM sales WHERE sale_status = '"+status+"' " +
                "AND user_id = '"+getUsername.getUserName()+"' " +
                "AND _id <> 0 AND sale_status <> 2 ",null);
    }

    public Cursor getStagesCount(String stage,String stage2){
        return mDbController.getReadableDatabase().rawQuery("SELECT COUNT(sale_stage) as sale_stage FROM sales WHERE  user_id = '"+getUsername.getUserName()+"'"+
                "AND  _id <> 0 AND sale_status <> 2 " +
                "AND (sale_stage = '"+stage+"' OR sale_stage = '"+stage2+"')",null);


    }

    public Cursor getOrganisationCount(){
        return mDbController.getReadableDatabase().rawQuery("SELECT COUNT(_id) AS _id FROM organisations WHERE _id <> 0 AND org_status = 1 " +
                "AND user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getOrganisationName(String orgSlugID){
        return mDbController.getReadableDatabase().rawQuery("SELECT o._id, o.org_name, o.org_slug_id FROM organisations as o, activities as a WHERE o.org_slug_id = a.act_foreign_key AND a.act_foreign_key = '"+orgSlugID+"' " +
                "AND o.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getContactName(String conSlugID){
        return mDbController.getReadableDatabase().rawQuery("SELECT c._id, c.con_name, c.con_slug_id, c.con_foreign_key, o.org_name FROM contacts as c , organisations as o, activities as a WHERE c.con_foreign_key = o.org_slug_id AND " +
                "c.con_slug_id = a.act_foreign_key AND a.act_foreign_key = '"+conSlugID+"' " +
                "AND c.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getSaleName(String saleSlugID){
        return mDbController.getReadableDatabase().rawQuery("SELECT s._id, s.sale_name, s.sale_slug_id, s.sale_foreign_key, s.sale_model FROM sales as s, activities as a WHERE s.sale_slug_id = a.act_foreign_key AND a.act_foreign_key = '"+saleSlugID+"' " +
                "AND s.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getContactsCount(){
        return mDbController.getReadableDatabase().rawQuery("SELECT COUNT(_id) AS _id FROM contacts WHERE _id <> 0 AND con_status <> 2 " +
                "AND user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getLeadsCount(){
        return mDbController.getReadableDatabase().rawQuery("SELECT COUNT(_id) AS _id FROM contacts WHERE _id <> 0 AND con_status = 0 " +
                "AND user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getActivitiesCount(int activityDone){
        return mDbController.getReadableDatabase().rawQuery("SELECT COUNT(act_status) as act_status FROM activities WHERE act_status = '"+activityDone+"' " +
                "AND user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getQuotationCount(int quotationDone){
        return mDbController.getReadableDatabase().rawQuery("SELECT COUNT(qu_status) as qu_status FROM  quotations AS q WHERE q.qu_status = '"+quotationDone+"' " +
                "AND user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getOrganisations(){
        return mDbController.getReadableDatabase().rawQuery("SELECT * FROM organisations AS o " +
                "WHERE o._id <> 0 AND o.org_status = 1 " +
                "AND o.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getOrganisation(String slugID){
        return mDbController.getReadableDatabase().rawQuery("SELECT * FROM organisations AS o " +
                "WHERE o._id <> 0 AND o.org_slug_id = '"+slugID+"'" +
                "AND o.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getOrganisationLike(String name){
        return mDbController.getReadableDatabase().rawQuery("SELECT o._id,o.org_slug_id,o.org_name " +
                "FROM organisations AS o WHERE o._id <> 0 AND o.org_status = 1 " +
                "AND o.user_id = '"+getUsername.getUserName()+"' "+
                "AND o.org_name LIKE ?",new String[]{name + "%"});
    }

    /*public Cursor getContactsLike(String name){
        return mDbController.getReadableDatabase().rawQuery("SELECT c._id, c.slug_id,c.contact_name,o.org_name,c.org_id " +
                "FROM contacts AS c, organisations " +
                "AS o WHERE c.org_id = o.slug_id AND c._id <> 0 AND c.contacts_done = 0 AND c.contact_name LIKE ?",new String[]{name + "%"});
    }*/

    public Cursor getContactsLike(String name){
        return mDbController.getReadableDatabase().rawQuery("SELECT c._id,c.con_slug_id,c.con_name," +
                "o.org_name,o.org_slug_id,c.con_foreign_key,c.con_model FROM contacts AS c, organisations AS o WHERE c.con_foreign_key = o.org_slug_id AND " +
                "c.con_model = 'organisations' AND c._id <> 0 " +
                "AND c.user_id = '"+getUsername.getUserName()+"' "+
                "AND c.con_status <> 2 AND c.con_name LIKE ?",new String[]{name + "%"});
    }

    public Cursor getContacts(){
        return mDbController.getReadableDatabase().rawQuery("SELECT c._id,c.con_slug_id,c.con_name,c.con_phone,c.con_email,c.con_website,c.con_source,c.con_address," +
                "o.org_name,o.org_slug_id,c.con_foreign_key,c.con_model,c.con_status,c.sync_done FROM contacts AS c, organisations AS o WHERE c.con_foreign_key = o.org_slug_id " +
                "AND c.user_id = '"+getUsername.getUserName()+"' AND "+
                "c.con_model = 'organisations' AND c._id <> 0 AND c.con_status <> 2",null);
    }

    public Cursor getPendingLeads(){
        return mDbController.getReadableDatabase().rawQuery("SELECT c._id,c.con_slug_id,c.con_name,c.con_phone,c.con_email,c.con_website,c.con_source,c.con_address," +
                "o.org_name,o.org_slug_id,c.con_foreign_key,c.con_model,c.con_status,c.sync_done FROM contacts AS c, organisations AS o WHERE c.con_foreign_key = o.org_slug_id " +
                "AND c.user_id = '"+getUsername.getUserName()+"' AND "+
                "c.con_model = 'organisations' AND c._id <> 0 AND c.con_status = 0",null);
    }

    public Cursor getContact(String slugID){
        return mDbController.getReadableDatabase().rawQuery("SELECT c._id,c.con_slug_id,c.con_name,c.con_phone,c.con_email,c.con_website,c.con_source,c.con_address," +
                "o.org_name,o.org_slug_id,c.con_foreign_key,c.con_model,c.sync_done FROM contacts AS c, organisations AS o WHERE c.con_foreign_key = o.org_slug_id " +
                "AND c.user_id = '"+getUsername.getUserName()+"' AND "+
                "c.con_model = 'organisations' AND c.con_slug_id = '"+slugID+"' AND c._id <> 0",null);
    }

   /* public Cursor getActivities(){
        return mDbController.getReadableDatabase().rawQuery("SELECT sa._id,a.activities,sa.activities_id,sa.time,sa.description,sa.feedback,sa.contact_id,sa.sale_id,sa.org_id,o.org_name,c.contact_name,s.sale_name " +
                        "FROM activities AS a,sale_activities AS sa,organisations AS o,contacts AS c, sales AS s " +
                        "WHERE sa.activities_id = a._id " +
                        "AND sa.sale_id = s._id " +
                        "AND sa.contact_id = c._id " +
                        "AND sa.org_id = o._id " +
                        "AND activity_done = 0",null);
    }*/

    public Cursor getActivities(){
        return mDbController.getReadableDatabase().rawQuery("SELECT a._id,a.act_slug_id,a.act_type,a.act_schedule,a.act_foreign_key,a.act_feedback,a.act_description," +
                "act_status,act_model,a.sync_done FROM activities AS a WHERE a.act_status <> 2 " +
                "AND a.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getAllSalesActivities(){
        return mDbController.getReadableDatabase().rawQuery("SELECT a._id,a.act_type,a.act_slug_id,a.act_description,a.act_schedule,a.act_feedback,a.act_status,a.act_model,a.act_foreign_key,a.sync_done "+
                "FROM activities AS a WHERE a.user_id = '"+getUsername.getUserName()+"'" ,null);
    }
    //,o.org_name,c.contact_name,s.sale_name
   // " AND c._id = sa.contact_id" +
     //       " AND o._id = sa.org_id" +

    public Cursor getAllActivities(){
        return mDbController.getReadableDatabase().rawQuery("SELECT a._id,a.activities FROM activities AS a WHERE a.user_id = '"+getUsername.getUserName()+"'",null);

    }

    /*public Cursor getQuotations(){
        return mDbController.getReadableDatabase().rawQuery("SELECT q._id, q.qu_name, q.qu_sale_id, q.qu_created, q.qu_status, q.qu_slug_id,s.sale_name FROM quotations AS q, sales AS s " +
                "WHERE q.qu_sale_id = s.sale_slug_id AND q.qu_status = 1 " +
                "AND q.user_id = '"+getUsername.getUserName()+"'",null);
                SUM(i.item_quantity * i.item_unit_price) AS cost,
    }*/

    public Cursor getQuotations(){
        return mDbController.getReadableDatabase().rawQuery("SELECT q._id,q.qu_name,q.qu_slug_id,q.qu_created,q.qu_sale_id,q.qu_status,q.qu_stage,q.qu_delivered,q.qu_lpo,q.qu_amount_collected,q.qu_payment_type,q.sync_done,c.con_name,c.con_slug_id,c.con_email,o.org_name,o.org_slug_id,s._id AS sales_id,s.sale_name,s.sale_slug_id " +
                        "FROM quotations AS q, sales AS s, contacts AS c, organisations AS o " +
                        "WHERE q.qu_sale_id = s.sale_slug_id " +
                        "AND s.sale_foreign_key = c.con_slug_id AND s.sale_model = 'contacts'" +
                        "AND  c.con_foreign_key = o.org_slug_id AND c.con_model = 'organisations'" +
                        "AND q.qu_status <> 2 "+
                "AND q.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getQuotationsStage(String stage){
        return mDbController.getReadableDatabase().rawQuery("SELECT q._id,q.qu_name,q.qu_slug_id,q.qu_created,q.qu_sale_id,q.qu_status,q.qu_stage,q.qu_delivered,q.qu_lpo,q.qu_amount_collected,q.qu_payment_type,q.sync_done,c.con_name,c.con_slug_id,c.con_email,o.org_name,o.org_slug_id,s._id AS sales_id,s.sale_name,s.sale_slug_id " +
                "FROM quotations AS q, sales AS s, contacts AS c, organisations AS o " +
                "WHERE q.qu_sale_id = s.sale_slug_id " +
                "AND s.sale_foreign_key = c.con_slug_id AND s.sale_model = 'contacts'" +
                "AND  c.con_foreign_key = o.org_slug_id AND c.con_model = 'organisations'" +
                "AND q.qu_status <> 2 "+
                "AND q.qu_stage = '"+stage+"' "+
                "AND q.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getQuotationsActivities(String slugID){
        return mDbController.getReadableDatabase().rawQuery("SELECT q._id,q.qu_name,q.qu_slug_id,q.qu_created,q.qu_sale_id,c.con_name,c.con_slug_id,o.org_name,o.org_slug_id,s.sale_name,s.sale_slug_id " +
                "FROM quotations AS q, sales AS s, contacts AS c, organisations AS o " +
                "WHERE q.qu_sale_id = s.sale_slug_id " +
                "AND s.sale_foreign_key = c.con_slug_id AND s.sale_model = 'contacts'" +
                "AND  c.con_foreign_key = o.org_slug_id AND c.con_model = 'organisations'" +
                "AND q.qu_slug_id = '"+slugID+"' "+
                "AND q.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getQuotation(String salesID){
        return mDbController.getReadableDatabase().rawQuery("SELECT q._id, q.qu_name, q.qu_sale_id FROM quotations AS q, sales AS s " +
                "WHERE q.qu_sale_id = s.sale_slug_id " +
                "AND s.sale_slug_id = '"+salesID+"' " +
                "AND q.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getQuotationsDetails(String quotationSlugID){
        return mDbController.getReadableDatabase().rawQuery("SELECT qd._id,(qd.item_quantity * qd.item_unit_price) AS total, qd.item_name, qd.item_quantity, qd.item_unit_price, qd.item_qu_slug_id, qd.item_status,qd.item_description,qd.item_slug_id,qd.sync_done " +
                "FROM items AS qd WHERE qd.item_qu_slug_id = '"+quotationSlugID+"' " +
                "AND qd.item_status = 1 " +
                "AND qd.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getQuotationDetailsSum(String quotationSlugID){
        return mDbController.getReadableDatabase().rawQuery("SELECT COUNT(qd.item_name) AS item_count, SUM(qd.item_unit_price * qd.item_quantity) AS price_sum " +
                "FROM items AS qd WHERE qd.item_qu_slug_id = '"+quotationSlugID+"' " +
                "AND qd.user_id = '"+getUsername.getUserName()+"' "+
                "AND qd.item_status <> 0",null);
    }

    public Cursor getQuotationDetailsTargets(){
        return mDbController.getReadableDatabase().rawQuery("SELECT COUNT(qd.item_name) AS item_count, SUM(qd.item_unit_price * qd.item_quantity) AS price_sum " +
                "FROM items AS qd, quotations AS q WHERE qd.item_qu_slug_id = q.qu_slug_id " +
                "AND qd.user_id = '"+getUsername.getUserName()+"' "+
                "AND q.qu_status = 1  ",null);
    }

    public Cursor getQuotationName(String quSlugID){
        return mDbController.getReadableDatabase().rawQuery("SELECT q._id, q.qu_name, q.qu_slug_id FROM quotations as q WHERE q.qu_slug_id = '"+quSlugID+"' " +
                "AND q._id <> 0 AND q.qu_status = 1 " +
                "AND s.user_id = '"+getUsername.getUserName()+"'",null);
    }

    public Cursor getStageID(String name){
        return mDbController.getReadableDatabase().rawQuery("SELECT _id FROM funnel_stages WHERE stage_name = ?",new String[]{name});
    }

    public Cursor getNotSyncData(String table){
        return mDbController.getReadableDatabase().rawQuery("SELECT * FROM " + table + " WHERE sync_done = 1 " +
                "AND user_id = '" + getUsername.getUserName() + "'", null);
    }

    public long insertContacts(ContentValues values){
        return mDbController.getWritableDatabase().insert("contacts", null, values);
    }

    public long insertOrganisation(ContentValues values){
        return mDbController.getWritableDatabase().insert("organisations", null, values);
    }

    public long insertSale(ContentValues values){
        return mDbController.getWritableDatabase().insert("sales", null, values);
    }

    public long insertSaleActivities(ContentValues values){
        return mDbController.getWritableDatabase().insert("activities", null, values);
    }

    public int updateSaleStage(ContentValues initialValues,int id){
        return mDbController.getWritableDatabase().update("sales", initialValues, "`_id` = " + id, null);
    }

    public long insertActivities(ContentValues initialValues){
        return mDbController.getWritableDatabase().insert("activities", null, initialValues);
    }

    public long insertQuotations(ContentValues initialValues){
        return mDbController.getWritableDatabase().insert("quotations", null, initialValues);
    }

    public long insertQuotationsDetails(ContentValues initialValues){
        return mDbController.getWritableDatabase().insert("items", null, initialValues);
    }

    public long insertComments(ContentValues initialValues){
        return mDbController.getWritableDatabase().insert("comments", null, initialValues);
    }

    public int updateSaleStatus(ContentValues initialValues,int id){
        return mDbController.getWritableDatabase().update("deals", initialValues, "`_id` = " + id, null);
    }

    public int updateContacts(ContentValues initialValues,int id){
        return mDbController.getWritableDatabase().update("contacts", initialValues, "`_id` = "+id, null);
    }


    public int updateOrganisations(ContentValues initialValues,int id){
        return mDbController.getWritableDatabase().update("organisations", initialValues, "`_id` = "+id, null);
    }

    public int updateActivities(ContentValues initialValues,int id){
        return mDbController.getWritableDatabase().update("activities", initialValues, "`_id` = "+id, null);
    }

    public int updateQuotations(ContentValues initialValues,int id){
        return mDbController.getWritableDatabase().update("quotations", initialValues, "`_id` = "+id, null);
    }

    public int updateQuotationsDetails(ContentValues initialValues,int id){
        return mDbController.getWritableDatabase().update("items", initialValues, "`_id` = "+id, null);
    }

    public int updateSlugID(ContentValues initialValues,int id,String tableName){
        return mDbController.getWritableDatabase().update(tableName, initialValues, "`_id` = "+id, null);
    }

    public int updateTables(ContentValues initialValues,String tableName){
        return mDbController.getWritableDatabase().update(tableName, initialValues,null, null);
    }

    public int updateComments(ContentValues initialValues,int id){
        return mDbController.getWritableDatabase().update("comments", initialValues, "`_id` = "+id, null);
    }

    public FunnelDB open() throws SQLException {
        mDbController = new DbController(mCtx);
        return this;
    }
}
