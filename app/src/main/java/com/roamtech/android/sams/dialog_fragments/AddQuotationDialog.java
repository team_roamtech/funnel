package com.roamtech.android.sams.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.roamtech.android.sams.R;

/**
 * Created by dennis on 6/4/15.
 */
public class AddQuotationDialog extends DialogFragment {
    AppCompatActivity mActivity;
    Button btnOk,btnCancel;
    EditText txtMessage;
    String message;

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_add_quotation);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.txt_add_quotation_title));
        dialog.setCanceledOnTouchOutside(false);
        txtMessage = (EditText) dialog.findViewById(R.id.txt_advisory);
        btnOk = (Button) dialog.findViewById(R.id.btn_ok);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent);
                intent.putExtra(mActivity.getResources().getString(R.string.bundle_sale_flag),1);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                dismiss();
            }
        });
        btnCancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent);
                intent.putExtra(mActivity.getResources().getString(R.string.bundle_sale_flag),2);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                dismiss();
            }
        });
        return dialog;
    }

}
