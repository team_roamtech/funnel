package com.roamtech.android.sams.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.ReportsAdapter;
import com.roamtech.android.sams.adapters.SpinnerReportsAdapter;
import com.roamtech.android.sams.dialog_fragments.AddDateDialogFragment;
import com.roamtech.android.sams.http.RestClient;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.models.ReportItems;
import com.roamtech.android.sams.parsers.ReportsParser;
import com.roamtech.android.sams.utils.AsyncLoader;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * Fragments that shows a report from the server to the Team Administrator the Performance of the team
 *
 * Created by Karuri on 2/23/2015.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class TeamStatisticsFragment extends Fragment implements View.OnClickListener,LoaderManager.LoaderCallbacks<ArrayList<ReportItems>>  {
    public static final String TAG = "TeamStatisticsFragment";
    Spinner spnPeople,spnReports;
    GridView grdReports;
    String report,reportName;
    LinearLayout lytChooseFromDate,lytChooseToDate,lytTitles;
    AppCompatActivity mActivity;
    SpinnerReportsAdapter spinnerReportsAdapter;
    String [] spinnerListArray;
    public static final int DIALOG_FRAGMENT_TO_DATE = 6;
    public static final int DIALOG_FRAGMENT_FROM_DATE = 7;
    String dateTo,dateFrom;
    TextView txtToTime,txtFromTime,txtTitleQuName,txtTitleConName,txtTitleItem;
    AddDateDialogFragment newFragment4;
    MainActivity mainActivity;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
    private SharedPreferences prefs;
    String username;
    ProgressBar mProgressBar;
    public static final int LOADER = 1003;
    ReportsAdapter reportsAdapter;
    private boolean isTablet = false;
    int dateMissing;

    public TeamStatisticsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        spinnerListArray = getResources().getStringArray(R.array.spn_team_reports);
        mainActivity = new MainActivity();
        newFragment4 = new AddDateDialogFragment();
        prefs = PreferenceManager
                .getDefaultSharedPreferences(mActivity);
        username = prefs.getString(mActivity.getResources().getString(R.string.bundle_username),null);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_team_statistics, container, false);
        spnPeople = (Spinner) rootView.findViewById(R.id.spn_people);
        spnPeople.setVisibility(View.GONE);
        spnReports = (Spinner) rootView.findViewById(R.id.spn_reports);
        lytChooseFromDate = (LinearLayout) rootView.findViewById(R.id.lyt_choose_from_date);
        lytChooseFromDate.setOnClickListener(this);
        lytChooseToDate = (LinearLayout) rootView.findViewById(R.id.lyt_choose_to_date);
        lytChooseToDate.setOnClickListener(this);
        lytTitles = (LinearLayout) rootView.findViewById(R.id.lyt_titles);
        txtToTime = (TextView) rootView.findViewById(R.id.txt_to_date);
        txtFromTime = (TextView) rootView.findViewById(R.id.txt_from_date);
        txtTitleQuName = (TextView) rootView.findViewById(R.id.txt_title_qu_name);
        txtTitleConName = (TextView) rootView.findViewById(R.id.txt_title_con_name);
        txtTitleItem = (TextView) rootView.findViewById(R.id.txt_title_item);
        grdReports = (GridView) rootView.findViewById(R.id.grd_reports);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        spinnerReportsAdapter = new SpinnerReportsAdapter(mActivity,0,spinnerListArray);
        spnReports.setAdapter(spinnerReportsAdapter);
        spnReports.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View view,
                                       int position, long arg3) {
                // TODO Auto-generated method stub
                report = spinnerListArray[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        Configuration config = getResources().getConfiguration();
        if (config.smallestScreenWidthDp >= 600 && getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            isTablet = true;
        }else{
            isTablet = false;
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_send).setVisible(true);
        //        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_send:
                mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == DIALOG_FRAGMENT_TO_DATE){
            if (resultCode == Activity.RESULT_OK){
                if(data.getExtras() != null) {
                    dateTo = data.getStringExtra(mActivity.getResources().getString(R.string.bundle_set_date));
                    txtToTime.setText(dateTo);
                }
            }
        }else if(requestCode == DIALOG_FRAGMENT_FROM_DATE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getExtras() != null) {
                    dateFrom = data.getStringExtra(mActivity.getResources().getString(R.string.bundle_set_date));
                    txtFromTime.setText(dateFrom);
                }
            }
        }

    }

    /**
     *
     * Creates a Json String which is sent to the server as an end point to retrieve a report
     *
     * @return Json String Created to be sent to the server as an end point to retrieve the report
     */
    public String reportsJsonData(){
        JSONArray jsonArray = new JSONArray();
        JSONObject reports = new JSONObject();
        Calendar c = Calendar.getInstance();
        try {
            reports.put(mActivity.getResources().getString(R.string.name_value_client_id), "true");
            reports.put(mActivity.getResources().getString(R.string.name_value_admin_id), "dennis_admin");
            reports.put(mActivity.getResources().getString(R.string.name_value_querykey), selectReport(report));
            reports.put(mActivity.getResources().getString(R.string.name_value_from_date), dateFrom);
            reports.put(mActivity.getResources().getString(R.string.name_value_to_date), dateTo);
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        jsonArray.put(reports);
        return jsonArray.toString();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.lyt_choose_from_date:
                newFragment4.setTargetFragment(TeamStatisticsFragment.this, DIALOG_FRAGMENT_FROM_DATE);
                newFragment4.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.lyt_choose_to_date:
                newFragment4.setTargetFragment(TeamStatisticsFragment.this, DIALOG_FRAGMENT_TO_DATE);
                newFragment4.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
        }
    }

    /**
     *
     * Class that runs in the Background handling requests to the server for the reports the reports are returned to the
     * Main thread as an ArrayList<ReportItems>
     *
     */
    public static class GetReports extends AsyncLoader<ArrayList<ReportItems>> {
        Context ctx;
        ArrayList<ReportItems> report = new ArrayList<ReportItems>();
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
        String jsonData;
        ReportsParser reportsParser;

        /**
         *
         * Constructor Initializes context
         *
         * @param context
         * @param jsonData Json data to be used as endpoint to make request to the server for the report
         */
        public GetReports(Context context,String jsonData){
            super(context);
            this.ctx = context;
            this.jsonData = jsonData;
            reportsParser = new ReportsParser(this.ctx);
        }

        @Override
        public ArrayList<ReportItems> loadInBackground() {
            if(jsonData != null) {
                nameValuePairs.add(new BasicNameValuePair(ctx.getResources().getString(R.string.name_value_data), jsonData));
                nameValuePairs.add(new BasicNameValuePair(ctx.getResources().getString(R.string.name_value_request), ctx.getResources().getString(R.string.name_value_report)));
                String json = "{}";
                try {
                    json = RestClient.makeRestRequest(RestClient.POST, MainModel.SYNC_URL, nameValuePairs);
                    Log.d(TAG, json);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                try {
                    report = reportsParser.parse(json);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return report;
            }else{
                return null;
            }
        }
    }


    @Override
    public Loader<ArrayList<ReportItems>> onCreateLoader(int id, Bundle args) {
        if(dateFrom == null) {
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.txt_advisory_dates_not_entered), Toast.LENGTH_LONG).show();
            dateMissing = 1;
            return new GetReports(mActivity, null);
        }else if(dateTo == null) {
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.txt_advisory_dates_not_entered),Toast.LENGTH_LONG).show();
            dateMissing = 1;
            return new GetReports(mActivity, null);
        }else{
            mProgressBar.setVisibility(View.VISIBLE);
            dateMissing = 0;
            return new GetReports(mActivity, reportsJsonData());
        }
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<ReportItems>> loader, ArrayList<ReportItems> data) {
        if (data != null) {
            grdReports.setVisibility(View.VISIBLE);
                lytTitles.setVisibility(View.VISIBLE);
                if(reportName.equals(mActivity.getResources().getString(R.string.name_value_salesPerGroup))) {
                    txtTitleQuName.setText(mActivity.getResources().getString(R.string.txt_title_wonlost_sales));
                    txtTitleConName.setText(mActivity.getResources().getString(R.string.txt_title_all_sales));
                    txtTitleItem.setText(mActivity.getResources().getString(R.string.txt_title_all_users));
                }else if(reportName.equals(mActivity.getResources().getString(R.string.name_value_customercontacts))){
                    txtTitleQuName.setText( mActivity.getResources().getStringArray(R.array.spn_report_items)[1]);
                    txtTitleConName.setText(mActivity.getResources().getString(R.string.txt_title_contact));
                    txtTitleItem.setText(mActivity.getResources().getString(R.string.db_table_telephone));
                }else if(reportName.equals(mActivity.getResources().getString(R.string.name_value_organizationcontacts))){
                    txtTitleQuName.setText(mActivity.getResources().getStringArray(R.array.spn_report_items)[1]);
                    txtTitleConName.setText("");
                    txtTitleItem.setText(mActivity.getResources().getString(R.string.txt_address));
            }
            reportsAdapter = new ReportsAdapter(mActivity, 0, null, data);
            grdReports.setAdapter(reportsAdapter);
        }else{
            if(dateMissing == 0){
                Toast.makeText(mActivity,"Data not Found",Toast.LENGTH_LONG).show();
            }
            grdReports.setVisibility(View.GONE);
            lytTitles.setVisibility(View.GONE);
        }
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<ReportItems>> loader) {
        grdReports.setAdapter(null);
    }

    /**
     *
     * Selects which report to get from the server
     *
     * @param report String of the report you wish to get about your team
     * @return String of the selected report
     */
    public String selectReport(String report){
        if(report.equals(mActivity.getResources().getStringArray(R.array.spn_team_reports)[0])){
            reportName = mActivity.getResources().getString(R.string.name_value_salesPerGroup);
        }else if(report.equals(mActivity.getResources().getStringArray(R.array.spn_team_reports)[1])){
            reportName = mActivity.getResources().getString(R.string.name_value_customercontacts);
        }else if(report.equals(mActivity.getResources().getStringArray(R.array.spn_team_reports)[2])){
            reportName = mActivity.getResources().getString(R.string.name_value_organizationcontacts);
        }
        return reportName;
    }
}
