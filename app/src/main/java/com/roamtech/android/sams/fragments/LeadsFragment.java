package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import  android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.NewContactsAdapter;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;

/**
 *
 *  Fragment that lists all Contacts in the Leads Stage
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class LeadsFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>,LaunchFragment {
    public static final String TAG = "LeadsFragment";
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    StagesStateFragment stagesStateFragment;
    int position2,fragmentPosition;
    String title2;
    AddContactsFragment addContactsFragment;
    ListView lstContacts;
    Cursor c;
    View infoView;
    ProgressBar mProgresBar;
    TextView txtNoContacts,txtAddContacts,txtTitle;
    NewContactsAdapter mAdapter;
    public static final int LOADER = 1451;
    String [] pagerItemsArray;
    boolean isVisible = false;


    /**
     * Constructor
     */
    public LeadsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        pagerItemsArray = mActivity.getResources().getStringArray(R.array.pager_title_items);
        mainActivity = new MainActivity();
        stagesStateFragment = new StagesStateFragment();

        Bundle args = getArguments();
        if(args != null){
            position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position),-1);
            title2 = args.getString(mActivity.getResources().getString(R.string.bundle_title));
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_contacts, container, false);
        lstContacts = (ListView) rootView.findViewById(R.id.lst_contacts);
        infoView = (View) rootView.findViewById(R.id.error_container);
        mProgresBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        txtNoContacts = (TextView) rootView.findViewById(R.id.txt_no_deals);
        txtAddContacts = (TextView) rootView.findViewById(R.id.txt_adddeals);

        mAdapter = new NewContactsAdapter(mActivity, null, 0,this);
        lstContacts.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(c.moveToPosition(position)) {
                    Bundle args = new Bundle();
                }
            }
        });

        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        Log.d(TAG, TAG + " " + "setUserVisibleHint");
        if (isVisibleToUser) {
            isVisible = isVisibleToUser;
            Bundle args = getArguments();
            if(args != null) {
                fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
                title2 = args.getString(mActivity.getResources().getString(R.string.bundle_title));
                position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
                Log.d(TAG," position setUserVisibleHint " + position2);

                //  mActivity.getSupportActionBar().setTitle(title2);
            }
            mActivity.getSupportActionBar().setTitle(pagerItemsArray[0]);
            mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }
    }


    @Override
    public void onResume(){
        super.onResume();


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.PAGER_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    int fragmentPos = data.getIntExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), 0);
                        Log.d(TAG, "ContactsFragment 1");
                        Intent intent = new Intent();
                        intent.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPos);
                        mActivity.setResult(Activity.RESULT_OK, intent);
                        getParentFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                }
            }
        }
    }


    @Override
    public void launchFragment(int position, Bundle bundle, int view) {
        bundle.putInt(mActivity.getResources().getString(R.string.bundle_position),position2);
        addContactsFragment = new AddContactsFragment();
        addContactsFragment.setArguments(bundle);
        addContactsFragment.setTargetFragment(LeadsFragment.this, MainModel.PAGER_FRAGMENT);
        mainActivity.addFragment(addContactsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addContactsFragment).getClass().getName());

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "-------- on create loader -----------------");
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"50");
        return new CursorLoader(getActivity(),data, null, null,
                new String[] {}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mProgresBar.setVisibility(View.GONE);
        c = data;
        if(c == null || c.getCount() == 0 ){
            Log.d(TAG, "-------- contacts count is " + String.valueOf(c.getCount()));
            infoView.setVisibility(View.VISIBLE);
            txtNoContacts.setText(mActivity.getResources().getString(R.string.txt_nocontacts));
            txtAddContacts.setText(mActivity.getResources().getString(R.string.txt_addcontacts));
            lstContacts.setVisibility(View.GONE);
        }else{
            mAdapter.swapCursor(c);
            Log.d(TAG, "-------- contacts count is greater than 0 is  " + String.valueOf(c.getCount()));
            infoView.setVisibility(View.GONE);
            lstContacts.setVisibility(View.VISIBLE);
            lstContacts.setAdapter(mAdapter);
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}
