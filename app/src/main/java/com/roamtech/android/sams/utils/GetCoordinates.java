package com.roamtech.android.sams.utils;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 *
 * Gets location coordinates of where the device is.
 * Created by dennis on 5/7/15.
 *  @author Dennis Mwangi Karuri
 */
public class GetCoordinates implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener {
    public static final String TAG = "GetCoordinates";
    public GoogleApiClient mGoogleApiClient;
    Double dLatitude,dLongitude;
    public Location mLastLocation;
    Context ctx;
    Boolean mRequestingLocationUpdates = true;

    /**
     *
     * @param ctx The active context that requires the coordinates
     */
    public GetCoordinates(Context ctx){
        this.ctx = ctx;
    }

    /**
     * Initializes and adds connection listeners to GoogleApiClient
     * @return initialized GoogleApiClien
     */
    public synchronized GoogleApiClient  buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(ctx)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        return mGoogleApiClient;
    }

    /**
     * Gets the coordinates latitude and longitude
     * @return array of double figures which are the latitude and longitude
     */
    public Double[] getCoordinates(){
        Double [] coordinates = {dLatitude,dLongitude};
        return coordinates;
    }



    @Override
    public void onConnected(Bundle bundle) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            dLatitude = mLastLocation.getLatitude();
            dLongitude = mLastLocation.getLongitude();
        } else {

        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
