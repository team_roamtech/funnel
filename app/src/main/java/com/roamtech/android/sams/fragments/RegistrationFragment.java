package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.async_tasks.SendData;
import com.roamtech.android.sams.dialog_fragments.AdvisoryDialogFragment;
import com.roamtech.android.sams.dialog_fragments.ProgressDialogFragment;
import com.roamtech.android.sams.interfaces.GoToPager;
import com.roamtech.android.sams.interfaces.ReturnAuthenticationResult;
import com.roamtech.android.sams.interfaces.ReturnRegistrationResult;
import com.roamtech.android.sams.interfaces.SelectionListener;
import com.roamtech.android.sams.views.ClearableAutoTextView;
import com.unboundid.ldap.sdk.LDAPException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * Registers Users to the application
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment implements SelectionListener,View.OnClickListener,ReturnRegistrationResult {
    public static String TAG = "RegistrationFragment";
    ClearableAutoTextView editOrgName;
    String searchItem;
    EditText editEmail,editUsername,editPassword,editCompany;
    Spinner spnTeamsize,spnCompanyIndustry;
    Button btnRegister;
    AdvisoryDialogFragment dialogAdvisory = new AdvisoryDialogFragment();
    MainActivity mainActivity;
    ProgressDialogFragment progressDialogFragment;
    AppCompatActivity mActivity;
    int position2;
    String companyIndustry,teamSize,date;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
    GoToPager mCallBack;
    String client_id;

    /**
     * Constructor
     */
    public RegistrationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack = (GoToPager) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            Bundle args = getArguments();
            if(args != null) {
                position2 = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        progressDialogFragment = new ProgressDialogFragment();
        mainActivity = new MainActivity();
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_registration2, container, false);
        editOrgName = (ClearableAutoTextView)rootView.findViewById(R.id.edit_organisation);
        editOrgName.setSelectionListener(this);
        editEmail = (EditText)rootView.findViewById(R.id.edit_email);
        editUsername = (EditText)rootView.findViewById(R.id.edit_username);
        editPassword = (EditText)rootView.findViewById(R.id.edit_password);
        spnCompanyIndustry = (Spinner)rootView.findViewById(R.id.spn_industry);
        spnCompanyIndustry.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //Get the selected value
                companyIndustry = spnCompanyIndustry.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });
        spnTeamsize = (Spinner)rootView.findViewById(R.id.spn_team_size);
        spnTeamsize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener(){

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                //Get the selected value
                teamSize = spnTeamsize.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }

        });
        btnRegister = (Button)rootView.findViewById(R.id.btn_register);
        btnRegister.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onItemSelection(ClearableAutoTextView.DisplayStringInterface selectedItem) {
        if(selectedItem != null) {
          //  Log.d(TAG,selectedItem.getDisplayString());
            client_id =  selectedItem.getDisplayString()[1];
            searchItem = selectedItem.getDisplayString()[0];
        }
        //editSearch.setText("");
    }

    @Override
    public void onReceiveLocationInformation(double lat, double lng) {

    }

    /**
     * Validates user details entered
     */
    public void fillData(){
        Bundle args = new Bundle();
        if(editEmail.getText().length() == 0) {
            args.putString(this.getResources().getString(R.string.txt_advisory), this.getResources().getString(R.string.txt_advisory_enter_email));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }else if(isValidEmail(editEmail.getText()) == false){
            args.putString(this.getResources().getString(R.string.txt_advisory), this.getResources().getString(R.string.txt_advisory_enter_valid_email));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }else if(editUsername.getText().length() == 0){
            args.putString(this.getResources().getString(R.string.txt_advisory), this.getResources().getString(R.string.txt_advisory_enter_username));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }else if(editPassword.getText().length() == 0){
            args.putString(this.getResources().getString(R.string.txt_advisory), this.getResources().getString(R.string.txt_advisory_enter_password));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }else if(editOrgName.getText().length() == 0){
            args.putString(this.getResources().getString(R.string.txt_advisory), this.getResources().getString(R.string.txt_advisory_enter_password));
            dialogAdvisory.setArguments(args);
            dialogAdvisory.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), getString(R.string.dialog_tag));
        }else{
            new SendData(mActivity,this).execute(regUserJsonData(),mActivity.getResources().getString(R.string.name_value_userMgt),regJsonData(),mActivity.getResources().getString(R.string.name_value_cReg));
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_register:
                fillData();
                break;
        }
    }

    /**
     *
     * Creates JSON data with company details entered
     *
     * @return String of JSON data created from company details
     */
    public String regJsonData(){
        JSONArray jsonArray = new JSONArray();
        JSONObject reg = new JSONObject();
        Calendar c = Calendar.getInstance();
            try {
                reg.put(mActivity.getResources().getString(R.string.name_value_email), editEmail.getText().toString());
                reg.put(mActivity.getResources().getString(R.string.name_value_admin), editUsername.getText().toString());
                reg.put(mActivity.getResources().getString(R.string.name_value_password), editPassword.getText().toString());
                reg.put(mActivity.getResources().getString(R.string.name_value_clt_company), editOrgName.getText().toString());
                reg.put(mActivity.getResources().getString(R.string.name_value_clt_industry),companyIndustry);
                if(client_id != null){
                    reg.put(mActivity.getResources().getString(R.string.name_value_client_id),client_id);
                }
                reg.put(mActivity.getResources().getString(R.string.name_value_team_size), teamSize);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            jsonArray.put(reg);
        return jsonArray.toString();
    }

    /**
     *
     * Creates JSON data with user details entered
     *
     * @return String of JSON data created from user details
     */
    public String regUserJsonData(){
        JSONArray jsonArray = new JSONArray();
        JSONObject reg = new JSONObject();
        Calendar c = Calendar.getInstance();
        date = dateFormat.format(c.getTime());
        try {
            reg.put(mActivity.getResources().getString(R.string.name_value_email), editEmail.getText().toString());
            reg.put(mActivity.getResources().getString(R.string.name_value_username), editUsername.getText().toString());
            reg.put(mActivity.getResources().getString(R.string.name_value_password), editPassword.getText().toString());
            reg.put(mActivity.getResources().getString(R.string.name_value_usseroles),"SUPERADMIN");
            reg.put(mActivity.getResources().getString(R.string.name_value_firstname),editUsername.getText().toString());
            reg.put(mActivity.getResources().getString(R.string.name_value_lastname), editUsername.getText().toString());
            reg.put(mActivity.getResources().getString(R.string.name_value_ou), mActivity.getResources().getString(R.string.name_value_roamtech));
            reg.put(mActivity.getResources().getString(R.string.name_value_action), "add");
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        jsonArray.put(reg);
        return jsonArray.toString();
    }

    @Override
    public void onStartTask() {
        progressDialogFragment = new ProgressDialogFragment.Builder()
                .setMessage(getString(R.string.loading)).setCancelableOnTouchOutside(false)
                .build();
        progressDialogFragment.show(mActivity.getSupportFragmentManager(),getString(R.string.dialog_tag));
    }

    @Override
    public void onReturnResult(Boolean authenticate) throws LDAPException {
        if(progressDialogFragment != null) {
            progressDialogFragment.dismiss(mActivity.getSupportFragmentManager());
        }
        if(authenticate == true){

           /* Intent i = new Intent(mActivity, AdminActivity.class);
            startActivity(i);
            mActivity.finish();*/
            mCallBack.moveToPager(0,1);
            clearFields();
        }else{
            //Toast.mak
        }
    }

    /**
     * Clears all user detail fields
     */
    public void clearFields(){
        editUsername.setText("");
        editEmail.setText("");
        editOrgName.setText("");
        editPassword.setText("");
    }

    /**
     *
     * Checks if the email is valid
     *
     * @param target String of email entered
     * @return boolean if email is valid
     */
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
