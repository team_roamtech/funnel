package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.interfaces.ReturnCursor;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * Fragment that Displays Calendar
 *
 * Created by dennis on 4/2/15.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 * {@see ReturnCursor } interface.
 */
public class MyCalendarFragment extends Fragment implements ReturnCursor{
    public static final String TAG = "MyCalendarFragment";
    private CaldroidFragment caldroidFragment;
    MainActivity mainActivity;
    AppCompatActivity mActivity;
    int activityDone;
    String activityDate;
    ProgressBar progressBar;
    Cursor cursor;
    private static View rootView;
    OnAddSelectedListener mCallBack2;
    ArrayList<Date> arrayListDates = new ArrayList<Date>();
    ArrayList<Integer> arrayListInts = new ArrayList<Integer>();
    Date actDate;

    /**
     * Constructor
     */
    public MyCalendarFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mainActivity = new MainActivity();
        if(savedInstanceState != null) {
            Log.d(TAG, "onCreate frantic");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG,"on create view happens");

        final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        if (rootView != null) {
            ViewGroup parent = (ViewGroup) rootView.getParent();
            if (parent != null)
                parent.removeView(rootView);
        }
        try {
            rootView = inflater.inflate(R.layout.fragment_my_calendar, container, false);
        } catch (InflateException e) {

        }
        Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"33");
        cursor = mActivity.getContentResolver().query(data_id, null, null,new String[] {}, null);
        if(cursor != null || cursor.getCount() != 0 ) {
            setCustomResourceForDates(cursor);
        }

       return rootView;
    }

    /**
     *
     * Sets the dates on the Calendar
     *
     * @param cursor Cursor that contains dates to be set on the calendar
     */
    private void setCustomResourceForDates(Cursor cursor) {
        // caldroidFragment.setCaldroidListener(listener);
        caldroidFragment = new CaldroidFragment();
        caldroidFragment.setCaldroidListener(listener);
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CaldroidFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CaldroidFragment.YEAR, cal.get(Calendar.YEAR));
        args.putBoolean(CaldroidFragment.ENABLE_SWIPE, true);
        args.putBoolean(CaldroidFragment.SIX_WEEKS_IN_CALENDAR, true);
        args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, true);

        // Uncomment this to customize startDayOfWeek
        args.putInt(CaldroidFragment.START_DAY_OF_WEEK,
                CaldroidFragment.TUESDAY); // Tuesday

        // Uncomment this line to use Caldroid in compact mode
        args.putBoolean(CaldroidFragment.SQUARE_TEXT_VIEW_CELL, false);

        caldroidFragment.setArguments(args);
        mainActivity.addFragment(caldroidFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.my_calendar, getChildFragmentManager(), ((Object) caldroidFragment).getClass().getName());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        if(cursor != null || cursor.getCount() != 0 ) {
            cursor.moveToFirst();
            while (cursor.isAfterLast() == false) {
                activityDone = cursor.getInt(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_status)));
                activityDate = cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_schedule)));
                Date actDate = new Date();
                try {
                    actDate = dateFormat.parse(activityDate);
                    Log.d(TAG, String.valueOf(activityDate));
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                if (caldroidFragment != null) {
                    if (activityDone == 1) {
                        caldroidFragment.setBackgroundResourceForDate(R.color.purple_theme,
                                actDate);
                        caldroidFragment.setTextColorForDate(R.color.white_text, actDate);

                    } else if (activityDone == 0) {
                        caldroidFragment.setBackgroundResourceForDate(R.color.gray,
                                actDate);
                        caldroidFragment.setTextColorForDate(R.color.white_text, actDate);
                    } else {
                        //  caldroidFragment.setBackgroundResourceForDate(R.color.lost_red,
                        //        activityDate);
                        //caldroidFragment.setTextColorForDate(R.color.white_text, activityDate);
                    }
                    cursor.moveToNext();
                }
            }
        }
    }


    final CaldroidListener listener = new CaldroidListener() {

        @Override
        public void onSelectDate(Date date, View view) {
                if(arrayListDates.contains(date)) {
                    int index = arrayListDates.indexOf(date);
                    if (cursor.moveToPosition(arrayListInts.get(index))) {
                        Bundle args = new Bundle();
                        args.putInt(mActivity.getResources().getString(R.string.bundle_position),4);
                        args.putInt(mActivity.getResources().getString(R.string.db_table_id), cursor.getInt(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_id))));
//                    args.putInt(mActivity.getResources().getString(R.string.db_table_act_links), cursor.getInt(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_links))));
                        args.putString(mActivity.getResources().getString(R.string.db_table_act_schedule), cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_schedule))));
                        args.putString(mActivity.getResources().getString(R.string.db_table_act_description), cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_description))));
                        args.putString(mActivity.getResources().getString(R.string.db_table_act_model), cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_model))));
                        args.putString(mActivity.getResources().getString(R.string.db_table_act_foreign_key), cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_foreign_key))));
                        args.putInt(mActivity.getResources().getString(R.string.db_table_act_status), cursor.getInt(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_status))));
                        args.putInt(mActivity.getResources().getString(R.string.db_table_sync_done), cursor.getInt(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_sync_done))));

                        args.putString(mActivity.getResources().getString(R.string.db_table_act_feedback), cursor.getString(cursor.getColumnIndex(mActivity.getResources().getString(R.string.db_table_act_feedback))));
                        mCallBack2.addSelectedItem(args, 4, 0);
                    }
                }
        }

        @Override
        public void onChangeMonth(int month, int year) {


        }

        @Override
        public void onLongClickDate(Date date, View view) {

        }

        @Override
        public void onCaldroidViewCreated() {
            if (caldroidFragment.getLeftArrowButton() != null) {

            }
        }

    };


    @Override
    public void onResume(){
        super.onResume();
        Log.d(TAG,TAG);
        Log.d(TAG, "saved instance state null");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        // TODO Auto-generated method stub
        super.onSaveInstanceState(outState);

        if (caldroidFragment != null) {
            Log.d(TAG,"CALDROID SAVED STATE");
            caldroidFragment.saveStatesToKey(outState, "CALDROID_SAVED_STATE");
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == 2) {
                Log.d(TAG,"handler set tied");
               // setCustomResourceForDates(cursor);
               // processJson(result2);
                //mProgresBar.setVisibility(View.GONE);
            }
        }
    };

    @Override
    public void onStartTaskCursor() {
        Log.d(TAG,"Progress Dialog Fragment");

    }

    @Override
    public void onReturnCursor(Cursor cursor) {
      /* if(cursor != null || cursor.getCount() != 0) {
            this.cursor=cursor;
            handler.sendEmptyMessage(2);
        }*/
        setCustomResourceForDates(cursor);
        progressBar.setVisibility(View.GONE);
    }
}



