package com.roamtech.android.sams.parsers;

import android.content.Context;
import android.util.Log;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.models.ReportItems;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 *
 * Parses Json data gotten from the server and Adds the reports to a ListView to be displayed
 *
 * Created by dennis on 5/14/15.
 * @author Dennis Mwangi Karuri
 */
public class ReportsParser {
    public static final String TAG = "ReportsParser";
    Context ctx;
    ArrayList<ReportItems> report = new ArrayList<ReportItems>();
    String quName,conName,items;
    ReportItems reportItems;

    /**
     *
     * Constructor Initializes the Context
     *
     * @param ctx
     */
    public ReportsParser(Context ctx){
        this.ctx = ctx;
    }

    /**
     *
     *  Parses and gets the Report items from the Json data sent from the server and adds them to an arrayList of Report Items
     *  to be displayed
     *
     * @param jsonString
     * @return Arraylist with the Report Items to be added to a ListView
     * @throws Exception
     */
    public ArrayList<ReportItems> parse(String jsonString) throws Exception {
        Log.d(TAG,jsonString);
        JSONObject jObject = null;
        JSONArray jArray = null;
        try {
            if (jsonString != null) {
                jObject = new JSONObject(jsonString);
                if(jObject.has(ctx.getResources().getString(R.string.json_rows)) && !jObject.isNull(ctx.getResources().getString(R.string.json_rows))) {
                    jArray = jObject.getJSONArray(ctx.getResources().getString(R.string.json_rows));
                    for(int i = 0; i < jArray.length(); i++){
                        if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.db_table_qu_name)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.db_table_qu_name))) {
                            quName = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.db_table_qu_name));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.json_distinct_sales)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.json_distinct_sales))){
                            quName = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.json_distinct_sales));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.db_table_organisation_name)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.db_table_organisation_name))){
                            quName = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.db_table_organisation_name));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.name_value_wonlostSales)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.name_value_wonlostSales))){
                            quName = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.name_value_wonlostSales));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.json_admin_users_id)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.json_admin_users_id))){
                            quName = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.json_admin_users_id));
                        }
                        if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.db_table_con_name)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.db_table_con_name))) {
                            conName = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.db_table_con_name));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.json_contacts)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.json_contacts))){
                            conName = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.json_contacts));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.name_value_allSales)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.name_value_allSales))){
                            conName = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.name_value_allSales));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.name_value_admin_id)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.name_value_admin_id))){
                            conName = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.name_value_admin_id));
                        }
                        if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.json_items)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.json_items))) {
                            items = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.json_items));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.json_organisations)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.json_organisations))){
                            items = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.json_organisations));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.db_table_con_phone)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.db_table_con_phone))){
                            items = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.db_table_con_phone));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.db_table_org_address)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.db_table_org_address))) {
                            items = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.db_table_org_address));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.name_value_allUsers)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.name_value_allUsers))) {
                            items = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.name_value_allUsers));
                        }else if(jArray.getJSONObject(i).has(ctx.getResources().getString(R.string.name_value_username)) && !jArray.getJSONObject(i).isNull(ctx.getResources().getString(R.string.name_value_username))) {
                            items = jArray.getJSONObject(i).getString(ctx.getResources().getString(R.string.name_value_username));
                        }
                        report.add(setModel(quName,conName,items));
                    }
                }else{
                    report = null;
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return report;
    }

    /**
     *
     * Creates a Report object by adding all the parameters to a Report item
     *
     * @param quName Quotation name a part of the report
     * @param conName Contact name a part of the report
     * @param items Items in the quotation part of the report
     * @return an Object of the Reportitems with all items of the report
     */
    public ReportItems setModel(String quName,String conName,String items){
        reportItems = new ReportItems();
        Log.d(TAG,quName+" "+conName+" "+items);
        reportItems.setQuName(quName);
        reportItems.setConName(conName);
        reportItems.setItems(items);
        return reportItems;
    }
}
