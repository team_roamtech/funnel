package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.fragments.RegistrationFragment;
import com.roamtech.android.sams.fragments.UserSignInFragment;

/**
 * Created by dennis on 4/22/15.
 */
public class SigninPagerAdapter extends FragmentStatePagerAdapter {
    Context ctx;
    UserSignInFragment userSignInFragment;
    RegistrationFragment registrationFragment;

    public SigninPagerAdapter(Context ctx,FragmentManager fm){
        super(fm);
        this.ctx = ctx;

    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                userSignInFragment = new UserSignInFragment();
                Bundle args = new Bundle();
                args.putInt(ctx.getResources().getString(R.string.bundle_position),1);
                userSignInFragment.setArguments(args);
                return userSignInFragment;
            case 1:
                registrationFragment = new RegistrationFragment();
                Bundle args3 = new Bundle();
                args3.putInt(ctx.getResources().getString(R.string.bundle_position),3);
                registrationFragment.setArguments(args3);
                return registrationFragment;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 1;
    }
    @Override
    public Parcelable saveState(){
        return null;
    }
}
