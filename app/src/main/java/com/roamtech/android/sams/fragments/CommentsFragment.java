package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.dialog_fragments.AddNewContactDialogFragment;
import com.roamtech.android.sams.dialog_fragments.AddNewOrganisationDialogFragment;
import com.roamtech.android.sams.dialog_fragments.AddQuotationDialogFragment;
import com.roamtech.android.sams.dialog_fragments.AddSaleDialogFragment;
import com.roamtech.android.sams.dialog_fragments.ProgressDialogFragment;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 *
 * Captures comments and feedback from the user about the app and saves them in the server
 *
 * Created by Karuri on 2/22/2015.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class CommentsFragment extends Fragment implements View.OnClickListener{
    public static final String TAG = "CommentsFragment";
    public static final int DIALOG_COMMENTS_FRAGMENT = 1302;
    EditText editTitle,editNotes;
    TextView txtCommentOn;
    Button btnSendComment;
    Spinner spnCategory;
    AppCompatActivity mActivity;
    ProgressDialogFragment progressDialogFragment;
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    GetUsername getUsername;
    String slugID;
    int commentsID;
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    int syncDone = 1;
    Double [] coordinates;
    GoogleApiClient mGoogleApiClient;
    String strComments,strTitle,strModel,foreignKey,commentOn,strIssue;
    MainActivity mainActivity;
    AddNewContactDialogFragment addNewContactDialogFragment;
    AddNewOrganisationDialogFragment addNewOrganisationDialogFragment;
    AddSaleDialogFragment addSaleDialogFragment;
    AddQuotationDialogFragment addQuotationDialogFragment;
    String [] drawerListArray;
    ImageButton imgClose;

    /**
     * Constructor
     */
    public CommentsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        prefs = PreferenceManager
                .getDefaultSharedPreferences(mActivity);
        progressDialogFragment = new ProgressDialogFragment();
        mainActivity = new MainActivity();
        editor = prefs.edit();
        getUsername = new GetUsername(mActivity);
        commonHash = new CommonHash(mActivity);
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        drawerListArray = getResources().getStringArray(R.array.main_activity_items);
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
        mActivity.getSupportActionBar().setTitle(mActivity.getResources().getString(R.string.title_feedback));
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_comments, container, false);
        editTitle = (EditText) rootView.findViewById(R.id.edit_title);
        editNotes = (EditText) rootView.findViewById(R.id.edit_notes);
        txtCommentOn = (TextView) rootView.findViewById(R.id.txt_comment_on);
        imgClose = (ImageButton) rootView.findViewById(R.id.fab_button);
        imgClose.setOnClickListener(this);
        spnCategory = (Spinner) rootView.findViewById(R.id.spn_categories);
        spnCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        txtCommentOn.setVisibility(View.GONE);
                        break;
                    case 1:
                        if(foreignKey == null) {
                            strModel = mActivity.getResources().getString(R.string.db_table_model_3);
                            addSaleDialogFragment = new AddSaleDialogFragment();
                            addSaleDialogFragment.setTargetFragment(CommentsFragment.this, MainModel.DIALOG_FRAGMENT_ADD_SALE);
                            addSaleDialogFragment.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                        }
                        break;
                    case 2:
                        if(foreignKey == null) {
                            strModel = mActivity.getResources().getString(R.string.db_table_model_4);
                            addQuotationDialogFragment = new AddQuotationDialogFragment();
                            addQuotationDialogFragment.setTargetFragment(CommentsFragment.this, MainModel.DIALOG_FRAGMENT_ADD_QUOTATION);
                            addQuotationDialogFragment.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), "dialog");
                        }
                        break;
                    case 3:
                        if(foreignKey == null) {
                            strModel = mActivity.getResources().getString(R.string.db_table_model_1);
                            addNewContactDialogFragment = new AddNewContactDialogFragment();
                            addNewContactDialogFragment.setTargetFragment(CommentsFragment.this, MainModel.DIALOG_FRAGMENT_ADD_CONTACT);
                            addNewContactDialogFragment.show(mainActivity.addDialogFragment(mActivity.getSupportFragmentManager()), "dialog");
                        }
                        break;
                    case 4:
                        if(foreignKey == null) {
                            strModel = mActivity.getResources().getString(R.string.db_table_model_2);
                            addNewOrganisationDialogFragment = new AddNewOrganisationDialogFragment();
                            addNewOrganisationDialogFragment.setTargetFragment(CommentsFragment.this, MainModel.DIALOG_FRAGMENT_ADD_ORGANISATION);
                            addNewOrganisationDialogFragment.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                        }
                        break;
                    case 5:
                        txtCommentOn.setVisibility(View.VISIBLE);
                        txtCommentOn.setHint(mActivity.getResources().getString(R.string.txt_write_issue));
                        break;
                }
                Bundle args = getArguments();
                if(args != null){
                    foreignKey = args.getString(mActivity.getResources().getString(R.string.db_table_act_foreign_key));
                    if(args.containsKey(mActivity.getResources().getString(R.string.db_table_org_name))) {
                        spnCategory.setSelection(4);
                        commentOn = args.getString(mActivity.getResources().getString(R.string.db_table_org_name));
                        txtCommentOn.setVisibility(View.VISIBLE);
                        txtCommentOn.setText(commentOn);
                    }else if(args.containsKey(mActivity.getResources().getString(R.string.db_table_con_name))){
                        spnCategory.setSelection(3);
                        commentOn = args.getString(mActivity.getResources().getString(R.string.db_table_con_name));
                        txtCommentOn.setVisibility(View.VISIBLE);
                        txtCommentOn.setText(commentOn);
                    }else if(args.containsKey(mActivity.getResources().getString(R.string.db_table_sale_name))){
                        spnCategory.setSelection(1);
                        commentOn = args.getString(mActivity.getResources().getString(R.string.db_table_sale_name));
                        txtCommentOn.setVisibility(View.VISIBLE);
                        txtCommentOn.setText(commentOn);
                    }else if(args.containsKey(mActivity.getResources().getString(R.string.db_table_qu_name))){
                        spnCategory.setSelection(2);
                        commentOn = args.getString(mActivity.getResources().getString(R.string.db_table_qu_name));
                        txtCommentOn.setVisibility(View.VISIBLE);
                        txtCommentOn.setText(commentOn);
                    }
                    strModel = args.getString(mActivity.getResources().getString(R.string.db_table_act_model));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnSendComment = (Button) rootView.findViewById(R.id.btn_send_comment);
        btnSendComment.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.DIALOG_FRAGMENT_ADD_CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getExtras() != null) {
                    txtCommentOn.setVisibility(View.VISIBLE);
                    commentOn = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_name));
                    foreignKey = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_slug_id));
                    Log.d(TAG, TAG + String.valueOf(foreignKey));
                    txtCommentOn.setText(commentOn);
                }
            }
        }else if(requestCode == MainModel.DIALOG_FRAGMENT_ADD_SALE) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getExtras() != null) {
                    txtCommentOn.setVisibility(View.VISIBLE);
                    commentOn = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_name));
                    foreignKey = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_slug_id));
                    txtCommentOn.setText(commentOn);
                }
            }
        }else if(requestCode == MainModel.DIALOG_FRAGMENT_ADD_ORGANISATION) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.getExtras() != null) {
                    txtCommentOn.setVisibility(View.VISIBLE);
                    foreignKey = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id));
                    commentOn = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_name));
                    txtCommentOn.setText(commentOn);
                }
            }
        }else if(requestCode == MainModel.DIALOG_FRAGMENT_ADD_QUOTATION ){
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    txtCommentOn.setVisibility(View.VISIBLE);
                    foreignKey = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_qu_slug_id));
                    commentOn = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_qu_name));
                    txtCommentOn.setText(commentOn);
                }
            }
        }

    }

    /**
     *
     * Validates data entered by the user
     *
     * @return boolean if all data has been validated
     */
    public boolean fillData() {
        boolean fillData = false;
        Bundle args = new Bundle();
        if (editTitle.length() < 1) {
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.edit_title), Toast.LENGTH_LONG).show();
            fillData = false;
        }else if(editNotes.length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.txt_write_comment), Toast.LENGTH_LONG).show();
            fillData = false;
        }else{
            if(slugID == null) {
                commentsID = (int) insertComments();
                try {
                    slugID = commonHash.getHash(String.valueOf(insertComments()));
                    Log.d(TAG, "Common Hash is  " + slugID);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                insertComments();
            }else{
                insertComments();
            }
            fillData = true;
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_comment_saved),Toast.LENGTH_LONG).show();
            clearTextFields();
        }
        return fillData;
    }

    /**
     *
     * Inserts comments into the DB
     *
     * @return Id of comment inserted
     */
    public long insertComments(){
        syncDone = 1;
        coordinates = getCoordinates.getCoordinates();
        int playListId = 0;
        strComments = editNotes.getText().toString();
        strTitle = editTitle.getText().toString();
        strIssue = txtCommentOn.getText().toString();
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_cm_slug_id), slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_cm_title), strTitle);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_cm_issue), strIssue);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_cm_description), strComments);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_cm_model), strModel);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_cm_foreign_key), foreignKey);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_cm_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_cm_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_cm_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_cm_longitude), 0.0);
        }
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());
        if(commentsID == 0) {
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "54");
            initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
            Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
            playListId = (int) ContentUris.parseId(insertUri);
            Log.d(TAG, "------- primary id == " + String.valueOf(playListId));
        }else{
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "55");
            playListId = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(commentsID),mActivity.getResources().getString(R.string.flag_sync)});
            Log.d(TAG, "------- primary id == 2" + String.valueOf(commentsID));
        }

        return playListId;
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_send_comment:
                fillData();
                break;
            case R.id.fab_button:
                Intent intent2 = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent2);
                if(getTargetFragment() != null) {
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent2);
                }
                mActivity.getSupportFragmentManager().popBackStackImmediate();
                break;
            default:

                break;
        }
    }

    /**
     * Clears all the TextFields entered
     */
    public void clearTextFields(){
        spnCategory.setSelection(0);
        editTitle.setText(null);
        editNotes.setText(null);
        txtCommentOn.setText(null);
    }
}
