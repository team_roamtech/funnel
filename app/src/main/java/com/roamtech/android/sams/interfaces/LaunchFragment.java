package com.roamtech.android.sams.interfaces;

import android.os.Bundle;

/**
 *
 * Used by classes with ListView adapters to get data and position of a selected item in a ListView and
 * pass that data to the Main Thread
 *
 * Created by dennis on 2/23/15.
 * @author Dennis Mwangi Karuri
 */
public interface LaunchFragment {

    /**
     *
     * @param position
     * @param bundle
     * @param view
     */
    public void launchFragment(int position,Bundle bundle,int view);
}
