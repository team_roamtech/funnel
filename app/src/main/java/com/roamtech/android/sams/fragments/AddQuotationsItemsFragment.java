package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.dialog_fragments.DeleteDialog;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddQuotationsItemsFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = "AddQuotationsItemsFragment";
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    ImageButton imgClose;
    String quotationID;
    int quotationItemID;
    int status = 1,syncDone;
    String slugID;
    String itemName,description;
    Double quantity,unitPrice;
    EditText editItemName,editQuantity,editPrice,editDescription;
    Button btnAdd,btnEdit,btnDone;
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    GetUsername getUsername;
    DeleteDialog newFragment6;


    public AddQuotationsItemsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        commonHash = new CommonHash(mActivity);
        mainActivity = new MainActivity();
        Bundle args = getArguments();
        if(args != null){
            quotationItemID = args.getInt(mActivity.getResources().getString(R.string.db_table_id));
            quotationID = args.getString(mActivity.getResources().getString(R.string.db_table_item_qu_slug_id));
            itemName = args.getString(mActivity.getResources().getString(R.string.db_table_item_name));
            quantity = args.getDouble(mActivity.getResources().getString(R.string.db_table_item_quantity));
            unitPrice = args.getDouble(mActivity.getResources().getString(R.string.db_table_item_unit_price));
            description = args.getString(mActivity.getResources().getString(R.string.db_table_item_description));
            slugID = args.getString(mActivity.getResources().getString(R.string.db_table_item_slug_id));
            syncDone = args.getInt(mActivity.getResources().getString(R.string.db_table_sync_done));
        }
        newFragment6 = new DeleteDialog();
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        getUsername = new GetUsername(mActivity);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_quotations_items, container, false);
        editItemName = (EditText) rootView.findViewById(R.id.edit_item_name);
        if(itemName != null){
            editItemName.setText(itemName);
        }
        editQuantity = (EditText) rootView.findViewById(R.id.edit_quantity);
        editQuantity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null) {
                    try {
                        quantity = Double.parseDouble(s.toString().replace(",", ""));
                    } catch (NumberFormatException e) {
                        //Error
                    }
                }
            }
        });
        if(quantity != null & quantity != 0){
            editQuantity.setText(MainModel.doubleToStringNoDecimal(quantity));
        }
        editPrice = (EditText) rootView.findViewById(R.id.edit_price);
        editPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s != null) {
                    try {
                        unitPrice = Double.parseDouble(s.toString().replace(",", ""));
                    } catch (NumberFormatException e) {
                        //Error
                    }
                }
            }
        });
        if(unitPrice != null & unitPrice != 0){
            editPrice.setText(MainModel.doubleToStringNoDecimal(unitPrice));
        }
        editDescription = (EditText) rootView.findViewById(R.id.edit_notes);
        if(description != null){
            editDescription.setText(description);
        }
        btnAdd = (Button) rootView.findViewById(R.id.btn_add);
        btnAdd.setOnClickListener(this);
        btnDone = (Button) rootView.findViewById(R.id.btn_done);
        btnDone.setOnClickListener(this);
        if(quotationItemID != 0) {
            btnEdit = (Button) rootView.findViewById(R.id.btn_edit);
            btnEdit.setOnClickListener(this);
            btnEdit.setVisibility(View.VISIBLE);
            btnAdd.setVisibility(View.GONE);
            btnDone.setVisibility(View.GONE);
        }
        imgClose = (ImageButton) rootView.findViewById(R.id.fab_button);
        imgClose.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == MainModel.DIALOG_FRAGMENT_DELETE_DIALOG ) {
            if (resultCode == Activity.RESULT_OK) {
                status = 0;
                insertItems();
                Intent intent = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                getFragmentManager().popBackStackImmediate();
            } else {

            }
        }
    }

    public void fillData(){
        if(editItemName.getText().length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_item_name), Toast.LENGTH_LONG).show();
        }else if(editQuantity.getText().length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_quantity), Toast.LENGTH_LONG).show();
        }else if(editPrice.getText().length() < 1){
            Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_enter_price), Toast.LENGTH_LONG).show();
        }else {
            if (slugID == null) {
                quotationItemID = (int) insertItems();
             //   Log.d(TAG, "ITEM ID   " + quotationItemID);
                try {
                    slugID = commonHash.getHash(String.valueOf(quotationItemID));
               //     Log.d(TAG, "Common Hash is  " + slugID);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                insertItems();
            } else {
                insertItems();
            }
        }
    }

    public int insertItems(){
        syncDone = 1;
        coordinates = getCoordinates.getCoordinates();
        int quotationDetailsID;
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_qu_slug_id), quotationID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_name), editItemName.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_quantity), quantity);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_unit_price), unitPrice);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_description), editDescription.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_slug_id), slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_item_status), status);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_item_longitude), 0.0);
        }
        initialValues.put(mActivity.getResources().getString(R.string.user_id), getUsername.getUserName());

        if(quotationItemID == 0){
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"22");
            Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
            initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
            quotationDetailsID = (int) ContentUris.parseId(insertUri);
        }else{
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"31");
            quotationDetailsID = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(quotationItemID),mActivity.getResources().getString(R.string.flag_sync)});
        }

        return quotationDetailsID;
    }

    public void clearAll(){
        editItemName.setText("");
        editQuantity.setText("");
        editDescription.setText("");
        editPrice.setText("");
        quotationItemID = 0;
        slugID = null;
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_add:
                fillData();
                clearAll();
                Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_add_new_item), Toast.LENGTH_LONG).show();
                break;
            case R.id.btn_edit:
                fillData();
                clearAll();
                //Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_add_new_item), Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent1);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent1);
                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.btn_done:
                Intent intent = new Intent();
                mActivity.setResult(Activity.RESULT_OK, intent);
                getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.fab_button:
                if(quotationItemID == 0){
                    Intent intent2 = new Intent();
                    mActivity.setResult(Activity.RESULT_OK, intent2);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent2);
                    getFragmentManager().popBackStackImmediate();
                }else{
                    Bundle args = new Bundle();
                    args.putString(mActivity.getResources().getString(R.string.txt_advisory),mActivity.getResources().getString(R.string.txt_advisory_delete) + " " +mActivity.getResources().getString(R.string.txt_advisory_items));
                    newFragment6.setArguments(args);
                    newFragment6.setTargetFragment(AddQuotationsItemsFragment.this, MainModel.DIALOG_FRAGMENT_DELETE_DIALOG);
                    newFragment6.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                }
                break;
        }
    }
}
