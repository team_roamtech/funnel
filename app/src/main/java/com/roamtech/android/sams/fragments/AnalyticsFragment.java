package com.roamtech.android.sams.fragments;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.HomeAdapter;
import com.roamtech.android.sams.http.NetworkConnectionStatus;
import com.roamtech.android.sams.http.RestClient;
import com.roamtech.android.sams.interfaces.LaunchFragment;
import com.roamtech.android.sams.models.HomeItems;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.AsyncLoader;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * Fragments that displays a summary of the performance of the user
 *
 * Created by Karuri on 2/22/2015.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class AnalyticsFragment extends Fragment implements LoaderManager.LoaderCallbacks<ArrayList<HomeItems>>,View.OnClickListener,SwipeRefreshLayout.OnRefreshListener,LaunchFragment{
    public  static final String TAG = "AnalyticsFragment";
    MainActivity mainActivity;
    AppCompatActivity mActivity;
    GridView grdAnalytics;
    SwipeRefreshLayout mSwipeRefreshLayout;
    Boolean isVisible;
    public static final int LOADER = 1200;
    int fragmentPosition;
    HomeAdapter homeAdapter;
    AddOrganisationFragment addOrganisationFragment;
    ContactsFragment contactsFragment;
    OrganisationsFragment organisationsFragment;
    ActivitiesFragment activitiesFragment;
    String username;
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;
    StagesStateFragment stagesStateFragment;
    SalesFragment salesFragment;
    String [] drawerListArray;

    /**
     * Constructor
     */
    public AnalyticsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mainActivity = new MainActivity();
        organisationsFragment = new OrganisationsFragment();
        contactsFragment = new ContactsFragment();
        activitiesFragment = new ActivitiesFragment();
        drawerListArray = getResources().getStringArray(R.array.main_activity_items);
        prefs = PreferenceManager
                .getDefaultSharedPreferences(mActivity);
        editor = prefs.edit();
        username = prefs.getString(mActivity.getResources().getString(R.string.bundle_username), null);
        Bundle args = getArguments();
        fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPosition);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_analytics, container, false);
        grdAnalytics = (GridView) rootView.findViewById(R.id.gridView_analytics);
        if(fragmentPosition == 0){
            grdAnalytics.setNumColumns(3);
        }else if(fragmentPosition == 1 || fragmentPosition == 2){
            grdAnalytics.setNumColumns(2);
        }else{
            grdAnalytics.setNumColumns(1);
        }
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_container);
        mSwipeRefreshLayout.setColorSchemeColors(R.color.login_blue, R.color.register_green, R.color.purple_theme);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        grdAnalytics.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });
        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        isVisible = isVisibleToUser;

        if (isVisible) {
            Log.d(TAG, "FragmentPosition = " + String.valueOf(fragmentPosition) );
                mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
        }
        if(fragmentPosition == 0){

        }
    }


    @Override
    public Loader<ArrayList<HomeItems>> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "OnCreateLoader = " + String.valueOf(fragmentPosition) );
        return new GetHomeItems(mActivity,fragmentPosition,username);

    }

    @Override
    public void onLoadFinished(Loader<ArrayList<HomeItems>> loader, ArrayList<HomeItems> data) {
        Log.d(TAG, "OnLoadFinished = " + String.valueOf(fragmentPosition) );
        homeAdapter = new HomeAdapter(mActivity,data,this);
        grdAnalytics.setAdapter(homeAdapter);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<HomeItems>> loader) {

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.fab_button:
                if(fragmentPosition == 0){
                    //mainActivity.addFragment(contactsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) contactsFragment).getClass().getName());
                }else if(fragmentPosition == 1){
                    mainActivity.addFragment(activitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) activitiesFragment).getClass().getName());
                }else if(fragmentPosition == 2){
                    //mainActivity.addFragment(contactsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) contactsFragment).getClass().getName());
                }else if(fragmentPosition == 3){
                    mainActivity.addFragment(contactsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) contactsFragment).getClass().getName());
                }else if(fragmentPosition == 4){
                    mainActivity.addFragment(organisationsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) organisationsFragment).getClass().getName());
                }
                break;
        }
    }

    @Override
    public void onRefresh() {
        mActivity.getSupportLoaderManager().restartLoader(LOADER, null, this);
    }

    @Override
    public void launchFragment(int position, Bundle bundle, int view) {
        Bundle args = new Bundle();
        if(fragmentPosition == 0){
            switch (position){
                case 3:
                    stagesStateFragment = new StagesStateFragment();
                    args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), 0);
                    args.putInt(getString(R.string.bundle_position),2);
                    stagesStateFragment.setArguments(args);
                    mainActivity.addFragment(stagesStateFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) stagesStateFragment).getClass().getName() + " " + String.valueOf(2));
                    break;
                case 4:
                  /*  stagesStateFragment = new StagesStateFragment();
                    args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), 1);
                    args.putInt(getString(R.string.bundle_position),2);
                    stagesStateFragment.setArguments(args);
                    mainActivity.addFragment(stagesStateFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) stagesStateFragment).getClass().getName() + " " + String.valueOf(2));*/

                    args.putInt(getString(R.string.bundle_position),position);
                    args.putString(getString(R.string.bundle_title), drawerListArray[3]);
                    args.putInt(getString(R.string.bundle_converted_sale),0);
                    salesFragment = new SalesFragment();
                    salesFragment.setArguments(args);
                    mainActivity.addFragment(salesFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) salesFragment).getClass().getName());
                    break;
                case 5:
                    args.putInt(getString(R.string.bundle_position),position);
                    args.putString(getString(R.string.bundle_title), drawerListArray[3]);
                    args.putInt(getString(R.string.bundle_converted_sale),1);
                    salesFragment = new SalesFragment();
                    salesFragment.setArguments(args);
                    mainActivity.addFragment(salesFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) salesFragment).getClass().getName());
                    break;
                default:

                    break;

            }
        }

    }

    /**
     * Loader that loads fragments in the background
     */
    public static class GetHomeItems extends AsyncLoader<ArrayList<HomeItems>> {
        Context ctx;
        ArrayList<HomeItems> items = new ArrayList<>();
        Cursor cursor;
        int fragmentPosition;
        String [] pagerItemsArray;
        String username;
        private SharedPreferences prefs;
        SharedPreferences.Editor editor;

        /**
         *
         * @param context Context which receives the result of the background thread
         * @param fragmentPosition position of the fragment to be loaded
         * @param username username of the app user
         */
        public GetHomeItems(Context context,int fragmentPosition,String username) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.fragmentPosition = fragmentPosition;
            pagerItemsArray = context.getResources().getStringArray(R.array.pager_title_items);
            this.username = username;
            prefs = PreferenceManager
                    .getDefaultSharedPreferences(ctx);
            editor = prefs.edit();
        }

        @Override
        public ArrayList<HomeItems> loadInBackground() {
            HomeItems homeItems;
            switch(fragmentPosition) {
                case 0:
                    Log.d(TAG, "GetHomeItems = " + String.valueOf(fragmentPosition) );
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_sales_targets));
                    try {
                        homeItems.setNumber(MainModel.doubleToStringNoDecimal(prefs.getFloat(ctx.getResources().getString(R.string.bundle_targets), 0)));
                    }catch(NumberFormatException ex){
                        ex.printStackTrace();
                    }
                    items.add(homeItems);
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_achieved));
                    try {
                        homeItems.setNumber(MainModel.doubleToStringNoDecimal(runTargets()));
                    }catch(NumberFormatException ex){
                        ex.printStackTrace();
                    }
                    items.add(homeItems);
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_remaining));
                    try {
                        homeItems.setNumber(MainModel.doubleToStringNoDecimal(runRemaining(prefs.getFloat(ctx.getResources().getString(R.string.bundle_targets), 0), runTargets())));
                    }catch(NumberFormatException ex){
                        ex.printStackTrace();
                    }
                    items.add(homeItems);
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_leads));
                    homeItems.setNumber(String.valueOf(runLeads()));
                    items.add(homeItems);
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_pending_sales));
                    homeItems.setNumber(String.valueOf(runSalesStage(pagerItemsArray[1],pagerItemsArray[2])));
                    items.add(homeItems);
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_converted));
                    homeItems.setNumber(String.valueOf(runSalesStage(pagerItemsArray[3],pagerItemsArray[4])));
                    items.add(homeItems);
                    break;
                case 1:
                    Log.d(TAG, "GetHomeItems = " + String.valueOf(fragmentPosition) );
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_pending_activities));
                    homeItems.setNumber(String.valueOf(runActivities(1)));
                    items.add(homeItems);
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_closed_activities));
                    homeItems.setNumber(String.valueOf(runActivities(0)));
                    items.add(homeItems);
                    break;
                case 2:
                    Log.d(TAG, "GetHomeItems = " + String.valueOf(fragmentPosition) );
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_pending_quotations));
                    homeItems.setNumber(String.valueOf(runQuotations(0)));
                    items.add(homeItems);
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_closed_quotations));
                    homeItems.setNumber(String.valueOf(runQuotations(1)));
                    items.add(homeItems);
                    break;
                case 3:
                    Log.d(TAG, "GetHomeItems = " + String.valueOf(fragmentPosition) );
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_title_contact));
                    homeItems.setNumber(String.valueOf(runContacts()));
                    items.add(homeItems);
                    break;
                case 4:
                    Log.d(TAG, "GetHomeItems = " + String.valueOf(fragmentPosition) );
                    homeItems = new HomeItems();
                    homeItems.setTitle(ctx.getResources().getString(R.string.txt_title_organisations));
                    homeItems.setNumber(String.valueOf(runOrganisations()));
                    items.add(homeItems);
                    break;
            }
            return items;
        }

        /**
         *
         * Get remaining targets not achieved
         *
         * @param targets float of targets set
         * @param achieved float of targets achieved
         * @return float o target remaining after subtracting target achieved from target set
         */
        public float runRemaining(float targets,float achieved){
            float remaining;
            remaining = targets - achieved;
            if(remaining > 0){
                return remaining;
            }else{
                return 0;
            }
        }

        /**
         *
         * Gets the total number of active sales
         *
         * @param salesStatus status of the sale either pending won or lost
         * @return Integer of the total number of sales
         */

        public int runSales(int salesStatus){
            int salesCount = 0;
            Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"24");
            cursor = ctx.getContentResolver().query(data_id, null, null,new String[] {String.valueOf(salesStatus)}, null);
            cursor.moveToFirst();
            if(cursor.getCount() != 0 && cursor != null) {
                salesCount = cursor.getInt(cursor.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_status)));
                Log.d(TAG, TAG + " " + String.valueOf(salesCount));
            }
            return salesCount;
        }

        /**
         *
         * Gets the targets set by the Admin
         *
         * @return float of the set targets
         */
        public float runTargets(){
            Cursor cursor;
            float targetAchieved = 0;
            Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"34");
            cursor = ctx.getContentResolver().query(data_id, null, null,new String[] {}, null);
            cursor.moveToFirst();
            if(cursor.getCount() != 0 && cursor != null) {
                targetAchieved = cursor.getFloat(cursor.getColumnIndex(ctx.getResources().getString(R.string.query_name_price_sum)));
                Log.d(TAG, TAG + " " + String.valueOf(targetAchieved));
            }
            return targetAchieved;
        }

        /**
         *
         * Gets the total number of sales not in this two stages
         *
         * @param stage  Stage of sale
         * @param stage2 Stage of sale
         * @return Integer of the total number of sales not in this two stages
         */
        public int runSalesStage(String stage,String stage2){
            int salesStageCount = 0;
            Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"44");
            cursor = ctx.getContentResolver().query(data_id, null, null,new String[] {stage,stage2}, null);
            cursor.moveToFirst();
            if(cursor.getCount() != 0 && cursor != null) {
                salesStageCount = cursor.getInt(cursor.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_stage)));
/*                Log.d(TAG, TAG + " " + cursor.getString(cursor.getColumnIndex(ctx.getResources().getString(R.string.user_id))));
                cursor.moveToPosition(1);
                Log.d(TAG, TAG + " " + cursor.getString(cursor.getColumnIndex(ctx.getResources().getString(R.string.user_id))));*/
            }
            return salesStageCount;
        }

        /**
         *
         * Gets the total number of sales not in this two stages
         *
         * @param activityDone int of activity status
         * @return
         */
        public int runActivities(int activityDone){
            int activitiesCount = 0;
            Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"25");
            cursor = ctx.getContentResolver().query(data_id, null, null,new String[] {String.valueOf(activityDone)}, null);
            cursor.moveToFirst();
            if(cursor.getCount() != 0 && cursor != null) {
                activitiesCount = cursor.getInt(cursor.getColumnIndex(ctx.getResources().getString(R.string.db_table_act_status)));
                Log.d(TAG, TAG + " " + String.valueOf(activitiesCount));
            }
            return activitiesCount;
        }

        public int runQuotations(int quotationsDone){
            int quotationsCount = 0;
            Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"28");
            cursor = ctx.getContentResolver().query(data_id, null, null,new String[] {String.valueOf(quotationsDone)}, null);
            cursor.moveToFirst();
            if(cursor.getCount() != 0 && cursor != null) {
                quotationsCount = cursor.getInt(cursor.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_status)));
                Log.d(TAG, TAG + " " + String.valueOf(quotationsCount));
            }
            return quotationsCount;
        }

        public int runContacts(){
            int contactsCount = 0;
            Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"26");
            cursor = ctx.getContentResolver().query(data_id, null, null,new String[] {}, null);
            cursor.moveToFirst();
            if(cursor.getCount() != 0 && cursor != null) {
                contactsCount = cursor.getInt(cursor.getColumnIndex(ctx.getResources().getString(R.string.db_table_id)));
                Log.d(TAG, TAG + " " + String.valueOf(contactsCount));
            }
            return contactsCount;
        }

        public int runLeads(){
            int contactsCount = 0;
            Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"52");
            cursor = ctx.getContentResolver().query(data_id, null, null,new String[] {}, null);
            cursor.moveToFirst();
            if(cursor.getCount() != 0 && cursor != null) {
                contactsCount = cursor.getInt(cursor.getColumnIndex(ctx.getResources().getString(R.string.db_table_id)));
                Log.d(TAG, TAG + " " + String.valueOf(contactsCount));
            }
            return contactsCount;
        }


        public int runOrganisations(){
            int organisationsCount = 0;
            Uri data_id = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"27");
            cursor = ctx.getContentResolver().query(data_id, null, null,new String[] {}, null);
            cursor.moveToFirst();
            if(cursor.getCount() != 0 && cursor != null) {
                organisationsCount = cursor.getInt(cursor.getColumnIndex(ctx.getResources().getString(R.string.db_table_id)));
                Log.d(TAG, TAG + " " + String.valueOf(organisationsCount));
            }
            return organisationsCount;
        }
    }
}
