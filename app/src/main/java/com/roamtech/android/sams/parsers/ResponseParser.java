package com.roamtech.android.sams.parsers;

import android.content.Context;

import com.roamtech.android.sams.R;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * Parses Json data gotten from the server and returns a boolean value depending on what was parsed
 *
 * Created by dennis on 4/24/15.
 * @author Dennis Mwangi Karuri
 *
 */
public class ResponseParser {
    Context ctx;

    /**
     *
     * Constructor Initializes the Context
     *
     * @param ctx
     */
    public ResponseParser(Context ctx){
        this.ctx = ctx;
    }

    /**
     *
     * Parses a Json String and gets if the Json String has a success or error String then returns a boolean value
     * true if success string was found false if error string was found
     *
     * @param jsonString Json string to be parsed gotten from the server
     * @return boolean the result assigned after the Json string is parsed
     * @throws Exception
     */
    public boolean parse(String jsonString) throws Exception {
        JSONObject jObject = null;
        JSONObject jObject2 = null;
        try {
            if (jsonString != null) {
                jObject = new JSONObject(jsonString);
                if(jObject.has(ctx.getResources().getString(R.string.json_success)) && !jObject.isNull(ctx.getResources().getString(R.string.json_success))) {
                    return true;
                }else if(jObject.has(ctx.getResources().getString(R.string.json_error)) && !jObject.isNull(ctx.getResources().getString(R.string.json_error))){
                     return false;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}
