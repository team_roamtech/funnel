package com.roamtech.android.sams.interfaces;

import android.os.Bundle;

/**
 *
 * Used by classes with ListView adapters to get data and position of a selected item in a ListView and
 * pass that data to the Main Thread
 *
 * Created by dennis on 3/13/15.
 * @author Dennis Mwangi Karuri
 */
public interface OnAddSelectedListener {

     /**
      *
      * Gets data of selected item in a ListView and its position
      *
      * @param args Data Bundles of the selected Item
      * @param position Position of the selected Item
      * @param listSelect integer id of the selected ListView
      */
     public void addSelectedItem(Bundle args,int position,int listSelect);
}
