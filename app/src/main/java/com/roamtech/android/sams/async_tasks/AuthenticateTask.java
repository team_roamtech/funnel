package com.roamtech.android.sams.async_tasks;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.roamtech.android.sams.R;

import com.roamtech.android.sams.client.LDAPServerInstance;
import com.roamtech.android.sams.interfaces.ReturnAuthenticationResult;
import com.roamtech.android.sams.models.LoginItems;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.utils.GetUsername;
import com.unboundid.ldap.sdk.Entry;
import com.unboundid.ldap.sdk.ExtendedResult;
import com.unboundid.ldap.sdk.Filter;
import com.unboundid.ldap.sdk.LDAPConnection;
import com.unboundid.ldap.sdk.LDAPException;
import com.unboundid.ldap.sdk.ResultCode;
import com.unboundid.ldap.sdk.RootDSE;
import com.unboundid.ldap.sdk.SearchRequest;
import com.unboundid.ldap.sdk.SearchResult;
import com.unboundid.ldap.sdk.SearchScope;
import com.unboundid.ldap.sdk.extensions.PasswordModifyExtendedRequest;
import com.unboundid.ldap.sdk.extensions.PasswordModifyExtendedResult;
import com.unboundid.util.LDAPTestUtils;


/**
 * Created by dennis on 10/14/14.
 */
public class AuthenticateTask extends AsyncTask<Object, Void, LoginItems>{
    public static final String TAG = "AuthenticateTask";
    Context context;
    Fragment fragment;
    String userDN;
    public static LDAPConnection connection = null;
    private SharedPreferences prefs;
    String dnUser;
    static final int SIZE_LIMIT = 100;
    static final int TIME_LIMIT_SECONDS = 30;
    LoginItems loginItems;
    String uid;
    int flag;
    GetUsername getUsername;

    private Entry entry = null;

    public AuthenticateTask(Context context, Fragment fragment,String uid,int flag){
        this.context = context;
        this.fragment = fragment;
        prefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        loginItems = new LoginItems();
        this.uid = uid;
        this.flag = flag;
        getUsername = new GetUsername(context);
    }

    @Override
    protected void onPreExecute() {
        // TODO Auto-generated method stub
        super.onPreExecute();
        ReturnAuthenticationResult ac = (ReturnAuthenticationResult) fragment;
        ac.onStartTask();
    }
    @Override
    protected LoginItems doInBackground(Object... params) {
        LDAPServerInstance ldapServer = (LDAPServerInstance) params[0];
        if(flag == MainModel.USERSIGINFRAGMENT) {
            Log.d(TAG,"USERSIGINFRAGMENT string");
            return authenticate(ldapServer, context);
        }else if(flag == MainModel.SETTINGSFRAGMENTS){
            Log.d(TAG,"SETTINGSFRAGMENTS string");
            String oldPassword = (String)params[1];
            String newPassword = (String)params[2];
            return modifyPassword(ldapServer,oldPassword,newPassword);
        }else {
        /*dnUser = prefs.getString(context.getResources().getString(R.string.bundle_dnuser),null);
        try {
            return isGroupContainUser(connection,"SUPERADMIN",dnUser);
        } catch (LDAPException e) {
            e.printStackTrace();
        }*/
            return null;
        }
    }

    @Override
    protected void onPostExecute(LoginItems loginItems) {
        // TODO Auto-generated method stub
        super.onPostExecute(loginItems);
//        Log.d(TAG,String.valueOf(loginItems.isUser()));
        ReturnAuthenticationResult ac = (ReturnAuthenticationResult) fragment;
        try {
            ac.onReturnResult(loginItems);
        } catch (LDAPException e) {
            e.printStackTrace();
        }
    }

    public LoginItems modifyPassword(LDAPServerInstance ldapServerInstance,String oldPassword,String newPassword){
        LoginItems loginItems1 = new LoginItems();
        PasswordModifyExtendedRequest passwordModifyRequest =
                new PasswordModifyExtendedRequest(
                        getUsername.getDnUser(), // The user to update
                        oldPassword, // The current password for the user.
                        null); // The new password.  null = server will generate
        Log.d(TAG,"ldapServerInstance string"+ldapServerInstance.toString());

        PasswordModifyExtendedResult passwordModifyResult = null;
        try
        {
            connection = ldapServerInstance.getConnection();
            Log.d(TAG,"connection string"+connection.toString());
            if(connection != null) {
                Log.d(TAG,"connection does exist");
                passwordModifyResult = (PasswordModifyExtendedResult)
                        connection.processExtendedOperation(passwordModifyRequest);
            }else{
                Log.d(TAG,"connection does not exist");
            }
            // This doesn't necessarily mean that the operation was successful, since
            // some kinds of extended operations return non-success results under
            // normal conditions.
        }
        catch (LDAPException le)
        {
            // For an extended operation, this generally means that a problem was
            // encountered while trying to send the request or read the result.

            try {
                passwordModifyResult = new PasswordModifyExtendedResult(
                        new ExtendedResult(le));
            } catch (LDAPException e) {
                e.printStackTrace();
            }
        }

        Log.d(TAG,"Password ResultCode is "+passwordModifyResult.getResultCode());

        try {
            LDAPTestUtils.assertResultCodeEquals(passwordModifyResult,
                    ResultCode.SUCCESS);
        }catch(AssertionError e){
            e.printStackTrace();
        }
        String serverGeneratedNewPassword =
                passwordModifyResult.getGeneratedPassword();
        Log.d(TAG,"Password is "+serverGeneratedNewPassword);

        return loginItems1;
    }

    public LoginItems authenticate(LDAPServerInstance ldapServer,final Context context) {
        String filterString;
        try {
            connection = ldapServer.getConnection();

            if (connection != null) {
                RootDSE s = connection.getRootDSE();
                String[] baseDNs = null;
                if (s != null) {
                    baseDNs = s.getNamingContextDNs();
                }
                filterString = "(uid=" + uid + ')';
                Filter filter = null;
                filter = Filter.create(filterString);

                final SearchRequest request = new SearchRequest(MainModel.BASEDNS,
                        SearchScope.SUB, filter);
                request.setSizeLimit(SIZE_LIMIT);
                request.setTimeLimitSeconds(TIME_LIMIT_SECONDS);
                SearchResult result;
                result = connection.search(request);
                Log.d(TAG,"Search done "+result);
                final int entryCount = result.getEntryCount();
                if (entryCount == 0)
                {
                    loginItems.setUser(false);
                    loginItems.setRoles(null);
                    loginItems.setResultCode(-1);
                    loginItems.setMail(null);

                }
                else if (entryCount == 1)
                {
                    entry = result.getSearchEntries().get(0);
                    if (hasContactAttributes(entry))
                    {
                        String name = entry.getAttributeValue(MainModel.ATTR_UID);
                        String roles = entry.getAttributeValue(MainModel.ATTR_EMALI_ROLES) ;
                        String clientId = entry.getAttributeValue((MainModel.ATTR_CLIENT_ID));
                        String email = entry.getAttributeValue(MainModel.ATTR_PRIMARY_MAIL);
                        Log.d(TAG,"------------"+name+"---------"+roles+"---------------"+clientId+"-------"+email);
                        String [] userRoles = roles.split(",");
                        loginItems.setUser(true);
                        loginItems.setRoles(userRoles);
                        loginItems.setResultCode(0);
                        loginItems.setMail(email);
                        //  Toast.makeText(this,"display user",Toast.LENGTH_SHORT).show();
                    }else{
                        loginItems.setUser(false);
                        loginItems.setRoles(null);
                        loginItems.setResultCode(-1);
                        loginItems.setMail(null);
                    }
                }
                //getUserDetails("",connection);
                //isGroupContainUser(connection,null,"ou=roamtech,dc=roamtech,dc=com");
                }

                return loginItems;
        } catch (LDAPException e) {
            Log.e(TAG, "Error authenticating "+ e.getResultCode().intValue());
            loginItems.setUser(false);
            loginItems.setRoles(null);
            loginItems.setResultCode(e.getResultCode().intValue());
            loginItems.setMail(null);
            return loginItems;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    private boolean hasContactAttributes(final Entry e)
    {
        // The entry must have a full name attribute to be considered a user.
        if (! e.hasAttribute(MainModel.ATTR_UID) & !e.hasAttribute(MainModel.ATTR_EMALI_ROLES))
        {
            return false;
        }


        // The user also needs at least one of an e-mail address or phone number.
        return (e.hasAttribute(MainModel.ATTR_PRIMARY_MAIL) ||
                e.hasAttribute(MainModel.ATTR_GIVEN_NAME) ||
                e.hasAttribute(MainModel.ATTR_CLIENT_ID) ||
                e.hasAttribute(MainModel.ATTR_SN_NAME) ||
                e.hasAttribute(MainModel.ATTR_CN_NAME));
    }
}
