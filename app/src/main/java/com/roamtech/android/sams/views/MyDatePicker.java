package com.roamtech.android.sams.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.DatePicker;

/**
 * Created by dennis on 4/17/15.
 */
public class MyDatePicker extends DatePicker {


    public MyDatePicker(Context context) {
        super(context);
    }

    public MyDatePicker(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyDatePicker(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public MyDatePicker(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public void updateDate(int year, int month, int dayOfMonth) {
               // mDelegate.updateDate(year, month, dayOfMonth);
     }

    public void init(int year, int monthOfYear, int dayOfMonth,
                     OnDateChangedListener onDateChangedListener) {
       super.init(year,monthOfYear,dayOfMonth,onDateChangedListener);
    }



}
