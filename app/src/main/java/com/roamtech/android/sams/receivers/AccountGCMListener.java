package com.roamtech.android.sams.receivers;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GcmListenerService;
import com.roamtech.android.sams.models.MainModel;

/**
 *
 * Send result of Phone Registration to be saved
 *
 * Created by dennis on 11/17/15.
 *  @author Dennis Mwangi Karuri
 */
public class AccountGCMListener extends GcmListenerService {

    private static final String TAG = "AccountGCMListener";
    private SharedPreferences prefs;
    SharedPreferences.Editor editor;



    @Override
    public void onMessageReceived(String from, Bundle data) {
        prefs = PreferenceManager
                .getDefaultSharedPreferences(this);
        editor = prefs.edit();
        //Get message sent from data bundle
        String message = data.getString(MainModel.MESSAGE);
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);
        sendNotification(message);
    }

    /**
     *
     * Send result of Phone Registration to be saved
     * @param message message received whether Phone Registraion was successful or not
     */
    private void sendNotification(String message) {
        Log.d(TAG," Message is "+message);
        if(message.equals(MainModel.ACTIVATE)){
            MainModel.activateAccount(prefs,true);
        }else if(message.equals(MainModel.DEACTIVATE)){
            MainModel.activateAccount(prefs,false);
        }
    }
}
