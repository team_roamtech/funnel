package com.roamtech.android.sams.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.HomeAdapter;
import com.roamtech.android.sams.adapters.HomePagerAdapter;
import com.roamtech.android.sams.interfaces.GoToFragment;
import com.roamtech.android.sams.models.HomeItems;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.AsyncLoader;
import com.roamtech.android.sams.views.HeaderGridView;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 *
 * Fragment that loads initially when the app starts and houses the analytics
 *
 * Created by Karuri on 2/22/2015.
 * @author Dennis Mwangi Karuri
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements LoaderManager.LoaderCallbacks<String[]>,View.OnClickListener{
    public  static final String TAG = "HomeFragment";
    AppCompatActivity mActivity;
    MainActivity mainActivity;
    public static final int LOADER = 1100;
    HomePagerAdapter homePagerAdapter;
    ViewPager mPager;
    FrameLayout frameLayout;
    int fragmentPosition;
    int position;
    ImageButton imgFab;
    TextView txtAddSale;
    String [] pagerItemsArray;
    StagesStateFragment stagesStateFragment;
    ActivitiesFragment activitiesFragment;
    QuotationsFragment quotationsFragment;
    ContactsFragment contactsFragment;
    OrganisationsFragment organisationsFragment;
    NewDealFragment newDealFragment;
    String [] drawerListArray;
    AddActivitiesFragment addActivitiesFragment;
    CommentsFragment commentsFragment;


    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mainActivity = new MainActivity();
        stagesStateFragment = new StagesStateFragment();
        activitiesFragment = new ActivitiesFragment();
        quotationsFragment = new QuotationsFragment();
        contactsFragment = new ContactsFragment();
        organisationsFragment = new OrganisationsFragment();
        newDealFragment = new NewDealFragment();
        drawerListArray = getResources().getStringArray(R.array.main_activity_items);
        Bundle args = getArguments();
        if (savedInstanceState != null) {
            Log.d(TAG,"---------||||||||||||||||||----------"+ mActivity.toString());
            pagerItemsArray = savedInstanceState.getStringArray(mActivity.getResources().getString(R.string.bundle_pager_items));
            position = savedInstanceState.getInt(mActivity.getResources().getString(R.string.bundle_position));
            fragmentPosition = savedInstanceState.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
        }else {
            if (args != null) {
                pagerItemsArray = args.getStringArray(mActivity.getResources().getString(R.string.bundle_pager_items));
                position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
                fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
                Log.d(TAG,"Fragment position " + position);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        String [] homeListArray = mActivity.getResources().getStringArray(R.array.home_items);
        frameLayout = (FrameLayout) rootView.findViewById(R.id.frm_layouts);
        imgFab = (ImageButton) rootView.findViewById(R.id.fab_button);
        imgFab.setOnClickListener(this);
        txtAddSale = (TextView) rootView.findViewById(R.id.txt_add_a_sale);
        mPager = (ViewPager) rootView.findViewById(R.id.viewPager);
        mPager.setAdapter(new HomePagerAdapter(mActivity.getSupportFragmentManager(), mActivity));
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                fragmentPosition = position;
                switch (position) {
                    case 0:
                        frameLayout.setBackgroundColor(mActivity.getResources().getColor(R.color.login_blue));
                        txtAddSale.setText(mActivity.getResources().getString(R.string.txt_add_a_sale));
                        break;
                    case 1:
                        frameLayout.setBackgroundColor(mActivity.getResources().getColor(R.color.activities_green));
                        txtAddSale.setText(mActivity.getResources().getString(R.string.txt_add_activity));
                        break;
                    case 2:
                        frameLayout.setBackgroundColor(mActivity.getResources().getColor(R.color.quotations_green));
                        txtAddSale.setText(mActivity.getResources().getString(R.string.txt_add_quotation));
                        break;
                    case 3:
                        frameLayout.setBackgroundColor(mActivity.getResources().getColor(R.color.contacts_green));
                        txtAddSale.setText(mActivity.getResources().getString(R.string.txt_add_contact));
                        break;
                    case 4:
                        frameLayout.setBackgroundColor(mActivity.getResources().getColor(R.color.organisations_purple));
                        txtAddSale.setText(mActivity.getResources().getString(R.string.txt_add_organisation));
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        if(pagerItemsArray != null && pagerItemsArray.length > 1){
            homePagerAdapter = new HomePagerAdapter(getChildFragmentManager(),mActivity);
            if(homePagerAdapter != null) mPager.setAdapter(homePagerAdapter);
            homePagerAdapter.notifyDataSetChanged();
            mPager.setCurrentItem(fragmentPosition, true);
        }else {
            mActivity.getSupportLoaderManager().initLoader(LOADER, null, this);
            Log.d(TAG, " OnCreateView " + fragmentPosition);
        }

        return rootView;
    }

    @Override
    public void onResume(){
        super.onResume();
        mActivity.getSupportActionBar().setTitle(mActivity.getResources().getString(R.string.title_dash_board));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_flag).setVisible(true);
        menu.findItem(R.id.action_comments).setVisible(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.action_flag:
                addActivitiesFragment = new AddActivitiesFragment();
                addActivitiesFragment.setTargetFragment(HomeFragment.this, MainModel.ACTIVITY_FRAGMENT);
                mainActivity.addFragment(addActivitiesFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) addActivitiesFragment).getClass().getName());
                break;
            case R.id.action_comments:
                commentsFragment = new CommentsFragment();
                commentsFragment.setTargetFragment(HomeFragment.this, MainModel.COMMENTS_FRAGMENT);
                mainActivity.addFragment(commentsFragment, true, FragmentTransaction.TRANSIT_ENTER_MASK, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) commentsFragment).getClass().getName());
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        if(pagerItemsArray != null){
            outState.putStringArray(mActivity.getResources().getString(R.string.bundle_pager_items),pagerItemsArray);
            outState.putInt(mActivity.getResources().getString(R.string.bundle_position),position);
            outState.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position),fragmentPosition);
        }
    }

    /**
     * Loads Fragments in the Background
     */
    public static class GetFragments extends AsyncLoader<String[]> {
        String [] arrayItems;
        Context ctx;

        /**
         *
         * Constructor
         *
         * @param context Context where result of background thread is returned
         * @param arrayItems array of items to be loaded
         */
        public GetFragments(Context context,String [] arrayItems) {
            super(context);
            // TODO Auto-generated constructor stub
            this.ctx = context;
            this.arrayItems = arrayItems;

        }

        @Override
        public String[] loadInBackground() {
            return arrayItems;
        }
        //Process the JSON result to Data we can use
    }

    @Override
    public Loader<String[]> onCreateLoader(int id, Bundle args) {
        return new GetFragments(mActivity, mActivity.getResources().getStringArray(R.array.home_items));
    }

    @Override
    public void onLoadFinished(Loader<String[]> loader, String [] data) {
        homePagerAdapter = new HomePagerAdapter(getChildFragmentManager(),mActivity);
        if(homePagerAdapter != null)
            mPager.setAdapter(homePagerAdapter);
        homePagerAdapter.notifyDataSetChanged();
    }

    @Override
    public void onLoaderReset(Loader<String[]> loader) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.PAGER_FRAGMENT) {
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    int fragmentPos = data.getIntExtra(mActivity.getResources().getString(R.string.bundle_fragment_position), 0);
                    stagesStateFragment = new StagesStateFragment();
                    Bundle args = new Bundle();
                    args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), fragmentPos);
                    stagesStateFragment.setArguments(args);
                    mainActivity.addFragment(stagesStateFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(),((Object) stagesStateFragment).getClass().getName());
                    // mActivity.getSupportFragmentManager().popBackStackImmediate();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        Bundle args = new Bundle();
        switch(v.getId()){
            case R.id.fab_button:
                switch(fragmentPosition){
                    case 0:
                      /*  args.putInt(getString(R.string.bundle_position),2);
                        newDealFragment.setTargetFragment(HomeFragment.this, MainModel.PAGER_FRAGMENT);
                        newDealFragment.setArguments(args);
                        mainActivity.addFragment(newDealFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) newDealFragment).getClass().getName());*/
                        stagesStateFragment = new StagesStateFragment();
                        args.putInt(mActivity.getResources().getString(R.string.bundle_fragment_position), 0);
                        args.putInt(getString(R.string.bundle_position),2);
                        stagesStateFragment.setArguments(args);
                        mainActivity.addFragment(stagesStateFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) stagesStateFragment).getClass().getName() + " " + String.valueOf(2));
                        break;
                    case 1:
                        args.putString(getString(R.string.bundle_title),drawerListArray[4]);
                        args.putInt(getString(R.string.bundle_position),4);
                        activitiesFragment.setArguments(args);
                        mainActivity.addFragment(activitiesFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) activitiesFragment).getClass().getName());
                        break;
                    case 2:
                        args.putString(getString(R.string.bundle_title),drawerListArray[5]);
                        args.putInt(getString(R.string.bundle_position),5);
                        quotationsFragment.setArguments(args);
                        mainActivity.addFragment(quotationsFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) quotationsFragment).getClass().getName());
                        break;
                    case 3:
                        args.putString(getString(R.string.bundle_title),drawerListArray[6]);
                        args.putInt(getString(R.string.bundle_position),6);
                        contactsFragment.setArguments(args);
                        mainActivity.addFragment(contactsFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) contactsFragment).getClass().getName());
                        break;
                    case 4:
                        args.putString(getString(R.string.bundle_title),drawerListArray[7]);
                        args.putInt(getString(R.string.bundle_position),7);
                        organisationsFragment.setArguments(args);
                        mainActivity.addFragment(organisationsFragment, true, FragmentTransaction.TRANSIT_NONE, R.id.content_frame, mActivity.getSupportFragmentManager(), ((Object) organisationsFragment).getClass().getName());
                        break;
                }
                break;
        }

    }
}
