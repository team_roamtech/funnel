package com.roamtech.android.sams.adapters;

import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.interfaces.LaunchFragment;

/**
 * Created by Karuri on 2/22/2015.
 */
public class QuotationsAdapter extends CursorAdapter {
    public static final String TAG = "QuotationsAdapter";
    private LayoutInflater vi;
    Cursor c;
    Context ctx;
    Fragment fragment;
    LaunchFragment mCallBack;

    public QuotationsAdapter(Context context, Cursor c, int flags,Fragment fragment) {
        super(context, c, flags);
        vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.ctx = context;
        this.c = c;
        this.fragment = fragment;
        Log.d(TAG, "QuotationsAdapter constructor");
        try {
            mCallBack = (LaunchFragment) fragment;
        } catch (ClassCastException e) {
            throw new ClassCastException(fragment.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View rootView = vi.inflate(R.layout.list_quotations, parent, false);
        ViewHolder holder  =   new ViewHolder();
        holder.lytQuotation = (LinearLayout) rootView.findViewById(R.id.lyt_quotation);
        holder.txtQuotationName = (TextView) rootView.findViewById(R.id.txt_quotation_name);
        holder.txtContactName = (TextView) rootView.findViewById(R.id.txt_contact_name);
        holder.txtQuotationDate = (TextView) rootView.findViewById(R.id.txt_cost);
        holder.txtOrgName = (TextView) rootView.findViewById(R.id.txt_org_name);
        holder.mInnerElements = (LinearLayout) rootView.findViewById(R.id.lyt_inner_elements);
        holder.imgDown = (ImageView) rootView.findViewById(R.id.img_down);
        holder.animSideDown = AnimationUtils.loadAnimation(context,
                R.anim.slide_down);
        // set animation listener
        holder.animSlideUp = AnimationUtils.loadAnimation(context,
                R.anim.slide_up);


        rootView.setTag(holder);
        return rootView;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final Cursor c = cursor;
        final ViewHolder holder  =   (ViewHolder)    view.getTag();
        holder.txtQuotationName.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_qu_name))));
        holder.txtContactName.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_con_name))));
        if(!c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_org_name))).equals(ctx.getResources().getString(R.string.db_table_org_entry))) {
            holder.txtOrgName.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_org_name))));
        }
        holder.txtQuotationDate.setText(c.getString(c.getColumnIndex(context.getResources().getString(R.string.db_table_qu_created))));
        holder.cursorPosition = c.getPosition();

        holder.lytQuotation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                c.moveToPosition(holder.cursorPosition);
                Bundle args = new Bundle();
                args.putInt(ctx.getResources().getString(R.string.db_table_id), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_id))));
                args.putInt(ctx.getResources().getString(R.string.db_table_sales_id), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sales_id))));
                args.putInt(ctx.getResources().getString(R.string.db_table_sale_slug_id), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_slug_id))));
                args.putString(ctx.getResources().getString(R.string.db_table_sale_name), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sale_name))));
                args.putString(ctx.getResources().getString(R.string.db_table_qu_name), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_name))));
               // Log.d(TAG, c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_name))));
                args.putString(ctx.getResources().getString(R.string.db_table_qu_slug_id), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_slug_id))));
                args.putString(ctx.getResources().getString(R.string.db_table_qu_created),c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_created))));
                args.putString(ctx.getResources().getString(R.string.db_table_qu_sale_id), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_sale_id))));
                args.putInt(ctx.getResources().getString(R.string.db_table_qu_status), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_status))));
                args.putInt(ctx.getResources().getString(R.string.db_table_sync_done), c.getInt(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_sync_done))));
                args.putString(ctx.getResources().getString(R.string.db_table_qu_stage), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_stage))));
                args.putString(ctx.getResources().getString(R.string.db_table_qu_payment_type), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_payment_type))));
                args.putString(ctx.getResources().getString(R.string.db_table_qu_delivered), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_delivered))));
                args.putString(ctx.getResources().getString(R.string.db_table_qu_lpo), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_lpo))));
                args.putString(ctx.getResources().getString(R.string.db_table_qu_amount_collected), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_qu_amount_collected))));
                args.putString(ctx.getResources().getString(R.string.db_table_con_name), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_con_name))));
                args.putString(ctx.getResources().getString(R.string.db_table_con_slug_id), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_con_slug_id))));
                args.putString(ctx.getResources().getString(R.string.db_table_con_email), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_con_email))));
                args.putString(ctx.getResources().getString(R.string.db_table_org_name), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_name))));
                args.putString(ctx.getResources().getString(R.string.db_table_org_slug_id), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_slug_id))));
                args.putString(ctx.getResources().getString(R.string.db_table_org_slug_id), c.getString(c.getColumnIndex(ctx.getResources().getString(R.string.db_table_org_slug_id))));

                mCallBack.launchFragment(holder.cursorPosition,args,0);
            }
        });
        holder.animSideDown.setAnimationListener(new Animation.AnimationListener() {

            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        holder.animSlideUp.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                Log.d(TAG, "animation end");
                if (animation == holder.animSlideUp) {
                    holder.mInnerElements.setVisibility(View.GONE);
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        holder.imgDown.setBackgroundResource(R.drawable.ic_action_down2);
        holder.imgDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.mInnerElements.getVisibility() == View.GONE) {
                    holder.mInnerElements.setVisibility(View.VISIBLE);
                    holder.mInnerElements.startAnimation(holder.animSideDown);
                    holder.imgDown.setBackgroundResource(R.drawable.ic_action_up2);
                } else {
                    holder.mInnerElements.setVisibility(View.GONE);
                    holder.mInnerElements.startAnimation(holder.animSlideUp);
                    holder.imgDown.setBackgroundResource(R.drawable.ic_action_down2);
                }
            }
        });

        /*if(!cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_organisation_name))).equals(context.getResources().getString(R.string.db_table_org_entry))) {
            holder.txtOrgName.setText(cursor.getString(cursor.getColumnIndex(context.getResources().getString(R.string.db_table_organisation_name))));
        }else{
            holder.txtOrgName.setVisibility(View.GONE);
        }*/
    }


    private class ViewHolder{
        public LinearLayout lytQuotation;
        public LinearLayout mInnerElements;
        TextView txtQuotationName;
        TextView txtContactName;
        TextView txtOrgName;
        public TextView txtQuotationDate;
        public ImageView imgDown;
        public Animation animSideDown,animSlideUp;
        public int cursorPosition;
    }
}
