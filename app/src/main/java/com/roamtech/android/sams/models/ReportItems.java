package com.roamtech.android.sams.models;

/**
 *
 * Class that stores report requirements together
 *
 * Created by dennis on 5/14/15.
 * @author Dennis Mwangi Karuri
 */
public class ReportItems {
    String quName;
    String conName;
    String items;

    /**
     *
     * Constructor
     *
     */
    public ReportItems(){
    }

    public String getQuName() {
        return quName;
    }

    public void setQuName(String quName) {
        this.quName = quName;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }
}
