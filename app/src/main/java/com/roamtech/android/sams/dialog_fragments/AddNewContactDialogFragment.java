package com.roamtech.android.sams.dialog_fragments;

import android.app.Activity;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FilterQueryProvider;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.ContactsAdapter;
import com.roamtech.android.sams.fragments.AddContactsFragment;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.interfaces.CheckProvider;
import com.roamtech.android.sams.interfaces.SendJsonData;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by dena on 1/28/15.
 */
public class AddNewContactDialogFragment extends DialogFragment implements View.OnClickListener,LoaderManager.LoaderCallbacks<Cursor> {
    AppCompatActivity mActivity;
    AutoCompleteTextView actName;
    EditText editAddress;
    LinearLayout lytAddNewContact;
    ContactsAdapter mAdapter;
    Cursor c;
    String strSequence = null;
    String contactName,organisationName;
    TextView txtAddNewContact,txtAddPhoneNum;
    long contact_id;
    String organisation_id;
    public static final int LOADER = 1000;
    public static final String TAG = "AddNewContactDialogFragment";
    AddContactsFragment addContactsFragment;
    String slugID;
    int syncDone;
    int status = 0;
    SendJsonData mCallBack3;
    CheckProvider mCallBack4;
    String model = "organisations";
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    GetUsername getUsername;



    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack3 = (SendJsonData) activity;
            mCallBack4 = (CheckProvider) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        commonHash = new CommonHash(mActivity);
        //mActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE|WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        addContactsFragment = new AddContactsFragment();
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        getUsername = new GetUsername(mActivity);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog =  new Dialog(mActivity);
        dialog.setContentView(R.layout.dialog_link_to);
        dialog.show();
        dialog.setTitle(mActivity.getResources().getString(R.string.dialog_title));
        dialog.setCanceledOnTouchOutside(false);
        actName = (AutoCompleteTextView) dialog.findViewById(R.id.edit_contact_name);
        actName.setThreshold(3);
        editAddress = (EditText) dialog.findViewById(R.id.edit_address);
        editAddress.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_CLASS_PHONE);
        txtAddPhoneNum = (TextView) dialog.findViewById(R.id.edit_address);
        txtAddPhoneNum.setHint(mActivity.getResources().getString(R.string.txt_phone_num));
        txtAddNewContact = (TextView) dialog.findViewById(R.id.txt_add_new_contact);
        txtAddNewContact.setText(mActivity.getResources().getString(R.string.txt_add_new_contact));
        lytAddNewContact = (LinearLayout) dialog.findViewById(R.id.lyt_add_contact);
        lytAddNewContact.setOnClickListener(this);
        lytAddNewContact.setVisibility(View.GONE);
        mAdapter = new ContactsAdapter(mActivity, null, 0);
        actName.setAdapter(mAdapter);
        mAdapter.setFilterQueryProvider(filter);

        actName.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                // TODO Auto-generated method stub
                //if(cameFromUser == true){
                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(actName.getWindowToken(), 0);
                if(c.moveToPosition(arg2)){
                    organisationName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_org_name)));
                    contactName = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_con_name)));
                    contact_id = c.getLong(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_id)));
                    organisation_id = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_org_slug_id)));
                    slugID = c.getString(c.getColumnIndex(mActivity.getResources().getString(R.string.db_table_con_slug_id)));
                 //   Log.d(TAG,"''''''''''''''''''''''''''''' slug id "+ contact_id);
                    Intent intent = new Intent();
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_id),contact_id);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_con_name),contactName);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id),organisation_id);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_name),organisationName);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_con_slug_id),slugID);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    Log.d(TAG,String.valueOf(getTargetRequestCode()));
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    dismiss();
                }
            }

        });
        return dialog;
    }

    @Override
    public void onStart(){
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onResume(){
        super.onResume();
    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    FilterQueryProvider filter = new FilterQueryProvider() {
        public Cursor runQuery(CharSequence str) {
            if(str != null && str.length() > 2){
                strSequence = str.toString();
                mActivity.getSupportLoaderManager().restartLoader(LOADER,null,AddNewContactDialogFragment.this);
                runAddView();
            }
            return c;
        }
    };

  /*  public void runStringLoader3(){
        getLoaderManager().restartLoader(LOADER, null, this);
    }*/
    public void runAddView(){
        lytAddNewContact.setVisibility(View.VISIBLE);
    }



    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.lyt_add_contact:
                if(actName.length() < 1) {
                    Toast.makeText(mActivity, mActivity.getResources().getString(R.string.toast_no_name), Toast.LENGTH_LONG).show();
                }else if(editAddress.length() < 1) {
                    Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_valid_phone), Toast.LENGTH_LONG).show();
                }else if(editAddress.getText().length() > 0 && addContactsFragment.isValidPhoneNum(editAddress.getText()) == false){
                    Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_valid_phone), Toast.LENGTH_LONG).show();
                }else{
                    contactName = actName.getText().toString();
                    if(slugID == null) {
                        contact_id = (int)insertContacts();
                        try {
                            slugID = commonHash.getHash(String.valueOf(contact_id));
                           // Log.d(TAG, "Common Hash is  " + slugID);
                        } catch (NoSuchAlgorithmException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                        insertContacts();
                    }else{
                        insertContacts();
                    }
                    /*String jsonData = jsonData("insert");
                    Log.d(TAG,jsonData);
                    mCallBack3.jsonData(jsonData);
                    mCallBack4.transactionID(FunnelProvider.UPDATE_CONTACTS);
                    updateContact((int)contact_id);*/
                    Intent intent = new Intent();
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_id),contact_id);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_con_name),contactName);
                    intent.putExtra(mActivity.getResources().getString(R.string.db_table_con_slug_id),slugID);
                    mActivity.setResult(Activity.RESULT_OK, intent);
                   // Log.d(TAG,String.valueOf(getTargetRequestCode()));
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    dismiss();
                }
                break;
        }
    }

    public int updateContact(int contactID){
        ContentValues initialValues = new ContentValues();
        Log.d(TAG, "Common Hash is  " + slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_slug_id), slugID);
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "11");
        contactID = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(contactID),mActivity.getResources().getString(R.string.flag_sync)});
        Log.d(TAG,"updated orgID == "+String.valueOf(contactID));

        return contactID;
    }

    /*public long insertContacts(){
        syncDone = 1;
        coordinates = getCoordinates.getCoordinates();
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"0");
        ContentValues initialValues = new ContentValues();
        initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_name), actName.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_phone), editAddress.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_email), "");
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_website), "");
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_source), "");
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_address), "");
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_foreign_key), String.valueOf(0));
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_model), model);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_status), status);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_slug_id), slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_latitude), coordinates[0]);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_longitude), coordinates[1]);
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());

        Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
        Long playListId = ContentUris.parseId(insertUri);
        Log.d(TAG,"-------"+ String.valueOf(playListId));
        return playListId;
    } */

    public long insertContacts(){
        int playListId = 0;
        coordinates = getCoordinates.getCoordinates();
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_name), actName.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_phone), editAddress.getText().toString());
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_email), "");
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_website), "");
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_source), "");
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_address), "");
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_foreign_key), String.valueOf(0));
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_model), model);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_status), status);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_con_slug_id), slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_latitude), 0.0);
        }
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_con_longitude), 0.0);
        }
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());
        if(contact_id == 0) {
            initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "0");
            Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
            playListId = (int)ContentUris.parseId(insertUri);
        }else{
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "11");
            playListId = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(contact_id),mActivity.getResources().getString(R.string.flag_sync)});
        }
        Log.d(TAG,"-------"+ String.valueOf(playListId));
        return playListId;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI,"6");
        return new CursorLoader(mActivity,data, null, null,
                new String[] {strSequence}, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        c = data;
        Log.d(TAG, String.valueOf(c.getCount()));
        mAdapter.swapCursor(c);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    public String jsonData(String transaction){
        JSONObject con = new JSONObject();
        Log.d(TAG,transaction);
        try {
            con.put(mActivity.getResources().getString(R.string.user_id), "dennis.panda");
            con.put(mActivity.getResources().getString(R.string.db_table_con_name), actName.getText().toString());
            con.put(mActivity.getResources().getString(R.string.db_table_con_phone), editAddress.getText().toString());
            con.put(mActivity.getResources().getString(R.string.db_table_con_email), "");
            con.put(mActivity.getResources().getString(R.string.db_table_con_website), "");
            con.put(mActivity.getResources().getString(R.string.db_table_con_source), "");
            con.put(mActivity.getResources().getString(R.string.db_table_con_address), "");
            con.put(mActivity.getResources().getString(R.string.db_table_con_foreign_key), 0);
            con.put(mActivity.getResources().getString(R.string.db_table_con_status), status);
            con.put(mActivity.getResources().getString(R.string.db_table_con_model), model);
            con.put(mActivity.getResources().getString(R.string.db_table_con_slug_id), slugID);
            con.put(mActivity.getResources().getString(R.string.transaction_con),transaction);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(con);
        return jsonArray.toString();
    }
}
