package com.roamtech.android.sams.providers;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.roamtech.android.sams.R;
import com.roamtech.android.sams.databases.FunnelDB;
import com.roamtech.android.sams.interfaces.CheckProvider;

/**
 *
 * Seats on top of the DB and manages access to the SQLite DB making data readily available
 *
 * Created by dennis on 1/20/15.
 * @author Dennis Mwangi Karuri
 * @see ContentProvider
 */
public class FunnelProvider extends ContentProvider {
    public static final String TAG = "FunnelProvider";
    public static String AUTHORITY = "com.roamtech.android.sams.providers"+".FunnelProvider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);
    private FunnelDB mDbController;
    public static final String WORDS_MIME_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE +
            "/vnd.com.roamtech.android.funnel";
    CheckProvider mCallBack;

    public static final int INSERT_CONTACTS = 0;
    public static final int INSERT_ORGANISATION = 1;
    public static final int INSERT_SALE = 2;
    public static final int INSERT_ACTIVITIES = 3;
    public static final int INSERT_SALE_ACTIVITIES= 4;
    public static final int INSERT_QUOTATIONS = 21;
    public static final int INSERT_QUOTATIONS_DETAILS = 22;
    public static final int INSERT_COMMENTS = 54;
    public static final int GET_SALES_STAGE = 5;
    public static final int GET_SALE = 48;
    public static final int GET_SALES = 51;
    public static final int GET_CONVERTED_SALES = 53;
    public static final int GET_CONTACTS_LIKE= 6;
    public static final int GET_ORGANISATION_LIKE = 7;
    public static final int GET_STAGE = 8;
    public static final int GET_CONTACTS = 14;
    public static final int GET_CONTACT = 46;
    public static final int GET_ORGANISATIONS = 15;
    public static final int GET_ORGANISATION = 45;
    public static final int GET_ACTIVITIES = 16;
    public static final int GET_ALL_ACTIVITIES = 17;
    public static final int GET_SALES_LIKE = 18;
    public static final int GET_SALES_WON_LIKE = 19;
    public static final int GET_QUOTATIONS = 20;
    public static final int GET_QUOTATIONS_STAGE = 49;
    public static final int GET_QUOTATIONS_DETAILS = 23;
    public static final int GET_SALES_COUNT = 24;
    public static final int GET_STAGE_COUNT = 44;
    public static final int GET_SALE_ACTIVITIES = 43;
    public static final int GET_ACTIVITIES_COUNT = 25;
    public static final int GET_CONTACTS_COUNT = 26;
    public static final int GET_LEADS_COUNT = 52;
    public static final int GET_ORGANISATIONS_COUNT = 27;
    public static final int GET_QUOTATIONS_COUNT = 28;
    public static final int GET_QUOTATIONS_DETAILS_SUM = 29;
    public static final int GET_QUOTATION = 32;
    public static final int GET_QUOTATION_ACTIVITIES = 47;
    public static final int GET_ALL_SALES_ACTIVITIES = 33;
    public static final int GET_QUOTATION_DETAILS_TARGETS = 34;
    public static final int GET_QUOTATIONS_LIKE = 41;
    public static final int GET_NOT_SYNC_DATA = 36;
    public static final int GET_ORG_NAME = 38;
    public static final int GET_CON_NAME = 39;
    public static final int GET_SALE_NAME = 40;
    public static final int GET_QUOTATION_NAME = 42;
    public static final int UPDATE_STAGE = 9;
    public static final int UPDATE_STATUS = 10;
    public static final int UPDATE_CONTACTS = 11;
    public static final int UPDATE_ORGANISATION = 12;
    public static final int UPDATE_ACTIVITIES = 13;
    public static final int UPDATE_QUOTATIONS = 30;
    public static final int UPDATE_QUOTATIONS_DETAILS = 31;
    public static final int UPDATE_SLUG_ID = 35;
    public static final int UPDATE_COMMENTS = 55;
    public static final int UPDATE_TABLES = 37;
    public static final int GET_PENDING_LEADS = 50;

    private static final UriMatcher sURIMatcher = buildUriMatcher();

    /**
     *
     * @return All the URI's added to a matcher. To be used to determine the URI that needs to be executed
     */
    private static UriMatcher buildUriMatcher() {
        UriMatcher matcher =  new UriMatcher(UriMatcher.NO_MATCH);
        matcher.addURI(AUTHORITY, "0", INSERT_CONTACTS);
        matcher.addURI(AUTHORITY,"1",INSERT_ORGANISATION);
        matcher.addURI(AUTHORITY,"2",INSERT_SALE);
        matcher.addURI(AUTHORITY,"3",INSERT_ACTIVITIES);
        matcher.addURI(AUTHORITY,"4",INSERT_SALE_ACTIVITIES);
        matcher.addURI(AUTHORITY,"21",INSERT_QUOTATIONS);
        matcher.addURI(AUTHORITY,"22",INSERT_QUOTATIONS_DETAILS);
        matcher.addURI(AUTHORITY,"54",INSERT_COMMENTS);
        matcher.addURI(AUTHORITY,"5",GET_SALES_STAGE);
        matcher.addURI(AUTHORITY,"48",GET_SALE);
        matcher.addURI(AUTHORITY,"51",GET_SALES);
        matcher.addURI(AUTHORITY,"53",GET_CONVERTED_SALES);
        matcher.addURI(AUTHORITY,"6",GET_CONTACTS_LIKE);
        matcher.addURI(AUTHORITY,"7",GET_ORGANISATION_LIKE);
        matcher.addURI(AUTHORITY,"14",GET_CONTACTS);
        matcher.addURI(AUTHORITY,"46",GET_CONTACT);
        matcher.addURI(AUTHORITY,"50",GET_PENDING_LEADS);
        matcher.addURI(AUTHORITY,"15",GET_ORGANISATIONS);
        matcher.addURI(AUTHORITY,"45",GET_ORGANISATION);
        matcher.addURI(AUTHORITY,"16",GET_ACTIVITIES);
        matcher.addURI(AUTHORITY,"17",GET_ALL_ACTIVITIES);
        matcher.addURI(AUTHORITY,"18",GET_SALES_LIKE);
        matcher.addURI(AUTHORITY,"19",GET_SALES_WON_LIKE);
        matcher.addURI(AUTHORITY,"20",GET_QUOTATIONS);
        matcher.addURI(AUTHORITY,"49",GET_QUOTATIONS_STAGE);
        matcher.addURI(AUTHORITY,"23",GET_QUOTATIONS_DETAILS);
        matcher.addURI(AUTHORITY,"24",GET_SALES_COUNT);
        matcher.addURI(AUTHORITY,"44",GET_STAGE_COUNT);
        matcher.addURI(AUTHORITY,"43",GET_SALE_ACTIVITIES);
        matcher.addURI(AUTHORITY,"25",GET_ACTIVITIES_COUNT);
        matcher.addURI(AUTHORITY,"26",GET_CONTACTS_COUNT);
        matcher.addURI(AUTHORITY,"52",GET_LEADS_COUNT);
        matcher.addURI(AUTHORITY,"27",GET_ORGANISATIONS_COUNT);
        matcher.addURI(AUTHORITY,"28",GET_QUOTATIONS_COUNT);
        matcher.addURI(AUTHORITY,"29",GET_QUOTATIONS_DETAILS_SUM);
        matcher.addURI(AUTHORITY,"32",GET_QUOTATION);
        matcher.addURI(AUTHORITY,"47",GET_QUOTATION_ACTIVITIES);
        matcher.addURI(AUTHORITY,"33",GET_ALL_SALES_ACTIVITIES);
        matcher.addURI(AUTHORITY,"34",GET_QUOTATION_DETAILS_TARGETS);
        matcher.addURI(AUTHORITY,"41",GET_QUOTATIONS_LIKE);
        matcher.addURI(AUTHORITY,"36",GET_NOT_SYNC_DATA);
        matcher.addURI(AUTHORITY,"38",GET_ORG_NAME);
        matcher.addURI(AUTHORITY,"39",GET_CON_NAME);
        matcher.addURI(AUTHORITY,"40",GET_SALE_NAME);
        matcher.addURI(AUTHORITY,"42",GET_QUOTATION_NAME);
        matcher.addURI(AUTHORITY,"8", GET_STAGE);
        matcher.addURI(AUTHORITY,"9", UPDATE_STAGE);
        matcher.addURI(AUTHORITY,"10",UPDATE_STATUS);
        matcher.addURI(AUTHORITY,"11",UPDATE_CONTACTS);
        matcher.addURI(AUTHORITY,"12",UPDATE_ORGANISATION);
        matcher.addURI(AUTHORITY,"13",UPDATE_ACTIVITIES);
        matcher.addURI(AUTHORITY,"30",UPDATE_QUOTATIONS);
        matcher.addURI(AUTHORITY,"31",UPDATE_QUOTATIONS_DETAILS);
        matcher.addURI(AUTHORITY,"35",UPDATE_SLUG_ID);
        matcher.addURI(AUTHORITY,"55",UPDATE_COMMENTS);
        matcher.addURI(AUTHORITY,"37",UPDATE_TABLES);

        return matcher;
    }


    @Override
    public boolean onCreate() {
        mDbController = new FunnelDB(getContext());
        return false;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor c = null;
        // Matches the URI sent and executes the URI that matches  by running a query in the DB
        switch (sURIMatcher.match(uri)) {
            case GET_SALES_STAGE:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    Log.d(TAG, String.valueOf(selectionArgs[0]));
                    c = mDbController.getSales(selectionArgs[0],Integer.parseInt(selectionArgs[1]));
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_SALE:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    Log.d(TAG, String.valueOf(selectionArgs[0]));
                    c = mDbController.getSale(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_SALES:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getSales(Integer.parseInt(selectionArgs[0]));
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_CONVERTED_SALES:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getConvertedSales(Integer.parseInt(selectionArgs[0]));
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_CONTACTS_LIKE:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getContactsLike(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_ORGANISATION_LIKE:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getOrganisationLike(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_STAGE:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getStageID(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_CONTACTS:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getContacts();
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_CONTACT:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getContact(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_PENDING_LEADS:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getPendingLeads();
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_ORGANISATIONS:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getOrganisations();
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_ORGANISATION:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getOrganisation(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_ACTIVITIES:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getActivities();
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_ALL_ACTIVITIES:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getAllActivities();
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_SALES_LIKE:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getSalesLike(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_SALES_WON_LIKE:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getSalesWonLike(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_SALE_ACTIVITIES:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getSaleActivities(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_QUOTATIONS:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getQuotations();
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_QUOTATIONS_STAGE:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getQuotationsStage(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_QUOTATIONS_DETAILS:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getQuotationsDetails(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_QUOTATIONS_LIKE:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getQuotationsLike(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_QUOTATION_ACTIVITIES:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getQuotationsActivities(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_SALES_COUNT:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getSalesCount(Integer.parseInt(selectionArgs[0]));
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_ACTIVITIES_COUNT:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getActivitiesCount(Integer.parseInt(selectionArgs[0]));
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_QUOTATIONS_COUNT:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getQuotationCount(Integer.parseInt(selectionArgs[0]));
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_CONTACTS_COUNT:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getContactsCount();
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_LEADS_COUNT:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getLeadsCount();
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_ORGANISATIONS_COUNT:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getOrganisationCount();
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_STAGE_COUNT:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getStagesCount(selectionArgs[0],selectionArgs[1]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_QUOTATIONS_DETAILS_SUM:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getQuotationDetailsSum(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_QUOTATION:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getQuotation(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();

                }
                return c;
            case GET_ALL_SALES_ACTIVITIES:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getAllSalesActivities();
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return c;
            case GET_QUOTATION_DETAILS_TARGETS:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getQuotationDetailsTargets();
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return c;
            case GET_NOT_SYNC_DATA:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getNotSyncData(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return c;
            case GET_ORG_NAME:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getOrganisationName(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return c;
            case GET_CON_NAME:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getContactName(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return c;
            case GET_SALE_NAME:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getSaleName(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return c;
            case GET_QUOTATION_NAME:
                if (uri == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try{
                    c = mDbController.getQuotationName(selectionArgs[0]);
                    c.setNotificationUri(getContext().getContentResolver(), uri);
                }catch(Exception e){
                    e.printStackTrace();
                }
                return c;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
    }

    @Override
    public String getType(Uri uri) {
        // Matches the URI sent and returns the Mime type
        switch (sURIMatcher.match(uri)) {
            case INSERT_CONTACTS:
                return WORDS_MIME_TYPE;
            case INSERT_ORGANISATION:
                return WORDS_MIME_TYPE;
            case INSERT_SALE:
                return WORDS_MIME_TYPE;
            case INSERT_ACTIVITIES:
                return WORDS_MIME_TYPE;
            case INSERT_SALE_ACTIVITIES:
                return WORDS_MIME_TYPE;
            case INSERT_QUOTATIONS:
                return WORDS_MIME_TYPE;
            case INSERT_QUOTATIONS_DETAILS:
                return WORDS_MIME_TYPE;
            case INSERT_COMMENTS:
                return WORDS_MIME_TYPE;
            case GET_SALES_STAGE:
                return WORDS_MIME_TYPE;
            case GET_SALES:
                return WORDS_MIME_TYPE;
            case GET_CONVERTED_SALES:
                return WORDS_MIME_TYPE;
            case GET_CONTACTS_LIKE:
                return WORDS_MIME_TYPE;
            case GET_ORGANISATION_LIKE:
                return WORDS_MIME_TYPE;
            case GET_CONTACTS:
                return WORDS_MIME_TYPE;
            case GET_ORGANISATIONS:
                return WORDS_MIME_TYPE;
            case GET_ACTIVITIES:
                return WORDS_MIME_TYPE;
            case GET_ALL_ACTIVITIES:
                return WORDS_MIME_TYPE;
            case GET_SALES_LIKE:
                return WORDS_MIME_TYPE;
            case GET_SALES_WON_LIKE:
                return WORDS_MIME_TYPE;
            case GET_SALES_COUNT:
                return WORDS_MIME_TYPE;
            case GET_STAGE_COUNT:
                return WORDS_MIME_TYPE;
            case GET_SALE_ACTIVITIES:
                return WORDS_MIME_TYPE;
            case GET_ACTIVITIES_COUNT:
                return WORDS_MIME_TYPE;
            case GET_QUOTATIONS_COUNT:
                return WORDS_MIME_TYPE;
            case GET_CONTACTS_COUNT:
                return WORDS_MIME_TYPE;
            case GET_LEADS_COUNT:
                return WORDS_MIME_TYPE;
            case GET_ORGANISATIONS_COUNT:
                return WORDS_MIME_TYPE;
            case GET_QUOTATION:
                return WORDS_MIME_TYPE;
            case GET_ALL_SALES_ACTIVITIES:
                return WORDS_MIME_TYPE;
            case GET_QUOTATION_DETAILS_TARGETS:
                return WORDS_MIME_TYPE;
            case GET_QUOTATION_NAME:
                return WORDS_MIME_TYPE;
            case GET_NOT_SYNC_DATA:
                return WORDS_MIME_TYPE;
            case GET_ORG_NAME:
                return WORDS_MIME_TYPE;
            case GET_CON_NAME:
                return WORDS_MIME_TYPE;
            case GET_SALE_NAME:
                return WORDS_MIME_TYPE;
            case GET_PENDING_LEADS:
                return WORDS_MIME_TYPE;
            case UPDATE_STAGE:
                return WORDS_MIME_TYPE;
            case UPDATE_STATUS:
                return WORDS_MIME_TYPE;
            case UPDATE_CONTACTS:
                return WORDS_MIME_TYPE;
            case UPDATE_ORGANISATION:
                return WORDS_MIME_TYPE;
            case UPDATE_ACTIVITIES:
                return WORDS_MIME_TYPE;
            case UPDATE_QUOTATIONS:
                return WORDS_MIME_TYPE;
            case UPDATE_QUOTATIONS_DETAILS:
                return WORDS_MIME_TYPE;
            case UPDATE_SLUG_ID:
                return WORDS_MIME_TYPE;
            case UPDATE_COMMENTS:
                return WORDS_MIME_TYPE;
            default:
                throw new IllegalArgumentException("Unknown URL " + uri);
        }
    }



    @Override
    public Uri insert(Uri uri, ContentValues values) {
        // Matches the URI sent and executes the URI that matches  by doing an insert to the DB
        switch (sURIMatcher.match(uri)) {
            case INSERT_CONTACTS:
                if (values == null) {
                    throw new IllegalArgumentException(
                            "values must be provided for the Uri: " + uri);
                }
                try{
                    long id = mDbController.insertContacts(values);

                    if (id>0) {
                      //  getContext().getContentResolver().notifyChange(uri, null);
                        return ContentUris.withAppendedId(uri, id);
                    }

                }catch(Exception e){
                    e.printStackTrace();
                }
                return uri;
            case INSERT_ORGANISATION:
                if (values == null) {
                    throw new IllegalArgumentException(
                            "values must be provided for the Uri: " + uri);
                }
                try{
                    long id = mDbController.insertOrganisation(values);
                    if (id>0) {
                        return ContentUris.withAppendedId(uri, id);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return uri;
            case INSERT_SALE:
                if (values == null) {
                    throw new IllegalArgumentException(
                            "values must be provided for the Uri: " + uri);
                }
                try{
                    long id = mDbController.insertSale(values);
                    if (id>0) {
                        return ContentUris.withAppendedId(uri, id);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return uri;
            case INSERT_ACTIVITIES:
                if (values == null) {
                    throw new IllegalArgumentException(
                            "values must be provided for the Uri: " + uri);
                }
                try{
                    long id = mDbController.insertActivities(values);
                    if (id>0) {
                        return ContentUris.withAppendedId(uri, id);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return uri;
            case INSERT_SALE_ACTIVITIES:
                if (values == null) {
                    throw new IllegalArgumentException(
                            "values must be provided for the Uri: " + uri);
                }
                try{
                    long id = mDbController.insertSaleActivities(values);
                    if (id>0) {
                        return ContentUris.withAppendedId(uri, id);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return uri;
            case INSERT_QUOTATIONS:
                if (values == null) {
                    throw new IllegalArgumentException(
                            "values must be provided for the Uri: " + uri);
                }
                try{
                    long id = mDbController.insertQuotations(values);
                    if (id>0) {
                        return ContentUris.withAppendedId(uri, id);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return uri;
            case INSERT_QUOTATIONS_DETAILS:
                if (values == null) {
                    throw new IllegalArgumentException(
                            "values must be provided for the Uri: " + uri);
                }
                try{
                    long id = mDbController.insertQuotationsDetails(values);
                    if (id>0) {
                        return ContentUris.withAppendedId(uri, id);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return uri;
            case INSERT_COMMENTS:
                if (values == null) {
                    throw new IllegalArgumentException(
                            "values must be provided for the Uri: " + uri);
                }
                try{
                    long id = mDbController.insertComments(values);
                    if (id>0) {
                        return ContentUris.withAppendedId(uri, id);
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                return uri;
        }
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        // Matches the URI sent and executes the URI that matches  by doing an update to the DB
        int id = 0;
        switch (sURIMatcher.match(uri)) {
            case UPDATE_STAGE:
                if (selectionArgs == null || values == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try {
                    id = mDbController.updateSaleStage(values, Integer.parseInt(selectionArgs[0]));
                    if (id>0) {
                        if(selectionArgs[1].equals(getContext().getString(R.string.flag_sync))) {
                           // getContext().getContentResolver().notifyChange(uri, null);
                        }
                    }
                    System.out.println(selectionArgs[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case UPDATE_STATUS:
                if (selectionArgs == null || values == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try {
                    id = mDbController.updateSaleStatus(values, Integer.parseInt(selectionArgs[0]));
                    System.out.println(selectionArgs[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case UPDATE_CONTACTS:
                if (selectionArgs == null || values == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try {
                    id = mDbController.updateContacts(values, Integer.parseInt(selectionArgs[0]));
                    if (id>0) {
                        if(selectionArgs[1].equals(getContext().getString(R.string.flag_sync))) {
                           // getContext().getContentResolver().notifyChange(uri, null);
                        }
                    }
                    System.out.println(selectionArgs[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case UPDATE_ORGANISATION:
                if (selectionArgs == null || values == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try {
                    id = mDbController.updateOrganisations(values, Integer.parseInt(selectionArgs[0]));
                    if (id>0) {
                        if(selectionArgs[1].equals(getContext().getString(R.string.flag_sync))) {
                           // getContext().getContentResolver().notifyChange(uri, null);
                        }
                    }
                    System.out.println(selectionArgs[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case UPDATE_ACTIVITIES:
                if (selectionArgs == null || values == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try {
                    id = mDbController.updateActivities(values, Integer.parseInt(selectionArgs[0]));
                    if (id>0) {
                        if(selectionArgs[1].equals(getContext().getString(R.string.flag_sync))) {
                           // getContext().getContentResolver().notifyChange(uri, null);
                        }
                    }
                    System.out.println(selectionArgs[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case UPDATE_QUOTATIONS:
                if (selectionArgs == null || values == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try {
                    id = mDbController.updateQuotations(values, Integer.parseInt(selectionArgs[0]));
                    if (id>0) {
                        if(selectionArgs[1].equals(getContext().getString(R.string.flag_sync))) {
                           // getContext().getContentResolver().notifyChange(uri, null);
                        }
                    }
                    System.out.println(selectionArgs[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case UPDATE_SLUG_ID:
                if (selectionArgs == null || values == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try {
                    id = mDbController.updateSlugID(values, Integer.parseInt(selectionArgs[0]), selectionArgs[1]);
                    System.out.println(selectionArgs[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case UPDATE_QUOTATIONS_DETAILS:
                if (selectionArgs == null || values == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try {
                    id = mDbController.updateQuotationsDetails(values, Integer.parseInt(selectionArgs[0]));
                    if (id>0) {
                        if(selectionArgs[1].equals(getContext().getString(R.string.flag_sync))) {
                           // getContext().getContentResolver().notifyChange(uri, null);
                        }
                    }
                    System.out.println(selectionArgs[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case UPDATE_COMMENTS:
                if (selectionArgs == null || values == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try {
                    id = mDbController.updateComments(values, Integer.parseInt(selectionArgs[0]));
                    if (id>0) {
                        if(selectionArgs[1].equals(getContext().getString(R.string.flag_sync))) {
                            // getContext().getContentResolver().notifyChange(uri, null);
                        }
                    }
                    System.out.println(selectionArgs[0]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                break;
            case UPDATE_TABLES:
                if (selectionArgs == null || values == null) {
                    throw new IllegalArgumentException(
                            "selectionArgs must be provided for the Uri: " + uri);
                }
                try {
                    id = (int)mDbController.updateTables(values,selectionArgs[0]);
                    System.out.println(selectionArgs[0]);
                    return id;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);
        }
        return id;
    }
}
