package com.roamtech.android.sams.fragments;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.roamtech.android.sams.MainActivity;
import com.roamtech.android.sams.R;
import com.roamtech.android.sams.adapters.SpinnerActivitiesAdapter;
import com.roamtech.android.sams.dialog_fragments.AddDateDialogFragment;
import com.roamtech.android.sams.dialog_fragments.AddNewContactDialogFragment;
import com.roamtech.android.sams.dialog_fragments.AddNewOrganisationDialogFragment;
import com.roamtech.android.sams.dialog_fragments.AddQuotationDialogFragment;
import com.roamtech.android.sams.dialog_fragments.AddSaleDialogFragment;
import com.roamtech.android.sams.dialog_fragments.DeleteDialog;
import com.roamtech.android.sams.generators.CommonHash;
import com.roamtech.android.sams.interfaces.CheckProvider;
import com.roamtech.android.sams.interfaces.OnAddSelectedListener;
import com.roamtech.android.sams.interfaces.SendJsonData;
import com.roamtech.android.sams.models.MainModel;
import com.roamtech.android.sams.providers.FunnelProvider;
import com.roamtech.android.sams.receivers.AlarmReceiver;
import com.roamtech.android.sams.utils.GetCoordinates;
import com.roamtech.android.sams.utils.GetUsername;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddActivitiesFragment extends Fragment implements View.OnClickListener,
        Animation.AnimationListener {
    public static final String TAG = "AddActivitiesFragment";
    Spinner spnActivities;
    TextView txtTime,txtPerson,txtOrganisation,txtSale,txtQuotation;
    EditText editNotes,editFeedBack;
    Button btnAddActivity,btnEditActivity;
    MainActivity mainActivity;
    AddNewContactDialogFragment newFragment;
    AddNewOrganisationDialogFragment newFragment2;
    AddSaleDialogFragment newFragment3;
    AddDateDialogFragment newFragment4;
    AddQuotationDialogFragment newFragment8;
    LinearLayout lytChoosePerson,lytChooseOrganisation,lytChooseSale,lytChooseDate,lytChooseQuotation;
    String contact_name,organisation_name,sale_name,quotation_name;
    String activity;
    String organisationID;
    String contactID;
    String saleID;
    String quotationID;
    String foreignKey;
    int activityID;
    AppCompatActivity mActivity;
    Cursor c;
    SpinnerActivitiesAdapter spinnerActivitiesAdapter;
    String date;
    String strDescription,strFeedback;
    DeleteDialog newFragment6;
    String [] arrayMainItems;
    int activityDone = 1;
    TextView txtFeedBack;
    Animation animSideDown,animSlideUp;
    int position;
    String slugID;
    int syncDone = 1;
    String model;
    String type;
    TypedArray spinnerItemsImgArray;
    String [] spinnerListArray;
    SendJsonData mCallBack3;
    CheckProvider mCallBack4;
    Cursor cu;
    CommonHash commonHash;
    GetCoordinates getCoordinates;
    GoogleApiClient mGoogleApiClient;
    Double [] coordinates;
    AlarmReceiver alarm = new AlarmReceiver();
    ArrayList<Integer> dateID = new ArrayList<Integer>();
    GetUsername getUsername;
    ImageButton imgClose;
    int salesID,sales_sync_done,sales_status,sales_fragmentPosition;
    Double sales_cost;
    String saleName,sales_stageID,sales_contact_name,sales_organisation_name,sales_paymentType,sales_strDescription,sales_slugID,sales_foreignKey,sales_model;
    String mainLink;

    public AddActivitiesFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context activity){
        super.onAttach(activity);
        mActivity = (AppCompatActivity)activity;
        try {
            mCallBack3 = (SendJsonData) activity;
            mCallBack4 = (CheckProvider) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement AddActivitiesFragment");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        commonHash = new CommonHash(mActivity);
        mainActivity = new MainActivity();
        newFragment = new AddNewContactDialogFragment();
        newFragment2 = new AddNewOrganisationDialogFragment();
        newFragment3 = new AddSaleDialogFragment();
        newFragment4 = new AddDateDialogFragment();
        newFragment6 = new DeleteDialog();
        newFragment8 = new AddQuotationDialogFragment();
        arrayMainItems = getResources().getStringArray(R.array.main_activity_items);
        spinnerListArray = getResources().getStringArray(R.array.activity_items);
        spinnerItemsImgArray = getResources().obtainTypedArray(R.array.spinner_imgs);

        Bundle args = getArguments();
        if(args != null){
            activityID = args.getInt(mActivity.getResources().getString(R.string.db_table_id));
            Log.d(TAG," activity ID  = "+ activityID);
            slugID = args.getString(mActivity.getResources().getString(R.string.db_table_act_slug_id));
            foreignKey = args.getString(mActivity.getResources().getString(R.string.db_table_act_foreign_key));
            Log.d(TAG," foreign key  = "+ foreignKey);
            activity = args.getString(mActivity.getResources().getString(R.string.db_table_act_type));
            date = args.getString(mActivity.getResources().getString(R.string.db_table_act_schedule));
            strDescription = args.getString(mActivity.getResources().getString(R.string.db_table_act_description));
            strFeedback = args.getString(mActivity.getResources().getString(R.string.db_table_act_feedback));
            syncDone = args.getInt(mActivity.getResources().getString(R.string.db_table_sync_done));
            if(args.getInt(mActivity.getResources().getString(R.string.db_table_act_status)) != 0) {
                activityDone = args.getInt(mActivity.getResources().getString(R.string.db_table_act_status));
            }

            Log.d(TAG,"activity done of deal fragments is "+ activityDone);
            position = args.getInt(mActivity.getResources().getString(R.string.bundle_position));
            Log.d(TAG,"position of deal fragments is "+ position);
            organisation_name = args.getString(mActivity.getResources().getString(R.string.db_table_org_name));
            contact_name = args.getString(mActivity.getResources().getString(R.string.db_table_con_name));
            sale_name = args.getString(mActivity.getResources().getString(R.string.db_table_sale_name));
            quotation_name = args.getString(mActivity.getResources().getString(R.string.db_table_qu_name));
            model = args.getString(mActivity.getResources().getString(R.string.db_table_act_model));

            salesID = args.getInt(mActivity.getResources().getString(R.string.db_table_sales_id));
            saleName = args.getString(mActivity.getResources().getString(R.string.db_table_sale_name));
            sales_stageID = args.getString(mActivity.getResources().getString(R.string.db_table_sale_stage));

            sales_fragmentPosition = args.getInt(mActivity.getResources().getString(R.string.bundle_fragment_position));
            sales_sync_done = args.getInt(mActivity.getResources().getString(R.string.db_table_sync_done));

            sales_contact_name = args.getString(mActivity.getResources().getString(R.string.db_table_con_name));
            sales_organisation_name = args.getString(mActivity.getResources().getString(R.string.db_table_org_name));
            sales_cost = args.getDouble(mActivity.getResources().getString(R.string.db_table_sale_cost));
            sales_paymentType = args.getString((mActivity.getResources().getString(R.string.db_table_sale_payment_type)));
            sales_status = args.getInt(mActivity.getResources().getString(R.string.db_table_sale_status));
            sales_strDescription = args.getString(mActivity.getResources().getString(R.string.db_table_sale_description));
            sales_slugID = args.getString(mActivity.getResources().getString(R.string.db_table_sale_slug_id));
            sales_foreignKey = args.getString(mActivity.getResources().getString(R.string.db_table_sale_foreign_key));
            sales_model = args.getString(mActivity.getResources().getString(R.string.db_table_sale_model));

        }
        getCoordinates = new GetCoordinates(mActivity);
        mGoogleApiClient = getCoordinates.buildGoogleApiClient();
        getUsername = new GetUsername(mActivity);
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_add_activities, container, false);
        spnActivities = (Spinner) rootView.findViewById(R.id.spn_activities);

        txtOrganisation = (TextView) rootView.findViewById(R.id.txt_organisation);
        if(organisation_name != null && !organisation_name.equalsIgnoreCase(mActivity.getResources().getString(R.string.db_table_org_entry))){
            txtOrganisation.setText(organisation_name);
            mainLink = organisation_name;
        }
        txtPerson = (TextView) rootView.findViewById(R.id.txt_person);
        if(contact_name != null && !contact_name.equalsIgnoreCase(mActivity.getResources().getString(R.string.db_table_org_entry)) ){
            txtPerson.setText(contact_name);
            mainLink = contact_name;
        }
        txtSale = (TextView) rootView.findViewById(R.id.txt_sales);
        if(sale_name != null && !sale_name.equalsIgnoreCase(mActivity.getResources().getString(R.string.db_table_org_entry))){
            txtSale.setText(sale_name);
            mainLink = sale_name;
        }
        txtQuotation = (TextView) rootView.findViewById(R.id.txt_choose_quotation);
        if(quotation_name != null && !quotation_name.equalsIgnoreCase(mActivity.getResources().getString(R.string.db_table_org_entry))){
            txtQuotation.setText(quotation_name);
            mainLink = quotation_name;
        }
        txtTime = (TextView) rootView.findViewById(R.id.txt_date);
        if(date != null ){
            txtTime.setText(date);
        }
        txtTime.setOnClickListener(this);
        editNotes = (EditText) rootView.findViewById(R.id.edit_notes);
        if(strDescription != null){
            editNotes.setText(strDescription);
        }
        editFeedBack = (EditText) rootView.findViewById(R.id.edit_feedback);
        if(strFeedback != null){
            editFeedBack.setText(strFeedback);
        }
        btnAddActivity = (Button) rootView.findViewById(R.id.btn_add_activity);
        btnAddActivity.setOnClickListener(this);
        btnEditActivity = (Button) rootView.findViewById(R.id.btn_edit_activity);
        btnEditActivity.setOnClickListener(this);
        if(activityID != 0){
            btnAddActivity.setVisibility(View.GONE);
            btnEditActivity.setVisibility(View.VISIBLE);
        }
        imgClose = (ImageButton) rootView.findViewById(R.id.fab_button);
        imgClose.setOnClickListener(this);
        lytChoosePerson = (LinearLayout) rootView.findViewById(R.id.lyt_choose_person);
        lytChoosePerson.setOnClickListener(this);
        lytChooseOrganisation = (LinearLayout) rootView.findViewById(R.id.lyt_choose_organisation);
        lytChooseOrganisation.setOnClickListener(this);
        lytChooseSale = (LinearLayout) rootView.findViewById(R.id.lyt_sales_deal);
        lytChooseSale.setOnClickListener(this);
        lytChooseQuotation = (LinearLayout) rootView.findViewById(R.id.lyt_quotations);
        lytChooseQuotation.setOnClickListener(this);
        lytChooseDate = (LinearLayout) rootView.findViewById(R.id.lyt_choose_date);
        lytChooseDate.setOnClickListener(this);
        spinnerActivitiesAdapter = new SpinnerActivitiesAdapter(mActivity,0,spinnerListArray,spinnerItemsImgArray);
        spnActivities.setAdapter(spinnerActivitiesAdapter);
        spnActivities.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View view,
                                       int position, long arg3) {
                // TODO Auto-generated method stub
                activity = spinnerListArray[position];
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        if(activity != null) {
            for (int k = 0; k < spinnerListArray.length; k++) {
                if (spinnerListArray[k].equals(activity)) {
                    spnActivities.setSelection(k);
                    break;
                }
            }
        }

        txtFeedBack = (TextView) rootView.findViewById(R.id.txt_feedback);
        txtFeedBack.setClickable(true);
        txtFeedBack.setFocusable(true);
        txtFeedBack.setOnClickListener(this);
        // load the animation
        animSideDown = AnimationUtils.loadAnimation(mActivity,
                R.anim.slide_down);

        // set animation listener
        animSideDown.setAnimationListener(this);

        animSlideUp = AnimationUtils.loadAnimation(mActivity,
                R.anim.slide_up);
        animSlideUp.setAnimationListener(this);
        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //inflater.inflate(R.menu.events, men
        super.onCreateOptionsMenu(menu, inflater);
       // menu.findItem(R.id.action_delete).setVisible(true);
        //        getActivity().invalidateOptionsMenu();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume(){
        super.onResume();
        mActivity.getSupportActionBar().setTitle(arrayMainItems[4]);
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.lyt_choose_person:
                newFragment.setTargetFragment(AddActivitiesFragment.this, MainModel.DIALOG_FRAGMENT_ADD_CONTACT);
                newFragment.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.lyt_choose_organisation:
                newFragment2.setTargetFragment(AddActivitiesFragment.this, MainModel.DIALOG_FRAGMENT_ADD_ORGANISATION);
                newFragment2.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.lyt_sales_deal:
                Bundle args = new Bundle();
                args.putInt(mActivity.getResources().getString(R.string.bundle_sale_flag),1);
                newFragment3.setArguments(args);
                newFragment3.setTargetFragment(AddActivitiesFragment.this, MainModel.DIALOG_FRAGMENT_ADD_SALE);
                newFragment3.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.lyt_quotations:
                Bundle args2 = new Bundle();
                args2.putInt(mActivity.getResources().getString(R.string.bundle_sale_flag),1);
                newFragment8.setArguments(args2);
                newFragment8.setTargetFragment(AddActivitiesFragment.this, MainModel.DIALOG_FRAGMENT_ADD_QUOTATION);
                newFragment8.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.lyt_choose_date:
                newFragment4.setTargetFragment(AddActivitiesFragment.this, MainModel.DIALOG_FRAGMENT_DATE);
                newFragment4.show( mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.txt_date:
                newFragment4.setTargetFragment(AddActivitiesFragment.this, MainModel.DIALOG_FRAGMENT_DATE);
                newFragment4.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
                break;
            case R.id.btn_add_activity:
                if(fillData()) {
                    dateID.add(activityID);
                    alarm.setAlarm(mActivity, dateID, activityID, activity+ " "+mainLink);
                }
                break;
            case R.id.btn_edit_activity:
                if(fillData()) {
                    if(dateID != null & dateID.size() > 0) {
                        dateID.add(activityID);
                        alarm.setAlarm(mActivity, dateID, activityID, activity + " " + mainLink);
                        Log.d(TAG,"Date edited");
                    }else{
                        Log.d(TAG,"Date not edited");
                    }
                }
                break;
            case R.id.txt_feedback:
                if(editFeedBack.getVisibility() == View.GONE){
                    editFeedBack.setVisibility(View.VISIBLE);
                    editFeedBack.startAnimation(animSideDown);
                }else{
                    editFeedBack.setVisibility(View.VISIBLE);
                    editFeedBack.startAnimation(animSlideUp);
                }
                break;
            case R.id.fab_button:
                if(activityID == 0){
                    Intent intent = new Intent();
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    mActivity.getSupportFragmentManager().popBackStackImmediate();
                }else{
                    Bundle args3 = new Bundle();
                    args3.putString(mActivity.getResources().getString(R.string.txt_advisory),mActivity.getResources().getString(R.string.txt_advisory_delete) + " " +arrayMainItems[4]);
                    newFragment6.setArguments(args3);
                    newFragment6.setTargetFragment(AddActivitiesFragment.this, MainModel.DIALOG_FRAGMENT_DELETE_DIALOG);
                    newFragment6.show(mainActivity.addDialogFragment(getFragmentManager()), "dialog");
            }
                break;
        }
    }

    public boolean fillData(){
        boolean fillData = false;
        Bundle args = new Bundle();
        if(txtTime.length() < 1){
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_set_time),Toast.LENGTH_LONG).show();
            fillData = false;
        }else if(foreignKey == null){
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_link_to),Toast.LENGTH_LONG).show();
            fillData = false;
        }else{
            if(slugID == null) {
                activityID = (int) insertActivity();
                try {
                    slugID = commonHash.getHash(String.valueOf(activityID));
                    Log.d(TAG, "Common Hash is  " + slugID);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                insertActivity();
            }else{
                insertActivity();
            }
            Intent intent = new Intent();
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_name),saleName);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_foreign_key),sales_foreignKey);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_model),sales_model);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sync_done),sales_sync_done);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_cost),sales_cost);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_stage),sales_stageID);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_status),sales_status);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_id),salesID);
            intent.putExtra(mActivity.getResources().getString(R.string.bundle_fragment_position),sales_fragmentPosition);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_description),sales_strDescription);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_sale_slug_id),sales_slugID);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_con_name), contact_name);
            intent.putExtra(mActivity.getResources().getString(R.string.db_table_org_name), organisation_name);
            mActivity.setResult(Activity.RESULT_OK, intent);
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
            mActivity.getSupportFragmentManager().popBackStackImmediate();
            fillData = true;
            Toast.makeText(mActivity,mActivity.getResources().getString(R.string.toast_activity_added),Toast.LENGTH_LONG).show();
        }
        return fillData;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MainModel.DIALOG_FRAGMENT_ADD_CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    contact_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_name));
                    foreignKey = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_slug_id));
                    Log.d(TAG, TAG + String.valueOf(foreignKey));
                    if(data.getExtras().containsKey(mActivity.getResources().getString(R.string.db_table_org_slug_id))){
                        organisationID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id));
                        organisation_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_name));
                        if(!organisation_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))) {
                            txtOrganisation.setText(organisation_name);
                        }
                        if(!contact_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))) {
                            mainLink = contact_name;
                            txtPerson.setText(contact_name);
                        }
                    }else{
                        organisationID = null;
                        txtOrganisation.setText(mActivity.getResources().getString(R.string.txt_choose_organisation));
                    }
                    saleID = null;
                    if(!contact_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))) {
                        mainLink = contact_name;
                        txtPerson.setText(contact_name);
                    }
                    txtSale.setText(mActivity.getResources().getString(R.string.txt_sales));
                    txtQuotation.setText(mActivity.getResources().getString(R.string.txt_choose_quotation));
                    model = mActivity.getResources().getString(R.string.db_table_model_1);
                }
            }
        }else if(requestCode == MainModel.DIALOG_FRAGMENT_ADD_ORGANISATION){
            if (resultCode == Activity.RESULT_OK) {
                if(data.getExtras() != null) {
                    foreignKey = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id));
                    Log.d(TAG,TAG + String.valueOf(organisationID));
                    organisation_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_name));
                    if(!organisation_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
                        txtOrganisation.setText(organisation_name);
                        mainLink = organisation_name;
                    }
                    txtPerson.setText(mActivity.getResources().getString(R.string.txt_choose_person));
                    txtSale.setText((mActivity.getResources().getString(R.string.txt_sales)));
                    txtQuotation.setText(mActivity.getResources().getString(R.string.txt_choose_quotation));
                    contactID = null;
                    saleID = null;
                }
                model = mActivity.getResources().getString(R.string.db_table_model_2);
            }
        }else if(requestCode == MainModel.DIALOG_FRAGMENT_DATE){
            if (resultCode == Activity.RESULT_OK){
                if(data.getExtras() != null) {
                    date = data.getStringExtra(mActivity.getResources().getString(R.string.bundle_set_date));
                    dateID = data.getIntegerArrayListExtra(mActivity.getResources().getString(R.string.bundle_array_dates));
                    txtTime.setText(date);
                }
            }
        }
        else if(requestCode == MainModel.DIALOG_FRAGMENT_ADD_SALE){
            if(resultCode == Activity.RESULT_OK){
                if(data.getExtras() != null) {
                    organisationID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id));
                    organisation_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_name));
                    contactID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_slug_id));
                    contact_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_name));
                    foreignKey = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_slug_id));
                    sale_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_name));
                    if(!contact_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
                        txtPerson.setText(contact_name);
                    }
                    if(!organisation_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
                        txtOrganisation.setText(organisation_name);
                    }
                    if(!sale_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
                        txtSale.setText(sale_name);
                        mainLink = sale_name;
                    }
                    txtQuotation.setText(mActivity.getResources().getString(R.string.txt_choose_quotation));
                }
                model = mActivity.getResources().getString(R.string.db_table_model_3);
            }
        }else if(requestCode == MainModel.DIALOG_FRAGMENT_DELETE_DIALOG ){
                if (resultCode == Activity.RESULT_OK) {
                    alarm.cancelAlarm(mActivity, dateID, activityID, activity + " " + mainLink);
                    activityDone = 2;
                    insertActivity();
                    Intent intent = new Intent();
                    mActivity.setResult(Activity.RESULT_OK, intent);
                    getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, intent);
                    mActivity.getSupportFragmentManager().popBackStackImmediate();
                }else{

                }
        }else if(requestCode == MainModel.DIALOG_FRAGMENT_ADD_QUOTATION ){
            if(data.getExtras() != null) {
                organisationID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_slug_id));
                organisation_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_org_name));
                contactID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_slug_id));
                contact_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_con_name));
                saleID = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_slug_id));
                sale_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_sale_name));
                foreignKey = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_qu_slug_id));
                quotation_name = data.getStringExtra(mActivity.getResources().getString(R.string.db_table_qu_name));
                if(!contact_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
                        txtPerson.setText(contact_name);
                    }
                if(!organisation_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
                        txtOrganisation.setText(organisation_name);
                }
                if(!sale_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
                    txtSale.setText(sale_name);
                }
                if(!quotation_name.equals(mActivity.getResources().getString(R.string.db_table_org_entry))){
                    txtQuotation.setText(quotation_name);
                    mainLink = quotation_name;
                }

                }else{

                }
            model = mActivity.getResources().getString(R.string.db_table_model_4);
            }
    }

    public long insertActivity(){
        syncDone = 1;
        coordinates = getCoordinates.getCoordinates();
        int playListId = 0;
        strDescription = editNotes.getText().toString();
        strFeedback = editFeedBack.getText().toString();
        ContentValues initialValues = new ContentValues();
        initialValues.put(mActivity.getResources().getString(R.string.db_table_act_slug_id), slugID);
        Log.d(TAG," ''''----------......... "+slugID);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_act_type), activity);
        Log.d(TAG," ''''----------......... "+activity);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_act_description), strDescription);
        Log.d(TAG," ''''----------......... "+strDescription);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_act_feedback), strFeedback);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_act_schedule), date);
        Log.d(TAG," ''''----------......... "+ date);

        initialValues.put(mActivity.getResources().getString(R.string.db_table_act_status), activityDone);
        Log.d(TAG," ''''----------......... activity done is "+ activityDone);

        initialValues.put(mActivity.getResources().getString(R.string.db_table_act_model), model);
        Log.d(TAG," ''''----------......... "+model);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_act_foreign_key), foreignKey);
        Log.d(TAG," ''''----------......... "+foreignKey);
        initialValues.put(mActivity.getResources().getString(R.string.db_table_sync_done), syncDone);
        Log.d(TAG," ''''----------......... "+syncDone);
        if(coordinates[0] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_act_latitude), coordinates[0]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_act_latitude), 0.0);
        }
        Log.d(TAG," ''''----------......... "+coordinates[0]);
        if(coordinates[1] != null) {
            initialValues.put(mActivity.getResources().getString(R.string.db_table_act_longitude), coordinates[1]);
        }else{
            initialValues.put(mActivity.getResources().getString(R.string.db_table_act_longitude), 0.0);
        }
        initialValues.put(mActivity.getResources().getString(R.string.user_id),getUsername.getUserName());
        if(activityID == 0) {
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "3");
            initialValues.putNull(mActivity.getResources().getString(R.string.db_table_id));
            Uri insertUri = mActivity.getContentResolver().insert(data, initialValues);
            playListId = (int)ContentUris.parseId(insertUri);
            Log.d(TAG, "------- primary id == " + String.valueOf(playListId));
        }else{
            Uri data = Uri.withAppendedPath(FunnelProvider.CONTENT_URI, "13");
            playListId = mActivity.getContentResolver().update(data,initialValues,null,new String[]{String.valueOf(activityID),mActivity.getResources().getString(R.string.flag_sync)});
            Log.d(TAG, "------- primary id == 2" + String.valueOf(activityID));
        }

        return playListId;
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        Log.d(TAG,"animation end");
        if (animation == animSlideUp) {
            editFeedBack.setVisibility(View.GONE);

        }
        if (animation == animSideDown) {
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    public ArrayList<Integer> getDateFromString(String date){
        ArrayList<Integer> dateID = new ArrayList<Integer>();
        int dates;
        Log.d(TAG,date);
        String[]tokens = date.split("-|\\:|\\ ");
        for(int i = 0; i < tokens.length;i++){
            dates = Integer.parseInt(tokens[i]);
            dateID.add(dates);
        }
        return dateID;
    }
}
